
//export const BASE_URL = 'https://192.168.225.197/voffice';

 //export const BASE_URL = 'http://183.82.98.147:8000/voffice';    //'https://' + '15.206.228.98' + ':4430/voffice';
 export const BASE_URL = 'http://127.0.0.1:8000/voffice';
 export const API_VERSION = '';

export const environment = {
  production: false,
  API_URL : `${BASE_URL}/`,//${API_VERSION}/`,
  STATIC_PATH: '../static/assets/',
  DYNAMIC_ASSETS: 'assets/',
  FileRoot:`${BASE_URL}/file/`,
  SFU_SERVER : 'http://127.0.0.1' + ':3001/',
  SFU_ADDRESS: 'ws://' + '127.0.0.1' + ':3001/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
