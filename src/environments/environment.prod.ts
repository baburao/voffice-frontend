export const BASE_URL = 'https://' + '15.206.228.98' + ':4430/voffice';
export const API_VERSION = '';

export const environment = {
  production: true,
  API_URL : `${BASE_URL}/`,//${API_VERSION}/`,
  STATIC_PATH: '../static/assets/',
  DYNAMIC_ASSETS: 'assets/',
  FileRoot:`${BASE_URL}/file/`,
  SFU_ADDRESS: 'wss://' + '15.206.228.98' + ':3001/'
};

