import { NgModule, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Routes, RouterModule } from '@angular/router';
import {MainWrapperComponent} from './layouts/main-wrapper/main-wrapper.component';
import {WrapperWithSidebarComponent} from './layouts/wrapper-with-sidebar/wrapper-with-sidebar.component';
import {AuthGuard} from './_guards/auth.guard';
import {UnauthenticatedGuard} from './_guards/unauthenticated.guard';
//import {WrapperWithSidebar2Component} from './layouts/wrapper-with-sidebar-2/wrapper-with-sidebar-2.component';



import { ConfService } from './services/conferences/conf.service';

import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from './services/global/user.service';
import {DisconnectComponent} from "./modules/shared/disconnect/disconnect.component";

@Injectable({ providedIn: 'root' })
export class CallResolve implements Resolve<any> {

  constructor(private confService: ConfService) {}

  resolve(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
     return new Promise<any>((resolve: any, reject: any) =>{
	
		this.confService.leaveCall();
		resolve();
	})
  }
}

export const MainRoutes: Routes = [
 
  {
    path: '',
    //component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/conference/conference.module').then(mod => mod.ConferenceModule),
    //canActivate: [AuthGuard],
    //resolve: { data : CallResolve }
  },
{
    path: 'myoffice',
    //component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/my-office/myoffice.module').then(mod => mod.MyOfficeModule),
    data: { breadcrumbs: 'myoffice' },
    //canActivate: [AuthGuard],
	//resolve: { data : CallResolve }
  },
    
{
  path: 'office',
  //component: WrapperWithSidebarComponent,
  loadChildren: () => import('./modules/office/office.module').then(mod => mod.OfficeModule),
  data: { breadcrumbs: 'office' },
  //canActivate: [AuthGuard],
//resolve: { data : CallResolve }
},
  {
    path: '',
    //component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/team/team.module').then(mod => mod.TeamModule),
  //canActivate: [AuthGuard],
	//resolve: { data : CallResolve }
  },
  {
    path: '',
    //component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/my-team/myteam.module').then(mod => mod.MyTeamModule),
  //canActivate: [AuthGuard],
	//resolve: { data : CallResolve }
  },

  {
    path: '',
    //component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/department/department.module').then(mod => mod.DepartmentModule),
    //canActivate: [AuthGuard],
	//resolve: { data : CallResolve }
  },
  {
    path: '',
    loadChildren: () => import('./modules/user/user.module').then(mod => mod.UserModule),
  //resolve: { data : CallResolve }
  },
  // {
  //   path: '',
  //   loadChildren: () => import('./modules/admin/admin.module').then(mod => mod.AdminModule),
  // //resolve: { data : CallResolve }
  // },
  {
    path: 'disconnected',
    component: DisconnectComponent
  },
  

];



