import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JwtInterceptor } from './services/global/jwt.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HeaderComponent } from './layouts/header/header.component';
import { FooterComponent } from './layouts/footer/footer.component';
import {MainWrapperComponent} from './layouts/main-wrapper/main-wrapper.component';
import { ToastrModule } from 'ngx-toastr';
/*
import { HeaderComponent } from './layouts/header/header.component';
import { FooterComponent } from './layouts/footer/footer.component';
import {MainWrapperComponent} from './layouts/main-wrapper/main-wrapper.component';
import {ChatbarComponent} from './layouts/chatbar/chatbar.component';
import {WrapperWithSidebarComponent} from './layouts/wrapper-with-sidebar/wrapper-with-sidebar.component';
import {LeftSidebarComponent} from './layouts/left-sidebar/left-sidebar.component';
import {HeaderTwoComponent} from './layouts/header-two/header-two.component';
import { BarNotificationComponent } from './modules/notifications/bar-notification/bar-notification.component';
import { AllNotificationsComponent } from './modules/notifications/all-notifications/all-notifications.component';
import { AudioPlayerComponent } from './layouts/audio-player/audio-player.component';

*/
import { MainModule } from './main.module';
@NgModule({
  declarations: [
    AppComponent,
	HeaderComponent,
    FooterComponent,
	MainWrapperComponent,
    /*HeaderComponent,
    FooterComponent,
    MainWrapperComponent,
    ChatbarComponent,
    WrapperWithSidebarComponent,
    LeftSidebarComponent,
    HeaderTwoComponent,
    BarNotificationComponent,
    AllNotificationsComponent,
    AudioPlayerComponent*/
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
    }),

	//MainModule,
  ],
  providers: [DatePipe, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },  ],
  bootstrap: [AppComponent]
})
// @ts-ignore
export class AppModule { }
