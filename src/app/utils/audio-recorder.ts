declare var MediaRecorder: any;

export class AudioRecorder {

	
	mediaRecorder : any = null;
	recordedBlobs: any[] = [];
	//buffer: any = null;
	//this.audioElement = inAudioElement;
	
	recordingReadyEvent: any | null = null;
	
	recordStream: any = null;
	
	record(onFail: any) {
		
		var self = this;
		
		const constraints = {
			audio: {
			//echoCancellation: {exact: hasEchoCancellation}
			},
			video: false
		};
		
		navigator.mediaDevices.getUserMedia(constraints).then(function (stream){			
			
			self.recordStream = stream;
			self.mediaRecorder = new MediaRecorder(stream);
			
			self.mediaRecorder.onstop = (event: any) => {
			    
				if(self.recordedBlobs.length <= 0) {
					return;
				}
				var buffer = new Blob(self.recordedBlobs, { 'type' : self.recordedBlobs[0].type });

				if(self.recordingReadyEvent != null) {
					self.recordingReadyEvent(buffer);
				}				
			};
			
			self.mediaRecorder.ondataavailable = function(event: any) {
		
				if (event.data && event.data.size > 0) {
					self.recordedBlobs.push(event.data);
				}
			}
			
			self.mediaRecorder.start();
		
		}).catch(onFail);
	}
	
	stop(onReady: any) {
		
		this.recordingReadyEvent = onReady;
		this.mediaRecorder.stop();
		
		this.recordStream.getTracks().forEach(function(track: any) {
        if (track.readyState == 'live') {
            track.stop();
        }
    });
	}
	
	
}