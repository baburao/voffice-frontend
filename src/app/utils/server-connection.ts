
export class  ServerConnection  {
	
	socket: any = null;
	reconnecting: boolean = false;
	active: boolean = false;
	closed: boolean = false;
  
	userToken: string = "";

	onOpen: any = null;
	onSignal: any = null;
	onUpdate: any = null;
	onCall: any = null;
	onClosed: any = null;
	onError: any = null;
	
	
	connect(token: string) : void {

		var self = this;		
		
		this.userToken = token;
		
		try {
			if(window.location.protocol == 'http:') {
					
				self.socket = new WebSocket('ws://' + window.location.hostname + ':8000/ws/office/?token=' + this.userToken + ' ');
			} else {
				self.socket = new WebSocket('wss://' + window.location.hostname + ':4430/ws/office/?token=' + this.userToken + ' ');
			}
		} catch {
			if(self.onError) {
				self.onError();
			}
		}
		self.socket.binaryType = 'arraybuffer';	
		
		self.socket.addEventListener('open', function (event: any) {			
			self.reconnecting = false;			

			console.log('WebSocket opened ', event);
			
			self.active = true;
				
			if(self.onOpen) {
				self.onOpen();
			}
			//self.send({test:"test"})
			
			//self.socket.send(JSON.stringify(message));
		});
		
		self.socket.addEventListener('message', function (event: any) {		
			
			var message = JSON.parse(event.data);		
			
				
			if(message.type == 'signal') {

				if(self.onSignal != null) {
					self.onSignal(message);
				}
				
			} else if(message.type == 'update') {
				
				if(self.onUpdate != null) {
					self.onUpdate(message.update , message[message.update]);
				}
			} else if(message.type == 'call') {
				
				if(self.onCall != null) {
					self.onCall(message.call);
				}
			} else if(message.type == 'close') {
				console.log("Forced close connection")
        if(!self.closed && self.onClosed != null) {
          self.onClosed();
        }
				self.close();
				
			}
		});
		
		self.socket.addEventListener('error', function (event: any) {
			console.log('WebSocket Error ', event, self.active);
			if(self.onError) {
				self.onError();
			}
			if(self.active) {
				self.active = false;
				//self.reconnect();
			}
			
		});
		self.socket.addEventListener('close', function (event: any) {
			console.log('WebSocket closed ', event);
			
			if(self.active) {
        if(!self.closed && self.onClosed != null) {
          self.onClosed();
        }
        self.close();
				//self.reconnect();
			} 
		});
	}	
	
	close(): void {
    this.closed = true;
		this.active = false;
		this.socket.close();
	}
	send(message: any): void {
		
		var self = this;
				
		if(this.socket == null || this.socket.readyState  == 0){
			setTimeout(function(){self.send(message)}, 100);
			
			return;
		}
		
		this.socket.send(JSON.stringify(message));
	}
	
	reconnect(): void {
		var self = this;
		if(this.reconnecting) {
			return;
		}
		console.log("Reconnect...");
		this.reconnecting = true;
		setTimeout(function(){self.connect(self.userToken);}, 1000);
	}
}
