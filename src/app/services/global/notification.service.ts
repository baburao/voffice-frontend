import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

import {WebSocketService} from './web-socket.service';
import {ConversationService} from './conversation.service';
import {ConfService} from './../conferences/conf.service';
import { Notification } from '../../interfaces/notification';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 

import { Conference } from '../../interfaces/conference';
import  {UtilsService} from "../global/utils.service";
import  {UserService} from "./user.service";
import api_urls from '../../constants/api_urls';
import { DirectCall } from 'src/app/call/direct-call';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notificationList : Notification[] = [];
  public notificationSource = new BehaviorSubject<any>(this.notificationList);
  notifications = this.notificationSource.asObservable();

  notificationCount : number = 0;
  public notificationCountSource = new BehaviorSubject<any>(this.notificationCount);
  notificationCountObservable = this.notificationCountSource.asObservable();
  
  private isNotificationWindowOpen: boolean = false;

  directCall: DirectCall| null = null;

  notificationSoundPath = '../../../assets/audio/VOffisNotificationSound.mp3';
  callSoundPath = '../../../assets/audio/VOffisCallSound.mp3';

  audio = new Audio();
  
  constructor(private http: HttpClient, 
    private webSocketService: WebSocketService, 
    private confService: ConfService, 
    private util: UtilsService,
    private conversationService: ConversationService,
    private userService: UserService) { 
	
  //this.confService.getConferences().subscribe((conferences: {[key: number]: Conference}) => {
        
    
  	this.http.get(api_urls.GetNotifications).subscribe((notifications: any) => {				
  						 			
  		for(var u in notifications) {						
  			
    			var newNotification: any = this.createNotification(notifications[u]);
          
          if(newNotification == null) {
            continue;
          }
    		  if(this.isNotificationWindowOpen) {      
            this.markNotificationRead(newNotification.id);
            newNotification.has_read = true;
          } else {
            this.notificationCount += 1
          }
    			this.notificationList.push(newNotification);
  		  }
        
        this.notificationCountSource.next(this.notificationCount);
  	 });
	 //});
   
	 this.webSocketService.onUpdateNotification.subscribe((data: any) => {
		
		var newNotification: any = this.createNotification(data);
		
		if(newNotification != null) {
		
			this.notificationList.push(newNotification);
			this.addNotification(this.notificationList);
      if(!this.isNotificationWindowOpen) {
        this.notificationCount += 1
        this.notificationCountSource.next(this.notificationCount);
      }
      
		}
	});

    this.conversationService.onNewMessageNotification.subscribe((data: any) => {
      this.pushNotification(
          this.createNewMessageNotification( data.message.conversation, data.message.sender, -1));
      
      if(!this.isNotificationWindowOpen) {
        this.notificationCount += 1
        this.notificationCountSource.next(this.notificationCount);
      }
          
    });

    this.confService.directCallObservable.subscribe((newCall: DirectCall) => {
      this.directCall = newCall;
   });
  }
  pushNotification(notification: Notification): void {
    this.notificationList.push(notification);
    this.addNotification(this.notificationList);
  }
  addNotification(notificationList: object) {
    this.notificationSource.next(notificationList);
  }

  getNotifications(){ // will fetch initial notifications from server
    this.addNotification(this.notificationList)
  }
  
  createNotification(data: any): Notification {
  	var newNotification: any = null;
  		
  	if(data.type =='ci') {
  		newNotification = this.createConferenceNotification(data.target, data.id);
  	} else if(data.type =='mc') {
      newNotification = this.createMissedCallNotification(data.target, data.source, data.id);
    } else if(data.type =='nm') {
      newNotification = this.createNewMessageNotification(data.target, data.source, data.id);
    }
	
	 return newNotification;
  }
  
  createConferenceNotification(conferenceId: number, id: number){
	
    if(!(conferenceId in this.confService.conferences)) {
      return null;
    }
    var conference: Conference = this.confService.conferences[conferenceId];
	 console.log(conference , conferenceId , this.confService.conferences)
    return {
          id: id,
          title: `${conference.assignedUsers.length} Participants`,
          content: `Conference created : ${conference.title}`,
          has_read: false,
          type: 'G',
          icon: this.util.abbreviate(conference.title),
          iconType: 'T',
          buttonText: 'Join Meeting',
          buttonLink: '/conferences', //'join/'+conference.id,
          category: 'conference',
          data: null
      };
  }
  createMissedCallNotification(conversationId: number, userId: number, id: number){
  
    
    return {
          id: id,
          title: `Missed call`,
          content: `${this.userService.getUser(userId)?.firstname} tried to call you`,
          has_read: false,
          type: 'Y',
          icon: this.util.abbreviate(this.userService.getUser(userId)!.firstname),
          iconType: 'T',
          buttonText: '',
          buttonLink: '',
          category: 'call',
          data: null
      };
  }
  createNewMessageNotification(conversationId: number, sender: number, id: number){  
    
    /*var typeName: string = '';
    
    switch(message.type) {
      case 't':
        typeName ='a message';
        break;
      case 'f':
        typeName ='a file';
        break;
      case 'a':
        typeName ='an audio message';
        break;
    }*/
    
    var preNotificationIndex = this.notificationList.findIndex((notification: Notification) => {
      
      if(notification.category != 'conversation') {
        return false;
      }
      return notification.data.conversation == conversationId && notification.data.sender == sender;
    });
    
    var preNotificationCount = 1;
    var idList: number[] = [id];
    
    if(preNotificationIndex != -1) {
      
      this.notificationList[preNotificationIndex].data.count += 1;
      preNotificationCount = this.notificationList[preNotificationIndex].data.count;
      idList = idList.concat(this.notificationList[preNotificationIndex].data.idList);
      this.notificationList.splice(preNotificationIndex, 1);
      
    }
    return {
          id: id,
          title: `New Message`,
          content: `${this.userService.getUser(sender)?.firstname} send you ${preNotificationCount} messages`,
          has_read: false,
          type: 'Y',
          icon: this.util.abbreviate(this.userService.getUser(sender)!.firstname),
          iconType: 'T',
          buttonText: '',
          buttonLink: '',
          category: 'conversation',
          data: {
            conversation: conversationId,
            sender: sender,
            count: preNotificationCount,
            idList: idList
           }
      };
  }
  
  markNotificationRead(id: number): void {
    if(id == -1) {
      return;
    }
    this.http.post(api_urls.ReadNotification, {id: id}).subscribe();
  }
  
  setNotificationWindowOpen(isOpen: boolean) {
    this.isNotificationWindowOpen = isOpen;
    
    if(this.isNotificationWindowOpen) {
      
      for(var i = 0; i < this.notificationList.length; i++) {
        
        if(!this.notificationList[i].has_read) {
          
          this.markNotificationRead(this.notificationList[i].id);
          this.notificationList[i].has_read = true;
          
          if(this.notificationList[i].data != null) {
            for(var j = 0; j < this.notificationList[i].data.idList.length; j++) {
              
              this.markNotificationRead(this.notificationList[i].data.idList[j]);
            }
          }
        }
      }
      this.notificationCount = 0;
      this.notificationCountSource.next(this.notificationCount);
    }
  }

  playNotificationAudio(notificationSound = false){
    this.audio.src = notificationSound ? this.notificationSoundPath : this.callSoundPath;
    this.audio.load();
    this.audio.play();
    
  }

  stopNotificationAudio() {
    this.audio.pause();
    this.audio.currentTime = 0;
  }

}
