import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { WebSocketService } from './web-socket.service';
import { NotificationService } from './notification.service';
import { UserService } from './user.service';
import { User } from '../../interfaces/user';
import { Message } from '../../interfaces/message';
import { Conversation } from '../../interfaces/conversation';

import api_urls from '../../constants/api_urls';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {

  private numUnreadMessagesCounter: number = 0;
  private numUnreadMessagesSource = new BehaviorSubject<number>(this.numUnreadMessagesCounter);
  //Number of all unread messages
  numUnreadMessages = this.numUnreadMessagesSource.asObservable();

  private currentConversationValue: Conversation | null = null;
  private currentConversationSource = new BehaviorSubject<Conversation | null>(this.currentConversationValue);
  //The current selected conversation, in which the user sends and receives messages
  currentConversation = this.currentConversationSource.asObservable();

  conversationList: Conversation[] = [];
  private conversationSource = new BehaviorSubject<Conversation[]>(this.conversationList);
  //List of all conversations this user is part of
  conversations = this.conversationSource.asObservable();

  removedUsers: number[] = [];
  private removedUsersSource = new BehaviorSubject<number[]>(this.removedUsers);
  //The current selected conversation, in which the user sends and receives messages
  removedUsersObserver = this.removedUsersSource.asObservable();
  
  @Output() onMessageConversation = new EventEmitter<any>();
  
  private oneToOneConversationMapping: { [key: number]: Conversation } = {};

  @Output() onNewMessage = new EventEmitter();
  @Output() onNewMessageNotification = new EventEmitter<any>();


  constructor(private http: HttpClient, private userService: UserService, private webSocketService: WebSocketService
  //,
   // private notificationService: NotificationService
    ) {
    if (localStorage.getItem('quickchatRemovedusers')) {
      this.removedUsers = JSON.parse(localStorage.getItem('quickchatRemovedusers')as any);
      console.log(this.removedUsers);
      this.removedUsersSource.next(this.removedUsers);
    }
    //First get the number of unread messages
    this.http.get<Conversation[]>(api_urls.GetMessageStatus).subscribe((status: any) => {

      this.numUnreadMessagesCounter = status.messages;
      this.numUnreadMessagesSource.next(this.numUnreadMessagesCounter);

      //Then read the list of all conversations
      this.http.get<Conversation[]>(api_urls.ListConversations).subscribe((conversations: Conversation[]) => {

        for (var c in conversations) {

          this.mapConversation(conversations[c]);
        }

        this.conversationList = conversations;
        this.sortConversations();
        this.conversationSource.next(this.conversationList);

      });
    });
    this.conversationSource.next(this.conversationList);

    //Listen for messages from the web socket
    this.webSocketService.onUpdateMessage.subscribe((message: any) => {

     console.log(message)
      this.removedUsers = this.removedUsers.filter(item =>{ return item != message.conversation});        
      localStorage.setItem('quickchatRemovedusers', JSON.stringify(this.removedUsers));
      this.removedUsersSource.next(this.removedUsers);
      
      if (this.currentConversationValue != null && message.conversation == this.currentConversationValue.id) {

        message.new_messge = false;
        //Add new message to the current conversation
        this.currentConversationValue.messages!.push(message);
        this.currentConversationValue.latest = message;

        this.onNewMessage.emit();
        this.webSocketService.serverConnection!.send({ type: 'message', message: message.id })
      } else {

        var conversation: Conversation = this.getConversationById(message.conversation);
        // Need to check this - if check is quick fix
        if(conversation) {
          conversation.latest = message;
          conversation.unread += 1;

          
          if(message.type != 'c') {
            this.onNewMessageNotification.emit({conversation: conversation.id, message: message});
          }
        }
        //this.notificationService.pushNotification(
        //  this.notificationService.createNewMessageNotification( conversation.id, message.sender));
        //Increase unread message counter
        //this.numUnreadMessagesCounter += 1;
        //this.numUnreadMessagesSource.next(this.numUnreadMessagesCounter);
      }

      this.sortConversations();
    });

    //Listen for new conversations
    this.webSocketService.onUpdateConversation.subscribe((newConversation: any) => {

      var oldIndex: number = this.conversationList.findIndex((conversation: Conversation) => { return conversation.id == newConversation.id })

      if (oldIndex < 0) {
        this.conversationList.push(newConversation);
      } else {
        this.conversationList[oldIndex] = newConversation;
      }
      this.mapConversation(newConversation);
    });
  }

  private sortConversations() {

    this.conversationList = this.conversationList.sort((a: Conversation, b: Conversation) => {

      if (a.latest == null && b.latest == null) {
        return 0;
      } else if (a.latest == null) {
        return 1;
      } else if (b.latest == null) {
        return -1;
      }
      return a.latest.date > b.latest.date ? -1 : 1;
    });
  }
  private mapConversation(conversation: Conversation) {

    if (this.isOneToOne(conversation)) {

      var otherUserId: number = this.getOtherUser(conversation);

      this.oneToOneConversationMapping[otherUserId] = conversation;
    }
  }
  /*
  * Check if a conversation is a one to one conversation
  */
  isOneToOne(conversation: Conversation): boolean {
    if (conversation == null || conversation.users == null) {
      return false;
    }
    return conversation.users.length == 2;
  }
  /*
  * Get the other user ID of a one to one conversation
  */
  getOtherUser(conversation: Conversation): number {
    if (conversation.users[0] == this.userService.currentUser?.id) {
      return conversation.users[1];
    } else {
      return conversation.users[0];
    }
  }
  /*
  * Get the one to one conversation for target user, if there is one, returns null otherwise
  */
  getOneToOneConversation(targetUserId: number) {

    if (targetUserId in this.oneToOneConversationMapping) {
      return this.oneToOneConversationMapping[targetUserId];
    }

    return null;
  }
  /*
  * Select a conversation by id and updates currentConversation
  * If conversationId = -1, current conversation will be set to null
  *
  * Returns the current selected conversation, or null if conversation was deselected
  */

  selectConversation(conversationId: number): Conversation | null {

    console.log("selectConversation", conversationId);
    var conversation: Conversation | null = null;

    if (conversationId >= 0) {

      conversation = this.getConversationById(conversationId);
      if (conversation == null) {
        console.log("Convo Error", this.conversationList)
        return null;
      }
    }
    return this.setCurrentConversation(conversation);
  }
  setCurrentConversation(conversation: Conversation | null): Conversation | null {

    if (conversation != null) {

      this.http.get<Message[]>(api_urls.RequestConversation + conversation.id).subscribe((messages: Message[]) => {

        /*for(var m in messages) {
        	
          if(messages[m].new_message) {
            this.numUnreadMessagesCounter -= 1;	
          }
        }*/

        //this.numUnreadMessagesSource.next(this.numUnreadMessagesCounter);		
        conversation.unread = 0;
        this.currentConversationValue = conversation;

        if (this.currentConversationValue != null) {
          this.currentConversationValue.messages = messages;

          this.currentConversationSource.next(this.currentConversationValue);
        }

        this.onNewMessage.emit();
      });

    } else {
      this.currentConversationValue = null;
      this.currentConversationSource.next(this.currentConversationValue);
    }


    return this.currentConversationValue;
  }
  /*
  * Sends a text message to the current conversation
  */
  sendMessage(text: string): void {

    if (this.currentConversationValue == null) {
      return;
    }

    this.http.post(api_urls.SendMessage, {
      conversation: this.currentConversationValue.id,
      text: text
    }).subscribe();

  }
  sendFileMessage(file: any): void {

    if (this.currentConversationValue == null) {
      return;
    }

    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    formData.append('conversation', this.currentConversationValue.id + "");
    formData.append('path', "");

    this.http.post(api_urls.SendFileMessage, formData).subscribe(() => { return true; });
  }
  sendAudioMessage(audioBuffer: any): void {

    if (this.currentConversationValue == null) {
      return;
    }

    const formData: FormData = new FormData();
    formData.append('audio', audioBuffer);

    formData.append('conversation', this.currentConversationValue.id + "");

    this.http.post(api_urls.SendAudioMessage, formData).subscribe(() => { return true; });
  }
  /*
  * Create a new conversation with the target user
  */
  createConversation(targetUserId: number): any {
    
    return this.http.post<Conversation>(api_urls.AddConversation, {
      topic: this.userService.getUser(targetUserId).firstname + ', ' + this.userService.currentUser.firstname,
      users: [targetUserId]
    }).pipe(map((conversation: Conversation) => {
      this.conversationList.push(conversation);
      return conversation;
    }));

  }
  createGroupConversation(topic: string, targetUserIds: number[]): any {
    return this.http.post<Conversation>(api_urls.AddConversation, {
      topic: topic,
      users: targetUserIds
    }).pipe(map((conversation: Conversation) => {
      this.conversationList.push(conversation);
      return conversation;
    }));

  }
  
  public getConversationById(conversationId: number): any {
    return this.conversationList.find((conversation: Conversation) => { return conversation.id == conversationId });
  }
  public getCallMessage(conversation: Conversation, callId: number): any {
    if(conversation.messages == null) {
      return null;
    }
    return conversation.messages!.find((message: Message) => { 
      return ((message.call != null && message.call.established) ? message.call.id == callId : false) 
    });
  }
  removeQuickChatUser(conversationId) {
    this.removedUsers.push(conversationId); 
    localStorage.setItem('quickchatRemovedusers', JSON.stringify(this.removedUsers));
    this.removedUsersSource.next(this.removedUsers);
  }
}

