import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';

import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class ValidationMessageService {
  renderer2: Renderer2;
  constructor(private toastr: ToastrService, rendererFactory: RendererFactory2) {
    this.renderer2 = rendererFactory.createRenderer(null, null);
  }
  V_company_Emailformat = /^[a-zA-Z0-9._]+@[a-zA-Z0-9._]+\.[a-zA-Z]{2,4}$/;
  V_company_name = "Please enter company name.";


  /*Create employee validation messages */
  V_emp_first_name = "Please enter first name.";
  V_emp_last_name = "Please enter last name.";
  V_emp_dob = "Please enter date of birth.";
  V_emp_gender = "Please enter gender.";
  V_emp_emailid = "Please enter email address.";
  V_emp_phone_number = "Please enter phone number.";
  V_emp_emer_contact_name = "Please enter emergency contact name.";
  V_emp_emer_contact_number = "Please enter emergency contact number.";
  V_emp_valid_emailid = "Please enter valid email address.";
  V_emp_marital_status = "Please select marital status.";
  V_emp_haskids = "Please select has kids.";
  V_emp_sonordaughter = "Please select son or daughter.";
  V_emp_marriagedate = "Please select marriage date.";
  V_emp_emailactivation = "Your email address activated.";
  V_emp_createemployee = "Employee registration successfully.";
  V_emp_resendemail = "Resend active email address successfully.";
  /*End */

  V_designation_name = "Please enter Designation Name";
  V_designation_desc = "Please enter Designation Description";
  V_designation_success = "Designation Added Successfully";
  V_designation_usuccess = "Designation Updated Successfully";
  V_msg_DuplicateRecord = "Designation Name already exist.";
  V_department_name = "Please enter Department Name";
  V_department_desc = "Please enter Department Description";
  V_department_success = "Department Added Successfully";
  V_department_usuccess = "Department Updated Successfully";
  V_msg_DuplicateDepRecord = "Department Name already exist.";
  V_msg_email = "Please Enter Valid Email.";
  V_branch_success = "Branch Added Successfully";
  V_branch_updSuccess = "Branch Updated Successfully"
  V_msg_DuplicateRecordB = "Branch Name already exist.";
  V_msg_DeleterWarn = "Are you sure to delete Designation";
  V_sc_success = "Sub Company Added Successfully";
  V_sc_updSuccess = "Sub Company Updated Successfully"
  V_msg_DuplicateRecordsc = "Sub Company Name already exist.";
  V_subcompany_name = "Please Enter Sub Company Name";
  V_branch_name = "Please Enter Branch Name";
  V_Invalid_Email_Format = "Please Enter Valid Email Format";
  V_user_success = "User Added Successfully";
  V_user_DuplicateRecord = "User Name already exist.";
  V_user_updSuccess = "User Updated Successfully";
  V_user_name = "Please Enter User Name";
  V_designation = "Please Enter designation";
  V_branch = "Please Enter branch";
  V_subcompany = "Please Enter subcompany";

  //ROLES
  V_role_Name = 'Enter Role Name';
  V_role_Added = 'Role Added';
  V_role_Dup = 'Duplicate Role Name Found';
  V_role_Upd = 'Role Updated';
  V_role_Select = 'Select a Role';
  V_role_permissions_update = 'Role Permission Updated';
  V_user_permissions_update = 'User Permission Updated';
  V_User_Select = 'Select an User';



  //EMPTY FIELD VALIDATION
  funValidateEmptyText(value, message, id) {
    if (value == "" || value == null) {
      this.toastr.warning(message);
      this.renderer2.selectRootElement(id).focus();
      return true;
    } else {
      return false;
    }
  }

  //EMAIL VALIDATION
  funValidateEmail(value, message, id) {
    if (value != null) {
      if (!value.match(this.V_company_Emailformat)) {
        this.toastr.warning(message);
        this.renderer2.selectRootElement(id).focus();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}


