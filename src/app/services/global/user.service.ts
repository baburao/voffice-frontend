import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import {BehaviorSubject} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 

import {WebSocketService} from './web-socket.service';

import { User, CurrentUser, Designation } from '../../interfaces/user';
import { Room } from '../../interfaces/room';

import api_urls from '../../constants/api_urls';

import {AuthService} from '../accounts/auth.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  //currentUser: User| null = null;	
  get currentUser(): CurrentUser {
	return this.authService.currentUserValue;
  }	
  users: {[key: number]: User} = {};
  private designations: {[key: number]: Designation} = {};

  private usersSource = new BehaviorSubject< {[key: number]: User}>(this.users);
  //List of all conversations this user is part of
  usersObservable = this.usersSource.asObservable();

  private meetingRoomList : {[key: number]: Room} = {};
  private meetingRoomsSource = new BehaviorSubject< {[key: number]: Room}>(this.meetingRoomList);
  //List of all conversations this user is part of
  meetingRooms = this.meetingRoomsSource.asObservable();

  dataLoaded: boolean = false;
  
  constructor(private http: HttpClient, private authService: AuthService, private webSocketService: WebSocketService) { 
	
	this.authService.currentUser.subscribe((user: User | null) => {
		if(user == null) {
			this.dataLoaded = false;
		}
	});
	//this.http.get(api_urls.GetUser).subscribe((user: any) => {		
		
			
	
	//});
  }
  init(resolve: any, reject: any) {
	
          
    console.log("Init");
  	this.webSocketService.init().then( () => {
      //resolve(1);
      this.dataLoaded = true;
      console.log("webSocket connected");
  		this.http.get(api_urls.ListEmployees).subscribe((data: any) => {				
  			
        //console.log(this.authService.currentUserValue);
  			//this.currentUser = data.user;
  			this.users = data.users;
  			
  			this.usersSource.next(this.users);  
        
  			this.webSocketService.onUpdateStatus.subscribe((data : any) => {
  				
  				if(data.user != this.currentUser?.id) {
  					this.users[data.user].is_online = data.is_online;
  				}
  			});
        this.webSocketService.onUpdateUser.subscribe((data : any) => {
          
          if(data.id != this.currentUser!.id) {
            this.users[data.id] = data;
          }
        });
        
        
  			this.http.get(api_urls.ListMeetingRooms).subscribe((rooms: any) => {	
  				
  				this.meetingRoomList = rooms;
  				this.meetingRoomsSource.next(this.meetingRoomList);
  				
  			});
  			this.http.get(api_urls.Designations).subscribe((designations: any) => {
          
          for(var d in designations) {
            this.designations[designations[d].id] = designations[d];
          }
          console.log("Data loaded");
          this.dataLoaded = true;
          resolve(1);
        });
  			
  		});
  	}).catch(()=> {
  		console.log("Data loading error");		
  		//this.authService.resetUser();
  		//this.router.navigate(['']);			
  		reject();
  	});
  }
  loadData() : Promise<any> {
	
	return new Promise<any>((resolve: any, reject: any) => {
		
		if(!this.dataLoaded) {
			
			this.init(resolve, reject);			
		} else {
			resolve(1);
		}	
				
	});
  }
  /*
  * Subsribe to get a list of all users
  */
  getUsers(): Observable<{[key: number]: User}> {
  	return this.usersObservable;
  }  
  getUser(id: number): User {
	
  	if(id == this.currentUser?.id) {
  		return this.currentUser;
  	}
	
    if(!(id in this.users)) {
      return {
        id: id,
        firstname: "User " + id,
        lastname: "",
        designation: 1,
        is_online: false
      }
    }
  	return this.users[id];
	}
	getDesignation(user: User | null): Designation {
    
    if(!user || !(user.designation in this.designations)) {
      return {
        id: 0,
        name: "-"
      }
    }
    
    return this.designations[user.designation];
  }
	getDesignationName(user: User | null): string {
    return this.getDesignation(user).name;
  }
  getDesignationNameUserId(id: number): string {
    return this.getDesignation(this.getUser(id)).name;
  }
  getDesignationArray() : any[] {
    return Object.values(this.designations);
  }
	//getDesignations() {
	//	return this.http.get(api_urls.Designations);
	//}
  addDesignation(name: string, description: string) {
    return this.http.post(api_urls.Designations, {name: name, description: description});
  }
	updateUserProfile(userUpdateObj) {
		this.http.post(api_urls.UpdateUser, userUpdateObj).subscribe(() => { 
			localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
		}); 
	}
  addEmployee(firstName: string, lastName: string, email: string, designationId: number) {
    return this.http.post(api_urls.AddEmployee, {firstname: firstName, lastname: lastName, email: email, designation: designationId})
  }
  assignDesignation(userId: number, designationId: number) {
    return this.http.post(api_urls.AssignDesignation, {user: userId, designation: designationId})
  }
	changePassword(newPassword:string) {
		return this.http.post(api_urls.ChangePassword, {password: newPassword});
	}
	
}
