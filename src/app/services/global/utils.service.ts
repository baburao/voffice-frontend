import {Injectable} from '@angular/core';
import {Notification} from "../../interfaces/notification";
import {BehaviorSubject} from "rxjs";
import {Conversation} from "../../interfaces/conversation";

@Injectable({
  providedIn: 'root'
})
export class UtilsService {


  // @ts-ignore
  conferenceConversation : Conversation = {};
  public conversationSource = new BehaviorSubject<any>(this.conferenceConversation);
  confConvo = this.conversationSource.asObservable();

  constructor() { }

  updateConfConvo(convo: object) {
    this.conversationSource.next(convo);
  }

   abbreviate(text: string){

    try {
       // @ts-ignore
       return  text.match(/[A-Z]/g).join('');
    }catch (e) {
      return text.slice(0,1)
    }
	}

	 fullname(usr: any,type= true){
	  if (type){
	    return usr.value.firstname + ' ' + usr.value.lastname;
    }
    else{
      return usr.firstname + ' ' + usr.lastname;
    }
  }

  designationName(designnations: any, designId) {
    if (designId != null && designnations) {
      let res = designnations.find(item => item.id === designId);
      return res.name
    }
    return '';
  }
}
