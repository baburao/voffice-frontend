import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 

import { BehaviorSubject } from 'rxjs';
import { Observable } from "rxjs/Observable";
import "rxjs/Observable";
import "rxjs/add/observable/of";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";

import api_urls from '../../constants/api_urls';
import { Company } from '../../interfaces/company';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  
  //private company: Company | null = null;
  private companySource = new BehaviorSubject<Company | null>(null);
  //List of all conversations this user is part of
  companyObservable = this.companySource.asObservable();
  constructor(private http: HttpClient) { 
    
    //this.http.get(api_urls.ListEmployees).subscribe((data: any) => {  });      
    this.updateCompany();  
  }
  
  updateCompany() {
    this.http.get<Company>(api_urls.GetCompany).subscribe((company : Company) => {
      this.companySource.next(company);
    });
  }
  assignUserToDepartment(userId: number, departmentId: number) {
    this.http.post(api_urls.AssignUser, { user: userId, type: 'department', level: departmentId }).subscribe( () => {
      this.updateCompany(); 
    });
  }
  addDepartment(parentBranchId: number, name: string) {
    return this.http.post(api_urls.AddDepartment, { parent: parentBranchId, name: name });
  }
  addBranch(parentBranchId: number, name: string) {
    return this.http.post(api_urls.AddDepartment, { parent: parentBranchId, name: name });
  }
  
  resetPassword(userId: number, password: string) {
    return this.http.post(api_urls.ResetPassword, { user: userId, password: password });
  }

  getcompanyemployees(companyid){
    return this.http.get(api_urls.getcompanyemployees+"?company_id="+companyid);
  }
  createEmployee(empData){
    return this.http.post(api_urls.createemployee,empData);
  }

  getDesignations(companyid){
    return this.http.get(api_urls.getDesignations+"?company_id="+companyid);
  }

  addDesignation(company_id: number, ip_designation_id: number, ip_designation_name: string, ip_description: string, ip_company_id: number, ip_active: boolean){
    return this.http.post(api_urls.addDesignations, { company_id: company_id, ip_designation_id: ip_designation_id, ip_designation_name: ip_designation_name, ip_description: ip_description, ip_company_id:ip_company_id, ip_active:ip_active});
  }

// COMPANY ROLES
getcmproles(companyid){
  return this.http.get(api_urls.GetcompanyRolesbycmpyId+"?company_id="+companyid);
}

addupdateroles(roleName,roleId,active,companyId){
  return this.http.post(api_urls.AddUpdatecompoanyRole,{
    'role_name' : roleName,
    'role_id':roleId,
    'is_active':active,
    'ip_company_id':companyId
  })
}

// ACTIVE ROLES
getactiverl(companyid){
  return this.http.get(api_urls.getactiveroles+"?company_id="+companyid);

}

getpermbyroleid(roleid,cmpyid){
  return this.http.post(api_urls.getscreenpermission,{
    'role_id':roleid,
    'company_id':cmpyid
  })
}
  getDepartments(companyid){
    return this.http.get(api_urls.getDepartments+"?company_id="+companyid);
  }
  getSubCompanyDepartments(data){
    return this.http.post(api_urls.getsubcompanybranchdepartments,data);
  }

  addDepartments(company_id: number, ip_department_id: number, ip_department_name: string, ip_branch_id: number, ip_room_id: number, ip_active: boolean, ip_description: string){
    return this.http.post(api_urls.addDepartments, { company_id: company_id, ip_department_id: ip_department_id, ip_department_name: ip_department_name, ip_branch_id: ip_branch_id, ip_room_id: ip_room_id, ip_active: ip_active, ip_description: ip_description});
  }

  getBranches(companyid){
    return this.http.get(api_urls.getBranches+"?company_id="+companyid);
  }

  addCBranch(company_id: number, ip_branch_id: number, ip_branch_name: string, ip_group_company_id: number, ip_address: string, ip_phnoe_number: number, ip_business_name: string, ip_business_email: string,
    ip_business_phone_number: number, ip_legal_name: string, ip_legal_email: string, ip_legal_phone_number: number, ip_corporate_name: string,  ip_corporate_email: string,            
    ip_corporate_phone_number: number, ip_career_name: string, ip_career_email: string, ip_career_phone_number: number, ip_branch_head: string, ip_active: boolean,
    ip_company_department: number, ip_sub_company_department: number, company_type: string ){
    return this.http.post(api_urls.addBranch, { company_id:company_id, ip_branch_id : ip_branch_id,  ip_branch_name : ip_branch_name, ip_group_company_id :
      ip_group_company_id, ip_address : ip_address, ip_phnoe_number :ip_phnoe_number, ip_business_name :ip_business_name, 
      ip_business_email :ip_business_email, ip_business_phone_number :ip_business_phone_number,  ip_legal_name :ip_legal_name, 
      ip_legal_email :ip_legal_email,  ip_legal_phone_number :ip_legal_phone_number,  ip_corporate_name :ip_corporate_name, 
      ip_corporate_email :ip_corporate_email, ip_corporate_phone_number :ip_corporate_phone_number,  ip_career_name :ip_career_name, 
      ip_career_email :ip_career_email,  ip_career_phone_number :ip_career_phone_number,  ip_branch_head :ip_branch_head, 
      ip_active :ip_active,ip_company_department: ip_company_department,ip_sub_company_department:ip_sub_company_department, company_type: company_type });
  }
  getRolesByCompanyid(companyid){
    return this.http.get(api_urls.GetcompanyRolesbycmpyId+"?company_id="+companyid);
  }
  delEmployee(employeeid){
    return this.http.post(api_urls.delEmployee,employeeid );
  }
  getEmployeeDataById(empdata){
    return this.http.post(api_urls.getEmployeeDataById,empdata );
  }
  activeEmployeeEmailId(empdata){
    return this.http.post(api_urls.activationEmailAddress,empdata );
  }
  resendEmailAddress(emailData){
    return this.http.post(api_urls.resendemail,emailData );
  }
  updatescrper(permissionData,companyId){
    return this.http.post(api_urls.updatescrpermissions,{
      'company_id':companyId,
      'inscrnroleprmsnjson': permissionData
    })  
  }

  getSubCompanies(companyid){
    return this.http.get(api_urls.getSubCompanies+"?company_id="+companyid);
  }

  addsubCompany(company_id : number,ip_id : number,ip_name: string,ip_address : string,ip_phone_number : number,ip_business_name : string,
    ip_business_email : string,ip_business_phone_number : number,ip_legal_name : string,ip_legal_email : string,ip_legal_phone_number : number,
    ip_corporate_name : string,ip_corporate_email : string,ip_corporate_phone_number : number,ip_career_name : string,ip_career_email : string,
    ip_career_phone_number : number,ip_facebook: string,ip_linkedin: string,ip_twitter: string,ip_youtube:string,ip_instagram:string, ip_active: boolean,
    ip_view_branch_departments: boolean, ip_company_department: Number, ip_sub_company_department: Number){
      return this.http.post(api_urls.addSubCompany, { company_id : company_id,ip_id : ip_id,ip_name: ip_name,ip_address : ip_address,ip_phone_number : ip_phone_number,
        ip_business_name : ip_business_name,ip_business_email : ip_business_email,ip_business_phone_number : ip_business_phone_number,ip_legal_name : ip_legal_name,
        ip_legal_email : ip_legal_email,ip_legal_phone_number : ip_legal_phone_number,ip_corporate_name : ip_corporate_name,ip_corporate_email : ip_corporate_email,
        ip_corporate_phone_number : ip_corporate_phone_number,ip_career_name : ip_career_name,ip_career_email : ip_career_email,
        ip_career_phone_number : ip_career_phone_number,ip_facebook: ip_facebook,ip_linkedin: ip_linkedin,ip_twitter: ip_twitter,ip_youtube:ip_youtube,
        ip_instagram:ip_instagram, ip_active: ip_active, ip_view_branch_departments: ip_view_branch_departments,
        ip_company_department: ip_company_department, ip_sub_company_department: ip_sub_company_department});
    } 

  getactEmployee(companyid){
    return this.http.post(api_urls.getactemp, {
      company_id: companyid
    });
  }

  getuserpermission(companyid, userId){
    return this.http.post(api_urls.getusrpermissions, {
      user_id: userId,
      company_id: companyid
    });
  }

  updateusrpermission(permissionData,companyId){
    return this.http.post(api_urls.updateusrperissions,{
      'company_id':companyId,
      'inusrpermission_data' : permissionData
    })
  }

  getSubCompanysByid(companyid){
    return this.http.get(api_urls.getSubCompanies+"?company_id="+companyid);
  }

  getCompanies(companyid) {
    return this.http.get(api_urls.getcompanies + "?company_id=" + companyid);
  }
  
  addCompany(company_id:Number,ip_name: String,ip_server_database: boolean,ip_database_connection: String,
    ip_domain: String,ip_view_groupcompany_branch_departments: boolean, ip_has_group_companies: boolean,
    ip_group_company_count: Number, ip_has_branches: Boolean, ip_branch_count: Number, ip_address: String,
    ip_phone_number: Number, ip_business_name: string,
    ip_business_email: string, ip_business_phone_number: number, ip_legal_name: string, ip_legal_email: string, ip_legal_phone_number: number,
    ip_corporate_name: string, ip_corporate_email: string, ip_corporate_phone_number: number, ip_career_name: string, ip_career_email: string,
    ip_career_phone_number: number, ip_facebook: string, ip_linkedin: string,  ip_youtube: string, ip_instagram: string, 
    ip_twitter: string) {
    return this.http.post(api_urls.addcompanies, {
      company_id: company_id, ip_name: ip_name,ip_server_database: ip_server_database,ip_database_connection: ip_database_connection,
      ip_domain: ip_domain,ip_view_groupcompany_branch_departments: ip_view_groupcompany_branch_departments, ip_has_group_companies: ip_has_group_companies,
      ip_group_company_count: ip_group_company_count, ip_has_branches: ip_has_branches, ip_branch_count: ip_branch_count, ip_address: ip_address, ip_phone_number: ip_phone_number,
      ip_business_name: ip_business_name, ip_business_email: ip_business_email, ip_business_phone_number: ip_business_phone_number, ip_legal_name: ip_legal_name,
      ip_legal_email: ip_legal_email, ip_legal_phone_number: ip_legal_phone_number, ip_corporate_name: ip_corporate_name, ip_corporate_email: ip_corporate_email,
      ip_corporate_phone_number: ip_corporate_phone_number, ip_career_name: ip_career_name, ip_career_email: ip_career_email,
      ip_career_phone_number: ip_career_phone_number, ip_facebook: ip_facebook, ip_linkedin: ip_linkedin, ip_youtube: ip_youtube,
      ip_instagram: ip_instagram, ip_twitter: ip_twitter
    });
  }

  updCompany(company_id:Number, ip_company_id: Number, ip_name: string, ip_server_database: boolean,ip_database_connection: String,
    ip_domain: String,ip_view_groupcompany_branch_departments: boolean, ip_has_group_companies: boolean,
    ip_group_company_count: Number, ip_has_branches: Boolean, ip_branch_count: Number, ip_address: String,
    ip_phone_number: Number, ip_business_name: string,
    ip_business_email: string, ip_business_phone_number: number, ip_legal_name: string, ip_legal_email: string, ip_legal_phone_number: number,
    ip_corporate_name: string, ip_corporate_email: string, ip_corporate_phone_number: number, ip_career_name: string, ip_career_email: string,
    ip_career_phone_number: number, ip_facebook: string, ip_linkedin: string,  ip_youtube: string, ip_instagram: string, 
    ip_twitter: string) {
    return this.http.post(api_urls.updateCompany, {
      company_id: company_id, ip_company_id:ip_company_id, ip_name: ip_name, ip_server_database: ip_server_database,ip_database_connection: ip_database_connection,
      ip_domain: ip_domain,ip_view_groupcompany_branch_departments: ip_view_groupcompany_branch_departments, ip_has_group_companies: ip_has_group_companies,
      ip_group_company_count: ip_group_company_count, ip_has_branches: ip_has_branches, ip_branch_count: ip_branch_count, ip_address: ip_address, ip_phone_number: ip_phone_number,
      ip_business_name: ip_business_name, ip_business_email: ip_business_email, ip_business_phone_number: ip_business_phone_number, ip_legal_name: ip_legal_name,
      ip_legal_email: ip_legal_email, ip_legal_phone_number: ip_legal_phone_number, ip_corporate_name: ip_corporate_name, ip_corporate_email: ip_corporate_email,
      ip_corporate_phone_number: ip_corporate_phone_number, ip_career_name: ip_career_name, ip_career_email: ip_career_email,
      ip_career_phone_number: ip_career_phone_number, ip_facebook: ip_facebook, ip_linkedin: ip_linkedin, ip_youtube: ip_youtube,
      ip_instagram: ip_instagram, ip_twitter: ip_twitter
    });
  }

  // getsaasDepartments(companyid) {
  //   return this.http.get(api_urls.getsaasDepartments + "?company_id=" + companyid);
  // }

  // addsaasDepartments(ip_department_id: number, ip_department_name: string) {
  //   return this.http.post(api_urls.addsaasDepartments, { ip_department_id: ip_department_id, ip_department_name: ip_department_name });
  // }

  
  // getsaasUser() {
  //   return this.http.get(api_urls.getsaasUsers);
  // }

  // addsaasUser(ip_id: number,ip_first_name: string, ip_last_name: string,
  //    ip_middle_name: string, ip_role_id: number, ip_vendor_name: string, ip_phone_number: number,
  //     ip_email_id: string, ip_contact_address1: string, ip_contact_address2:string,
  //       ip_password: string,ip_status: boolean) {
  //   return this.http.post(api_urls.addsaasUsers,{ ip_id: ip_id,ip_first_name: ip_first_name, ip_last_name: ip_last_name,
  //     ip_middle_name: ip_middle_name, ip_role_id: ip_role_id, ip_vendor_name: ip_vendor_name, ip_phone_number: ip_phone_number,
  //      ip_email_id: ip_email_id, ip_contact_address1: ip_contact_address1, ip_contact_address2:ip_contact_address2,
  //        ip_password: ip_password,ip_status: ip_status });
  // }



  get(url): Observable<any> {
    return this.http
      .get(url)
      .map((res) => {
        return res;
      })
  }
}
