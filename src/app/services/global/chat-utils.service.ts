import { Injectable, Renderer2, RendererFactory2  } from '@angular/core';
import { UserService } from './user.service';
import { UtilsService } from './utils.service';
import { BehaviorSubject } from 'rxjs';
import { stat } from 'fs';
import { ConversationService } from './conversation.service';

@Injectable({
  providedIn: 'root'
})
export class ChatUtilsService {

  contactClass = 'contactsbox-toggle-open';
  groupClass = 'creategroup-toggle-open';
  chatDrawerClass = 'chatbox-right-toggle-open';

  private renderer: Renderer2;
  constructor(private userService: UserService,private utils: UtilsService,
    rendererFactory: RendererFactory2,
    private conversationService:ConversationService) {
      this.renderer = rendererFactory.createRenderer(null, null);
     }

  private chatWindowSatus: string = 'closed';
  private chatWindowSatusSource = new BehaviorSubject<string>(this.chatWindowSatus);
  // chat window status
  chatWindowState = this.chatWindowSatusSource.asObservable();


  private groupEditSource = new BehaviorSubject<boolean>(false);
  // chat window status
  groupEditState = this.groupEditSource.asObservable();


  changeChatWindowStatus(status:string) {
    this.chatWindowSatusSource.next(status);
    return true;
  }

  toggleContacts() { // toggles Contact list.
    const hasClass = this.hasClass(this.contactClass);
    if (hasClass) {
      this.removeClass(this.contactClass);
    } else {
      this.addClass(this.contactClass);
    }
  }

  // Toggle group create window
  toggleGroup() {    
    const hasClass = this.hasClass(this.groupClass);
    if (hasClass) {
      this.removeClass(this.groupClass);
    } else {
      this.addClass(this.groupClass);
    }
  }

  hasClass(cls: string) { // checks if class exists
    return document.body.classList.contains(cls);
  }

  addClass(cls: string) { // adds class to body
    this.renderer.addClass(document.body, cls);
  }

  removeClass(cls: string) { // removes class from Body
    this.renderer.removeClass(document.body, cls);
  }


  isCurrentUser(userId: number): boolean {
    return this.userService.currentUser?.id == userId;
  }
  fullname(usr: any, type: any) {
    return this.utils.fullname(usr, type);
  }
  
  formatDate(date:any){
    return `${date.getDay()}-${date.getMonth()}-${date.getFullYear()}`;
  }

  smartDate(dte:any){
    const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ];
    const date = new Date(dte);
    return date.getDate()+' '+monthNames[date.getMonth()];
  }
  chatClass(userId: number){
    return this.isCurrentUser(userId) ? 'receiver-msg-contain' : 'sender-msg-contain';
  }

  abbreviate(text: string){
	  return this.utils.abbreviate(text);
  }

  ciricleBGC() {
    const colours = ['pink-l', 'blue-l', 'green-l'];
    //return  colours[Math.floor(Math.random() * colours.length)]
    return 'pink-l';
  }

  isDirect(conversation) {
    return this.conversationService.isOneToOne(conversation);
  }

  toggleChatDrawer(id = -1, toggleContact= true, doNotSetConversation =  false) { // toggles Chat Bar and selects Conversation
    
    if(toggleContact) {
      this.toggleContacts();
    }
    if(!doNotSetConversation) {
      if (id > 0) {
        this.conversationService.selectConversation(id);
      } else {
        this.conversationService.selectConversation(-1);
      }
    }      
   
    let hasClass;

    hasClass = this.hasClass(this.chatDrawerClass);
    if (hasClass) {
      this.removeClass(this.chatDrawerClass);
    } else {
      this.addClass(this.chatDrawerClass)
    }
  }

  selectConversation(conversationId: number) {
    this.conversationService.selectConversation(conversationId);
  }

    // Get latest message count from conversation
    getLatestMessageCount(conversations) {
      let count = 0;
      if (conversations.length) {
        conversations.forEach(e => {
          if (e.latest)
            count++;
        })
      }
      return count;
    }

    seteditGroup(isEdit = false) {
      this.groupEditSource.next(isEdit);
    }
}
