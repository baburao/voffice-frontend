// import { Injectable } from '@angular/core';
// import { LocalStorageService } from 'ngx-webstorage';

import { Injectable, NgModuleRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import api_urls from 'src/app/constants/api_urls';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import {User, CurrentUser} from '../../interfaces/user';


@Injectable({
    providedIn: 'root'
  })
export class SecurityService {

    userScreenPermissions = [{operation_id:1}];
    screen_operations = {add:false,edit:false,delete:false};

    //  Screens List
    screen_designation = 1
    screen_department = 2
    
    //  Operations List
    operation_add = 1
    operation_edit = 2
    operation_delete = 3

    constructor(
        ) { }

    
    access_denied = ['./home']
    getScreenPermissions(screen_id) {
        var user = JSON.parse(String(localStorage.getItem('currentUser')))
        this.userScreenPermissions = user.screen_permissions.filter(x => x.screen_id == screen_id)
        // console.log(this.userScreenPermissions);
        for (let i = 0; i < this.userScreenPermissions.length; i++) {
            if(this.userScreenPermissions[i].operation_id == this.operation_add){
                this.screen_operations.add = true;
            }
            if(this.userScreenPermissions[i].operation_id == this.operation_edit){
                this.screen_operations.edit = true;
            }
            if(this.userScreenPermissions[i].operation_id == this.operation_delete){
                this.screen_operations.delete = true;
            }
        }
    }


}