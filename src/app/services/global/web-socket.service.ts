import { Injectable, Output, EventEmitter } from '@angular/core'; 
import {Router} from "@angular/router";

import { ServerConnection } from '../../utils/server-connection';
import {AuthService} from '../accounts/auth.service';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  serverConnection: ServerConnection | null = null;
  
  @Output() onSignal = new EventEmitter<any>();
  @Output() onUpdateConference = new EventEmitter<any>();
  @Output() onUpdateMessage = new EventEmitter<any>();
  @Output() onUpdateConversation = new EventEmitter<any>();
  @Output() onUpdateNotification = new EventEmitter<any>();
  @Output() onUpdateRoomStatus = new EventEmitter<any>();
  @Output() onUpdateStatus = new EventEmitter<any>();
  @Output() onUpdateTeam = new EventEmitter<any>();
  @Output() onDeleteTeam = new EventEmitter<any>();
  @Output() onDeleteConference = new EventEmitter<any>();
  @Output() onUpdateUser = new EventEmitter<any>();
  @Output() onCall = new EventEmitter<any>();
	
  constructor(private router: Router, private authService: AuthService) { 
  }
  init(): Promise<any> {	
  	
  	return new Promise( (resolve: any, reject: any)=> {
  		var self = this;
  		//Connect websocket		
  		this.serverConnection = new ServerConnection();
  		this.serverConnection.onSignal = function(data: any) {
  			
  			self.onSignal.emit(data);
  		}	
  		this.serverConnection.onOpen = () => {
  			resolve();
  		}
  		this.serverConnection.onUpdate = function(type:string, data: any) {
  			
  			if(type == 'conference') {
  				self.onUpdateConference.emit(data);
  			} else if(type == 'conferenceDelete') {
  				self.onDeleteConference.emit(data);
  			} else if(type == 'message') {
  				self.onUpdateMessage.emit(data);
  			} else if(type == 'conversation') {
  				self.onUpdateConversation.emit(data);
  			} else if(type == 'notification') {			
  				self.onUpdateNotification.emit(data);
  			} else if(type == 'roomStatus') {			
  				self.onUpdateRoomStatus.emit(data);
  			}  else if(type == 'status') {			
  				self.onUpdateStatus.emit(data);
  			}  else if(type == 'team') {			
  				self.onUpdateTeam.emit(data);			
  			}  else if(type == 'teamDelete') {			
  				self.onDeleteTeam.emit(data);			
  			}  else if(type == 'user') {			
  				self.onUpdateUser.emit(data);
  			}
  		}
  		this.serverConnection.onCall = function(data: any) {			
  			self.onCall.emit(data);
  		}
  		this.serverConnection.onClosed = function() {
  			
  			self.router.navigate(['disconnected']);
  		}
  		this.serverConnection.onError = () => {
  			
  			this.authService.resetUser();
  			this.router.navigate(['']);
  			
  			reject();
  		}
  		
  		var token: string = JSON.parse(localStorage.getItem('currentUser') as string).token;		
  		this.serverConnection.connect(token);
  	});
	
  }
  isConnectionClosed() {
    return this.serverConnection != null ? this.serverConnection.closed : false;
  }
}
