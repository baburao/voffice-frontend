import { TestBed } from '@angular/core/testing';

import { ChatUtilsService } from './chat-utils.service';

describe('ChatUtilsService', () => {
  let service: ChatUtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatUtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
