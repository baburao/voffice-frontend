import { Injectable, OnInit, OnDestroy } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { WebSocketService } from './web-socket.service';
import { Room } from '../../interfaces/room';
import { Team } from '../../interfaces/team';
import api_urls from '../../constants/api_urls';



@Injectable({
  providedIn: 'root'
})
export class OfficeService {


  private roomMap: { [key: number]: Room } = {};
  private roomsSource = new BehaviorSubject<{ [key: number]: Room }>(this.roomMap);
  //List of all conversations this user is part of
  rooms = this.roomsSource.asObservable();

  private teamMap: { [key: number]: Team } = {};
  private teamSource = new BehaviorSubject<{ [key: number]: Team }>(this.teamMap);
  //List of all conversations this user is part of
  teams = this.teamSource.asObservable();

  constructor(private http: HttpClient, private webSocketService: WebSocketService) {

    this.http.get(api_urls.GetOffice).subscribe((rooms: any) => {

      this.roomMap = rooms.rooms;
      this.roomsSource.next(this.roomMap);
      //console.log("Rooms", this.roomMap);
    });
    this.http.get(api_urls.ListTeams).subscribe((teams: any) => {

      this.teamMap = teams;
      this.teamSource.next(this.teamMap);
      //console.log("Teams", this.teamMap);
    });

    this.webSocketService.onUpdateRoomStatus.subscribe((data: any) => {

      if(data.room in this.roomMap) {
        if (data.enter) {
          this.roomMap[data.room].slots[parseInt(data.slot)] = data.user;
        } else {
          this.roomMap[data.room].slots[parseInt(data.slot)] = -1;
        }
      } else {
        console.log("Room error!" , this.roomMap[data.room])
      }
    });

    this.webSocketService.onUpdateTeam.subscribe((data: any) => {
      this.teamMap[data.id] = data;
      this.teamSource.next(this.teamMap);
      //console.log("Team", data);
    });
    this.webSocketService.onDeleteTeam.subscribe((data: any) => {
      delete this.teamMap[data];
      this.teamSource.next(this.teamMap);
      //console.log("Team", data);
    });
  }
  enterWorkspace(): void {

    this.http.post(api_urls.EnterWorkspace, {}).subscribe();
  }
  enterRoom(roomId: number): void {

    this.http.post(api_urls.EnterRoom, { room: roomId }).subscribe();
  }
  saveOffice(): void {
    this.http.post(api_urls.SaveOffice, { rooms: this.roomMap }).subscribe(() => { });
  }
  createTeam(name: string, members: number[]) {

    this.http.post(api_urls.AddTeam, { name: name, members: members }).subscribe();
  }
  assignToTeam(teamId: number, user: number) {

    this.http.post(api_urls.AssignToTeam, { team: teamId, user: user }).subscribe();
  }
  removeFromTeam(teamId: number, user: number) {

    this.http.post(api_urls.RemoveFromTeam, { team: teamId, user: user }).subscribe();
  }

  isHead(userId: number, departmentRoom: Room): boolean {
    return userId in departmentRoom.heads;
  }
  
  deleteTeam(teamId: number) {
    this.http.post(api_urls.DeleteTeam, { team: teamId }).subscribe();
  }
}
