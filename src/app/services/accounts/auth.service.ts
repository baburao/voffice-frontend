import { Injectable, NgModuleRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import api_urls from 'src/app/constants/api_urls';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import {User, CurrentUser} from '../../interfaces/user';

import {MainModule} from '../../main.module';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<CurrentUser>;
  public currentUser: Observable<CurrentUser>;
	
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<CurrentUser>(JSON.parse(localStorage.getItem('currentUser') as string));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): CurrentUser {
        return this.currentUserSubject.value;
    }

  login(loginDetails: any) {
     return this.http.post(api_urls.Login, loginDetails).pipe(map(user => {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user as CurrentUser);
          return user;
     }));
  }

  logout() {
      this.http.get(api_urls.Logout).subscribe((e) => {
        
      });
    window.location.href = '#'
      this.resetUser();
   }
  resetUser() {
	// remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    localStorage.removeItem('quickchatRemovedusers');
    // @ts-ignore
    this.currentUserSubject.next(null);
  }
}

