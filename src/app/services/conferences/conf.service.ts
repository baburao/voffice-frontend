import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from "@angular/router";
import {BehaviorSubject} from 'rxjs';

import api_urls from 'src/app/constants/api_urls';

import { WebSocketService } from '../global/web-socket.service';
import { UserService } from '../global/user.service';

import { Conference } from '../../interfaces/conference';
import { Conversation } from '../../interfaces/conversation';
import { Message } from '../../interfaces/message';
import { User } from '../../interfaces/user';

//import { ServerConnection } from '../../utils/server-connection';
import { Call2, CallPeer, FakeCall } from '../../call/call2';
import { DirectCall, CallState } from '../../call/direct-call';
import { DebugCall } from '../../call/call';
import {NotificationService} from "../global/notification.service";
import {Notification} from "../../interfaces/notification";
import  {UtilsService} from "../global/utils.service";

import  { ConversationService } from "../global/conversation.service";

@Injectable({
  providedIn: 'root'
})
/*
* Handles most interaction with the backend, and provides all data
*
*/
export class ConfService {

	//serverConnection: ServerConnection;
	activeCall: Call2 | null = null;
	  databack= new BehaviorSubject <any> ({});
	  
	private activeCallSource = new BehaviorSubject<Call2 | null>(this.activeCall);
  	//List of all conversations this user is part of
 	activeCallObservable = this.activeCallSource.asObservable();

	//Conference creator should subscribe here, to be notfied about when the user selects a conference to edit
	@Output() onEditConference = new EventEmitter<Conference>();
	//@Output() onNewCall = new EventEmitter<Call>();

	conferences: {[key: number]: Conference} = {};
	private conferencesSource = new BehaviorSubject<{[key: number]: Conference}>(this.conferences);
  	//List of all confrences this user is assigned to
 	conferencesObservable = this.conferencesSource.asObservable();

  directCall: DirectCall | null = null;
  private directCallSource = new BehaviorSubject<any>(null);
  directCallObservable = this.directCallSource.asObservable();
  
  public showGrid: boolean = true;
  
	//incomingCall: any = null;
	//private incomingCallSource = new BehaviorSubject<any>(null);
	//incomingCallObservable = this.incomingCallSource.asObservable();

	//public callDeclineSource = new BehaviorSubject<any>(null);
	//callDeclineObservable = this.callDeclineSource.asObservable();

	//users: {[key: number]: User} = {};


	//@Output() onPeerScreenshare = new EventEmitter<CallPeer>();
	

	constructor(private http: HttpClient, private router: Router,
	  private webSocketService: WebSocketService,
              private userService: UserService, private conversationService: ConversationService) {
}
init(): Promise<any> {  
    
    return new Promise( (resolve: any, reject: any)=> {
      
  		var self = this;
  		//this.updateConferences();
      
  		//Get inital data
  		/*this.userService.getUsers().subscribe((userMap: any) => {
  
  			//self.users = userMap;
  
  			this.updateConferences();
  		});*/
  
  		//this.webSocketService.onSignal.subscribe((data: any) =>{ });
      
  		this.webSocketService.onUpdateConference.subscribe((data: any) =>{
  			
  			self.conferences[data.id] = self.parseConference(data);
  		});
  		this.webSocketService.onDeleteConference.subscribe((data: any) =>{
  			
        console.log("delete conference ", data)
  			delete self.conferences[data];
  		});
      
  		this.webSocketService.onCall.subscribe((data: any) =>{			
  				
  			if(data.type == 'offer') {        
           
          var newCall: DirectCall = new DirectCall( this.userService.currentUser!.id, this.http);        
          
          var conversation: Conversation | null = this.conversationService.getConversationById(data.conversation);
           if(conversation == null) {
            newCall.declineCall();
            return;
          }
             
          newCall.incoming(conversation, data.user, data.id);
          
  				if(this.activeCall != null || this.directCall != null) {
            newCall.declineCall();
            return;
          }
          this.directCall = newCall;        
          
  				console.log("Call", this.directCall);
          this.directCallSource.next(this.directCall);
          
  			} else if(data.type == 'decline') {
                 
          console.log("Declined!", this.directCall , data)
          if(this.directCall != null && this.directCall.callId == data.id) {
            this.directCall.callDeclined(data.user);
          }
  			//	this.callDeclineSource.next(null);
          
          
  			} else if(data.type == 'leave') {
          
  			} else if(data.type == 'end') {
          
          
          console.log("end!", this.directCall , data)
          
          if(this.directCall != null && this.directCall.callId == data.id) {
            
            if(this.directCall.state != 'declined') {
              this.directCall.leaveCall();
              this.directCall = null;
              this.directCallSource.next(null);    
            }    
          }
          
          var callConversation: Conversation = this.conversationService.getConversationById(data.conversation);
          
          var callMessage: Message = this.conversationService.getCallMessage(callConversation, data.id);
          
          if(callMessage != null) {
            callMessage.call.end_date = data.end_date;
          }
        }
  		});
      
      this.http.get(api_urls.ListConferences).subscribe((data :any) => {

        for(var c in data) {
  
          var conference = data[c];
          this.conferences[conference.id] = this.parseConference(conference);
        }
        
        this.conferencesSource.next(this.conferences);
        resolve();
      });
    
    
  		//Updates the time left of all conferences
  		setInterval(function(){
  
  			for(var c in self.conferences) {
  
  				var time:number = (self.conferences[c].startDate.getTime() - Date.now());
  
  				self.conferences[c].time = time;
  
  			}
  		}, 3000);
      
    });
  }
	//Conference management functions
	/*
	* Subsribe to get a list of all conferences
	*/
	getConferences(): Observable<{[key: number]: Conference}> {
  		return this.conferencesObservable;
	}
	/*
	* Saves the currently edited conference
  * TODO map() -> subscribe() stuff
	*/
	saveConference(conference: Conference , success: any, fail: any) : any {
		 var self = this;
		 let newcnfs = null;
		 this.http.post(api_urls.SaveConference, {
			id: conference.id,
			start: conference.startDate.toJSON(),
			title: conference.title,
			duration: conference.duration,
			meeting_room: conference.meeting_room,
			users: conference.assignedUsers
		}).subscribe(addedConference => {
			var newConference = self.parseConference(addedConference);
			self.conferences[newConference.id] = newConference;

			//self.onEditConference.emit(newConference);
			//const oldNotifications = this.ns.notificationSource.getValue();
			//oldNotifications.unshift(this.createNotification(newConference));
      		//this.ns.addNotification(oldNotifications);

			self.onEditConference.emit(newConference);
			
			success();
		},(e: any) => fail(e));
	}
	/*
	* Call to notify conference editor that a new conference should be created
	*/
	newConference() : void {

		this.onEditConference.emit({
			id: -1, title:'', assignedUsers: [], creator: -1, duration: 0, 
			startDate: new Date(Date.now()), time: 0, meeting_room: 0, conversation: null, call: 0});

	}
	/*
	* Edit a conference, called from conference list component
	*/
	editConference(conferenceId: number) : void {

		var conference = this.conferences[conferenceId];

		this.onEditConference.emit(conference);
	}
  deleteConference(conferenceID: number) {
    this.http.post(api_urls.DeleteConference, {id: conferenceID}).subscribe((data :any) => {
      delete this.conferences[conferenceID];
    });
  }

// upadte Conference


	//Call related functions
	/*
	* conference
	*/

	createCall(conferenceId: number): void {

		var self = this;

		if(conferenceId >= 0) {
  
			var conference = this.conferences[conferenceId];
			this.activeCall = new Call2(this.userService.currentUser!.id, conference.call, conference, () => {
				//this.activeCall = null;
				//this.activeCallSource.next(null);
			}, (sharingPeer: CallPeer ) => {
				//this.onPeerScreenshare.emit(sharingPeer);
			});
			this.showGrid = true;
		} else {

			var count: number = Math.abs(conferenceId);

			console.log("Create Debug Conference");
			
			/*var assignedUsers: number[] = [];
			
			var debugConf = {id: 666, title:'DebugConf', assignedUsers: assignedUsers,
			 	creator: 1, duration: 0, startDate: new Date(Date.now()), time: 0, meeting_room: 1}
			
			for(var i = 0; i < count; i++) {

				var u: number = (i ) + 1;

				debugConf.assignedUsers.push(u);
			}
			
			this.conferences[666] = debugConf;*/
			this.activeCall = new FakeCall(this.userService.currentUser!.id, count);
		}

		this.activeCallSource.next(this.activeCall);
		this.activeCall!.join().then(() => {});
	}
	leaveCall(): void {
		
		if(this.activeCall != null) {
			
			/*if(this.activeCall.isConference()) {
				this.conversationService.setCurrentConversation(null);
			}*/
			this.activeCall.leaveCall();
			
			this.activeCall = null;
			this.activeCallSource.next(null);
		}
	}
	private updateConferences() : void {
		var self = this;

		
	}
  getAllConferences() {
    return this.http.get(api_urls.ListAllConferences);
  }
	public parseConference(jsonData: any) : Conference {

		var time:number = (new Date(jsonData.start).getTime() - Date.now());

		return {
			id: jsonData.id,
			title: jsonData.title,
			duration: jsonData.duration,
			startDate: new Date(jsonData.start),
			creator: jsonData.creator,//sers[jsonData.creator],
			assignedUsers: jsonData.users,
			time: time,
			meeting_room: jsonData.meeting_room,
			conversation: jsonData.conversation,
			call: jsonData.call
		};
	}
  
  getAvailableMeetingRooms(startDate: Date, duration: number, exclude: number) {
    return this.http.post(api_urls.GetAvailabeMeetingRooms, {start: startDate, duration: duration, exclude: exclude});
  }
	callUser(conversationId: number, withCam: boolean): void {
    
    if(this.directCall != null) {
      return;
    }    
    
    var conversation: Conversation | null = this.conversationService.getConversationById(conversationId);
    
    if(conversation == null || !this.conversationService.isOneToOne(conversation)) {
      return;
    }
    
    var targetUserId: number = this.conversationService.getOtherUser(conversation);
    this.directCall = new DirectCall(this.userService.currentUser!.id, this.http);
    this.directCall.callUser(conversation, targetUserId, withCam, ()=>{});
    this.directCallSource.next(this.directCall);
		/*this.http.post(api_urls.CallUser, {user: userId}).subscribe((data :any) => {
			
			if(data.called) {
				
				console.log("called");
				this.activeCall = new Call2(this.userService.currentUser!.id, data.call, null,() => {
				//this.activeCall = null;
				//this.activeCallSource.next(null);
				}, (sharingPeer: CallPeer ) => {});
				
				this.activeCall.onNewPeer = onConnected;
				this.activeCallSource.next(this.activeCall);
				this.activeCall!.join().then(() => {});
		
			} else {
				
			}
		});*/
	}
	
	
	//acceptCall(call: any, onConnected: any): void {
	acceptCall(): void {
		
    if(this.directCall == null) {
      return;
    }
    this.directCall.acceptCall();
    this.conversationService.selectConversation(this.directCall.conversation!.id);
		/*this.activeCall = new Call2(this.userService.currentUser!.id, call.call, null,() => {
		//this.activeCall = null;
		//this.activeCallSource.next(null);
		}, (sharingPeer: CallPeer ) => {});
		
		this.activeCall.onNewPeer = onConnected;
		this.activeCallSource.next(this.activeCall);
		this.activeCall!.join().then(() => {});*/
	}	
	//declineCall(call: any): void {
	declineCall(): void {
		
    if(this.directCall == null) {
      return;
    }
    this.directCall.declineCall(); 
    this.directCall = null;      
    this.directCallSource.next(null);   
		//this.http.post(api_urls.DeclineCall, {user: call.user}).subscribe((data :any) => {});		
		//this.incomingCallSource.next(null);
	}
	leaveDirectCall(): void {
    
    if(this.directCall == null) {
      return;
    }
    
    var oldCall = this.directCall;
    this.directCall = null;      
    this.directCallSource.next(null);    
    
    oldCall.leaveCall();  
    
    //this.http.post(api_urls.DeclineCall, {user: call.user}).subscribe((data :any) => {});   
    //this.incomingCallSource.next(null);
  }
	closeDirectCall(): void {
    this.directCall = null;      
    this.directCallSource.next(null);  
  }
}
