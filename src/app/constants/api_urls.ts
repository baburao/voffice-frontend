import {environment} from '../../environments/environment';

export const API_URL = environment.API_URL;

export default {
  Login : API_URL + 'login/',
  Logout : API_URL + 'logout/',
  ResetPassword : API_URL + 'resetPassword/',
  GetUser : API_URL + 'user/',
  UpdateUser : API_URL + 'updateUser/',
  ChangePassword : API_URL + 'changePassword/', // post {password: newPassword}
  AddEmployee : API_URL + 'addEmployee/', 
  AssignDesignation : API_URL + 'assignDesignation/', 
  
  AddDepartment : API_URL + 'addDepartment/', 
  AddBranch : API_URL + 'addBranch/', 

  ListEmployees : API_URL + 'listUsers/employee',

  GetMessageStatus : API_URL + 'messageStatus/', 
  AddConversation : API_URL + 'addConversation/', 
  AddToConversation : API_URL + 'addToConversation/', //post {conversation: conversationId, user: userId}
  RequestConversation : API_URL + 'conversation/',  //+ conversation id
  SendMessage : API_URL + 'sendMessage/',  
  SendFileMessage : API_URL + 'sendFileMessage/',  
  SendAudioMessage : API_URL + 'sendAudioMessage/',  
  ListConversations : API_URL + 'conversations/', 
  GetAudioMessage : API_URL + 'audio/', 
  GetFile : API_URL + 'file/', 

  CallUser : API_URL + 'call/', 
  DeclineCall : API_URL + 'declineCall/', 
  LeaveCall : API_URL + 'leaveCall/', 
  JoinCall : API_URL + 'joinCall/', 
 
  GetOffice : API_URL + 'office/',  
  SaveOffice : API_URL + 'saveOffice/',  
  GetOfficeStatus : API_URL + 'officeStatus/',  
  EnterWorkspace : API_URL + 'enterWorkspace/',  
  EnterRoom : API_URL + 'enterRoom/',  
  GetDepartment : API_URL + 'department/',  
  AssignUser : API_URL + 'assignUser/',  

  CreateMeetingRoom : API_URL + 'createMeetingRoom/',  
  ListMeetingRooms : API_URL + 'listMeetingRooms/', 
  GetAvailabeMeetingRooms : API_URL + 'getAvailabeMeetingRooms/', 
     
  AddTeam : API_URL + 'addTeam/',  
  DeleteTeam : API_URL + 'deleteTeam/',  
  AssignToTeam : API_URL + 'assignToTeam/',  
  RemoveFromTeam : API_URL + 'removeFromTeam/',  
  ListTeams : API_URL + 'listTeams/', 
   
  GetCompany : API_URL + 'company/',  

  Designations : API_URL + 'designations/',  

  ListConferences : API_URL + 'listConferences/',
  ListAllConferences : API_URL + 'listAllConferences/',
  SaveConference : API_URL + 'saveConference',
  DeleteConference : API_URL + 'deleteConference/',

  GetNotifications : API_URL + 'notifications/',
  ReadNotification : API_URL + 'readNotification/',

  UploadAvatar : API_URL + 'uploadAvatar/',

  ProfileImageSmall : API_URL + 'image/small/', // + user id
  ProfileImageBig : API_URL + 'image/big/', // + user id

  
  // ROLES END POINT
    AddcompoanyRole: API_URL + 'addcompanyrole/',
    GetcompanyRolesbycmpyId:API_URL + 'admin/getcmpyrolesbyid',
    AddUpdatecompoanyRole: API_URL + 'admin/addupdatecompanyroles/',
    getactiveroles: API_URL + 'admin/getactroles', 
     

  /*ADMIN WISE END POINTS*/
  createemployee : API_URL + 'admin/createemployee/',
  getdesignationbycompanyid : API_URL + 'voffice/admin/createemployee/',
  getcompanyemployees : API_URL + 'admin/getcompanyemployees',
  getDesignations : API_URL + 'admin/getdesignation',
  addDesignations : API_URL + 'admin/adddesignation/',

  // SCREENS PERMISSIONS
  getscreenpermission: API_URL + 'admin/getrolepermissions/',
  updatescrpermissions:API_URL + 'admin/addscreenpermission',
  delEmployee : API_URL + 'admin/delemployee/',
  getEmployeeDataById : API_URL + 'admin/getemployeedatabyid/',
  activationEmailAddress : API_URL + 'admin/activationemailaddress/',
  resendemail : API_URL + 'admin/sendmail/',

  //DEPARTMENTS
  getDepartments : API_URL + 'admin/getdepartments',
  addDepartments : API_URL + 'admin/addupddepartment',

  // BRANCHES
  getBranches : API_URL + 'admin/getbranch',
  addBranch : API_URL + 'admin/addbranch',

  // SUB-COMPANIES
  getSubCompanies : API_URL + 'admin/getsubcompanies',
  addSubCompany : API_URL + 'admin/addsubcompanies',


  getactemp : API_URL + 'admin/getactemp/',
  getusrpermissions : API_URL + 'admin/getuserpermissions/',
  updateusrperissions : API_URL + 'admin/updateusrpermission',

  //COMPANIES 
  getcompanies : API_URL + 'admin/getcompanies',
  addcompanies : API_URL + 'admin/addcompanies/',
  updateCompany: API_URL + 'company/updatecompany',


  // getsaasDepartments : API_URL + 'admin/getsaasdepartments/',
  // addsaasDepartments : API_URL + 'admin/addsaasdepartments/',
  // getsaasUsers: API_URL + 'department/getusers',
  // addsaasUsers: API_URL + 'department/addusers',
  getsubcompanybranchdepartments : API_URL + 'admin/getdepartmentsbycombranchid/',
  
};

