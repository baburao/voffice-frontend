import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/global/user.service';
import { User } from 'src/app/interfaces/user';
import { UtilsService } from 'src/app/services/global/utils.service';
import { OfficeService } from 'src/app/services/global/office.service';
import { Router } from '@angular/router';
declare var $:any;

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit {
  teamCreateForm: any;
  errors: any;
  
  users: { [key: number]: User } = {};
  selectedUsers: any;
  constructor(private formBuilder: FormBuilder,private officeService: OfficeService,
    private userService: UserService, private utils: UtilsService, private route:Router) {
    this.teamCreateForm = this.formBuilder.group(
      { title: '' }
    )
    this.selectedUsers = [];
   }

  ngOnInit(): void {
    this.getUsers();
  }
  ngAfterViewInit(): void {
    
    $('.create-conference-contactlist > ul').slimScroll({
      height: 'calc(100%)',
      width: '100%',
      color: '#fff'
    });
    
  }
  getUsers(): void {
	  this.userService.getUsers().subscribe((users) => {
      this.users = users;
    });
  }

  saveTeam() {
	  const title = this.teamCreateForm.value.title;
	  if(this.isValid()){
      this.officeService.createTeam(title, this.selectedUsers);
      this.route.navigate(['/team'])
    }    
  }

  toggleUser(selectedUserList) {    
    this.selectedUsers = selectedUserList;
    this.isValid();
  }
  abbreviate(text: string) {
    return this.utils.abbreviate(text)
  }

  isValid(): boolean {
	  this.errors = {};
	  if(this.teamCreateForm.value.title.length === 0){
	    this.errors['title'] = 'Title cannot be empty.'
    }
	  // @ts-ignore
    if (this.selectedUsers.length < 2){
      this.errors['assigned_users'] = 'Please select two or more contacts.'
    }
    return Object.keys(this.errors).length === 0;
  }

}
