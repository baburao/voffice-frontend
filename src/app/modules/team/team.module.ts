
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TeamRoutes} from './team-routes';
import { ListTeamComponent } from './list-team/list-team.component';
import { EditTeamComponent } from './edit-team/edit-team.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';

//import { ConversationModule } from 'src/app/modules/conversation/conversation.module';
import { DepartmentModule } from '../department/department.module';

@NgModule({

  declarations: [ListTeamComponent, EditTeamComponent,],


  imports: [
    CommonModule,
    MatTabsModule,
    MatIconModule,
    RouterModule.forChild(TeamRoutes),
    //ConversationModule,
    DepartmentModule

  ]
})
// @ts-ignore
export class TeamModule { }
