import { Routes } from '@angular/router';
import {ListTeamComponent} from './list-team/list-team.component';
import {EditTeamComponent} from './edit-team/edit-team.component';
import { CreateTeamComponent } from './create-team/create-team.component';
import { UsersContactListComponent } from '../shared/users-contact-list/users-contact-list.component';

export const TeamRoutes: Routes = [
	
  {
    path: 'team',
    data: { breadcrumbs: 'Team' },

    children: [
      { path: '',  component: ListTeamComponent, },
      {
        path: 'mycreate',
        data: { breadcrumbs: 'MyCreate' },
        component: CreateTeamComponent
      },
      {
        path: 'myedit',
        data: { breadcrumbs: 'MyEdit' },
        component: EditTeamComponent
      }
    ]
  },
  // {
  //   path: 'team/edit',
  //   component: EditTeamComponent,
  // },
  // {
  //   path: 'team/create',
  //   component: CreateTeamComponent,
  //   data: { breadcrumbs: 'Create' },
  // },

  // {
  //   path: 'team/xx',
  //   component: UsersContactListComponent
  // },
];

