import { Component, OnInit } from '@angular/core';

import { Team } from '../../../interfaces/team';
import { OfficeService } from '../../../services/global/office.service';
import { UserService } from '../../../services/global/user.service';
import { User } from 'src/app/interfaces/user';
import { UtilsService } from 'src/app/services/global/utils.service';
import api_urls from 'src/app/constants/api_urls';
import { Router } from '@angular/router';

declare  var $:  any;

@Component({
  selector: 'app-list-team',
  templateUrl: './list-team.component.html',
  styleUrls: ['./list-team.component.css']
})
export class ListTeamComponent implements OnInit {


  teams : {[key: number]: Team} = {};
  selectedTeam: Team | null = null;
  users: { [key: number]: User } = {};
  filters: any[] = [];
  //designations: any;

  smallProfileImage: string = api_urls.ProfileImageSmall;


	
  constructor(private officeService: OfficeService, public userService: UserService,private router: Router,  private utils: UtilsService) { }

  ngOnInit(): void {
    //this.getDesignations();
    this.getUsers();
	
	this.officeService.teams.subscribe((teams: {[key: number]: Team}) => {
		
    this.teams = teams; 
      console.log("this.teams", teams, this.teams)
		if(this.selectedTeam == null &&  Object.values(this.teams).length > 0) {
			this.selectedTeam = Object.values(this.teams)[0];
		} else {
      //this.selectedTeam = this.selectedTeam;
      
      console.log("select", this.selectedTeam)
      if(this.selectedTeam != null) {
        this.selectedTeam = this.teams[this.selectedTeam.id];
      }
    }
	});
  }

  ngAfterViewInit(): void {
      $('#lower-office-view-scroll').slimScroll({
        height: 'calc(100% - 110px)',
        width: '100%',
        color: '#fff'
      });

      $('#left-side-navigation-scroll > ul').slimScroll({
        height: '530px',
        width: '100%',
        color: '#fff'
    });
    
    
    $('.create-conference-inner-team').slimScroll({
      height:'500px',
      width: '100%',
      color: '#fff'
    });
  }
  selectTeam(team: Team): void {
	
	this.selectedTeam = team;
  }

  getUsers(): void {

	  this.userService.getUsers()
      .subscribe(users => this.users = users);
  }
  search(event:any) {
    const value = event.target.value.toLowerCase();
	  const len = event.target.value.length;
	  if(len>0){

	    // @ts-ignore
      let users = this.users;
      for (let k of Object.keys(users)){
        // @ts-ignore
        const fullname = this.fullname(this.users[k],false)
        const K = parseInt(k);

        if(fullname.toLowerCase().slice(0, len) === value && this.filtered(K)){
           // @ts-ignore
          this.filters = this.filters.filter(i => i !== K);
        }

        if(fullname.toLowerCase().slice(0, len) !== value){
          this.filters.push(K)
        }
      }
    }else {
	    this.filters = [];
    }
  }

  isTeamMembaer(id: string) {
    if (this.selectedTeam?.members.length)
      return this.selectedTeam!.members.includes(parseInt(id))
    else
      return false;
  }

  addMember(id: string){
    const userId = parseInt(id);
    this.selectedTeam!.members.push(userId);
    this.officeService.assignToTeam(this.selectedTeam!.id, userId);
  }

  filtered(id:number){
	  // @ts-ignore
	  return this.filters.includes(id)
  }
  fullname(user:any){
    return `${user.firstname} ${user.lastname}`;
  }

  /*getDesignations() {
    this.userService.getDesignations().subscribe(res => {
      this.designations = res;
    })
  }

  designation(designations, id) {
    return this.utils.designationName(designations, id);    
  }*/

  deleteTeam() {
    if(this.selectedTeam!=null)
    this.officeService.deleteTeam(this.selectedTeam.id);
    if( Object.values(this.teams).length > 0) {
			this.selectedTeam = Object.values(this.teams)[0];
		}
  }
  updateTeam(){
    if(this.selectedTeam!=null)
    this.router.navigate(['team/edit/' + this.selectedTeam.id]);
  }
}
