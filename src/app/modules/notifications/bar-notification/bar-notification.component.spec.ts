import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarNotificationComponent } from './bar-notification.component';

describe('BarNotificationComponent', () => {
  let component: BarNotificationComponent;
  let fixture: ComponentFixture<BarNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
