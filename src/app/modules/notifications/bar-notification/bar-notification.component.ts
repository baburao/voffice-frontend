import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import {Notification} from "../../../interfaces/notification";
import {NotificationService} from "../../../services/global/notification.service";
import { ConversationService } from '../../../services/global/conversation.service'
import { Conversation } from 'src/app/interfaces/conversation';

@Component({
  selector: 'app-bar-notification',
  templateUrl: './bar-notification.component.html',
  styleUrls: ['./bar-notification.component.css']
})
export class BarNotificationComponent implements OnInit {

  @Input() isOpen: boolean = false;
 
  notifications: Notification[] = [];
  conversations: Conversation[] = [];
  currentConversations: Conversation | null = null;
  chatDrawerClass = 'chatbox-right-toggle-open';
  contactClass = 'contactsbox-toggle-open';
  constructor(private ns: NotificationService, public conversationService: ConversationService,
    private renderer: Renderer2) { }

  ngOnInit(): void {
    this.ns.getNotifications();
    this.ns.notifications.subscribe(ntfs => {
      this.notifications = ntfs;
      if(this.notifications.length > 0) {
        this.ns.playNotificationAudio(true);
      }   
    });

    this.conversationService.conversations.subscribe(
      (conversations: Conversation[]) => {
        this.conversations = conversations;
      });
    
      this.conversationService.currentConversation.subscribe((newConversation: Conversation | null) => {  
        this.currentConversations = newConversation;
      });
  }



  openSelectedChat(conversationId:number) {
    this.currentConversations = this.conversations.find(e=> e.id == conversationId)!;   
    this.toggleChatDrawer(this.currentConversations.id, false, false, false, this.currentConversations);

  }

  isDirect(conversation) {
    return this.conversationService.isOneToOne(conversation)
  }

  toggleChatDrawer(id = -1, contactDrawer = false, force = false, action = false, conversation: any = null) { // toggles Chat Bar and selects Conversation
    let isDirect = this.conversationService.isOneToOne(conversation)
    this.toggleContacts();
    if (id > 0) {
      this.selectConversation(id);
    } else {
      this.selectConversation(-1);
    }
    let hasClass;
    if (isDirect) {
      hasClass = this.hasClass(this.chatDrawerClass);

      if (force) { hasClass = action; }

      if (hasClass) {
        this.removeClass(this.chatDrawerClass);
        this.addClass(this.chatDrawerClass);
      } else {
        this.addClass(this.chatDrawerClass);
      }
    }

  }

  selectConversation(conversationId: number) {
    this.conversationService.selectConversation(conversationId);
  }

  toggleContacts() { // toggles Contact list.
    const hasClass = this.hasClass(this.contactClass);
    if (hasClass) {
      this.removeClass(this.contactClass);
    }
  }

  hasClass(cls: string) { // checks if class exists
    return document.body.classList.contains(cls);
  }

  addClass(cls: string) { // adds class to body
    this.renderer.addClass(document.body, cls);
  }

  removeClass(cls: string) { // removes class from Body
    this.renderer.removeClass(document.body, cls);
  }
}
