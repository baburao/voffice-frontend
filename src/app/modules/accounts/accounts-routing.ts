import { Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import { EmployeeActiveComponent } from '../admin/employee-active/employeeactive.component';

export const AccountRoutes: Routes = [{
  path: '',
  component: LoginComponent
},
{
  path: 'reset',
  component: ResetPasswordComponent
},
{
  path: 'activeemployee/:companyid/:employeeid',
  component: EmployeeActiveComponent
}
];

