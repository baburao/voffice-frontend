import { Component, OnInit, Renderer2, OnDestroy} from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { User } from '../../../interfaces/user';
import {AuthService} from '../../../services/accounts/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  // @ts-ignore
  loginForm: FormGroup;
  SearchForm: FormGroup;
  submitted = false;
  loginError = {message: ''};
  // tslint:disable-next-line:ban-types
  favourites: Object = [];
  // tslint:disable-next-line:ban-types
  favouritesData: Object = [];
  bodyClass = 'login-page-bg';

  constructor(
    private authService: AuthService,
    public formBuilder: FormBuilder,
    private route: Router,
    private renderer: Renderer2
    ) {
        this.loginForm = this.formBuilder.group({
          employee: ['', [Validators.required]],
          password: ['', [Validators.required]]
        });

        this.SearchForm = this.formBuilder.group({
          query: ['', [Validators.required]],
        });
        this.renderer.addClass(document.body, this.bodyClass);
      }

  ngOnInit(): void {
  }
  ngOnDestroy() {
    this.renderer.removeClass(document.body, this.bodyClass);
  }

  get lf() { return this.loginForm.controls; }

  signIn() {
    this.loginError.message = '';
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    const emp = this.loginForm.value.employee;
    const pass = this.loginForm.value.password;

    const self = this;

    const loginDetails = {username: emp, password: pass};

    this.authService.login(loginDetails).subscribe(
    d => {this.route.navigate(['office']),
    // d["screen_permissions"][0].company_id
    localStorage.setItem("companyId", "6"),
    localStorage.setItem("userId", d["screen_permissions"][0].user_id),
    localStorage.setItem('operationsList', JSON.stringify(d[0].operations)),
    e => self.loginError.message = 'Please Enter correct employee id and password.'});
  }
}
