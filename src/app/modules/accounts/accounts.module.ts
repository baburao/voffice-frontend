import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {AccountRoutes} from './accounts-routing';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { EmployeeActiveComponent } from '../admin/employee-active/employeeactive.component';

@NgModule({
  declarations: [LoginComponent, ResetPasswordComponent,EmployeeActiveComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(AccountRoutes),
  ]
})

// @ts-ignore
export class AccountsModule { }
