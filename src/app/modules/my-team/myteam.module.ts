
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MyTeamRoutes} from './myteam-routes';
import { ListMyTeamComponent } from './list-myteam/list-myteam.component';
import { EditMyTeamComponent } from './edit-myteam/edit-myteam.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';

//import { ConversationModule } from 'src/app/modules/conversation/conversation.module';
import { DepartmentModule } from '../department/department.module';

@NgModule({

  declarations: [ListMyTeamComponent, EditMyTeamComponent,],


  imports: [
    CommonModule,
    MatTabsModule,
    MatIconModule,
    RouterModule.forChild(MyTeamRoutes),
    //ConversationModule,
    DepartmentModule

  ]
})
// @ts-ignore
export class MyTeamModule { }
