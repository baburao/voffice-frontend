import { Routes } from '@angular/router';
import { ListMyTeamComponent } from './list-myteam/list-myteam.component';
import { EditMyTeamComponent } from './edit-myteam/edit-myteam.component';
import { CreateMyTeamComponent } from './create-myteam/create-myteam.component';
import { UsersContactListComponent } from '../shared/users-contact-list/users-contact-list.component';

export const MyTeamRoutes: Routes = [
	
  {
    path: 'myteam',
    data: { breadcrumbs: 'MyTeam' },

    children: [
      { path: '',  component: ListMyTeamComponent, },
      {
        path: 'create',
        data: { breadcrumbs: 'Create' },
        component: CreateMyTeamComponent
      },
      {
        path: 'edit',
        data: { breadcrumbs: 'Edit' },
        component: EditMyTeamComponent
      }
    ]
  },
  // {
  //   path: 'team/edit',
  //   component: EditTeamComponent,
  // },
  // {
  //   path: 'team/create',
  //   component: CreateTeamComponent,
  //   data: { breadcrumbs: 'Create' },
  // },

  // {
  //   path: 'team/xx',
  //   component: UsersContactListComponent
  // },
];

