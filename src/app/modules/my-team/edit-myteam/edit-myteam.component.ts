import { Component, OnInit } from '@angular/core';

import { Team } from '../../../interfaces/team';
import { UserService } from '../../../services/global/user.service';
import { OfficeService } from '../../../services/global/office.service';

declare var $: any;

@Component({
  selector: 'app-edit-myteam',
  templateUrl: './edit-myteam.component.html',
  styleUrls: ['./edit-myteam.component.css']
})
export class EditMyTeamComponent implements OnInit {

  
  teams : {[key: number]: Team} = {};
  selectedTeam: Team | null = null;

  constructor(public userService: UserService, private officeService: OfficeService) { }

  ngOnInit(): void {
	
	this.officeService.teams.subscribe((teams: {[key: number]: Team}) => {
		
		this.teams = teams;		
		
		
		if(this.selectedTeam == null &&  Object.values(this.teams).length > 0) {
			this.selectedTeam = Object.values(this.teams)[0];
		} else {
			//this.selectedTeam = this.selectedTeam;
			
			if(this.selectedTeam != null) {
				this.selectedTeam = this.teams[this.selectedTeam.id];
			}
		}
		
		console.log(this.selectedTeam);
	});
  }
  createTeam() {
	
	var checkedUsers: any = $("input[name=selectUser]:checked");
	var members: number[] = [];
	
	for(var i = 0; i < checkedUsers.length; i++) {
		
		members.push($(checkedUsers[i]).attr('userId'));
	}
	
	this.officeService.createTeam($("#teamName").val(), members);
  }
	addUser(userId: number) {
		
		console.log("addUser", this.selectedTeam);
		if(this.selectedTeam == null) {
			return;
		}
		this.officeService.assignToTeam(this.selectedTeam!.id, userId);
	}
	removeUser(userId: number) {
		
		if(this.selectedTeam == null) {
			return;
		}
		this.officeService.removeFromTeam(this.selectedTeam!.id, userId);
	}
	selectTeam( teamId: string) {
		
		$("input[name=selectUser]").prop("checked", false);
		this.selectedTeam = this.teams[parseInt(teamId)];
		console.log(teamId, this.selectedTeam);	
	}
}
