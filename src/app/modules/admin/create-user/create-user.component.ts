import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/global/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  firstName: string ="";
  lastName: string ="";
  selectedDesignation: number = -1;
  
  //designations: any = null;
 
  constructor(public userService: UserService) { }

  ngOnInit(): void {
      if(this.userService.getDesignationArray().length > 0 ){
        this.selectedDesignation = this.userService.getDesignationArray()[0].id;
      }
  }
  createUser() : void {
    
    if(this.firstName.length == 0 || this.lastName.length == 0 || this.selectedDesignation == -1) {
      return;
    }
    this.userService.addEmployee(this.firstName, this.lastName, this.getMail(), this.selectedDesignation).subscribe(() => {
      this.firstName = "";
      this.lastName = "";
      this.selectedDesignation = this.userService.getDesignationArray()[0].id;
    });
  }
  getMail(): string {
    return this.firstName.toLowerCase() + '.' + this.lastName.toLowerCase() + '@creativelandasia.com';
  }
}
