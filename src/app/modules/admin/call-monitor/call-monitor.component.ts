import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 

import { UserService } from '../../../services/global/user.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-call-monitor',
  templateUrl: './call-monitor.component.html',
  styleUrls: ['./call-monitor.component.css']
})
export class CallMonitorComponent implements OnInit {

  options: any;
  packetLostChart: any;
  stats: any = {};
  selectedCall: any = null;
  selectedPeer: number = -1;
  autoUpdate: boolean = false;
  
  constructor(private http: HttpClient, public userService: UserService) {}

   data1: number[] = [1,1000];
    
  ngOnInit(): void {
    
    const xAxisData: string[] = [];
    //const data2: number[] = [];

    /*for (let i = 0; i < 100; i++) {
      xAxisData.push('category' + i);
      data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);
      data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);
    }*/

    this.options = {
      legend: {
        data: ['bar', 'bar2'],
        align: 'left',
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'bar',
          type: 'line',
          data: this.data1,
          animationDelay: (idx) => idx * 10,
        },
        /*{
          name: 'bar2',
          type: 'bar',
          data: data2,
          animationDelay: (idx) => idx * 10 + 100,
        },*/
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
    
    
    this.updateStats();
    
    setInterval(() => {
      if(this.autoUpdate) {
        this.updateStats();
    
      }
    },1000);
  }
  
  updateStats() {
    this.http.get(environment.SFU_SERVER + "stats/").subscribe((data: any) => {      
      this.stats = data;
      this.changeCall();
    });
  }
  
  changeCall() {
    
    if(this.selectedCall == null || this.selectedPeer == -1) {
      return;
    }
    
    var labels: Date[] = [];
    var values: any = {
      valuesIn: [],
      valuesOut: [],
      packetsLostIn: [],
      packetsLostOut: [],
      };
    
    var stats = this.stats[this.selectedCall].stats;
  
    for(let i in stats.data) {      
    
      
      if(!(this.selectedPeer in stats.data[i].stats)) {
        continue;
      }
      
      var peerStats = stats.data[i].stats[this.selectedPeer];
        
      var totalProduced: number = 0;
      var totalConsumed: number = 0;
      var totalPacketsLostIn: number = 0;
      var totalPacketsLostOut: number = 0;
      
      for(var p in peerStats.producer) {
        if(peerStats.producer[p].length == 0) {
          continue;
        }
        if(peerStats.producer[p][0].kind === 'audio') {
          totalProduced += peerStats.producer[p][0].bitrate;
          totalPacketsLostIn += peerStats.producer[p][0].packetsLost;
        }
      }
      for(var c in peerStats.consumer) {
        if(peerStats.consumer[c].length == 0) {
          continue;
        }
        if(peerStats.consumer[c][0].kind === 'audio') {
          totalConsumed += peerStats.consumer[c][0].bitrate;
          totalPacketsLostOut += peerStats.consumer[c][0].packetsLost;
        }
      }
      values.valuesIn.push(totalConsumed);
      values.valuesOut.push(totalProduced);
      values.packetsLostIn.push(totalPacketsLostIn);
      values.packetsLostOut.push(totalPacketsLostOut);
      
      labels.push(new Date(stats.data[i].start));
      
      /**
      bitrate: 2032
byteCount: 635
firCount: 0
fractionLost: 0
jitter: 51305724
kind: "audio"
mimeType: "audio/opus"
nackCount: 0
nackPacketCount: 0
packetCount: 15
packetsDiscarded: 0
packetsLost: 0
packetsRepaired: 0
packetsRetransmitted: 0
pliCount: 0
score: 10
ssrc: 1949064405
timestamp: 2026287704
type: "inbound-rtp"
       */
    }
    
    this.options = this.makeChart(labels, values.valuesIn, values.valuesOut);
    this.packetLostChart = this.makeChart(labels, values.packetsLostIn, values.packetsLostOut);
  }
  makeChart(labels, valuesIn, valuesOut) {
    
    var series: any[] = [];
    
    series.push({
      name: 'Out',
      type: 'line',
      data: valuesOut,
      animationDelay: (idx) => idx * 10,
    });
    series.push({
          name: 'In',
          type: 'line',
          data: valuesIn,
          animationDelay: (idx) => idx * 10,
        });
     return {
      legend: {
        data: ['bar', 'bar2'],
        align: 'left',
      },
      tooltip: {},
      xAxis: {
        data: labels,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      dataZoom: [{
        type: 'inside',
        start: 0,
        end: 100
        }, {
        start: 0,
        end: 100,
        handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
        handleSize: '80%',
        handleStyle: {
            color: '#fff',
            shadowBlur: 3,
            shadowColor: 'rgba(0, 0, 0, 0.6)',
            shadowOffsetX: 2,
            shadowOffsetY: 2
        }
      }],
      series:series,
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
  }
}
