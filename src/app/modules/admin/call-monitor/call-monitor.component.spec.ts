import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallMonitorComponent } from './call-monitor.component';

describe('CallMonitorComponent', () => {
  let component: CallMonitorComponent;
  let fixture: ComponentFixture<CallMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
