import { Routes } from '@angular/router';

import {AdminRootComponent} from './admin-root/admin-root.component';
import {BranchComponent} from './branch/branch.component';
import {CallMonitorComponent} from './call-monitor/call-monitor.component';
import { CompanyBranchesComponent } from './company-branches/company-branches.component';
import { CompanyDepartmentsComponent } from './company-departments/company-departments.component';
import { CompanyDesignationsComponent } from './company-designations/company-designations.component';
import { CompanyRolesComponent } from './company-roles/company-roles.component';
import { CompanySubcompaniesComponent } from './company-subcompanies/company-subcompanies.component';
// import { CompanyUsersComponent } from './company-users/company-users.component';
import {CompanyComponent} from './company/company.component';
import {CreateemployeeComponent} from './create-employee/create-employee.component';
import {CreateUserComponent} from './create-user/create-user.component';
import { DesignationComponent } from './designation/designation.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import { ScreenpermissionsComponent } from './screenpermissions/screenpermissions.component';
import { UserpermissionComponent } from './userpermission/userpermission.component';
export const AdminRoutes: Routes = [
  {
    path: 'admin',
    data: { breadcrumbs: 'Admin' },
    children: [
      { path: '',  component: BranchComponent, },
      { path: 'company',  component: CompanyComponent, },
      { path: 'createemployee',  component: CreateemployeeComponent, },
      { path: 'designation',  component: CompanyDesignationsComponent, },
      { path: 'department',  component: CompanyDepartmentsComponent, },
      { path: 'branch',  component: CompanyBranchesComponent, },
      { path: 'roles',  component: CompanyRolesComponent, },
      { path: 'subcompany',  component: CompanySubcompaniesComponent, },
      { path: 'rolepermissions',  component: ScreenpermissionsComponent, },
      {path: 'userpermissions', component: UserpermissionComponent},
      // {path: 'companyusers', component:CompanyUsersComponent}
    ]
  },
  {
    path: 'monitor',
    data: { breadcrumbs: 'Call Monitor' },
    children: [
      { path: '',  component: CallMonitorComponent, },
    ]
  },
  {
    path: 'resetpwd',
    data: { breadcrumbs: 'Reset' },
    children: [
      { path: '',  component: ResetPasswordComponent, },
    ]
  }
];

