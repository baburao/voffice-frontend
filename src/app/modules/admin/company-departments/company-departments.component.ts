import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { ToastrService } from 'ngx-toastr';

import { AdminService } from '../../../services/global/admin.service';
import { UserService } from '../../../services/global/user.service';
import { ValidationMessageService } from "../../../services/global/validationmessages.service";
@Component({
  selector: 'app-company-departments',
  templateUrl: './company-departments.component.html',
  styleUrls: ['./company-departments.component.css']
})
export class CompanyDepartmentsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;
  @ViewChild('focusitem') matinput: ElementRef
  newDepartmentName: string = "";
  newDepartmentDesc: string = "";
  showSearchPopup: boolean = true;
  savebtn: boolean = true;
  updbtn: boolean = true;
  tablehide: boolean = true;
  displayedColumns: string[] = ['name', 'description', 'active', 'action'];
  dataSource;
  departmentid;
  branch_id;
  room_id; 
  departmentsdata;
  ip_active: boolean = false;
  constructor(public adminService: AdminService, public userService: UserService, private validationmessages: ValidationMessageService,
    private toastr: ToastrService,) { }

  ngOnInit(): void {
    this.fungetDepartments()
  }
  update() {
    
  }
  funaddDepartment(): void {
    this.showSearchPopup = false
    this.savebtn = false
    this.updbtn = true
  }

  fungetDepartments(){
    var companyid = 6
    this.adminService.getDepartments(companyid).subscribe(resp => {
      console.log(resp);
      if(resp != null && resp != []){
      this.departmentsdata = resp
      this.dataSource = new MatTableDataSource(this.departmentsdata)
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort1
      this.tablehide = false
      }else{
        this.tablehide = false
        var emparr = []
        this.dataSource = emparr
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
      }
  });
  }

  closeSearchPopup(): void {
    this.showSearchPopup = true
    this.clearFields()
  }

  selectcheck(id){
    if(id.checked){
    this.ip_active = true
    }else{
    this.ip_active = false
    }
  }

  funsaveDepartment(): void{
    if(this.newDepartmentName=="" || this.newDepartmentName==null){
      this.toastr.warning(this.validationmessages.V_department_name);
      return;
    }
    if(this.newDepartmentDesc=="" || this.newDepartmentDesc==null){
      this.toastr.warning(this.validationmessages.V_department_desc);
      return;
    }
    var company_id = Number(localStorage.getItem("companyId"))
    var ip_department_id = 0
    var ip_branch_id = 0
    var ip_room_id = 0
    this.adminService.addDepartments(company_id, ip_department_id, this.newDepartmentName, ip_branch_id, ip_room_id, this.ip_active, this.newDepartmentDesc).subscribe(resp => {
      if(resp == 0){
        this.toastr.success(this.validationmessages.V_department_success);
        this.fungetDepartments()
        this.showSearchPopup = true
        this.clearFields()
      }  
      else{
        this.toastr.warning(this.validationmessages.V_msg_DuplicateDepRecord);
        this.matinput.nativeElement.focus()
      } 
  });
  }

  funEdit(element){
    this.showSearchPopup = false;
    this.savebtn = true
    this.updbtn = false
    for(let i = 0; i <= this.departmentsdata.length - 1; i++){
      if(this.departmentsdata[i].id === element.id){
      this.newDepartmentName = this.departmentsdata[i].name
      this.newDepartmentDesc = this.departmentsdata[i].description
      this.ip_active = this.departmentsdata[i].active
      break;
    }
    }
    this.departmentid = element.id
    this.branch_id = element.branch_id
    this.room_id = element.room_id
  }        

  funupdateDepartment(){
    if(this.newDepartmentName=="" || this.newDepartmentName==null){
      this.toastr.warning(this.validationmessages.V_department_name);
      return;
    }
    if(this.newDepartmentDesc=="" || this.newDepartmentDesc==null){
      this.toastr.warning(this.validationmessages.V_department_desc);
      return;
    }
    var company_id = Number(localStorage.getItem("companyId"))
    var ip_room_id = 0
    this.adminService.addDepartments(company_id,  this.departmentid, this.newDepartmentName, this.branch_id, ip_room_id , this.ip_active, this.newDepartmentDesc).subscribe(resp => {
      if(resp == 0){
        this.toastr.success(this.validationmessages.V_department_usuccess);
        this.fungetDepartments()
        this.showSearchPopup = true
        this.departmentid = 0
        this.branch_id = 0
        this.room_id = 0
        this.clearFields()
      }
      else{
        this.toastr.warning(this.validationmessages.V_msg_DuplicateDepRecord);
        this.matinput.nativeElement.focus()
      } 
  });
  }

  clearFields(){
    this.newDepartmentName = "";
    this.newDepartmentDesc = "";
    this.ip_active = false
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  funpreventSpace(event) {
    if (event.target.selectionStart == 0 && event.target.selectionEnd == 0) {
      event.preventDefault();
    }
  }

}

