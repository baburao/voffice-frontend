import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { AdminService } from 'src/app/services/global/admin.service';
import { UserService } from 'src/app/services/global/user.service';
import { ValidationMessageService } from 'src/app/services/global/validationmessages.service';
import {map, startWith} from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-userpermission',
  templateUrl: './userpermission.component.html',
  styleUrls: ['./userpermission.component.css']
})
export class UserpermissionComponent implements OnInit {
  @ViewChild(MatPaginator) paginator : MatPaginator;
  myControl = new FormControl();
  actempArrAY;
  userId = 0;
  usrpermoperationsData;
  uernoData = false;
  usrperDisplayedColumns : string[] = ['Screen Name', 'Operations'];
  filteredOptions;
  companyId = 6;
  // tslint:disable-next-line: max-line-length
  constructor(private adminService: AdminService, private userService: UserService, private toaster: ToastrService, private validationmessages: ValidationMessageService )  { }

  ngOnInit(): void {
    this.getactemployees();
  }
  // GETTING ACTIVE EMPLOYEES
  getactemployees(){
    try {
      this.adminService.getactEmployee(this.companyId).subscribe(res => {
        this.actempArrAY = res;
        this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );
      });
    } catch (error) {
      console.log(error);
    }
  }

  // GETTING EMPLOYEE PERMISSIONS BASED ON COMPANY ID
  getemppermission(){
    try {
      const userId = this.userId;
      this.adminService.getuserpermission(this.companyId, userId).subscribe(res => {
        this.usrpermoperationsData = res;
        this.usrpermoperationsData = new MatTableDataSource(this.usrpermoperationsData);
        this.usrpermoperationsData.paginator = this.paginator;
        this.uernoData = true;
      });
    } catch (error) {
      console.log(error);
    }
  }

  // CHANGE EVENT FOR EMPLOYEE SELECTION
  selectemp(event){
    const userName = event.option.value;
    const filterval = this.actempArrAY.filter(x => x.username === userName);
    if ( filterval.length !== 0){
      this.userId = filterval[0].id;
      this.getemppermission();
    }
  }

  // UPDATING OPERATIONS FOR AN EMPLOYEE BASED ON COMPANY ID
  saveusrpermissions() {
    try {
      if (this.userId !== 0 && this.usrpermoperationsData.length !== 0) {
        const tempusroerArray: any[] = [];
        this.usrpermoperationsData.forEach(element => {
          element.operations.forEach(inrEle => {
            if (inrEle.active === 1) {
              const SingleroleObj = {
                user_id: this.userId,
                screen_operation_id: inrEle.screen_operation_id,
                company_id: this.companyId,
                isactive: inrEle.active
              };
              tempusroerArray.push(SingleroleObj);
            }
          });
        });
        this.adminService.updateusrpermission(tempusroerArray, this.companyId).subscribe(res => {
          if(res === 1){
            this.toaster.success(this.validationmessages.V_user_permissions_update)
          }
        });
      } else {
        this.toaster.warning(this.validationmessages.V_role_Select);
      }
    } catch (error) {
      console.log(error);
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase(); 
    return this.actempArrAY.filter(x => x.username.toLowerCase().indexOf(filterValue) === 0);
  }
}
