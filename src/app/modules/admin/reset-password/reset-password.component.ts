import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/services/global/user.service';
import { User } from 'src/app/interfaces/user';
import {AdminService} from '../../../services/global/admin.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  newPassword: string = "test";
  inputAllowed: boolean = false;
  selectedUserId: number = -1;
  
  constructor(private adminService: AdminService, public userService:UserService) { }

  ngOnInit(): void {
  }

  selectUser(id: number) {
    this.selectedUserId = id;
  }
  
  resetPassword() {
    
    if(this.selectedUserId == -1 || !this.inputAllowed || this.newPassword.length < 4) {
      return;
    }
    console.log("reset")
    this.adminService.resetPassword(this.selectedUserId, this.newPassword).subscribe(() => {
      this.selectedUserId = -1;
      this.inputAllowed = false;
    console.log("reseted")
    });
  }
}
