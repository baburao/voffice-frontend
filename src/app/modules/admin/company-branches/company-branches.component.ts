import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { ToastrService } from 'ngx-toastr';

import { tr } from 'date-fns/locale';

import { AdminService } from '../../../services/global/admin.service';
import { UserService } from '../../../services/global/user.service';
import { ValidationMessageService } from "../../../services/global/validationmessages.service";

@Component({
  selector: 'app-company-branches',
  templateUrl: './company-branches.component.html',
  styleUrls: ['./company-branches.component.css']
})
export class CompanyBranchesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;
  @ViewChild('focusitem') matinput: ElementRef
  dataSource;
  branchesData;
  branchId;
  addBranch: boolean = true;
  tablehide: boolean = true;
  savebtn: boolean = true;
  updbtn: boolean = true;
  subcomp: boolean = true;
  pcompany: boolean = true;
  scompany: boolean = false;
  branchname;
  branchphnnumber;
  branchaddress;
  branchhead;
  businessname;
  businessmail;
  businessphnno;
  legalname;
  legalemail;
  legalphnno;
  corporatename;
  corporateemail;
  corporarephnno;
  careername;
  careeremail;
  careerphnno;
  compid;
  subcompid;
  subcompanyvalues;
  company_type = "C"
  ip_group_company_id = 0
  ip_active: boolean = false;
  ip_company_department;
  ip_sub_company_department;
  dropdownSettings = {}
  dropdownList: any[] = [];
  selectedItems: any[] = [];
  pdropdownSettings = {}
  pdropdownList: any[] = [];
  pselectedItems: any[] = [];
  displayedColumns: string[] = ['name', 'telnumber', 'branchhead', 'active', 'action'];
  constructor(public adminService: AdminService, public userService: UserService, private validationmessages: ValidationMessageService,
    private toastr: ToastrService,) { }

  ngOnInit(): void {
    this.fungetBranches()
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true,
      classes: "myclass custom-class",
    };
    this.pdropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true,
      classes: "myclass custom-class",
    };
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onItemDeSelect(item: any) {
    console.log(item);
  }
  onDeSelectAll(items: any) {
    this.selectedItems = items
  }
  ponItemSelect(item: any) {
    console.log(item);
  }
  ponSelectAll(items: any) {
    console.log(items);
  }
  ponItemDeSelect(item: any) {
    console.log(item);
  }
  ponDeSelectAll(items: any) {
    this.pselectedItems = items
  }

  fungetBranches() {
    var companyid = 6
    this.adminService.getBranches(companyid).subscribe(resp => {
      if (resp != null && resp != []) {
        this.branchesData = resp
        this.dataSource = new MatTableDataSource(this.branchesData)
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
        this.tablehide = false
        this.addBranch = true
      } else {
        this.tablehide = true
        this.addBranch = false
        var emparr = []
        this.dataSource = emparr
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
      }
    });
  }

  funarr;
  funtemparr: any[] = [];
  funaddBranch() {
    this.selectedItems = []
    this.clearFields()
    this.tablehide = true;
    this.addBranch = false;
    this.savebtn = false;
    this.updbtn = true;
    this.fungetDepartments()
    this.fungetsubcomp()
  }
  
  fungetsubcomp() {
    var companyid = 6
    this.adminService.getSubCompanies(companyid).subscribe(resp => {
      if (resp != null && resp != []) {
        this.subcompanyvalues = resp
      }
    });
  }

  fungetDepartments() {
    var companyid = 6
    this.adminService.getDepartments(companyid).subscribe(resp => {
      this.dropdownList = []
      if (resp != null && resp != []) {
        this.funarr = resp;
        for (let i = 0; i <= this.funarr.length - 1; i++) {
          this.funtemparr.push({
            id: resp[i].id,
            name: resp[i].name
          })
        }
      }
      this.dropdownList = this.funtemparr;
      this.pdropdownList = this.funtemparr
      console.log(this.dropdownList)
    })
  }


  funSaveBranch() {
    if (
      this.validationmessages.funValidateEmptyText(
        this.branchname,
        this.validationmessages.V_subcompany_name,
        "#bdname"
      )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.businessmail,
      this.validationmessages.V_Invalid_Email_Format,
      "#bdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.legalemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#ldemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.corporateemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.careeremail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cremail"
    )
    ) {
      return;
    }
    if (this.selectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.selectedItems, (s) => s.id);
      this.ip_company_department = ids;
    } else {
      this.ip_company_department = []
    }
    if (this.pselectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.pselectedItems, (s) => s.id);
      this.ip_sub_company_department = ids;
    } else {
      this.ip_sub_company_department = []
    }
    var company_id = Number(localStorage.getItem("companyId"))
    var ip_branch_id = 0
    // var ip_group_company_id = 0
    this.adminService.addCBranch(company_id, ip_branch_id, this.branchname, this.ip_group_company_id,
      this.branchaddress, this.branchphnnumber, this.businessname, this.businessmail, this.businessphnno,
      this.legalname, this.legalemail, this.legalphnno, this.corporatename, this.corporateemail,
      this.corporarephnno, this.careername, this.careeremail, this.careerphnno, this.branchhead,
      this.ip_active, this.ip_company_department, this.ip_sub_company_department, this.company_type).subscribe(resp => {
        if (resp == 0) {
          this.toastr.success(this.validationmessages.V_branch_success);
          this.fungetBranches()
          this.clearFields()
        } else if (resp != 0) {
          this.toastr.warning(this.validationmessages.V_msg_DuplicateRecordB);
          this.matinput.nativeElement.focus()
        }
      });
  }


  funEdit(id) {
    this.tablehide = true;
    this.addBranch = false;
    this.savebtn = true;
    this.updbtn = false;
    for (let i = 0; i <= this.branchesData.length - 1; i++) {
      if (this.branchesData[i].id == id.id) {
        this.branchname = this.branchesData[i].name
        this.branchaddress = this.branchesData[i].address
        this.branchphnnumber = this.branchesData[i].phone_number
        this.businessname = this.branchesData[i].business_contact_name
        this.businessmail = this.branchesData[i].business_contact_email
        this.businessphnno = this.branchesData[i].business_contact_phone_number
        this.legalname = this.branchesData[i].legal_contact_name
        this.legalemail = this.branchesData[i].legal_contact_email
        this.legalphnno = this.branchesData[i].legal_contact_phone_number
        this.corporatename = this.branchesData[i].corporate_contact_name
        this.corporateemail = this.branchesData[i].corporate_contact_email
        this.corporarephnno = this.branchesData[i].corporate_contact_phone_number
        this.careername = this.branchesData[i].career_contact_name
        this.careeremail = this.branchesData[i].career_contact_email
        this.careerphnno = this.branchesData[i].career_contact_phone_number
        this.branchhead = this.branchesData[i].branch_head
        this.dropdownList = this.branchesData[i].departments
        this.selectedItems = this.dropdownList.filter(x => x.count != 0)
        this.ip_group_company_id = this.branchesData[i].group_company_id
        this.pdropdownList = this.branchesData[i].group_company_department
        this.pselectedItems = this.pdropdownList.filter(x => x.count != 0)
        this.ip_active = this.branchesData[i].active
        // (this.dropdownList[i].group_company_id == null)? 0 : this.dropdownList[i].group_company_id
        break;
      }
    }
    this.branchId = id.id
  }

  funUpdBranch() {
    // if (this.funValidate()) {
    //   return;
    // }
    if (
      this.validationmessages.funValidateEmptyText(
        this.branchname,
        this.validationmessages.V_subcompany_name,
        "#bdname"
      )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.businessmail,
      this.validationmessages.V_Invalid_Email_Format,
      "#bdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.legalemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#ldemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.corporateemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.careeremail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cremail"
    )
    ) {
      return;
    }
    if (this.selectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.selectedItems, (s) => s.id);
      this.ip_company_department = ids;
    } else {
      this.ip_company_department = []
    }
    if (this.pselectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.pselectedItems, (s) => s.id);
      this.ip_sub_company_department = ids;
    } else {
      this.ip_sub_company_department = []
    }
    
    var company_id = Number(localStorage.getItem("companyId"))
    this.adminService.addCBranch(company_id, this.branchId, this.branchname, this.ip_group_company_id,
      this.branchaddress, this.branchphnnumber, this.businessname, this.businessmail, this.businessphnno,
      this.legalname, this.legalemail, this.legalphnno, this.corporatename, this.corporateemail,
      this.corporarephnno, this.careername, this.careeremail, this.careerphnno, this.branchhead,
      this.ip_active, this.ip_company_department,this.ip_sub_company_department, this.company_type).subscribe(resp => {
        if (resp == 0) {
          this.fungetBranches()
          this.toastr.success(this.validationmessages.V_branch_updSuccess);
        } else if (resp != 0) {
          this.toastr.warning(this.validationmessages.V_msg_DuplicateRecordB);
          this.matinput.nativeElement.focus()
        }
      });
  }


  selectcheck(id) {
    if (id.checked) {
      this.ip_active = true
    } else {
      this.ip_active = false
    }
  }
  
  selectcomp() {
      this.subcomp = true
      this.company_type = "c"
  }

  selectsubcomp() {
      this.subcomp = false
      this.company_type = "g"
  }

  selectcompany(id) {
    this.compid = id.id
  }

  selectsubcompany(id) {
    this.subcompid = id.id
    this.ip_group_company_id = this.subcompid
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  funAllowOnlyNumber(event) {
    var key;
    document.all ? (key = event.keyCode) : (key = event.which);
    return (
      (key >= 48 && key <= 57) ||
      key == 44 ||
      key == 43 ||
      key == 45 ||
      key == 32
    );
  }

  clearFields() {
    this.branchname = "",
      this.branchaddress = "",
      this.branchphnnumber = "",
      this.businessname = "",
      this.businessmail = "",
      this.businessphnno = "",
      this.legalname = "",
      this.legalemail = "",
      this.legalphnno = "",
      this.corporatename = "",
      this.corporateemail = "",
      this.corporarephnno = "",
      this.careername = "",
      this.careeremail = "",
      this.careerphnno = "",
      this.branchhead = "",
      this.ip_active = false,
      this.pselectedItems = []
      this.selectedItems = []
  }

  funpreventSpace(event) {
    if (event.target.selectionStart == 0 && event.target.selectionEnd == 0) {
      event.preventDefault();
    }
  }

}
