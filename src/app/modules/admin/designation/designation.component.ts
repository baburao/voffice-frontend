import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../services/global/user.service';

@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.css']
})
export class DesignationComponent implements OnInit {

  newDesignationName: string = "";
  newDesignationDesc: string = "";
  
  constructor(public userService: UserService) { }

  ngOnInit(): void {
    this.update();
  }
  update() {
    
  }
  addDesignation(): void {
    this.userService.addDesignation(this.newDesignationName , this.newDesignationDesc).subscribe(() => {
      this.update();
    });
  }
}
