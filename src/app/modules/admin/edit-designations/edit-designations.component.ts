import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../../services/global/user.service';

@Component({
  selector: 'app-edit-designations',
  templateUrl: './edit-designations.component.html',
  styleUrls: ['./edit-designations.component.css']
})
export class EditDesignationsComponent implements OnInit {

  //@Input() designations: any = null;
  selectedDesignation: number = -1;
  
  constructor(public userService: UserService) { }

  ngOnInit(): void {
  }

}
