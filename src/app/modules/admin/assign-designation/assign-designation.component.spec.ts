import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignDesignationComponent } from './assign-designation.component';

describe('AssignDesignationComponent', () => {
  let component: AssignDesignationComponent;
  let fixture: ComponentFixture<AssignDesignationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignDesignationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignDesignationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
