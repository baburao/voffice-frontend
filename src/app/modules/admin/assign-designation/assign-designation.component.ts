import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { UserService } from '../../../services/global/user.service';

@Component({
  selector: 'app-assign-designation',
  templateUrl: './assign-designation.component.html',
  styleUrls: ['./assign-designation.component.css']
})
export class AssignDesignationComponent implements OnInit {

  @Input() user: User | null = null;
  //@Input() designations: any = null;
  selectedDesignation: number = -1;
  
  constructor(public userService: UserService) { }

  ngOnInit(): void {
    this.selectedDesignation = this.user!.designation;
  }
  changeDesignation() {
    
    this.userService.assignDesignation(this.user!.id, this.selectedDesignation).subscribe(()=> {});
  }
}
