/*
Owner:Baburao M
Created Date:11/02/2020
Content:Create employee*/

import {
  Component,
  OnInit
} from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { MatTableDataSource } from '@angular/material/table';
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {FormControl} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../../../services/global/admin.service';
import { ValidationMessageService } from '../../../services/global/validationmessages.service';
import api_urls from '../../../constants/api_urls';
interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})

export class CreateemployeeComponent implements OnInit {
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;

  rolesList;
  designationList;
  employeeData;
  employeeData1;
  des;
  subcomresults;
  branchresults;
  departmentresults;
  subcompanyid;
  branchid;
  departmentid;
  comp_emp_type;
  haskids: boolean = true;
  childs: boolean = true;
  male:boolean = true;
  female:boolean = true;
  married:boolean = true;
  unmarried:boolean = true;
  son:boolean = true;
  daughter:boolean = true;
  empListTable:boolean = false;
  addEmpForm:boolean = true;
  addEmpFormBtn:boolean = false;
  empBackBtn:boolean = true;
  emphaskids:boolean = false;
  empmeritalstatus:boolean = false;
  empson:boolean = false;
  empdaugter:boolean = false;

  rolesArray: any = [];

  objemp = {
    empOfficeId: '',
    firstName: '',
    lastName: '',
    nickName: '',
    dateOfBirth: '',
    emailid: '',
    phoneno: '',
    emergencycontactname: '',
    emergencycontactno: '',
    gender: '',
    maritalstatus: '',
    haskids: '',
    son: '',
    daughter: '',
    empmarriagedate: '',
    subcompanyid:'',
    branchid:'',
    departmentid:'',
    joindate:''
  }
  projectDisplayedColumns: string[] = [
    "first_name",
    "email_id",
    "phoneno",
    "designation",
    "action"
  ];
  constructor(public adminService: AdminService, private toastr: ToastrService,
    private msgservice: ValidationMessageService,private http: HttpClient,) { 

    }

  ngOnInit() {
    this.haskids = false;
    this.childs = false;
    this.male = false;
    this.female = false;
    this.married = false;
    this.unmarried = false;
    this.son = false;
    this.daughter = false;
    this.emphaskids = false;
    this.empmeritalstatus = false;
    this.empson = false;
    this.empdaugter = false;
    this.getCompanyEmployees();

  }
  //GET EMPLOYEE LIST
  getCompanyEmployees() {
    try {
      var companyid = 6
      this.adminService.getcompanyemployees(companyid).subscribe(resp => {
        console.log(resp);
        this.employeeData1 = resp
        this.employeeData = new MatTableDataSource(this.employeeData1);
        this.employeeData.paginator = this.paginator;
       this.employeeData.sort = this.sort1;
      });
    } catch (error) {
      console.log(error)
    }
  }


  // GETTING COMPANYROLES BY COMPANY ID
  getcompanyroles() {
    try {
      var companyid = 6
      this.adminService.getRolesByCompanyid(companyid).subscribe(rolesresult => {
        this.rolesList = rolesresult;
      });
    } catch (error) {
      console.log(error)
    }
  }

  //SELECT ROLES
  selectRoles(rolesdata, event, index) {
    if (event.checked == true) {
      this.rolesArray.push({
        "user_id": 0,
        "role_id": rolesdata.id,
        "company_id": rolesdata.company_id,
        "deleted": false
      })
    } else {
      var arrayRoleIndex = this.rolesArray.findIndex(x => x.id === rolesdata.id);
      this.rolesArray.splice(arrayRoleIndex, 1);
    }
    console.log(this.rolesArray);
  }
  //GETTING COMPANY DESIGNATIONS BY COMPANY ID
  getDesignationsByCompanyId() {
    try {
      var companyid = 6
      this.adminService.getDesignations(companyid).subscribe(designationresult => {
        this.designationList = designationresult;
      });
    } catch (error) {
      console.log(error)
    }
  }

// ROLES DATA
getRolesData(){
  try {
    var companyid = 6
    this.adminService.get(api_urls.GetcompanyRolesbycmpyId+"?company_id="+6).subscribe(rolesresult => {
      this.rolesList = rolesresult;
      this.getDepartments()
    });
  } catch (error) {
    console.log(error)
  }
}

//DESIGNATIONS DATA
getDesignationData(){
  try {
    var companyid = 6
    this.adminService.get(api_urls.getDesignations+"?company_id="+6).subscribe(designationresult => {
      this.designationList = designationresult;
      this.getRolesData();
    });
  } catch (error) {
    console.log(error)
  }
}

//SUB COMPANYS
getSubCompanysData(){
  try {
    var companyid = 6
    this.adminService.get(api_urls.getSubCompanies+"?company_id="+6).subscribe(subcomresult => {
      this.subcomresults = subcomresult;
      this.getBranchsData();
    });
  } catch (error) {
    console.log(error)
  }
}

//BRANCHS
getBranchsData(){
  try {
    var companyid = 6
    this.adminService.get(api_urls.getBranches+"?company_id="+6).subscribe(branchresult => {
      this.branchresults = branchresult;
    });
  } catch (error) {
    console.log(error)
  }
}

//SHOW EMPLOYEE FORM
addEmployee(){
this.getDesignationData();
  this.empListTable = true;
  this.addEmpForm = false;
  this.empBackBtn = false;
  this.addEmpFormBtn = true;
  this.objemp.empOfficeId = "";
  this.objemp.firstName = "";
  this.objemp.lastName = "";
  this.objemp.dateOfBirth = "";
  this.objemp.emailid = "";
  this.objemp.phoneno = "";
  this.objemp.emergencycontactname = "";
  this.objemp.emergencycontactno = "";
  this.objemp.gender = "";
  this.objemp.maritalstatus = "";
  this.objemp.haskids = "";
  this.objemp.son = "";
  this.objemp.daughter = "";
  this.objemp.empmarriagedate = "";
  this.objemp.joindate = "";
}

//BACK TO EMPLOYEE LIST
backToEmpList(){
  this.empListTable = false;
  this.addEmpForm = true;
  this.empBackBtn = true;
  this.addEmpFormBtn = false;
  this.objemp.empOfficeId = "";
  this.objemp.firstName = "";
  this.objemp.lastName = "";
  this.objemp.dateOfBirth = "";
  this.objemp.emailid = "";
  this.objemp.phoneno = "";
  this.objemp.emergencycontactname = "";
  this.objemp.emergencycontactno = "";
  this.objemp.gender = "";
  this.objemp.maritalstatus = "";
  this.objemp.haskids = "";
  this.objemp.son = "";
  this.objemp.daughter = "";
  this.objemp.empmarriagedate = "";
  this.objemp.joindate = "";
}

applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.employeeData.filter = filterValue.trim().toLowerCase();

  if (this.employeeData.paginator) {
    this.employeeData.paginator.firstPage();
  }
}

getSubCompanys(event){
  if(event.checked==true){
  this.getSubCompanysData();    
}else{

}
}

//DEPARTMENTS
getDepartments(){
  try {
    var companyid = 6
    this.adminService.getDepartments(companyid).subscribe(designationresult => {
      this.departmentresults = designationresult;
    });
  } catch (error) {
    console.log(error)
  }
}

//SUB COMPANY DEPARTMENTS
getSubCompanyDepartments(subcompanyid){
  try {
    var combranchdata = {'company_id':6,'subcompanybranch_id':subcompanyid,'subcompanybranch_type':'g'};
    this.adminService.getSubCompanyDepartments(combranchdata).subscribe(designationresult => {
      this.departmentresults = designationresult;
    });
  } catch (error) {
    console.log(error)
  }
}

  ent_selectGender(event) {
    this.objemp.gender = event.value;
  }

//Select employee marital status
  ent_selectMaritalStatus(event) {
    if (event.value == "married") {
      this.empmeritalstatus = true;
      this.objemp.maritalstatus = "y"
    } else {
      this.empmeritalstatus = true;
      this.objemp.maritalstatus = "y"
    }
  }


 //employee select son or daughter
  ent_sonDaughter(event) {
    if (event.value == "son") {
      this.empson = true;
      this.empdaugter = false;
      this.emphaskids = true;
    } else {
      this.empson = false;
      this.empdaugter = true;
      this.emphaskids = true;
    }
  }




//CREATE EMPLOYEE
  ent_createEmployee(empData) {
    if (empData.firstName == "" || empData.firstName == null) {
      this.toastr.warning(this.msgservice.V_emp_first_name);
      return;
    }
    if (empData.lastName == "" || empData.lastName == null) {
      this.toastr.warning(this.msgservice.V_emp_last_name);
      return;
    }
    if (empData.dateOfBirth == "" || empData.dateOfBirth == null) {
      this.toastr.warning(this.msgservice.V_emp_dob);
      return;
    }
    if (this.objemp.gender == "" || this.objemp.gender == null) {
      this.toastr.warning(this.msgservice.V_emp_gender);
      return;
    }
    if (this.objemp.maritalstatus == "" || this.objemp.maritalstatus == null) {
      this.toastr.warning(this.msgservice.V_emp_marital_status);
      return;
    }
    if (this.objemp.maritalstatus == "married") {
      if (this.objemp.empmarriagedate == "" || this.objemp.empmarriagedate == null) {
        this.toastr.warning(this.msgservice.V_emp_marriagedate);
        return;
      }
    }
    if (empData.emailid == "" || empData.emailid == null) {
      this.toastr.warning(this.msgservice.V_emp_emailid);
      return;
    }
    var reEmail = /^([A-Za-z0-9_\.])+\@([A-Za-z0-9_\.])+\.([A-Za-z]{2,4})$/;
    if (!empData.emailid.match(reEmail)) {
      this.toastr.warning(this.msgservice.V_emp_valid_emailid);
      return;
    }
    if (empData.phoneno == "" || empData.phoneno == null) {
      this.toastr.warning(this.msgservice.V_emp_phone_number);
      return;
    }
    if (empData.emergencycontactname == "" || empData.emergencycontactname == null) {
      this.toastr.warning(this.msgservice.V_emp_emer_contact_name);
      return;
    }
    if (empData.emergencycontactno == "" || empData.emergencycontactno == null) {
      this.toastr.warning(this.msgservice.V_emp_emer_contact_number);
      return;
    }
    this.comp_emp_type = "c";
    if(this.objemp.subcompanyid==""){
      this.subcompanyid = null;
    }else{
      this.subcompanyid = this.objemp.subcompanyid;
      this.comp_emp_type = "g";
    }
    if(this.objemp.branchid==""){
      this.branchid = null;
    }else{
      this.branchid = this.objemp.branchid;
      this.comp_emp_type = "b";
    }
    if(this.objemp.departmentid==""){
      this.departmentid = null;
    }else{
      this.departmentid = this.objemp.departmentid;
    }

    var empobjdata = {
      empofficeid: 6, firstname: empData.firstName, lastname: empData.lastName,
      nickname: empData.firstName, passwords: "test",
      superuser: false, staff: false, datejoin: empData.joindate,subcompanyid:this.subcompanyid,
      branchid:this.branchid,departmentid:this.departmentid,
      emailid: empData.emailid, usernames: "user", usertype: "e", designationid: 1, dateofbirth: empData.dateOfBirth,
      gender1: this.objemp.gender, meritalstatus: this.empmeritalstatus, haskids: this.emphaskids, phonenumber: empData.phoneno,
      emercontactname: empData.emergencycontactname, emercontactnum: empData.emergencycontactno,
      daughters: this.empdaugter, sons: this.empson, marriagedate: empData.dateOfBirth, empuserid: 0,comemptype:this.comp_emp_type, rolesjson: this.rolesArray
    }
    this.adminService.createEmployee(empobjdata).subscribe((resp) => {
      this.toastr.success(this.msgservice.V_emp_createemployee);
    });
  }

  //DELETE EMPLOYEE FROM COMPANY
  delEmployee(employeeid) {
    var delObjEmployee = { employeeid: employeeid, status: false, companyid: 6 }
    this.adminService.delEmployee(delObjEmployee).subscribe((resp) => {
      this.getCompanyEmployees();
    });
  }

  //RESEND ACTIVE MAIL
  resendActiveMail(data){
    var objEmailData = {companyid:data.emp_office_id,name:data.first_name+" "+data.last_name,
    emailid:data.email,employeeid:data.user_id}
    this.adminService.resendEmailAddress(objEmailData).subscribe((resp) => {
      this.toastr.success(this.msgservice.V_emp_resendemail)
    });
  }

  //EDIT EMPLOYEE DATA BY ID
  editEmployeeData(employeeid){
    var delObjEmployee = { employeeid: employeeid, companyid: 6 }
    this.adminService.getEmployeeDataById(delObjEmployee).subscribe((resp) => {
      this.getcompanyroles();
      this.getDesignationsByCompanyId();
      console.log(resp);
      if(resp[0]['gender']=="m"){
       this.male = true;
       this.female = false;
      }else{
        this.male = false;
        this.female = true;
      }

      if(resp[0]['merital_status']==true){
        this.married = true;
        this.unmarried = false;
       }else{
        this.married = true;
        this.unmarried = false;
       }

     this.objemp.firstName = resp[0]['first_name'];
     this.objemp.lastName = resp[0]['last_name'];
     this.objemp.dateOfBirth = resp[0]['dob'];
     this.objemp.gender = resp[0]['gender'];
     this.objemp.maritalstatus = resp[0]['merital_status'];
     this.objemp.son = resp[0]['son'];
     this.son = resp[0]['son'];
     this.objemp.daughter = resp[0]['daughter'];
     this.daughter = resp[0]['daughter'];
     this.objemp.emailid = resp[0]['email'];
     this.objemp.phoneno = resp[0]['phone_number'];
     this.objemp.emergencycontactname = resp[0]['emergency_contact_name'];
     this.objemp.emergencycontactno = resp[0]['emergency_contact_number'];
     this.objemp.empmarriagedate = resp[0]['marriage_date'];
     this.empListTable = true;
     this.addEmpForm = false;
     this.empBackBtn = false;
     this.addEmpFormBtn = true;
    });
  }

  filterbyrole(event){

  }

  /*Number validations */
  funAllowOnlyNumber(event) {
    var key;
    document.all ? (key = event.keyCode) : (key = event.which);
    return key >= 48 && key <= 57;
  }
}