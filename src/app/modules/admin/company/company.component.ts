import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { ToastrService } from 'ngx-toastr';

import { truncateSync } from 'fs';

import { AdminService } from '../../../services/global/admin.service';
import { UserService } from '../../../services/global/user.service';
import { ValidationMessageService } from "../../../services/global/validationmessages.service";


@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;
  @ViewChild('focusitem') matinput: ElementRef
  dataSource;
  CompanyData;
  branchId;
  addSubCompanies: boolean = true;
  tablehide: boolean = true;
  savebtn: boolean = true;
  updbtn: boolean = true;
  companycheck: boolean = false;
  countcheck: boolean = true;
  branchcheck: boolean = false;
  countbranchcheck: boolean = true;
  groupcompanybranchdep: boolean = false;
  database: boolean = true;
  dbconncheck: boolean = true;
  dbconnection: string;
  updbtns: boolean = true;
  isdisabled: boolean = false;
  isdisabledname: boolean = false;
  companycount;
  branchcount;
  domain;
  businessname;
  businessmail;
  businessphnno;
  legalname;
  legalemail;
  legalphnno;
  corporatename;
  corporateemail;
  corporarephnno;
  careername;
  careeremail;
  careerphnno;
  companyname;
  companyphoneno;
  companyfacebook;
  companylinkedin;
  companytwitter;
  companyyoutube;
  companyinsta;
  companyaddress;
  ip_id;
  ip_company_department;
  ip_sub_company_department;
  displayedColumns: string[] = ['name', 'telnumber', 'action'];
  constructor(public adminService: AdminService, public userService: UserService, private validationmessages: ValidationMessageService,
    private toastr: ToastrService,) { }
  ngOnInit(): void {
    this.fungetCompanies()
  }

  fungetCompanies() {
    var companyid = 0
    this.adminService.getCompanies(companyid).subscribe(resp => {
      if (resp != null && resp != []) {
        this.CompanyData = resp
        this.dataSource = new MatTableDataSource(this.CompanyData)
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
        this.tablehide = false
        this.addSubCompanies = true
      } else {
        this.tablehide = true
        this.addSubCompanies = false
        var emparr = []
        this.dataSource = emparr
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
      }
    });
  }

  funaddCompany() {
    this.clearFields()
    this.tablehide = true;
    this.addSubCompanies = false;
    this.savebtn = false;
    this.updbtn = true;
    this.database = true;
    this.dbconncheck = true
  }


  funSaveCompany() {
    if (
      this.validationmessages.funValidateEmptyText(
        this.companyname,
        this.validationmessages.V_subcompany_name,
        "#fname"
      )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.businessmail,
      this.validationmessages.V_Invalid_Email_Format,
      "#bdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.legalemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#ldemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.corporateemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.careeremail,
      this.validationmessages.V_Invalid_Email_Format,
      "#customeremail"
    )
    ) {
      return;
    }
    var company_id = 0
    if (this.companycount == undefined) {
      this.companycount = null
    }
    if (this.branchcount == undefined) {
      this.branchcount = null
    }
    if (this.dbconnection == undefined) {
      this.dbconnection = ""
    }
    this.adminService.addCompany(company_id, this.companyname, this.database, this.dbconnection,
      this.domain, this.groupcompanybranchdep, this.companycheck, this.companycount,
      this.branchcheck, this.branchcount, this.companyaddress, this.companyphoneno, this.businessname,
      this.businessmail, this.businessphnno, this.legalname, this.legalemail, this.legalphnno,
      this.corporatename, this.corporateemail, this.corporarephnno, this.careername, this.careeremail,
      this.careerphnno, this.companyfacebook, this.companylinkedin,
      this.companyyoutube, this.companyinsta, this.companytwitter).subscribe(resp => {
        if (resp == 0) {
          this.toastr.success(this.validationmessages.V_sc_success);
          this.fungetCompanies()
          this.clearFields()
        }
        else {
          this.toastr.warning(this.validationmessages.V_msg_DuplicateRecordsc);
          this.matinput.nativeElement.focus()
        }
      });
  }


  funEdit(id) {
    this.tablehide = true;
    this.addSubCompanies = false;
    this.isdisabledname = false;
    this.savebtn = true;
    this.updbtn = false;
    this.updbtns = true;
    for (let i = 0; i <= this.CompanyData.length - 1; i++) {
      if (this.CompanyData[i].id == id.id) {
        this.ip_id = id.id
        this.companyname = this.CompanyData[i].name
        this.companyaddress = this.CompanyData[i].address
        this.companyphoneno = this.CompanyData[i].phone_number
        // this.companycount = this.CompanyData[i].group_company_count
        // this.branchcount = this.CompanyData[i].branch_count
        this.businessname = this.CompanyData[i].business_contact_name
        this.businessmail = this.CompanyData[i].business_contact_email
        this.businessphnno = this.CompanyData[i].business_contact_phone_number
        this.legalname = this.CompanyData[i].legal_contact_name
        this.legalemail = this.CompanyData[i].legal_contact_email
        this.legalphnno = this.CompanyData[i].legal_contact_phone_number
        this.corporatename = this.CompanyData[i].corporate_contact_name
        this.corporateemail = this.CompanyData[i].corporate_contact_email
        this.corporarephnno = this.CompanyData[i].corporate_contact_phone_number
        this.careername = this.CompanyData[i].career_contact_name
        this.careeremail = this.CompanyData[i].career_contact_email
        this.careerphnno = this.CompanyData[i].career_contact_phone_number
        this.companyfacebook = this.CompanyData[i].facebook
        this.companyinsta = this.CompanyData[i].instagram
        this.companylinkedin = this.CompanyData[i].linkedin
        this.companytwitter = this.CompanyData[i].twitter
        this.companyyoutube = this.CompanyData[i].youtube
        if (this.CompanyData[i].server_database == false) {
          this.database = false
          this.dbconncheck = false
          this.dbconnection = this.CompanyData[i].database_connection
        }
        if(this.CompanyData[i].has_group_companies != null){
        if (this.CompanyData[i].has_group_companies == false) {
          this.companycheck = false
          this.countcheck = true
          this.companycount = null
        } else {
          this.companycheck = true
          this.countcheck = false
          this.companycount = this.CompanyData[i].group_company_count
        }
      }{
        this.companycheck = false
        this.countcheck = true
      }
      if(this.CompanyData[i].has_branches != null){
        if (this.CompanyData[i].has_branches == false) {
          this.branchcheck = false
          this.countbranchcheck = true
          this.branchcount = null
        } else {
          this.branchcheck = true
          this.countbranchcheck = false
          this.branchcount = this.CompanyData[i].branch_count
        }
      }else{
        this.countbranchcheck = true
        this.branchcheck = false
      }
        break;
      }
    }
  }

  funeditCompany() {
    var Cid = localStorage.getItem("companyId")
    this.tablehide = true;
    this.addSubCompanies = false;
    this.savebtn = true;
    this.updbtn = true;
    this.updbtns = false;
    this.isdisabled = false;
    this.isdisabledname = false;
    for (let i = 0; i <= this.CompanyData.length - 1; i++) {
      if (this.CompanyData[i].id == Cid) {
        this.ip_id = Cid
        this.companyname = this.CompanyData[i].name
        this.companyaddress = this.CompanyData[i].address
        this.companyphoneno = this.CompanyData[i].phone_number
        this.businessname = this.CompanyData[i].business_contact_name
        this.businessmail = this.CompanyData[i].business_contact_email
        this.businessphnno = this.CompanyData[i].business_contact_phone_number
        this.legalname = this.CompanyData[i].legal_contact_name
        this.legalemail = this.CompanyData[i].legal_contact_email
        this.legalphnno = this.CompanyData[i].legal_contact_phone_number
        this.corporatename = this.CompanyData[i].corporate_contact_name
        this.corporateemail = this.CompanyData[i].corporate_contact_email
        this.corporarephnno = this.CompanyData[i].corporate_contact_phone_number
        this.careername = this.CompanyData[i].career_contact_name
        this.careeremail = this.CompanyData[i].career_contact_email
        this.careerphnno = this.CompanyData[i].career_contact_phone_number
        this.companyfacebook = this.CompanyData[i].facebook
        this.companyinsta = this.CompanyData[i].instagram
        this.companylinkedin = this.CompanyData[i].linkedin
        this.companytwitter = this.CompanyData[i].twitter
        this.companyyoutube = this.CompanyData[i].youtube
        if (this.CompanyData[i].server_database == false) {
          this.database = false
          this.dbconncheck = false
          this.dbconnection = this.CompanyData[i].database_connection
        }
        if(this.CompanyData[i].has_group_companies != null){
        if (this.CompanyData[i].has_group_companies == false) {
          this.companycheck = false
          this.countcheck = true
          this.companycount = null
        } else {
          this.companycheck = true
          this.countcheck = false
          this.companycount = this.CompanyData[i].group_company_count
        }
      }{
        this.companycheck = false
        this.countcheck = true
      }
      if(this.CompanyData[i].has_branches != null){
        if (this.CompanyData[i].has_branches == false) {
          this.branchcheck = false
          this.countbranchcheck = true
          this.branchcount = null
        } else {
          this.branchcheck = true
          this.countbranchcheck = false
          this.branchcount = this.CompanyData[i].branch_count
        }
      }else{
        this.countbranchcheck = true
        this.branchcheck = false
      }
        break;
      }
    }
  }

  funviewCompany() {
    this.funeditCompany()
    this.tablehide = true;
    this.addSubCompanies = false;
    this.savebtn = true;
    this.updbtn = true;
    this.updbtns = false;
    this.isdisabled = true;
    this.isdisabledname = true;
  }

  funUpdatecompany() {
    if (this.validationmessages.funValidateEmail(
      this.businessmail,
      this.validationmessages.V_Invalid_Email_Format,
      "#bdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.legalemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#ldemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.corporateemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.careeremail,
      this.validationmessages.V_Invalid_Email_Format,
      "#customeremail"
    )
    ) {
      return;
    }
    if (this.companycount == undefined) {
      this.companycount = null
    }
    if (this.branchcount == undefined) {
      this.branchcount = null
    }
    if (this.dbconnection == undefined) {
      this.dbconnection = ""
    }
    this.adminService.updCompany(this.ip_id, this.ip_id, this.companyname, this.database, this.dbconnection,
      this.domain, this.groupcompanybranchdep, this.companycheck, this.companycount,
      this.branchcheck, this.branchcount, this.companyaddress, this.companyphoneno, this.businessname,
      this.businessmail, this.businessphnno, this.legalname, this.legalemail, this.legalphnno,
      this.corporatename, this.corporateemail, this.corporarephnno, this.careername, this.careeremail,
      this.careerphnno, this.companyfacebook, this.companylinkedin,
      this.companyyoutube, this.companyinsta, this.companytwitter).subscribe(resp => {
        if (resp != undefined || resp != null || resp != []) {
          this.toastr.success(this.validationmessages.V_sc_updSuccess);
          this.fungetCompanies()
        }
      //   else {
      //     this.toastr.warning(this.validationmessages.V_msg_DuplicateRecordsc);
      //     this.matinput.nativeElement.focus()
      //   }
      });
  }


  funUpdcompany() {
    if (
      this.validationmessages.funValidateEmptyText(
        this.companyname,
        this.validationmessages.V_subcompany_name,
        "#fname"
      )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.businessmail,
      this.validationmessages.V_Invalid_Email_Format,
      "#bdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.legalemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#ldemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.corporateemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.careeremail,
      this.validationmessages.V_Invalid_Email_Format,
      "#customeremail"
    )
    ) {
      return;
    }
    if (this.companycount == undefined) {
      this.companycount = null
    }
    if (this.branchcount == undefined) {
      this.branchcount = null
    }
    if (this.dbconnection == undefined) {
      this.dbconnection = ""
    }
    // var company_id = 6
    this.adminService.addCompany(this.ip_id, this.companyname, this.database, this.dbconnection,
      this.domain, this.groupcompanybranchdep, this.companycheck, this.companycount,
      this.branchcheck, this.branchcount, this.companyaddress, this.companyphoneno, this.businessname,
      this.businessmail, this.businessphnno, this.legalname, this.legalemail, this.legalphnno,
      this.corporatename, this.corporateemail, this.corporarephnno, this.careername, this.careeremail,
      this.careerphnno, this.companyfacebook, this.companylinkedin,
      this.companyyoutube, this.companyinsta, this.companytwitter).subscribe(resp => {
        if (resp == 0) {
          this.toastr.success(this.validationmessages.V_sc_updSuccess);
          this.fungetCompanies()
        }
        else {
          this.toastr.warning(this.validationmessages.V_msg_DuplicateRecordsc);
          this.matinput.nativeElement.focus()
        }
      });
  }

  selectcheck(id) {
    if (id.checked) {
      this.countcheck = false
      this.companycheck = true
      this.companycount = null
    } else {
      this.countcheck = true
      this.companycheck = false
      this.companycount = null
    }
  }

  selectbranchcheck(id) {
    if (id.checked) {
      this.countbranchcheck = false
      this.branchcheck = true;
      this.branchcount = null
    } else {
      this.countbranchcheck = true
      this.branchcheck = true
      this.branchcount = null
    }
  }

  companybranchCheck(id) {
    if (id.checked) {
      this.groupcompanybranchdep = true
    } else {
      this.groupcompanybranchdep = false
    }
  }

  selectservercheck(id) {
    if (id.checked) {
      this.database = true
      this.dbconncheck = true
    } else {
      this.database = false
      this.dbconncheck = false
    }
  }

  funAllowOnlyNumber(event) {
    var key;
    document.all ? (key = event.keyCode) : (key = event.which);
    return (
      (key >= 48 && key <= 57) ||
      key == 44 ||
      key == 43 ||
      key == 45 ||
      key == 32
    );
  }

  clearFields() {
    this.companyname = "",
      this.companyaddress = "",
      this.companyphoneno = "",
      this.businessname = "",
      this.businessmail = "",
      this.businessphnno = "",
      this.legalname = "",
      this.legalemail = "",
      this.legalphnno = "",
      this.corporatename = "",
      this.corporateemail = "",
      this.corporarephnno = "",
      this.careername = "",
      this.careeremail = "",
      this.careerphnno = "",
      this.companyfacebook = "",
      this.companyinsta = "",
      this.companytwitter = "",
      this.companylinkedin = "",
      this.companyyoutube = ""
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  funpreventSpace(event) {
    if (event.target.selectionStart == 0 && event.target.selectionEnd == 0) {
      event.preventDefault();
    }
  }
}