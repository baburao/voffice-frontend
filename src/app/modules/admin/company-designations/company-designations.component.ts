import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { ToastrService } from 'ngx-toastr';

import { AdminService } from '../../../services/global/admin.service';
import { UserService } from '../../../services/global/user.service';
import { ValidationMessageService } from "../../../services/global/validationmessages.service";
@Component({
  selector: 'app-company-designations',
  templateUrl: './company-designations.component.html',
  styleUrls: ['./company-designations.component.css']
})
export class CompanyDesignationsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;
  @ViewChild('focusitem') matinput: ElementRef
  newDesignationName: string = "";
  newDesignationDesc: string = "";
  showSearchPopup: boolean = true;
  savebtn: boolean = true;
  updbtn: boolean = true;
  tablehide: boolean = true;
  displayedColumns: string[] = ['name', 'description', 'active', 'action'];
  dataSource;
  designationid;
  designationdata;
  ip_active: boolean = false;
  constructor(public adminService: AdminService, public userService: UserService, private validationmessages: ValidationMessageService,
    private toastr: ToastrService,) { }

  ngOnInit(): void {
    this.fungetDesignations()
  }
  update() {

  }
  funaddDesignation(): void {
    this.showSearchPopup = false
    this.savebtn = false
    this.updbtn = true
  }

  fungetDesignations() {
    var companyid = Number(localStorage.getItem("companyId"))
    this.adminService.getDesignations(companyid).subscribe(resp => {
      console.log(resp);
      if (resp != null && resp != []) {
        this.designationdata = resp
        this.dataSource = new MatTableDataSource(this.designationdata);
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
        this.tablehide = false
      } else {
        this.tablehide = false
        var emparr = []
        this.dataSource = emparr
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
      }
    });
  }

  closeSearchPopup(): void {
    this.showSearchPopup = true
    this.clearFields()
  }

  selectcheck(id) {
    if (id.checked) {
      this.ip_active = true
    } else {
      this.ip_active = false
    }
  }

  funsaveDesignation(): void {
    if (this.newDesignationName == "" || this.newDesignationName == null) {
      this.toastr.warning(this.validationmessages.V_designation_name);
      return;
    }
    if (this.newDesignationDesc == "" || this.newDesignationDesc == null) {
      this.toastr.warning(this.validationmessages.V_designation_desc);
      return;
    }
    var designationid = 0
    var company_id = Number(localStorage.getItem("companyId"))
    var ip_company_id = Number(localStorage.getItem("companyId"))
    this.adminService.addDesignation(company_id, designationid, this.newDesignationName, this.newDesignationDesc, ip_company_id, this.ip_active).subscribe(resp => {
      if (resp == 0) {
        this.toastr.success(this.validationmessages.V_designation_success);
        this.fungetDesignations()
        this.showSearchPopup = true
        this.clearFields()
      }
      else {
        this.toastr.warning(this.validationmessages.V_msg_DuplicateRecord);
        this.matinput.nativeElement.focus()
      }
    });
  }

  funEdit(id) {
    this.showSearchPopup = false;
    this.savebtn = true
    this.updbtn = false
    for (let i = 0; i <= this.designationdata.length - 1; i++) {
      if (this.designationdata[i].id === id) {
        this.newDesignationName = this.designationdata[i].name
        this.newDesignationDesc = this.designationdata[i].description
        this.ip_active = this.designationdata[i].active
        break;
      }
    }
    this.designationid = id
  }

  funupdateDesignation() {
    if (this.newDesignationName == "" || this.newDesignationName == null) {
      this.toastr.warning(this.validationmessages.V_designation_name);
      return;
    }
    if (this.newDesignationDesc == "" || this.newDesignationDesc == null) {
      this.toastr.warning(this.validationmessages.V_designation_desc);
      return;
    }
    var company_id = Number(localStorage.getItem("companyId"))
    var ip_company_id = Number(localStorage.getItem("companyId"))
    this.adminService.addDesignation(company_id, this.designationid, this.newDesignationName, this.newDesignationDesc, ip_company_id, this.ip_active).subscribe(resp => {
      if (resp == 0) {
        this.toastr.success(this.validationmessages.V_designation_usuccess);
        this.fungetDesignations()
        this.showSearchPopup = true
        this.designationid = 0
        this.clearFields()
      } else {
        this.toastr.warning(this.validationmessages.V_msg_DuplicateRecord);
        this.matinput.nativeElement.focus()
      }
    });
  }

  clearFields() {
    this.newDesignationName = "";
    this.newDesignationDesc = "";
    this.ip_active = false
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  funpreventSpace(event) {
    if (event.target.selectionStart == 0 && event.target.selectionEnd == 0) {
      event.preventDefault();
    }
  }
}

