import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {A11yModule} from '@angular/cdk/a11y';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import {OverlayModule} from '@angular/cdk/overlay';

import { NgxEchartsModule } from 'ngx-echarts';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown-angular7';

import { AdminRoutes } from './admin-routes';

import { AdminRootComponent } from './admin-root/admin-root.component';
import { AssignDesignationComponent } from './assign-designation/assign-designation.component';
import {BranchComponent} from './branch/branch.component';
import { CallMonitorComponent } from './call-monitor/call-monitor.component';
import { CompanyBranchesComponent } from './company-branches/company-branches.component';
import { CompanyDepartmentsComponent } from './company-departments/company-departments.component';
import { CompanyDesignationsComponent } from './company-designations/company-designations.component';
import { CompanyRolesComponent } from './company-roles/company-roles.component';
import { CompanySubcompaniesComponent } from './company-subcompanies/company-subcompanies.component';
// import { CompanyUsersComponent } from './company-users/company-users.component';
import {CompanyComponent} from './company/company.component';
import {CreateemployeeComponent} from './create-employee/create-employee.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { DesignationComponent } from './designation/designation.component';
import { EditDesignationsComponent } from './edit-designations/edit-designations.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ScreenpermissionsComponent } from './screenpermissions/screenpermissions.component';
import { UserpermissionComponent } from './userpermission/userpermission.component';
import { ViewBranchComponent } from './view-branch/view-branch.component';
import { ViewCompanyComponent } from './view-company/view-company.component';

@NgModule({
  declarations: [AdminRootComponent, ViewCompanyComponent, ViewBranchComponent,
     DesignationComponent, CreateUserComponent, AssignDesignationComponent,
      EditDesignationsComponent, CallMonitorComponent, ResetPasswordComponent,
      BranchComponent,CompanyComponent,CreateemployeeComponent, CompanyDesignationsComponent, CompanyRolesComponent, CompanyDepartmentsComponent, CompanyBranchesComponent,ScreenpermissionsComponent, CompanySubcompaniesComponent, UserpermissionComponent],
  imports: [
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    NgMultiSelectDropDownModule.forRoot(),
    RouterModule.forChild(AdminRoutes),
    MatTreeModule,
    OverlayModule,
    PortalModule,
    ScrollingModule,
    
    CommonModule,
    FormsModule,
    MatIconModule,
    ReactiveFormsModule,
    RouterModule.forChild(AdminRoutes),
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class AdminModule { }
