import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AdminService } from 'src/app/services/global/admin.service';
import { UserService } from 'src/app/services/global/user.service';
import { ValidationMessageService } from 'src/app/services/global/validationmessages.service';

@Component({
  selector: 'app-screenpermissions',
  templateUrl: './screenpermissions.component.html',
  styleUrls: ['./screenpermissions.component.css']
})
export class ScreenpermissionsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  roleArray;
  roleId = 0;
  finalScreenOperations;
  screennoData = false;
  scroperfinalResult;
  companyid = 6;
  myControl = new FormControl();
  filteredOptions;
  scrnoprnDisplayedColumns: string[] = ['Screen Name', 'Operations'];
  constructor(public userService: UserService, private validationmessages: ValidationMessageService
    ,         private toaster: ToastrService, public adminService: AdminService) { }

  ngOnInit(): void {
    this.getactroles();
  }

  // GETTING ALL ACTIVE ROLES
  getactroles() {
    try {
      this.scroperfinalResult = [];
      this.adminService.getactiverl(this.companyid).subscribe(res => {
        this.roleArray = res;
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      });
    } catch (error) {
      console.log(error);
    }

  }

  // DISPLAY PERMISSION BASED ON ROLE ID
  getpermissionbyrlid() {
    try {
      const roleId = this.roleId;
      this.adminService.getpermbyroleid(roleId, this.companyid).subscribe(res => {
        this.finalScreenOperations = res;
        this.finalScreenOperations = new MatTableDataSource(this.finalScreenOperations);
        this.finalScreenOperations.paginator = this.paginator;
        this.screennoData = true;
      });
    } catch (error) {
      console.log(error);
    }
  }


  saverolemap() {
    try {
      if (this.roleId !== 0 && this.finalScreenOperations.length !== 0) {
        const temproleoerArray: any[] = [];
        this.finalScreenOperations.forEach(element => {
          element.operations.forEach(inrEle => {
            if (inrEle.active === 1) {
              const SingleroleObj = {
                role_id: this.roleId,
                screen_operation_id: inrEle.screen_operation_id,
                company_id: this.companyid,
                isactive: inrEle.active
              };
              temproleoerArray.push(SingleroleObj);
            }
          });
        });
        this.adminService.updatescrper(temproleoerArray, this.companyid).subscribe(res => {
          this.toaster.success(this.validationmessages.V_role_permissions_update);
        });
      } else {
        this.toaster.warning(this.validationmessages.V_role_Select);
      }
    } catch (error) {
      console.log(error);
    }
  }

  // CHANGE EVENT FOR ROLE SELECTION
  selectrole(event){
    const rolename = event.option.value;
    const filterval = this.roleArray.filter(x => x.name === rolename);
    if ( filterval.length !== 0){
      this.roleId = filterval[0].id;
      this.getpermissionbyrlid();
    }
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.roleArray.filter(x => x.name.toLowerCase().indexOf(filterValue) === 0);
  }
}
