import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/global/user.service';
import api_urls from 'src/app/constants/api_urls';
import { ValidationMessageService } from 'src/app/services/global/validationmessages.service';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/global/admin.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-company-roles',
  templateUrl: './company-roles.component.html',
  styleUrls: ['./company-roles.component.css']
})
export class CompanyRolesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('myrolename') matinput: ElementRef;
  constructor(public userService: UserService, private validationmessages: ValidationMessageService
    , private toaster: ToastrService, public adminService: AdminService) { }
  displayedColumns: string[] = ['Role Name', 'Role Type', 'Active', 'Actions'];
  dataSource;
  rolesPoup = true;
  roleName = '';
  roleId;
  isUpdate = false;
  ischecked = true;
  isdisabled = true;
  companyId = 6;
  ngOnInit(): void {
    this.getcompanyroles();
  }

  // GETTING COMPANYROLES BY COMPANY ID
  getcompanyroles() {
    try {
      this.adminService.getcmproles(this.companyId).subscribe(res => {
      this.dataSource = res;
      this.dataSource = new MatTableDataSource(this.dataSource);
      this.dataSource.paginator = this.paginator;
      });
    } catch (error) {
      console.log(error);
    }
  }

  // FILTER VALUES BY ROLE NAME
  filterbyrole(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // OPENING POP FOR ADD / UPDATE ROLE
  openModal(type, ele) {
    if (type === 'add') {
      this.isUpdate = false;
      this.roleName = '';
      this.roleId = 0;
      this.ischecked = true;
      this.isdisabled = true;
    }
    else {
      this.isUpdate = true;
      this.roleName = ele.name;
      this.roleId = ele.id;
      this.ischecked = ele.active;
      this.isdisabled = false;
    }
    this.rolesPoup = false;
    setTimeout(() => {
      this.matinput.nativeElement.focus();
    }, 100);
  }

  closerolespoup() {
    this.rolesPoup = true;
    this.getcompanyroles();
  }

  // ADD OR UPDATE ROLE
  addorupdaterole(type) {
    if (this.roleName.trim() !== '') {
      try {
        if (type === 'add') {
          this.adminService.addupdateroles(this.roleName, this.roleId, true, this.companyId).subscribe(res => {
            if (res === 'Role Added') {
              this.toaster.warning(this.validationmessages.V_role_Added);
              this.roleName = '';
            } else if (res === 'Duplicate Record Found') {
              this.toaster.warning(this.validationmessages.V_role_Dup);
            }
          });
        } else if (type === 'update') {
          this.adminService.addupdateroles(this.roleName, this.roleId, this.ischecked, this.companyId).subscribe(res => {
            if (res === 'Role Updated') {
              this.toaster.warning(this.validationmessages.V_role_Upd);
            } else if (res === 'Duplicate Record Found') {
              this.toaster.warning(this.validationmessages.V_role_Dup);
            }
          });
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      this.toaster.warning(this.validationmessages.V_role_Name);
    }
  }

  // CHANGE EVENT FOR CHECK BOX CHECKED OR NOT
  isactiveevent(event) {
    const chk = event.checked;
    this.ischecked = chk;
  }
}
