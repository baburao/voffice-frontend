import { Component, OnInit, Input } from '@angular/core';
import {Department, Branch} from '../../../interfaces/company';
import { UserService } from 'src/app/services/global/user.service';
import { User } from 'src/app/interfaces/user';
import {AdminService} from '../../../services/global/admin.service';

@Component({
  selector: 'app-view-branch',
  templateUrl: './view-branch.component.html',
  styleUrls: ['./view-branch.component.css']
})
export class ViewBranchComponent implements OnInit {

  @Input() _branch : Branch | null = null;
  @Input() 
  set branch(b : Branch | null){ 
    this._branch = b;
    if(this.branch != null && this.selectedDepartment != null){
      this.selectedDepartment = this.branch!.departments.find((d: Department) => { return d.id == this.selectedDepartment!.id});
    }
    if(this.branch != null && this.branch.departments.length > 0 && this.selectedDepartment == null){
      this.selectedDepartment = this.branch.departments[0];
    } 
  };
  get branch() : Branch | null { return this._branch};
  
  selectedDepartment: Department | null | undefined= null;
  newDepartmentName: string = "";
  
  constructor(private adminService: AdminService, public userService:UserService) { }

  ngOnInit(): void {
    
  }

  filteredUserList() : User[] {
    //TODO slow?
    return Object.values(this.userService.users).filter((user: User) => { return !this.selectedDepartment!.users.includes(user.id)});     
  }
  
  assignUserToDepartment(userId: number, departmentId: number): void {
    this.adminService.assignUserToDepartment(userId, departmentId);
  }
  
  addDepartment(): void {
    if(this.newDepartmentName.length <= 0 || this.branch == null) {
      return;
    }
    this.adminService.addDepartment(this.branch.id, this.newDepartmentName).subscribe(() => {
      this.newDepartmentName = "";
    });
  }
}
