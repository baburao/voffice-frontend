import { Component, OnInit } from '@angular/core';

import {AdminService} from '../../../services/global/admin.service';
import {Company, Branch} from '../../../interfaces/company';

@Component({
  selector: 'app-view-company',
  templateUrl: './view-company.component.html',
  styleUrls: ['./view-company.component.css']
})
export class ViewCompanyComponent implements OnInit {

  company: Company | null = null;
  selectedBranch: Branch | null | undefined = null;
  
  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
    this.adminService.companyObservable.subscribe((company: Company | null) => {
      
      this.company = company; 
      if(this.company == null) {
        return;
      }
      if(this.selectedBranch == null && this.company.branches.length > 0) {
        this.selectedBranch = this.company.branches[0];
      } else if(this.selectedBranch != null) {
        this.selectedBranch = this.company.branches.find((b: Branch) => { return b.id == this.selectedBranch!.id});
      }
      console.log(this.company);
     });   
  }

}
