import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { ToastrService } from 'ngx-toastr';

import { AdminService } from '../../../services/global/admin.service';
import { UserService } from '../../../services/global/user.service';
import { ValidationMessageService } from "../../../services/global/validationmessages.service";

@Component({
  selector: 'app-company-subcompanies',
  templateUrl: './company-subcompanies.component.html',
  styleUrls: ['./company-subcompanies.component.css']
})
export class CompanySubcompaniesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;
  dataSource;
  subCompanyData;
  branchId;
  addSubCompanies: boolean = true;
  tablehide: boolean = true;
  savebtn: boolean = true;
  updbtn: boolean = true;
  businessname;
  businessmail;
  businessphnno;
  legalname;
  legalemail;
  legalphnno;
  corporatename;
  corporateemail;
  corporarephnno;
  careername;
  careeremail;
  careerphnno;
  ip_active;
  branchselect;
  companyname;
  companyphoneno;
  companyfacebook;
  companylinkedin;
  companytwitter;
  companyyoutube;
  companyinsta;
  companyaddress;
  ip_id;
  funarr;
  // subcompfunarr;
  ip_company_department;
  ip_sub_company_department;
  funtemparr: any[] = [];
  // subcompfuntemparr: any[] = [];
  dropdownSettings = {}
  dropdownList: any[] = [];
  selectedItems: any[] = [];
  subcompdropdownSettings = {}
  subcompdropdownList: any[] = [];
  subcompselectedItems: any[] = [];
  displayedColumns: string[] = ['name','companyname', 'telnumber', 'active', 'action'];
  constructor(public adminService: AdminService, public userService: UserService, private validationmessages: ValidationMessageService,
    private toastr: ToastrService,) { }
  ngOnInit(): void {
    this.fungetsubCompanies()

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true,
      classes: "myclass custom-class",
    };
    this.subcompdropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true,
      classes: "myclass custom-class",
    };
  }

  fungetsubCompanies() {
    var companyid = Number(localStorage.getItem("companyId"))
    this.adminService.getSubCompanies(companyid).subscribe(resp => {
      if (resp != null && resp != []) {
        this.subCompanyData = resp
        this.dataSource = new MatTableDataSource(this.subCompanyData)
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
        this.tablehide = false
        this.addSubCompanies = true
      } else {
        this.tablehide = true
        this.addSubCompanies = false
        var emparr = []
        this.dataSource = emparr
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort1
      }
    });
  }

  funaddsubCompany() {
    this.clearFields()
    this.tablehide = true;
    this.addSubCompanies = false;
    this.savebtn = false;
    this.updbtn = true;
    this.fungetDepartments()
  }

  fungetDepartments() {
    var companyid = Number(localStorage.getItem("companyId"))
    this.adminService.getDepartments(companyid).subscribe(resp => {
      this.dropdownList = []
      this.subcompdropdownList = []
      if (resp != null && resp != []) {
        this.funarr = resp;
        for (let i = 0; i <= this.funarr.length - 1; i++) {
          this.funtemparr.push({
            id: resp[i].id,
            name: resp[i].name
          })
        }
      }
      this.dropdownList = this.funtemparr;
      this.subcompdropdownList = this.funtemparr;
      console.log(this.dropdownList)
    })
  }

  // fungetsubcomp() {
  //   var companyid = 6
  //   this.adminService.getSubCompanies(companyid).subscribe(resp => {
  //     if (resp != null && resp != []) {
  //       this.subcompfunarr = resp;
  //       for (let i = 0; i <= this.subcompfunarr.length - 1; i++) {
  //         this.subcompfuntemparr.push({
  //           id: resp[i].id,
  //           name: resp[i].name
  //         })
  //       }
  //     }
  //     this.subcompdropdownList = this.subcompfuntemparr;
  //     console.log(this.subcompdropdownList)
  //   })
  // }


  funSaveSubCompany() {
    // if (this.funValidate()) {
    //   return;
    // }
    if (
      this.validationmessages.funValidateEmptyText(
        this.companyname,
        this.validationmessages.V_subcompany_name,
        "#fname"
      )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.businessmail,
      this.validationmessages.V_Invalid_Email_Format,
      "#bdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.legalemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#ldemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.corporateemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.careeremail,
      this.validationmessages.V_Invalid_Email_Format,
      "#customeremail"
    )
    ) {
      return;
    }
    if (this.selectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.selectedItems, (s) => s.id);
      this.ip_company_department = ids;
    } else {
      this.ip_company_department = []
    }
    if (this.subcompselectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.subcompselectedItems, (s) => s.id);
      this.ip_sub_company_department = ids;
    } else {
      this.ip_sub_company_department = []
    }
    var company_id = Number(localStorage.getItem("companyId"))
    var ip_group_company_id = 0
    this.adminService.addsubCompany(company_id, ip_group_company_id, this.companyname, this.companyaddress,
      this.companyphoneno, this.businessname, this.businessmail, this.businessphnno,
      this.legalname, this.legalemail, this.legalphnno, this.corporatename, this.corporateemail,
      this.corporarephnno, this.careername, this.careeremail, this.careerphnno, this.companyfacebook,
      this.companylinkedin, this.companytwitter, this.companyyoutube, this.companyinsta, this.ip_active,
      this.branchselect, this.ip_company_department, this.ip_sub_company_department).subscribe(resp => {
        if (resp == 0) {
          this.toastr.success(this.validationmessages.V_sc_success);
          this.fungetsubCompanies()
          this.clearFields()
        }
        else {
          this.toastr.warning(this.validationmessages.V_msg_DuplicateRecordsc);
          this.companyname = ""
        }
      });
  }


  funEdit(id) {
    this.tablehide = true;
    this.addSubCompanies = false;
    this.savebtn = true;
    this.updbtn = false;
    for (let i = 0; i <= this.subCompanyData.length - 1; i++) {
      if (this.subCompanyData[i].id == id.id) {
        this.companyname = this.subCompanyData[i].name
        this.companyaddress = this.subCompanyData[i].address
        this.companyphoneno = this.subCompanyData[i].phone_number
        this.businessname = this.subCompanyData[i].business_contact_name
        this.businessmail = this.subCompanyData[i].business_contact_email
        this.businessphnno = this.subCompanyData[i].business_contact_phone_number
        this.legalname = this.subCompanyData[i].legal_contact_name
        this.legalemail = this.subCompanyData[i].legal_contact_email
        this.legalphnno = this.subCompanyData[i].legal_contact_phone_number
        this.corporatename = this.subCompanyData[i].corporate_contact_name
        this.corporateemail = this.subCompanyData[i].corporate_contact_email
        this.corporarephnno = this.subCompanyData[i].corporate_contact_phone_number
        this.careername = this.subCompanyData[i].career_contact_name
        this.careeremail = this.subCompanyData[i].career_contact_email
        this.careerphnno = this.subCompanyData[i].career_contact_phone_number
        this.ip_active = this.subCompanyData[i].active
        this.companyfacebook = this.subCompanyData[i].facebook
        this.companyinsta = this.subCompanyData[i].instagram
        this.companylinkedin = this.subCompanyData[i].linkedin
        this.companytwitter = this.subCompanyData[i].twitter
        this.companyyoutube = this.subCompanyData[i].youtube
        this.branchselect = this.subCompanyData[i].view_branch_departments
        this.dropdownList = this.subCompanyData[i].group_company_department
        this.selectedItems = this.dropdownList.filter(x => x.count != 0)
        this.subcompdropdownList = this.subCompanyData[i].sub_company_departments;
        this.subcompselectedItems = this.subcompdropdownList.filter(x => x.count != 0)
        break;
      }
      this.ip_id = id.id
    }
  }

  funUpdSubcompany() {
    // if (this.funValidate()) {
    //   return;
    // }
    if (
      this.validationmessages.funValidateEmptyText(
        this.companyname,
        this.validationmessages.V_subcompany_name,
        "#fname"
      )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.businessmail,
      this.validationmessages.V_Invalid_Email_Format,
      "#bdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.legalemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#ldemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.corporateemail,
      this.validationmessages.V_Invalid_Email_Format,
      "#cdemail"
    )
    ) {
      return;
    }
    if (this.validationmessages.funValidateEmail(
      this.careeremail,
      this.validationmessages.V_Invalid_Email_Format,
      "#customeremail"
    )
    ) {
      return;
    }
    if (this.selectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.selectedItems, (s) => s.id);
      this.ip_company_department = ids;
    } else {
      this.ip_company_department = []
    }
    if (this.subcompselectedItems.length != 0) {
      var ids = Array.prototype.map.call(this.subcompselectedItems, (s) => s.id);
      this.ip_sub_company_department = ids;
    } else {
      this.ip_sub_company_department = []
    }
    var company_id = Number(localStorage.getItem("companyId"))
    this.adminService.addsubCompany(company_id, this.ip_id, this.companyname, this.companyaddress,
      this.companyphoneno, this.businessname, this.businessmail, this.businessphnno,
      this.legalname, this.legalemail, this.legalphnno, this.corporatename, this.corporateemail,
      this.corporarephnno, this.careername, this.careeremail, this.careerphnno, this.companyfacebook,
      this.companylinkedin, this.companytwitter, this.companyyoutube, this.companyinsta, this.ip_active,
      this.branchselect, this.ip_company_department, this.ip_sub_company_department).subscribe(resp => {
        if (resp == 0) {
          this.toastr.success(this.validationmessages.V_sc_updSuccess);
          this.fungetsubCompanies()
        }
        else {
          this.toastr.warning(this.validationmessages.V_msg_DuplicateRecordsc);
          this.companyname = ""
        }
      });
  }

  selectcheck(id) {
    if (id.checked) {
      this.ip_active = true
    } else {
      this.ip_active = false
    }
  }

  branchcheck(id) {
    if (id.checked) {
      this.branchselect = true
    } else {
      this.branchselect = false
    }
  }



  onItemSelect(item: any) {
    // this.selectedItems = item
  }
  onSelectAll(items: any) {
    // this.selectedItems = items;
  }
  onItemDeSelect(item: any) {
    // this.selectedItems = item
  }
  onDeSelectAll(items: any) {
    // this.selectedItems = items
    this.selectedItems = items
  }

  subcomponItemSelect(item: any) {
    // this.subcompselectedItems = item
  }
  subcomponSelectAll(items: any) {
    // this.subcompselectedItems = items
  }
  subcomponItemDeSelect(item: any) {
    // this.subcompselectedItems = item
  }
  subcomponDeSelectAll(items: any) {
    this.subcompselectedItems = items
  }

  funAllowOnlyNumber(event) {
    var key;
    document.all ? (key = event.keyCode) : (key = event.which);
    return (
      (key >= 48 && key <= 57) ||
      key == 44 ||
      key == 43 ||
      key == 45 ||
      key == 32
    );
  }

  clearFields() {
    this.companyname = "",
      this.companyaddress = "",
      this.companyphoneno = "",
      this.businessname = "",
      this.businessmail = "",
      this.businessphnno = "",
      this.legalname = "",
      this.legalemail = "",
      this.legalphnno = "",
      this.corporatename = "",
      this.corporateemail = "",
      this.corporarephnno = "",
      this.careername = "",
      this.careeremail = "",
      this.careerphnno = "",
      this.companyfacebook = "",
      this.companyinsta = "",
      this.companytwitter = "",
      this.companylinkedin = "",
      this.companyyoutube = ""
      this.ip_active = ""
      this.selectedItems = []
      this.subcompselectedItems = []
      this.branchselect = false
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  funpreventSpace(event) {
    if (event.target.selectionStart == 0 && event.target.selectionEnd == 0) {
      event.preventDefault();
    }
  }

}
