import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { UserService } from '../../../services/global/user.service';
import { AdminService } from '../../../services/global/admin.service';
import { ValidationMessageService } from '../../../services/global/validationmessages.service';
import { ToastrService } from 'ngx-toastr';
declare  var $:  any;

@Component({
  selector: 'app-employeeactive',
  templateUrl: './employeeactive.component.html',
  styleUrls: ['./employeeactive.component.css']
})
export class EmployeeActiveComponent implements OnInit {  
  companyid;
  employeeid;
  constructor(private activatedRoute: ActivatedRoute,public adminService: AdminService,
    private toastr: ToastrService,private msgservice: ValidationMessageService) { }

  ngOnInit(){
    this.companyid = this.activatedRoute.snapshot.paramMap.get("companyid");
    this.employeeid = this.activatedRoute.snapshot.paramMap.get("employeeid");
    this.employeeActivated(this.companyid,this.employeeid)
  }
  employeeActivated(companyid,employeeid){
    try {
      var activationdata = {companyid:employeeid,employeeid:companyid}
      this.adminService.activeEmployeeEmailId(activationdata).subscribe(rolesresult => {
        this.toastr.success(this.msgservice.V_emp_emailactivation);
      });
    } catch (error) {
      console.log(error)
    }
  }
}
