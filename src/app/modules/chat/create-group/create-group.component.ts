import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConversationService } from 'src/app/services/global/conversation.service';
import { Conversation } from 'src/app/interfaces/conversation';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { UtilsService } from 'src/app/services/global/utils.service';
import api_urls from 'src/app/constants/api_urls';
import { UserService } from 'src/app/services/global/user.service';
declare var $:any;
@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {
  @Input() users;
  isEditGroup = false;
  groupForm:FormGroup;

  groupUsers:number[] = [];
  errors: any;

  filters:any = [];
  //designations:any;

  editGroupUsers:number[] = [];
  editGroupTopic:string = '';

  smallProfileImage: string = api_urls.ProfileImageSmall;
  currentConversations: Conversation | null = null;
  constructor(private fb:FormBuilder,
    private conversationService: ConversationService, public chatUtil: ChatUtilsService,
    private utils: UtilsService, public userService:UserService) { 
    this.groupForm = this.fb.group({ title: '' });
  }

  ngOnInit(): void {  
    this.chatUtil.groupEditState.subscribe(status => {
      this.isEditGroup = status;
      this.conversationService.currentConversation.subscribe((newConversation: Conversation | null) => {
        this.currentConversations = newConversation;
        if(this.currentConversations && this.currentConversations?.users.length > 2) {
          this.initEditGroupValue();         
        }
      });
    })

    //this.getDesignations();
  }

  initEditGroupValue():void {
    console.log(this.currentConversations!.users)
    this.editGroupUsers = this.currentConversations!.users;
    this.editGroupTopic = this.currentConversations!.topic;
  }

  ngAfterViewChecked(): void {       
    // $('#create-group-surface-bg').slimScroll({
    //   height: '480px',
    //   width: '100%',
    //   color: '#fff'
    // });

    
    $('.users-list > ul').slimScroll({
      height: 'calc(100%)',
      width: '100%',
      color: '#fff'
    });

  }

  saveGroup() {
    if (this.isValid()) {
      // create group API
      this.conversationService.createGroupConversation(this.groupForm.value.title, this.groupUsers).subscribe((conversation: Conversation) => {
        this.conversationService.selectConversation(conversation.id);
        this.toggleGroup();
        this.errors = [];
      });
    }
  }

    // Group Form

    isValid(): boolean {
      this.errors = {};
      if (this.groupForm.value.title.length === 0) {
        this.errors['title'] = 'Title cannot be empty.'
      }
      // @ts-ignore
      if (this.groupUsers.length < 2) {
        this.errors['assigned_users'] = 'Please select two or more contacts.'
      }
      return Object.keys(this.errors).length === 0;
    }

    groupHasUser(id: number) {
      // @ts-ignore
      return this.groupUsers.indexOf(id) > -1;
    }
  
    toggleUser(userId: number): void {
      this.modifyUserList(userId, !this.groupHasUser(userId));
    }
  
    modifyUserList(userId: number, addUser: boolean): void {
      if (addUser) {
        // @ts-ignore
        this.groupUsers.push(userId);
        this.isValid();
      } else {
        // @ts-ignore
        const index = this.groupUsers.indexOf(userId, 0);
        if (index > -1) {
          this.groupUsers.splice(index, 1);
          this.isValid();
        }
  
      }
  
    }

    toggleGroup() {
      this.chatUtil.toggleGroup();
      this.errors = [];
    }

    search(event: any) {
      const value = event.target.value.toLowerCase();
      const len = event.target.value.length;
      if (len > 0) {
  
        // @ts-ignore
        let users = this.users;
        for (let k of Object.keys(users)) {
          // @ts-ignore
          const fullname = this.fullname(this.users[k], false)
          const K = parseInt(k);
  
          if (fullname.toLowerCase().slice(0, len) === value && this.filtered(K)) {
            // @ts-ignore
            this.filters = this.filters.filter(i => i !== K);
          }
  
          if (fullname.toLowerCase().slice(0, len) !== value) {
            this.filters.push(K)
          }
        }
      } else {
        this.filters = [];
      }
  
    }
    fullname(usr: any, type=true){
      return this.utils.fullname(usr, type)
     }

     filtered(id: number) {
      // @ts-ignore
      return this.filters.includes(id)
    }

    /*getDesignations() {
      this.userService.getDesignations().subscribe(res => {
        this.designations = res;
      })
    }

    designation(designations, id) {
      return this.utils.designationName(designations, id);    
    }*/

    updateGroup() {
      if (!this.isEditFormValid()) return;
      console.log('update group');
      this.toggleGroup();
      // update group API
      // this.editGroupTopic; // topic name
      // this.editGroupUsers; // users
      // this.currentConversations?.topic = this.editGroupTopic;
      // this.currentConversations?.users = this.editGroupUsers;
      
    }

    editGroupHasUser(id: number) {
      // @ts-ignore
      return this.editGroupUsers.indexOf(id) > -1;
    }

    toggleEditUser(userId: number): void {
      console.log('hehehe')
      this.modifyEditUserList(userId, !this.editGroupHasUser(userId));
    }

    modifyEditUserList(userId: number, addUser: boolean): void {
      console.log(addUser)
      if (addUser) {
        // @ts-ignore
        this.editGroupUsers.push(userId);
        this.isEditFormValid();
      } else {
        // @ts-ignore
        const index = this.editGroupUsers.indexOf(userId, 0);
        if (index > -1) {
          this.editGroupUsers.splice(index, 1);
          this.isEditFormValid();
        }
  
      }
  
    }

    isEditFormValid(): boolean {
      this.errors = {};
      if (!this.editGroupTopic) {
        this.errors['title'] = 'Title cannot be empty.'
      }
      // @ts-ignore
      if (this.editGroupUsers.length < 3) {
        this.errors['assigned_users'] = 'Please select two or more contacts.'
      }
      return Object.keys(this.errors).length === 0;
    }

}
