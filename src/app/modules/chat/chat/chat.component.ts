import { Component, OnInit } from '@angular/core';
import { Conversation } from 'src/app/interfaces/conversation';
import { UtilsService } from 'src/app/services/global/utils.service';
import { ConversationService } from 'src/app/services/global/conversation.service';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { UserService } from 'src/app/services/global/user.service';
import { User } from 'src/app/interfaces/user';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { Conference } from 'src/app/interfaces/conference';
import { Call2 } from 'src/app/call/call2';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  chatBarStatus: string = 'closed';

  conferenceConversation: Conversation | null = null;

  conversations: Conversation[] = [];  
  removedUsers: number[] = [];  

  users: { [key: number]: User } = {};
  currentConversations: Conversation | null = null;

  currentConference: Conference | null = null;
  currentCall: Call2 | null = null;
  constructor(private utils: UtilsService, public conversationService: ConversationService,
     public chatUtil:ChatUtilsService,
     private userService:UserService, private confService:ConfService) { }

  ngOnInit(): void {
    this.utils.confConvo.subscribe(convo => this.conferenceConversation = convo);
    this.conversationService.conversations.subscribe((conversations: Conversation[]) => {
      this.conversations = conversations
    });

    this.chatUtil.chatWindowState.subscribe((chatWindowState)=> {      
      if(chatWindowState) {
        this.chatBarStatus = chatWindowState;
        if(this.chatBarStatus == 'all'){
          this.chatUtil.toggleContacts();
        }
      }      
    })

    this.conversationService.removedUsersObserver.subscribe((removedUsers: number[]) => {      
      this.removedUsers = removedUsers;
    });

    this.conversationService.currentConversation.subscribe((newConversation: Conversation | null) => {
      this.currentConversations = newConversation;      
    });

    this.getUsers();

    this.confService.activeCallObservable.subscribe(( newCall: Call2| null) => {
      
      this.currentCall = newCall;
      this.currentConference = newCall != null ? newCall.conference : null;
    });
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => {
      this.users = users;
    });
  }
  
  toggleChatBar(status:string) {
    this.conversationService.selectConversation(-1);
    if(this.currentConference !=null){
      this.chatUtil.changeChatWindowStatus('all');
      return;
    }
    // check for quick chat if null set chatbar status all.
    if (!this.conversations.length || this.chatUtil.getLatestMessageCount(this.conversations) == 0
    || this.removedUsers.length == this.conversations.length) {
      this.chatUtil.changeChatWindowStatus('all');
      return;
    }
    this.chatUtil.changeChatWindowStatus(status);
  }

  toggleConferenceChatBar(status) {
  
    if(this.currentConference != null){
      this.conversationService.setCurrentConversation(this.currentConference.conversation);
      this.chatUtil.toggleChatDrawer(this.currentConference.conversation!.id, false, true);
      return;
    }
  }

  showConferenceChatIcon() {
    return this.currentConference !=null ? true : false;
  }

}
