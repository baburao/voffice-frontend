import { Component, OnInit, Input } from '@angular/core';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { ConversationService } from 'src/app/services/global/conversation.service';
import { Conversation } from 'src/app/interfaces/conversation';
import { User } from 'src/app/interfaces/user';
import api_urls from 'src/app/constants/api_urls';
import { DirectCall } from 'src/app/call/direct-call';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { UserService } from 'src/app/services/global/user.service';
import { NotificationService } from 'src/app/services/global/notification.service';

declare var $:any;
@Component({
  selector: 'app-all-contacts',
  templateUrl: './all-contacts.component.html',
  styleUrls: ['./all-contacts.component.css']
})
export class AllContactsComponent implements OnInit {
  
  @Input()  users: { [key: number]: User } = {};
  userSearchStr: string = '';
  usersSearchResult: any[] = [];
  searchedGroups: any[] = [];


  conversations: Conversation[] = [];
  directCall: DirectCall | null = null;

  smallProfileImage: string = api_urls.ProfileImageSmall;
  contactClass = 'contactsbox-toggle-open';

  constructor(public chatUtil:ChatUtilsService, public conversationService: ConversationService,
    private conferenceService:ConfService,
    private userService: UserService, private ns:NotificationService ) { }

  ngOnInit(): void {
    this.conversationService.conversations.subscribe((conversations: Conversation[]) => {
      this.conversations = conversations
    });

    this.conferenceService.directCallObservable.subscribe((newCall: DirectCall) => {
      this.directCall = newCall;
      if (this.directCall !== null && !this.chatUtil.hasClass(this.contactClass)) {
        this.ns.playNotificationAudio();
        this.toggleContacts()
      }
    });
  }

  ngAfterViewInit(): void {
    $('#all-contactsbox-main #all-contactsbox-surface-bg').slimScroll({
      height: '28vw',
      width: '100%',
      color: '#fff'
    });  
  }

  latest(convo: any) {
    if (convo.latest) {
      if(convo.latest.text.length > 30)
        return convo.latest.text.substring(0,70)+'...';
      else 
        return convo.latest.text;
    } else {
      return 'No messages.';
    }
  }

  toggleContacts() {
    this.chatUtil.toggleContacts();
  }

  toggleGroup() {    
    this.chatUtil.toggleGroup();
  }

  searchContact(searchStr) {
    this.userSearchStr = searchStr;
    let userArr: object[] = [];
    let conversations = this.conversations;
    if (searchStr.length == 0) {
      this.usersSearchResult = [];
      this.searchedGroups = [];
    }

    if (searchStr.length > 0) {
      let usersObj = this.users;
      if (Object.keys(usersObj).length) {
        Object.keys(usersObj).forEach(function(key) {
          usersObj[key].fullname = usersObj[key].firstname + " " + usersObj[key].lastname;
          userArr.push(usersObj[key]);
        });
        this.usersSearchResult = userArr.filter(o =>
          Object.keys(o).some(k => o['fullname'].toLowerCase().includes(searchStr.toLowerCase())));
      }

      if (conversations.length) {
        this.searchedGroups = conversations.filter(o => {
          if (o.users.length > 2) {
            return o['topic'].toLowerCase().includes(searchStr.toLowerCase())
          }
          return;
        });
      }
    }
  }

  startNewConversation(targetUserId: number) {
    this.searchContact('');
    if (this.isNewChat(targetUserId)) {
      this.conversationService.createConversation(targetUserId).subscribe((conversation: Conversation) => {
        this.chatUtil.selectConversation(conversation.id);
        this.chatUtil.toggleChatDrawer(conversation.id);
      });
    }
  }

  isNewChat(targetUserId: number) {
    for (let i = 0; i < this.conversations.length; i++) {
      if (this.conversations[i].users.length == 2 && this.conversations[i].users.includes(targetUserId) && this.conversations[i].users.includes(this.userService.currentUser!.id)) {
        this.chatUtil.toggleChatDrawer(this.conversations[i].id);
        return false;
      }
    }
    return true;
  }

  toggleChatDrawer(id:number) {
    this.chatUtil.toggleChatDrawer(id);
  }

  onRightClick(event) {
    console.log(event);
    return false;
  }
}
