import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import api_urls from 'src/app/constants/api_urls';
import { ConversationService } from 'src/app/services/global/conversation.service';
import { Conversation } from 'src/app/interfaces/conversation';
import { User } from 'src/app/interfaces/user';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { Message } from 'src/app/interfaces/message';
import { UserService } from 'src/app/services/global/user.service';
import { AudioRecorder } from 'src/app/utils/audio-recorder';
import { environment } from 'src/environments/environment';
import { UtilsService } from 'src/app/services/global/utils.service';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { NotificationService } from 'src/app/services/global/notification.service';
import { DirectCall } from 'src/app/call/direct-call';

declare var $: any;

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.css']
})
export class ChatWindowComponent implements OnInit {
  @Input() users: { [key: number]: User } = {};

  public audioRecorder: AudioRecorder | null = null;
  fileRoot = environment.FileRoot;

  smallProfileImage: string = api_urls.ProfileImageSmall;
  audioUrl: string = api_urls.GetAudioMessage;

  currentConversations: Conversation | null = null;
  conferenceConversation: Conversation | null = null;
  directCall: DirectCall| null = null;

  chatDrawerClass = 'chatbox-right-toggle-open';

  shouldScroollDown = false;
  showDeclineWindow = false;

  isRecording = false;
  
  constructor(public conversationService:ConversationService,
    public chatUtil: ChatUtilsService, public userService:UserService,
    private utils:UtilsService, private conferenceService:ConfService,
    private notificationService:NotificationService ) { }

  ngOnInit(): void {
    this.utils.confConvo.subscribe(convo => this.conferenceConversation = convo);
    this.conversationService.currentConversation.subscribe((newConversation: Conversation | null) => {
      this.currentConversations = newConversation;
      console.log(this.currentConversations)
    });

    this.conferenceService.directCallObservable.subscribe((newCall: DirectCall) => {
      this.directCall = newCall;
      if(this.directCall ==null) {
        this.showDeclineWindow = false;
        console.log(this.showDeclineWindow)
      }
    });

    this.conversationService.onNewMessage.subscribe(() => { this.shouldScroollDown = true; });
    
  }

  ngAfterViewInit(): void {   
    $('.chat-msgbox-popup #chat-msgbox-surface-bg').slimScroll({
      height: '28vw',
      width: '100%',
      color: '#fff',
      start: 'bottom',
    });
  }

  ngAfterViewChecked(): void {

    if (this.shouldScroollDown) {
      this.scrollDown();
      this.shouldScroollDown = false;
    }

    if(this.directCall != null && this.directCall.state == 'declined' && !this.showDeclineWindow) {
      this.showDeclineWindow = true;
      this.chatUtil.toggleChatDrawer(this.currentConversations!.id)
    }
  }

  get hasConvoConversation() {
    return !!this.conferenceConversation;
  }

  scrollDown() {
    var containers = $(".scrollDownContainer");
    for (var i = 0; i < containers.length; i++) {
      $(containers[i]).scrollTop(containers[i].scrollHeight);
    }
  }

  callDuration(message: Message): Date {    
    return new Date(new Date(message.call.end_date).getTime() - new Date(message.date).getTime());
  }

  isSelf(userId: number) {
    return this.userService.currentUser?.id == userId;
  }

  dateSection(index: number) {
    const messages = this.currentConversations?.messages;
    if (messages != null) {
      try {
        const current = this.chatUtil.formatDate(new Date(messages[index]['date']));
        const next = this.chatUtil.formatDate(new Date(messages[index + 1]['date']));
        return current !== next;
      } catch (e) {
        return true
      }
    }
    return false
  }

  sendMessage(text: string): void {
    this.conversationService.sendMessage(text);
    // this.scrollDown();
  }

  uploadFile(event: any) {
    const file = event.target.files.item(0);
    this.conversationService.sendFileMessage(file);
  }

  toggleRecording(): void {
    if (this.audioRecorder == null) {
      this.startRecording();
      this.isRecording = true;
    } else {
      this.stopRecording();
      this.isRecording = false;
    }
  }
  startRecording(): void {

    let self = this;

    this.audioRecorder = new AudioRecorder();
    this.audioRecorder.record(function() { self.audioRecorder = null; });
  }

  stopRecording() {
    if (this.audioRecorder == null) {
      return;
    }
    let self = this;

    this.audioRecorder.stop((buffer: any) => {

      self.conversationService.sendAudioMessage(buffer);
    });

    this.audioRecorder = null;
  }

  closechatWindow(forced = false) {
    if (forced) {
      this.chatUtil.removeClass(this.chatDrawerClass);
    }
    if (this.currentConversations == null || !this.currentConversations.is_conference) {
      this.chatUtil.selectConversation(-1);
    }
    this.chatUtil.seteditGroup(false);
  }

  currentConversionFilteredUsers(): number[] {

    if (this.currentConversations == null) {
      return [];
    }
    return this.currentConversations!.users.filter((targetId: number) => !this.isSelf(targetId));
  }

  callUser() {
      console.log('calling user');   
      if(this.currentConversations == null) {
        return;
      }
      console.log(this.currentConversations);
      
      this.chatUtil.toggleChatDrawer(this.currentConversations.id);
      this.conferenceService.callUser(this.currentConversations.id, false);
      this.notificationService.stopNotificationAudio();
  }
  
  showDeclinedWindow(): boolean {
     return this.directCall != null ? this.directCall.state == 'declined' : false;
  }
  localTime(message: Message) {
    return new Date(message.date).toString();
  }
  urlify(text) {
    var urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
    return text.replace(urlRegex, function(url,b,c) {
        var url2 = (c == 'www.') ?  'http://' +url : url;
        return '<a href="' +url2+ '" target="_blank">' + url + '</a>';
    }) 
  }

  onEditGroupClick() {
    this.chatUtil.toggleGroup();
    this.chatUtil.seteditGroup(true);
  }

  
}
