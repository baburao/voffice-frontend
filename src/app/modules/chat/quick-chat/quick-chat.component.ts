import { Component, OnInit, Input } from '@angular/core';
import { Conversation } from 'src/app/interfaces/conversation';
import { ConversationService } from 'src/app/services/global/conversation.service';
import api_urls from 'src/app/constants/api_urls';
import { UserService } from 'src/app/services/global/user.service';
import { User } from 'src/app/interfaces/user';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';

@Component({
  selector: 'app-quick-chat',
  templateUrl: './quick-chat.component.html',
  styleUrls: ['./quick-chat.component.css']
})
export class QuickChatComponent implements OnInit {
  @Input() confConversations : Conversation | null = null;
  @Input() conversations : Conversation[] = [];
  @Input() users: { [key: number]: User } = {};

  smallProfileImage: string = api_urls.ProfileImageSmall;

  removedUsers: number[] = [];
  constructor( public conversationService: ConversationService,
    private userService: UserService, public chatUtil: ChatUtilsService) { }

  ngOnInit(): void {
    this.conversationService.removedUsersObserver.subscribe((removedUsers: number[]) => {      
      this.removedUsers = removedUsers;
    });
  }

  setChatWindowStatus(status:string) {
    this.chatUtil.changeChatWindowStatus(status);
  }

  toggleChatSideBar(id:number, toogleContactStatus:boolean) {
    this.chatUtil.toggleChatDrawer(id, toogleContactStatus);
  }

  removeQuickChatUser(conversationId) {
    this.conversationService.removeQuickChatUser(conversationId);
    if (this.removedUsers.length == this.chatUtil.getLatestMessageCount(this.conversations)) {
      this.chatUtil.changeChatWindowStatus('closed');
    }   
  }

  isUserRemovedFromquickChat(id) {
    if (this.removedUsers.length) {
      if (this.removedUsers.includes(id)) {
        return false;

      } else {
        return true;

      }
    }
    return true;
  }
}
