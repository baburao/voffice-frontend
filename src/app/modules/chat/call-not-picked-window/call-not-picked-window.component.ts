import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import api_urls from 'src/app/constants/api_urls';
import { UserService } from 'src/app/services/global/user.service';
import { ConversationService } from 'src/app/services/global/conversation.service';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { DirectCall } from 'src/app/call/direct-call';

@Component({
  selector: 'app-call-not-picked-window',
  templateUrl: './call-not-picked-window.component.html',
  styleUrls: ['./call-not-picked-window.component.css']
})
export class CallNotPickedWindowComponent implements OnInit {
  @Input() users;
  @Input() currentConversations;
  @Output() onCallUserClick = new EventEmitter;
  @Output() toggleRecording = new EventEmitter;

  stopRecording: boolean = true;

  smallProfileImage: string = api_urls.ProfileImageSmall;
  directCall: DirectCall| null = null;
  constructor(public userService: UserService, public conversationService: ConversationService,
    private conferenceService:ConfService) { }

  ngOnInit(): void {
    this.conferenceService.directCallObservable.subscribe((newCall: DirectCall) => {
      this.directCall = newCall;
    });
  }

  callAgain(): void {    
    console.log("Again", this.directCall);
    
    if(this.directCall == null) {
      return;
    }
    var userId = this.directCall.targetUser;
    this.conferenceService.closeDirectCall();
    this.onCallUserClick.emit(userId);
  }

  sendMessage(text: string): void {
    console.log(text);    
    this.conversationService.sendMessage(text);
  }


  onUploadFile(event: any) {
    const file = event.target.files.item(0);
    this.conversationService.sendFileMessage(file);
  }

  onToggleRecording() {
    this.stopRecording = !this.stopRecording
    this.toggleRecording.emit()
  }
  
  closeWindow(isRecordMessageWindow = false) {
    // if (isRecordMessageWindow) {
    //   this.isRecording = false;
    // }

    this.conferenceService.closeDirectCall();
  }

  showDeclinedWindow(): boolean {
    return this.directCall != null ? this.directCall.state == 'declined' : false;
  }

}
