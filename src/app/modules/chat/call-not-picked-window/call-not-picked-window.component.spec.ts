import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallNotPickedWindowComponent } from './call-not-picked-window.component';

describe('CallNotPickedWindowComponent', () => {
  let component: CallNotPickedWindowComponent;
  let fixture: ComponentFixture<CallNotPickedWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallNotPickedWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallNotPickedWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
