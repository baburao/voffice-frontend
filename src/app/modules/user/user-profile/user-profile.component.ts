import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 

import { UserService } from '../../../services/global/user.service';
import api_urls from 'src/app/constants/api_urls';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private http: HttpClient, public userService: UserService) { }
  imageUrl: string = api_urls.ProfileImageBig + this.userService.currentUser!.id;

  ngOnInit(): void {
  }

  upload(event: any) {
	const file = event.target.files.item(0);	
	
	const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    this.http.post(api_urls.UploadAvatar, formData).subscribe(() => { 
		this.imageUrl = api_urls.ProfileImageBig + this.userService.currentUser!.id + "?" + new Date().getTime();
		
		console.log(this.imageUrl)
	});	
  }

  isAutoGainControl(): boolean {
    return localStorage.getItem('autoGainControl') === 'true'
  }
  isEchoCancellation(): boolean {
      return localStorage.getItem('echoCancellation') === 'true'
    }
  isNoiseSuppression(): boolean {
    return localStorage.getItem('noiseSuppression') === 'true'
  }

  selectAutoGainControl(event: any) {
    
    localStorage.setItem("autoGainControl", event.target.checked ? "true" : "false");
  }
  selectEchoCancellation(event: any) {
    
    localStorage.setItem("echoCancellation", event.target.checked ? "true" : "false");
  }
  selectNoiseSuppression(event: any) {
    
    localStorage.setItem("noiseSuppression", event.target.checked ? "true" : "false");
  }
}
