import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserRoutes } from './user-routes';



@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    CommonModule,
	RouterModule.forChild(UserRoutes),
  ]
})
export class UserModule { }
