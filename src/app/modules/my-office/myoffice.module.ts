import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MyOfficeViewComponent } from './myoffice-view/myoffice-view.component';
import { MyOfficeRoutes } from './myoffice-routes';
import { MyOfficeLayoutComponent, OfficeElementDirective } from './myoffice-layout/myoffice-layout.component';
import { FormsModule } from '@angular/forms';
import { MyRoomSlotComponent } from './myroom-slot/myroom-slot.component';

import { CalendarModule ,DateAdapter} from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


@NgModule({
  declarations: [MyOfficeViewComponent, MyOfficeLayoutComponent, OfficeElementDirective, MyRoomSlotComponent],
  imports: [
  RouterModule.forChild(MyOfficeRoutes),
  CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
  CommonModule, FormsModule
  ],

  exports: [MyRoomSlotComponent]
})
export class MyOfficeModule { }
