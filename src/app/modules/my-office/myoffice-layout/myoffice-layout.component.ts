import { AfterContentInit, ContentChildren, Directive, ElementRef, Component } from '@angular/core';
import { ViewChildren, OnInit, HostListener, DoCheck, QueryList, Input, OnChanges, ViewChild, Renderer2 } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import api_urls from 'src/app/constants/api_urls';

import { UserService } from '../../../services/global/user.service';
import { OfficeService } from '../../../services/global/office.service';
import { User } from '../../../interfaces/user';

declare var $: any;

@Directive({ selector: 'app-office-element' })
export class OfficeElementDirective {

	@Input() room!: any;

	element: any;
	jElement: any;
	//color: any = "#BBBBBB";

	constructor(private parent: MyOfficeLayoutComponent, private el: ElementRef) {

		//this.color = this.getRandomColor();
		//console.log("OfficeElementDirective", this.room);
		this.jElement = $(this.el.nativeElement);
		//this.jElement.css('background-color', this.color);

	}
	ngOnInit(): void {


		this.parent.collisionGrid.writeItem(this.room, this.room.id);

		this.updateLocation();
	}
	@HostListener('mouseup', ['$event']) onMouseUp(e) {

		if (!this.parent.editMode) {
			return;
		}

		if (this.parent.selectElement(this, e)) {

			e.stopImmediatePropagation();
		}
	}
	updateLocation(): void {

		this.jElement = $(this.el.nativeElement);
		this.jElement.css('width', (this.room.width * this.parent.cellSizeX) + this.parent.unit);
		this.jElement.css('height', (this.room.height * this.parent.cellSizeY) + this.parent.unit);

		this.jElement.css('left', ((this.room.x) * this.parent.cellSizeX) + this.parent.unit);
		this.jElement.css('top', ((this.room.y) * this.parent.cellSizeY) + this.parent.unit);

	}

	getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
}


export class GridItem {


	constructor(
		public x: number,
		public y: number,
		public width: number,
		public height: number,
	) { }
}


export class CollisionGrid {

	collisionGrid: number[][] = [];

	width: number = 4;
	height: number = 4;

	constructor(private owner: MyOfficeLayoutComponent) {
		this.collisionGrid = this.newArray(this.width, []);

		for (var x = 0; x < this.width; x++) {
			this.collisionGrid[x] = this.newArray(this.height, -1);
		}

	}
	newArray(count: number, defaultValue: any): any {
		var array: number[] = Array(count);

		for (var i = 0; i < count; i++) {

			array[i] = defaultValue;
		}
		return array;
	}

	extendGrid(maxX: number, maxY: number): void {

		var newWidth = Math.max(maxX, this.width);
		var newHeight = Math.max(maxY, this.height);

		if (this.width == newWidth && this.height == newHeight) {
			return;
		}

		for (var x = 0; x < this.width; x++) {
			this.collisionGrid[x] = this.collisionGrid[x].concat(this.newArray(newHeight - this.height, -1));
		}

		for (var x = this.width; x < newWidth; x++) {
			this.collisionGrid.push(this.newArray(newHeight, -1));
		}


		this.width = newWidth;
		this.height = newHeight;

		if (this.owner.gridContainer != null) {
			$(this.owner.gridContainer.nativeElement).css('height', this.height * this.owner.cellSizeY + this.owner.unit);
			$(this.owner.gridContainer.nativeElement).css('width', this.width * this.owner.cellSizeX + this.owner.unit);
		}
		this.owner.drawGrid();
	}

	contains(x: number, y: number): boolean {

		return x >= 0 && y >= 0 &&
			x < this.width &&
			y < this.height
	}
	writeItem(item: any, value: number): boolean {

		if (item.x < 0 || item.y < 0) {

			return false;
		}
		this.extendGrid(item.x + item.width, item.y + item.height);

		for (var x = item.x; x < item.x + item.width; x++) {
			for (var y = item.y; y < item.y + item.height; y++) {

				this.collisionGrid[x][y] = value;
			}
		}
		return true;
	}
	checkItem(item: any, value: number): boolean {

		if (item.x < 0 || item.y < 0) {

			return false;
		}

		for (var x = item.x; x < item.x + item.width; x++) {
			for (var y = item.y; y < item.y + item.height; y++) {


				if (this.contains(x, y) && this.collisionGrid[x][y] > -1 && this.collisionGrid[x][y] != value) {
					return false;
				}
			}
		}

		return true;
	}
}

@Component({
	selector: 'app-myoffice-layout',
	templateUrl: './myoffice-layout.component.html',
	styleUrls: ['./myoffice-layout.component.css']
})
export class MyOfficeLayoutComponent implements OnInit {

	@Input() rooms: { [key: number]: any } = {};

	@ViewChild('gridContainer') gridContainer;
	@ViewChild('gridOverlay') gridOverlay;

	@Input() editMode: boolean = false;
	@Input() set showGrid(show: boolean) {
		this._showGrid = show;
		if (show) {
			this.drawGrid();
		}
	};
	_showGrid: boolean = false;

	get showGrid(): boolean { return this._showGrid; }

	//cellSizeX: number = 100;
	//cellSizeY: number = 140;
	//unit = 'px';		

	cellSizeX: number = 7.5 / 2.0;
	cellSizeY: number = 9.0 / 2.0;
	unit = 'vw';

	collisionGrid: CollisionGrid = new CollisionGrid(this);
	childMapping: { [key: number]: any } = {};
	lastIndex: any = null;

	selectOffset: any = { x: 0, y: 0 };
	previewFrame: any = null;

	innerGridContainer: any = null;

	@ViewChildren(OfficeElementDirective) contentChildren!: QueryList<OfficeElementDirective>;

	selectedElement: OfficeElementDirective | null = null;
	roomname;
	isAllselected = true;
	constructor(private http: HttpClient, private renderer: Renderer2,
		private officeService: OfficeService, private userService: UserService) { }

	ngOnInit(): void {
		if (Object.keys(this.rooms).length !== 0) {
			let counter = 1;
			Object.keys(this.rooms).forEach((val, idx) => {
				if(this.rooms[val] !=null){
					this.rooms[val]['color'] = "bg-" + counter;
					if (counter === 13) {
						counter = 0;
					}
					counter++;
				}
			})
		}
	}
	ngAfterViewInit() {

		/*$('body').contextmenu(function(e)  { 
			e.stopImmediatePropagation();
			e.preventDefault();
			return false;
		});*/
		this.drawGrid();
		this.layoutGrid();
	}
	/*@HostListener('mouseenter') onMouseEnter() {
		
	}*/
	ngOnChanges() {

		this.layoutGrid();
		//console.log("bbb child...", this.contentChildren , this.contentChildren ? this.contentChildren.map(p => p.id).join(', ') : '_');
		//this.layouter.layoutGrid();
	}
	enterRoom(event: any, room: any) {

		if (this.isOwnRoom(room)) {

			this.http.post(api_urls.EnterWorkspace, {}).subscribe((data: any) => { });
		} else {

			this.http.post(api_urls.EnterRoom, { room: room.id }).subscribe((data: any) => { });
		}
		event.stopPropagation();
	}
	@HostListener('mousemove', ['$event']) onMouseMove(e: any) {

		if (!this.editMode) {
			return;
		}
		if (this.selectedElement == null) {
			return;
		}

		var indices = this.calcIndices(e);
		indices.x += this.selectOffset.x;
		indices.y += this.selectOffset.y;

		var futureLocation: any = {
			x: indices.x,
			y: indices.y,
			width: this.selectedElement.room.width,
			height: this.selectedElement.room.height,
		}

		var canPlace: boolean = this.collisionGrid.checkItem(futureLocation, this.selectedElement.room.id);

		this.updatePreview(indices.x, indices.y, canPlace);

	}
	@HostListener('mouseleave', ['$event']) onMouseLeave(e: any) { }

	@HostListener('mouseup', ['$event']) onMouseUp(e: any) {

		if (!this.editMode) {
			return;
		}
		if (e.button == 0) {
			if (this.selectedElement != null) {
				var indices = this.calcIndices(e);

				indices.x += this.selectOffset.x;
				indices.y += this.selectOffset.y;

				if (this.moveElement(indices)) {
					this.selectedElement = null;
					this.renderer.removeChild(this.gridContainer.nativeElement, this.previewFrame);
					this.previewFrame = null;
				}
			}
		} else {
			this.selectedElement = null;

			this.renderer.removeChild(this.gridContainer.nativeElement, this.previewFrame);
			this.previewFrame = null;

			e.preventDefault();
			//e.stopImmediatePropagation();

		}
	}

	move: boolean = false;

	selectElement(element: OfficeElementDirective, event: any): boolean {

		if (this.selectedElement != null) {
			return false;
		}

		var indices = this.calcIndices(event);
		this.selectOffset.x = element.room.x - indices.x;
		this.selectOffset.y = element.room.y - indices.y;

		this.selectedElement = element;
		this.activatePreview(element.room);
		this.updatePreview(element.room.x, element.room.y, true);

		return true;
	}


	layoutGrid(): void {

		if (this.contentChildren == null) {
			return;
		}

		this.contentChildren.forEach((child: OfficeElementDirective) => {
			child.updateLocation();
		});

		$(this.gridContainer.nativeElement).css('height', this.collisionGrid.height * this.cellSizeY + this.unit);
		$(this.gridContainer.nativeElement).css('width', this.collisionGrid.width * this.cellSizeX + this.unit);

		$(this.gridContainer.nativeElement).children(".grid-room-item").css('width', (100 / this.collisionGrid.width) + "%")
	}

	moveElement(indices: any) {
		if (this.selectedElement != null) {

			if (this.lastIndex == null) {
				this.lastIndex = indices;
			}

			var futureLocation: any = {
				x: indices.x,
				y: indices.y,
				width: this.selectedElement.room.width,
				height: this.selectedElement.room.height,
			}

			//var diffX = this.lastIndex.x - indices.x;
			//var diffY = this.lastIndex.y - indices.y;


			if (this.collisionGrid.checkItem(futureLocation, this.selectedElement.room.id)) {


				this.collisionGrid.writeItem(this.selectedElement.room, -1);
				this.collisionGrid.writeItem(futureLocation, this.selectedElement.room.id);

				this.selectedElement.room.x = indices.x;
				this.selectedElement.room.y = indices.y;

				this.layoutGrid();
				this.officeService.saveOffice();
				return true;
			} else {

				/*var dirX = this.calcDirection(diffX);
				var dirY = this.calcDirection(diffY);
				
				var curr = {
					x: futureLocation.x,
					y: futureLocation.y,
					width: futureLocation.width,
					height: futureLocation.height,
				};
				
				var stepX: boolean = dirX != 0;
				// && curr.y != this.lastIndex.y
				var t = 0;
				while(curr.x != this.lastIndex.x) {
							
					$('#' + curr.x + 'x' + curr.y).html("_" + t + "_");
					t += 1;
					console.log(curr);
					if(this.collisionGrid.checkItem(curr , this.selectedElement.room.id)) {
						
						this.collisionGrid.writeItem(this.selectedElement.room, -1);
						this.collisionGrid.writeItem(curr, this.selectedElement.room.id);
						
						this.selectedElement.room.x = curr.x;
						this.selectedElement.room.y = curr.y;
										
						this.layoutGrid();
						
						break;
					}
					
					stepX = dirX != null;
					curr.x += dirX;
				}
				*/
			}

			this.lastIndex = indices;
		}

		return false;
	}

	activatePreview(source: any) {

		if (this.previewFrame == null) {
			this.previewFrame = this.renderer.createElement('div');
			$(this.previewFrame).addClass('grid-preview');
			$(this.previewFrame).addClass('grid-preview-can-place');
		}
		var jElement = $(this.previewFrame);

		jElement.css('width', (source.width * this.cellSizeX) + this.unit);
		jElement.css('height', (source.height * this.cellSizeY) + this.unit);
		//jElement.css('background-color', color + "AA" );	

		this.renderer.appendChild(this.gridContainer.nativeElement, this.previewFrame);

	}
	updatePreview(x: number, y: number, canPlace: boolean) {

		var jElement = $(this.previewFrame);
		jElement.css('left', (x * this.cellSizeX) + this.unit);
		jElement.css('top', (y * this.cellSizeY) + this.unit);

		jElement.addClass(canPlace ? 'grid-preview-can-place' : 'grid-preview-cannot-place');
		jElement.removeClass(!canPlace ? 'grid-preview-can-place' : 'grid-preview-cannot-place');

	}

	calcDirection(diff: number) {

		if (diff == 0) {
			return 0;
		}

		return diff > 0 ? 1 : -1;
	}
	calcIndices(mouseEvent: any): any {

		var relativeLocation = this.calcRelative(mouseEvent);

		return {
			x: this.calcIndexX(relativeLocation.x),
			y: this.calcIndexY(relativeLocation.y)
		}
	}
	calcIndexX(location: number): number {
		const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
		var cellSizePx = vw * (this.cellSizeX / 100.0);

		return Math.floor(location / cellSizePx);
		//return Math.floor(location / this.cellSizeX);
	}
	calcIndexY(location: number): number {
		const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
		var cellSizePx = vw * (this.cellSizeY / 100.0);

		return Math.floor(location / cellSizePx);
		//return Math.floor(location / this.cellSizeY);
	}
	calcRelative(mouseEvent: any): any {

		var jContainer = $(this.gridContainer.nativeElement);

		var parentOffset = jContainer.offset();

		return {
			x: mouseEvent.pageX - parentOffset.left,
			y: mouseEvent.pageY - parentOffset.top
		};
	}

	drawGrid() {

		if (!this._showGrid || this.gridOverlay == null) {
			return;
		}
		/*for(var i in this.gridOverlay.nativeElement.children) {
		console.log(this.gridOverlay.nativeElement.children[i]);
			this.renderer.removeChild(this.gridOverlay.nativeElement, this.gridOverlay.nativeElement.children[i]);
		}*/

		if (this.innerGridContainer != null) {
			this.renderer.removeChild(this.gridOverlay.nativeElement, this.innerGridContainer);
		}
		this.innerGridContainer = this.renderer.createElement('div');
		this.renderer.appendChild(this.gridOverlay.nativeElement, this.innerGridContainer);

		//console.log("dsdddd", this.gridOverlay.nativeElement.children.length);
		for (var x = 0; x < this.collisionGrid.width; x++) {
			for (var y = 0; y < this.collisionGrid.height; y++) {

				const innerCell = this.renderer.createElement('div');
				$(innerCell).addClass('grid-inner-cell');
				innerCell.id = x + 'x' + y;

				const cell = this.renderer.createElement('div');

				var jElement = $(cell);
				jElement.css('width', this.cellSizeX + this.unit);
				jElement.css('height', this.cellSizeY + this.unit);

				jElement.css('left', (x * this.cellSizeX) + this.unit);
				jElement.css('top', (y * this.cellSizeY) + this.unit);

				jElement.addClass('grid-cell');

				this.renderer.appendChild(cell, innerCell);
				this.renderer.appendChild(this.innerGridContainer, cell);
				//this.gridOverlay.nativeElement.insertAdjacentHTML('beforeend','<div class="grid-cell" style="' + style + '"><div class="grid-inner-cell"></div></div>');
			}
		}
	}

	getSlotStyle(index: number, room: any) {
		return 'position: absolute;' +
			'width: ' + this.cellSizeX + this.unit + '; height: ' + this.cellSizeY + this.unit
			+ '; top: ' + this.calcYIndex(index, room.width) + this.unit
			+ '; left:' + this.calcXIndex(index, room.width) + this.unit;
	}
	calcXIndex(index: number, width: number) {
		return ((index % width) + 0) * this.cellSizeX;
	}
	calcYIndex(index: number, width: number) {
		return (Math.floor(index / width) + 1) * this.cellSizeY;
	}

	slotList(room: any) {

		var max: number = Math.min(room.size, (room.width) * (room.height));
		if (room.type == 'of') {
			return room.slots.slice(1, max);
		}
		return room.slots.slice(0, max);
	}

	isOwnRoom(room: any) {

		if (room.type == 'de' && room.workspaces != null) {

			return room.workspaces.includes(this.userService.currentUser!.id);
		} else if (room.type == 'of') {

			return this.userService.currentUser!.id == room.owner;
		}
	}
	changeRoomSize(id: number, x: number, y: number) {
		//console.log(this.rooms[id]);
		this.rooms[id].width += x;
		this.rooms[id].height += y;

		this.layoutGrid();
		this.officeService.saveOffice();

	}
	emptyMouseUp(e: any) {

		//e.preventDefault();
		e.stopPropagation();
	}

	// FILTERING DATA BY SELECTING VALUE FROM DROPDOWN
	filterbyroomname(event) {
		const curntroomId = event.target.value;
		if (curntroomId == 'All') {
			this.isAllselected = true;
		} else {
			this.isAllselected = false;
			const filtervl = Object.keys(this.rooms).findIndex(x => x === curntroomId)
			if (filtervl > -1) {
				this.roomname = this.rooms[Object.keys(this.rooms)[filtervl]]
				console.log(this.roomname);
			}
		}

	}
	//APPLYING BACKGROUND COLOR BASED ON DROPDOWN VALUE
	bgClor(val){
		return val
	}
}
