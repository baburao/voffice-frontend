import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges } from '@angular/core';

import {ActivatedRoute, Router} from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ConversationService } from '../../../services/global/conversation.service';
import { UserService } from '../../../services/global/user.service';
import { OfficeService } from '../../../services/global/office.service';
import { User } from '../../../interfaces/user';
import { Room } from '../../../interfaces/room';
import { Conversation } from '../../../interfaces/conversation';
import { AudioRecorder } from '../../../utils/audio-recorder';

import api_urls from 'src/app/constants/api_urls';
import {UtilsService} from "../../../services/global/utils.service";

declare var $:any;
@Component({
  selector: 'app-myoffice-view',
  templateUrl: './myoffice-view.component.html',
  styleUrls: ['./myoffice-view.component.css']
})
export class MyOfficeViewComponent implements OnInit, OnChanges {

  @ViewChild('officeContainer') myofficeContainer;


  users: {[key: number]: User} = {};
  rooms : {[key: number]: Room} = {};
  islayout = false;
  conversations: Conversation[] = [];
  currentConversations: Conversation | null = null;

  numUnreadMessages: number = 0;

  uploadFile: any = null;
  public audioRecorder: AudioRecorder | null = null;

  recording: boolean = false;

  showGrid: boolean = false;
  editMode: boolean = false;

  constructor(private http: HttpClient, public userService: UserService,
	 public conversationService: ConversationService,private utils: UtilsService, 
	private officeService: OfficeService, private activatedRoute: ActivatedRoute) { 
		
	var url: any = this.activatedRoute.url;	

	if(url._value.length > 0 && url._value[0].path === 'edit') {
		this.showGrid = true;
		this.editMode = true;
	}
  }

  ngOnInit(): void {
	this.getUsers();

	this.conversationService.conversations.subscribe((conversations: Conversation[]) => this.conversations = conversations);

	this.conversationService.numUnreadMessages.subscribe((numUnreadMessages: number) => this.numUnreadMessages = numUnreadMessages);

	this.officeService.rooms.subscribe((rooms: {[key: number]: Room}) => {
		if(Object.keys(rooms).length !=0){
      this.islayout = true;
      this.rooms = rooms;
    }
		
	});
	
	/*this.http.post(api_urls.Designations,{name: 'test2' , description:'desc'}).subscribe((data: any) => {		
		
		console.log(data);		
	});*/
  }

  ngAfterViewInit(): void {    
    // $('.grid-container').slimScroll({
    //   height: 'calc(100%)',
    //   width: '100%'
    // });
    
  }
  ngOnChanges(changes: any) {
	
	console.log("change")
  }
  enterWorkspace(): void {
	
	this.officeService.enterWorkspace();
  }
  enterRoom(roomId: number): void {
	
	this.officeService.enterRoom(roomId);	
  }
  sendMessage(text: string): void{

	this.conversationService.sendMessage(text);
  }

  changeFile(fileElement: any) {
	console.log(fileElement.files);
	//this.uploadFile = files.item(0);
	
	this.conversationService.sendFileMessage(fileElement.files.item(0));
  }

  toggleRecording(): void {
	if(this.audioRecorder == null){
		this.startRecording();
  	} else {
		this.stopRecording();
	}
  }

  startRecording(): void {
	
	var self = this;
	
	self.recording = true;
	console.log("start rec");
	
	this.audioRecorder = new AudioRecorder();
	this.audioRecorder.record(function(){ self.audioRecorder = null});
  }

  stopRecording() {
  	if(this.audioRecorder == null){
		return;
  	}
	var self = this;
	
	this.audioRecorder.stop((buffer: any) => {
		
		self.conversationService.sendAudioMessage(buffer);
	});
	
	this.audioRecorder = null;
  }

  isRecording(): boolean {
	return this.audioRecorder != null;
  }

  createConversation(targetUserId: number) {

	this.conversationService.createConversation(targetUserId).subscribe((conversation: Conversation) => {

		this.selectConversation(conversation.id);
	});
  }

  selectConversation(conversationId: number) {

	this.currentConversations = this.conversationService.selectConversation(conversationId);
  }

  isCurrentUser(userId: number): boolean {
	return this.userService.currentUser?.id == userId;
  }


  getUsers(): void {
	  this.userService.getUsers().subscribe(users => this.users = users);
  }

  getAttribute(element: any): any {
	
  }

  initGrid(): void {
	
	console.log(this.myofficeContainer);
  }
 
  testElements : {[key: number]: any} = {1: {id: 1, x: 2, y: 3, width: 2, height: 3}};
  
  tCounter  = 200;
  addElement(): void {	
	
	this.tCounter += 1;
  }  
  saveOffice(): void {	
	
	this.officeService.saveOffice(); 
  }  


}
