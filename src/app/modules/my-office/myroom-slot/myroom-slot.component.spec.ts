import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomSlotComponent } from './room-slot.component';

describe('RoomSlotComponent', () => {
  let component: RoomSlotComponent;
  let fixture: ComponentFixture<RoomSlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomSlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomSlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
