import { Routes } from '@angular/router';
import {MyOfficeViewComponent} from './myoffice-view/myoffice-view.component';

export const MyOfficeRoutes: Routes = [
  {
    path: '',
    data: { breadcrumbs: 'office' },
    children: [
      { path: '',  component: MyOfficeViewComponent, },
    ]
  },
  {
    path: 'edit',
    component: MyOfficeViewComponent
  },
 
];

