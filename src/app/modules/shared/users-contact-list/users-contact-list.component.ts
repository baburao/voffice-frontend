import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import api_urls from 'src/app/constants/api_urls';
import { UtilsService } from 'src/app/services/global/utils.service';
import { UserService } from 'src/app/services/global/user.service';

@Component({
  selector: 'app-users-contact-list',
  templateUrl: './users-contact-list.component.html',
  styleUrls: ['./users-contact-list.component.css']
})
export class UsersContactListComponent implements OnInit {

  @Input() users: { [key: number]: User } = {};
  @Output() toggleUserClick = new EventEmitter
  
  smallProfileImage: string = api_urls.ProfileImageSmall;
  filters: any;
  selectedUsersList: any;
  //designations: any;
  constructor(private utils: UtilsService, public userService:UserService) {
    this.filters = [];
    this.selectedUsersList = [];
   }

  ngOnInit(): void {
    //this.getDesignations();
  }
  
  /*getDesignations() {
    this.userService.getDesignations().subscribe(res => {
      this.designations = res;
    })
  }*/

  filtered(id:number){
	  // @ts-ignore
	  return this.filters.includes(id)
  }

  fullname(usr: any, type=true){
    return this.utils.fullname(usr, type)
   }
 
   abbreviate(text: string){
     return this.utils.abbreviate(text)
   }
  
  toggleUser(userId: number): void {
    if (!this.listHasUser(userId)) {
      this.selectedUsersList.push(userId)
    } else {
      const index = this.selectedUsersList.indexOf(userId, 0);
      if (index > -1) {
        this.selectedUsersList.splice(index, 1);
      }
    }    
    this.toggleUserClick.emit(this.selectedUsersList)
  }
  
  listHasUser(id: number) {
    return this.selectedUsersList.indexOf(id)  > -1;
  }

  search(event:any){
	  const value = event.target.value.toLowerCase();
	  const len = event.target.value.length;
	  if(len>0){

	    // @ts-ignore
      let users = this.users;
      for (let k of Object.keys(users)){
        // @ts-ignore
        const fullname = this.fullname(this.users[k],false)
        const K = parseInt(k);

        if(fullname.toLowerCase().slice(0, len) === value && this.filtered(K)){
           // @ts-ignore
          this.filters = this.filters.filter(i => i !== K);
        }

        if(fullname.toLowerCase().slice(0, len) !== value){
          this.filters.push(K)
        }


      }
    }else {
	    this.filters = [];
    }

  }
  /*designation(designations, id) {
    return this.utils.designationName(designations, id);    
  }*/

}
