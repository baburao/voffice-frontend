import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersContactListComponent } from './users-contact-list.component';

describe('UsersContactListComponent', () => {
  let component: UsersContactListComponent;
  let fixture: ComponentFixture<UsersContactListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersContactListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
