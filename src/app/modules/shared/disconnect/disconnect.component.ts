import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { WebSocketService } from '../../../services/global/web-socket.service';

@Component({
  selector: 'app-disconnect',
  templateUrl: './disconnect.component.html',
  styleUrls: ['./disconnect.component.css']
})
export class DisconnectComponent implements OnInit {

  constructor(private router: Router, private webSocketService: WebSocketService) { }

  ngOnInit(): void {
    console.log("connnnnn", this.webSocketService.isConnectionClosed(), this.webSocketService.serverConnection);
    if(!this.webSocketService.isConnectionClosed()) {
      this.router.navigate(['']);        
    }
  }
  reloadPage(): void {
    location.reload(); 
  }
}
