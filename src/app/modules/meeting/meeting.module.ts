import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListMeetingComponent } from './list-meeting/list-meeting.component';
import {MeetingRoutes} from "./meeting-routes";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [ListMeetingComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(MeetingRoutes),
  ]
})
export class MeetingModule { }
