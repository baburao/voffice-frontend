import { Routes } from '@angular/router';
import {ListMeetingComponent} from "./list-meeting/list-meeting.component";

export const MeetingRoutes: Routes = [
  {
    path: 'meetings',
    component: ListMeetingComponent
  },
];

