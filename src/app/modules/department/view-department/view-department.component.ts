import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OfficeService } from '../../../services/global/office.service';

import { Room } from 'src/app/interfaces/room';

import api_urls from 'src/app/constants/api_urls';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';

declare  var $:  any;

@Component({
  selector: 'app-view-department',
  templateUrl: './view-department.component.html',
  styleUrls: ['./view-department.component.css']
})
export class ViewDepartmentComponent implements OnInit {

  department: Room | null = null;

  numPeople: number = 0;

  constructor(private http: HttpClient, public officeService: OfficeService,
    private chatUtil:ChatUtilsService) { 
	
    this.http.get(api_urls.GetDepartment).subscribe((department: any) => {		
      
      if(department.id != -1) {
        
        this.department = department;
        this.numPeople = 0;
        
        if(department == null){
          return;
        }
        this.department!.workspaces.forEach((workspace: number) => {if(workspace != -1){ this.numPeople += 1;}});
    
      }	
    });
  }
  getUsers() {
    if(this.department == null) {
      return [];
    }
    return this.department.workspaces.filter((workspace: number) => {
      return workspace != -1 && !this.officeService.isHead(workspace, this.department!)
    });
  }
  getHeads() {
    if(this.department == null) {
      return [];
    }
    return this.department.workspaces.filter((workspace: number) => {
      return workspace != -1 && this.officeService.isHead(workspace, this.department!)
    });
  }
  ngOnInit(): void {
    this.addNoScrollClassToBody();
  }

  ngOnDestroy(): void {
   this.chatUtil.removeClass('no-vscroll'); 
  }

  ngAfterViewInit(): void {    
    $(function() {
      $('#dept-view-scroll').slimScroll({
          height: 'calc(100% - 110px)',
          width: '100%',
          color:'#fff'
      });
    });
  }

  addNoScrollClassToBody() {
    this.chatUtil.addClass('no-vscroll');
  }
}
