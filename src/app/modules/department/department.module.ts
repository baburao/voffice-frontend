
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DepartmentRoutes} from './department-routes';
import { ViewDepartmentComponent } from './view-department/view-department.component';

import { OfficeModule } from 'src/app/modules/office/office.module';
import { UserSmallProfileComponent } from './user-small-profile/user-small-profile.component';

@NgModule({

  declarations: [ViewDepartmentComponent, UserSmallProfileComponent],


  imports: [
    CommonModule,
    RouterModule.forChild(DepartmentRoutes),
    OfficeModule
  ],
  exports: [UserSmallProfileComponent]
})
// @ts-ignore
export class DepartmentModule { }
