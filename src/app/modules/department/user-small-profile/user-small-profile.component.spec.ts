import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSmallProfileComponent } from './user-small-profile.component';

describe('UserSmallProfileComponent', () => {
  let component: UserSmallProfileComponent;
  let fixture: ComponentFixture<UserSmallProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSmallProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSmallProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
