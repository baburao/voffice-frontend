import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { UserService } from '../../../services/global/user.service';
import { ConversationService } from '../../../services/global/conversation.service'

import api_urls from 'src/app/constants/api_urls';
import { Conversation } from 'src/app/interfaces/conversation';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { OfficeService } from 'src/app/services/global/office.service';
import { UtilsService } from 'src/app/services/global/utils.service';

@Component({
  selector: 'app-user-small-profile',
  templateUrl: './user-small-profile.component.html',
  styleUrls: ['./user-small-profile.component.css']
})
export class UserSmallProfileComponent implements OnInit {

  @Input() userId: number = -1;
  @Input() isSeated: boolean = false;
  @Input() isWorkplace: boolean = false;
  @Input() selectedTeam: any | null = null;

  smallImageAddress: string = api_urls.ProfileImageSmall;
  conversations: Conversation[] = [];
  currentConversations: Conversation | null = null;

  chatDrawerClass = 'chatbox-right-toggle-open';
  contactClass = 'contactsbox-toggle-open';

  //designations:any;

  constructor(public userService: UserService, private renderer: Renderer2,
    public conversationService: ConversationService, private conferenceService: ConfService,
    private officeService: OfficeService,private utils: UtilsService) { }

  ngOnInit(): void {
    //this.getDesignations();
    this.conversationService.conversations.subscribe(
      (conversations: Conversation[]) => {
        this.conversations = conversations;
      });
    
      this.conversationService.currentConversation.subscribe((newConversation: Conversation | null) => {  
        this.currentConversations = newConversation;
      });
  }

  makeCallClick(userId: number) {
    this.startChatClick(userId, true);
   
  }
  startChatClick(targetUserId: number, isCall=false) {
    if (this.isNewChat(targetUserId)) {
      this.conversationService.createConversation(targetUserId).subscribe((conversation: Conversation) => {
        this.currentConversations = conversation;
        this.selectConversation(conversation.id);
        this.toggleChatDrawer(conversation.id, false, false, false, conversation)
        if(isCall) {
          this.toggleChatDrawer(this.currentConversations!.id, true, false, false, this.currentConversations)
          this.conferenceService.callUser(this.currentConversations!.id, false);
        }
      });
    } else {
      if(isCall) {
        this.toggleChatDrawer(this.currentConversations!.id, true, false, false, this.currentConversations)
        this.conferenceService.callUser(this.currentConversations!.id, false);
      }
    }
  }

  isNewChat(targetUserId: number) {
    for (let i = 0; i < this.conversations.length; i++) {
      if (this.conversations[i].users.length == 2 && this.conversations[i].users.includes(targetUserId) && this.conversations[i].users.includes(this.userService.currentUser!.id)) {
        this.currentConversations = this.conversations[i];
        this.toggleChatDrawer(this.conversations[i].id, false, false, false, this.conversations[i])
        return false;
      }
    }
    return true;
  }

  selectConversation(conversationId: number) {
    this.conversationService.selectConversation(conversationId);
  }

  toggleChatDrawer(id = -1, contactDrawer = false, force = false, action = false, conversation: any = null) { // toggles Chat Bar and selects Conversation
    let isDirect = this.conversationService.isOneToOne(conversation)
    this.toggleContacts();
    if (id > 0) {
      this.selectConversation(id);
    } else {
      this.selectConversation(-1);
    }
    let hasClass;
    if (isDirect) {
      hasClass = this.hasClass(this.chatDrawerClass);

      if (force) { hasClass = action; }

      if (hasClass) {
        this.removeClass(this.chatDrawerClass);
        this.addClass(this.chatDrawerClass);
      } else {
        this.addClass(this.chatDrawerClass);
      }
    }

  }

  toggleContacts() { // toggles Contact list.
    const hasClass = this.hasClass(this.contactClass);
    if (hasClass) {
      this.removeClass(this.contactClass);
    }
  }

  hasClass(cls: string) { // checks if class exists
    return document.body.classList.contains(cls);
  }

  addClass(cls: string) { // adds class to body
    this.renderer.addClass(document.body, cls);
  }

  removeClass(cls: string) { // removes class from Body
    this.renderer.removeClass(document.body, cls);
  }

  removeUserFromTeam(userId) {
    if(this.selectedTeam == null) {
			return;
		}
		this.officeService.removeFromTeam(this.selectedTeam!.id, userId);
  }

  /*getDesignations() {
    this.userService.getDesignations().subscribe(res => {
      this.designations = res;
    })
  }

  designation(designations, id) {
    return this.utils.designationName(designations, id);    
  }*/
}