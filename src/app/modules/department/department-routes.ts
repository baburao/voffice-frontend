import { Routes } from '@angular/router';
import {ViewDepartmentComponent} from './view-department/view-department.component';

export const DepartmentRoutes: Routes = [
  {
    path: 'department',
    data: { breadcrumbs: 'Department' },
    children: [
      { path: '',  component: ViewDepartmentComponent, },
    ]
  }
];

