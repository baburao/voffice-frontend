import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ConferenceRoutes} from './conference-routes';
import { ListConferenceComponent } from './list-conference/list-conference.component';
import { CreateConferenceComponent } from './create-conference/create-conference.component';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { JoinConferenceComponent } from './join-conference/join-conference.component';
import { ConferenceUserComponent } from './conference-user/conference-user.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AllConfrencesComponent } from './all-confrences/all-confrences.component';
import { ConferenceOptionbarComponent } from './conference-optionbar/conference-optionbar.component';
import { ConferenceGridComponent } from './conference-grid/conference-grid.component';
import { ConferenceGridElementComponent } from './conference-grid-element/conference-grid-element.component';
import { ScreenShareComponent } from './screen-share/screen-share.component';


@NgModule({

  declarations: [ListConferenceComponent,
     CreateConferenceComponent, 
     JoinConferenceComponent, 
     ConferenceUserComponent, 
     AllConfrencesComponent,
     ConferenceOptionbarComponent,
     ConferenceGridElementComponent,
     ConferenceGridComponent,
     ScreenShareComponent],


  imports: [
    CommonModule,
    RouterModule.forChild(ConferenceRoutes),
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,

  ],
  exports: [ConferenceUserComponent]
})
// @ts-ignore
export class ConferenceModule { }
