import {Component, OnInit, Input, ViewChild, ElementRef, Renderer2, OnDestroy} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import { switchMap } from 'rxjs/operators';
import {ConfService} from "../../../services/conferences/conf.service";
import { ConversationService } from "../../../services/global/conversation.service";
import {Conference} from "../../../interfaces/conference";
import {User} from "../../../interfaces/user";
import { Conversation } from "../../../interfaces/conversation";
import {ActivatedRoute, Router} from "@angular/router";
import {UtilsService} from "../../../services/global/utils.service";
import api_urls from 'src/app/constants/api_urls';

//import { Call } from '../../../call/call';
import { Call2, CallPeer } from '../../../call/call2';
import {UserService} from "../../../services/global/user.service";
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { Stats } from 'fs';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-join-conference',
  templateUrl: './join-conference.component.html',
  styleUrls: ['./join-conference.component.css']
})
export class JoinConferenceComponent implements OnInit, OnDestroy {

  activeCall: Call2 | null = null;
  PRIMARY = 'primary';
  conferenceId: number = -1;
  // @ts-ignore
  //pinnedConnection: any = null;
  //activeScreen: number = -1;
  initialPin = false;
  bodyClass = 'no-vscroll';
  users: {[key: number]: User} = {};
  //activeVideo = false;
  //activeAudio = false;
  activeScreenShare = false;
  peopleTab = false;
  conferences : {[key: number]: Conference} = {};
  previewPlayerElement: HTMLVideoElement | null = null;
  filters: any[] = [];

  @ViewChild('previewPlayer')
  set previewPlayer(el: ElementRef) {
	if(el != null) {
    	this.previewPlayerElement = el.nativeElement;
		if(this.previewPlayerElement != null && this.activeCall != null){
			//this.previewPlayerElement.srcObject = this.activeCall!.previewTrackStream;
		}
	}

  }

  //designations: any;
  isParticiapantsPopUp = false;

  smallProfileImage: string = api_urls.ProfileImageSmall;
  constructor(private activatedRoute: ActivatedRoute, public confService: ConfService,
   private renderer: Renderer2, private sanitizer:DomSanitizer, public userService: UserService, private conversationService: ConversationService,
              private utils: UtilsService, private chatUtil:ChatUtilsService
  ) {
      this.renderer.addClass(document.body, this.bodyClass);
    }

  ngOnInit(): void {
    //this.getDesignations();
    this.getUsers();
	  this.activatedRoute.paramMap.subscribe(params =>  {

	    var conferenceId: any = params.get('cid');
	    this.conferenceId = parseInt(conferenceId);
	    this.confService.getConferences().subscribe((cfs: {[key: number]: Conference}) => {
        this.conferences = cfs;
		  });
  	});


	this.confService.activeCallObservable.subscribe(( newCall: any) => {
    this.activeCall = newCall;    
		if(this.activeCall != null && this.previewPlayerElement != null){
			//this.previewPlayerElement!.srcObject = this.activeCall.previewTrackStream;
		}

	});

	/*this.confService.onPeerScreenshare.subscribe((sharingPeer: CallPeer) => {

		if(sharingPeer != null) {
			this.activeScreen = sharingPeer.targetId;
		} else {
			this.activeScreen = -1;
    }
	});*/
  }
  ngAfterViewChecked() {
	  window.scrollTo(0, 0);
  }
  ngOnDestroy() {
      this.renderer.removeClass(document.body, this.bodyClass);
  }

  addScrollBar(): void {
    setTimeout(() => {
      $('#left-b-contact-list-main > ul').slimScroll({
        height: 'calc(100%)',
        width: '100%',
        color: '#5d5d5d'
      });
    },0);
  }

  addGridScrollBar() {
    setTimeout(() => {
      $('#lower-call-conference-main .member-img-grid').slimScroll({
        height: '100%',
        width: '100%'
      });
    }, 0);
    
  }

	connectCall(): void {

		if(this.activeCall == null) {

			if(this.conference != null) {
      			//this.utils.updateConfConvo(this.conference.conversation);
			}
			this.confService.createCall(this.conferenceId);
			if(this.conference != null) {
        //this.conversationService.setCurrentConversation(this.conference.conversation);
        this.chatUtil.changeChatWindowStatus('closed');
			}
			//this.activeCall = this.confService.createCall(this.conferenceId);
		}
	}
	
	isConnected() {
		return this.activeCall != null && this.activeCall.isConnected() && this.activeCall.isConferenceCall();
	}
	// Video
	toggleVideo(): void {

		if(this.activeCall == null) {
			return;
		}
		this.activeCall.toogleCam();
	}

	toggleAudio(): void {

		if(this.activeCall == null) {
			return;
		}
		this.activeCall!.toogleMuteAudio();
	}
  
  isCam(): boolean {
    if(this.activeCall == null) {
      return false;
    }
    return this.activeCall!.isCam();
  }
  getUsers(): void {
	  this.userService.getUsers()
      .subscribe(users => this.users = users);
  }
  
/*
  pinCall(key){
    this.pinnedConnection = key;
    let pinnedGrid = $('#' + key);
     if(key != this.PRIMARY) {
       pinnedGrid = $('#C-' + key);
     }
     const callsGrid = $('#'+ 'callsGRID');
     callsGrid.prepend(pinnedGrid);
     if(key != this.PRIMARY) {
       const prim = $('#' + this.PRIMARY);
       pinnedGrid.after(prim);
     }

    //
    //   $(function() {
    //           $('#member-img-grid-scroll').slimScroll({
    //               height: '100%',
    //               width: '100%'
    //           });
    //       });
    //
    //   $(document).ready(function() {
    //               // init
    //     $('.cv-carousel').carouselVertical();
    //     // for moving programmatically the carousel
    //     // you can do that
    //     $('.cv-carousel').trigger('goTo', [1]);
    //     // or that
    //     $('.cv-carousel').carouselVertical().trigger('goTo', [1]);
    //      // $('#I-'+connection).parent('.cv-item').hide()
    // });

  }

  UnpinCall(){
    this.pinnedConnection = null;
    const pinnedGrid = $('#'+ this.PRIMARY);
    const callsGrid = $('#'+ 'callsGRID');
     callsGrid.prepend(pinnedGrid)
  }
*/
  get conference(): any {
    return this.conferences[this.conferenceId]
  }

  // addPeople(){
  //   this.peopleTab = false;
  //   this.confService.saveConference(this.conference , () => {}, () => {})
  // }

  openPeopleTab() {
    this.peopleTab = true;
    this.addScrollBar();
  }
  closePeopleTab() {
    this.peopleTab = false;
  }

  fullname(user:any){
    return `${user.firstname} ${user.lastname}`;
  }

  fullnameUsingId(userId:any){
    if(this.users[userId])
      return `${this.users[userId].firstname} ${this.users[userId].lastname}`;
    else 
    return '';
  }

  isAssigned(id: string){
    return this.conference.assignedUsers.includes(parseInt(id))
  }

  assign(id: string){
    const userId =  parseInt(id);
    this.modifyUserList(userId, true);
  }
  unassign(id:string){
    const userId =  parseInt(id);
    this.modifyUserList(userId, false);

  }

  modifyUserList(userId: number, addUser: boolean) : void {

    if (addUser) {
      this.conference.assignedUsers.push(userId);
    } else {
      const index = this.conference.assignedUsers.indexOf(userId, 0);
      if (index > -1) {
        this.conference.assignedUsers.splice(index, 1);
      }
    }
    this.confService.saveConference(this.conference, () => {}, () => {});
  }
  removeRecording(id: number) {
    if(this.activeCall == null) {
       return;
    }
    this.activeCall.removeRecording(id);
  }
  getRecordingUrl(recording: any) {
    return this.sanitizer.bypassSecurityTrustUrl(recording.url);
  }
  /*pinClass(connection){
    if(connection === this.pinnedConnection){
      return 'video-conference-main'
    }
    else if(this.pinnedConnection){
      return 'video-conference-slider-main'
    }
    else {
      return ''
    }
  }

  innerClass(connection){

  if(connection === this.pinnedConnection){
      return 'video-conference-box'
    }
    else if(this.pinnedConnection){
      return 'cv-carousel cv-drag cv-loaded innerclassHeight'
    }
    else {
      return 'member-img-box'
    }
  }

  sliderGridClass(connection){
    if(connection === this.pinnedConnection){
      return ''
    }
    else if(this.pinnedConnection){
      return 'cv-stage-outer'
    }
    else {
      return 'member-img-contain'
    }
  }*/
  getSelfProfileImageAddress(): string {
    return api_urls.ProfileImageBig + this.userService.currentUser?.id;
  }

  getOrderedConnectionsLength(): number {
    if(this.activeCall == null) return 0;
    
    let orderConnection = this.activeCall.orderedConnections;  
      
    if(orderConnection == null) return 0;
    
    return orderConnection.length ? orderConnection.length + 1 : 1 ;
  }
  filtered(id:number){
	  // @ts-ignore
	  return this.filters.includes(id)
  }

  search(event:any){
	  const value = event.target.value.toLowerCase();
	  const len = event.target.value.length;
	  if(len>0){

	    // @ts-ignore
      let users = this.users;
      for (let k of Object.keys(users)){
        // @ts-ignore
        const fullname = this.fullname(this.users[k],false)
        const K = parseInt(k);

        if(fullname.toLowerCase().slice(0, len) === value && this.filtered(K)){
           // @ts-ignore
          this.filters = this.filters.filter(i => i !== K);
        }

        if(fullname.toLowerCase().slice(0, len) !== value){
          this.filters.push(K)
        }


      }
    }else {
	    this.filters = [];
    }

  }
/*
  getDesignations() {
    this.userService.getDesignations().subscribe(res => {
      this.designations = res;
    })
  }

  designation(designations, id) {
    return this.utils.designationName(designations, id);    
  }
*/
  totalUsersInConf() {
    return this.conference ? this.conference.conversation.users.length: 0;
  }

  togglePopup(status : boolean = false) {
    console.log(this.activeCall?.orderedConnections);

    this.isParticiapantsPopUp = status;
  }
}
