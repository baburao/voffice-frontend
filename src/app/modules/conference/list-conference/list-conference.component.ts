import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {ConfService} from "../../../services/conferences/conf.service";
import { UserService } from '../../../services/global/user.service';
import {Conference} from "../../../interfaces/conference";
import {User} from "../../../interfaces/user";
import {Room} from "../../../interfaces/room";
import {Router} from "@angular/router";

import api_urls from 'src/app/constants/api_urls';
import { BehaviorSubject } from 'rxjs';

declare var $:any;

@Component({
  selector: 'app-list-conference',
  templateUrl: './list-conference.component.html',
  styleUrls: ['./list-conference.component.css']
})
export class ListConferenceComponent implements OnInit {

  conferences: {[key: number]: Conference} = {};
  users: {[key: number]: User} = {};
  meetingRooms : {[key: number]: Room} = {};
  crossdata: any;
  cancle: boolean=false;
 



  smallImageAddress: string = api_urls.ProfileImageSmall;

  constructor(public confservice: ConfService, public userService: UserService, private router: Router) { }

  ngOnInit(): void {
   
	
	this.getUsers();
    this.getConferences();
	this.userService.meetingRooms.subscribe((meetingRooms: {[key: number]: Room}) => this.meetingRooms = meetingRooms);
	
	if(this.confservice.activeCall != null && this.confservice.activeCall.isConference()) {
		this.router.navigate(['conferences/join/' + this.confservice.activeCall.conference!.id]);
	}
  }

  ngAfterViewInit(): void {
    $('#lower-conference-main').slimScroll({
      height: 'calc(88%)',
      width: '100%',
      color: '#fff'
    });
    
  }

  getConferences(): void {
	this.confservice.getConferences()
      .subscribe(conferences => {this.conferences = conferences;
        console.log(conferences);
      });
  }
  getUsers(): void {
	this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  join(id: number){
    this.router.navigate(['conferences/join/' + id])
  }

  deleteConf(id:any , conference: string){
   
     
     if(confirm("                               Are you sure to delete "+conference)) {
      this.confservice.deleteConference(id);
    console.log("Implement delete functionality here");
  }
     

    }
   
    updateConf(conference , index){
    
         
      this.confservice.databack.next(conference);
     
      // this.confservice(this.crossdata);
      console.log("obj" , conference);
      //console.log("obj1" ,  this.crossdata);
      this.router.navigate(['conferences/edit/' + conference.value.id])
      // this.title.nativeElement.value=this.crossdata;
      // this.assignedUsers.nativeElement.value=this.crossdata;
      // this.meeting_room.nativeElement.value=this.crossdata;
      // this.startDate.nativeElement.value=this.crossdata;
      // this.time.nativeElement.value=this.crossdata;
     
    }
   
}
