import { Component, OnInit, Input, ViewChild, ElementRef, Inject } from '@angular/core';

import {ConfService} from "../../../services/conferences/conf.service";
import { UserService } from '../../../services/global/user.service';
//import { CallConnection } from '../../../call/call-connection';
import { CallPeer } from '../../../call/call2';
import {User} from "../../../interfaces/user";

import api_urls from 'src/app/constants/api_urls';

// declare var togglePipMode;
@Component({
  selector: 'app-conference-user',
  templateUrl: './conference-user.component.html',
  styleUrls: ['./conference-user.component.css']
})
export class ConferenceUserComponent implements OnInit {
 
  @Input() connection : CallPeer | null = null;
  @Input() mirrorVideo: boolean = false;
   
  users: {[key: number]: User} = {};

  videoPlayer: HTMLVideoElement | null = null;
  audioMeter: HTMLElement | null = null;

  imageAddress: string;

  @Input() volume: number = 1;

  @ViewChild('streamPlayer')
  set streamPlayer(el: ElementRef) {
    this.videoPlayer = el.nativeElement;
    this.updateVideoOutput();
  }
  /*@ViewChild('audioMeter')
  set setAudioMeter(el: ElementRef) {
    this.audioMeter = el.nativeElement;
  }*/
  constructor(private confservice : ConfService, private userService: UserService ) {	 
   
  	this.imageAddress = api_urls.ProfileImageBig;
  }

  ngOnInit(): void {
	 this.getUsers();		

	//this.connection!.onAudioMeterValue = function(value: number) {
		
		//console.log(self.audioMeter!.style)
		//self.audioMeter!.style['width'] = (value * 500) + 'px';
	//}
	
	//this.connection!.onStream = function(stream: any) {		
	//	self.connection!.setVideoOutput(self.videoPlayer);
	//}
  }
  ngOnChanges() {
	
    this.updateVideoOutput();
  }
  getUsers(): void {

	  this.userService.getUsers()
      .subscribe(users => this.users = users);
  }
  hasVideo(): boolean {
	return this.connection!.hasVideo();
  }

  doTest() : void {
  }

  updateVideoOutput() {
    
    if(this.videoPlayer != null) {
      this.connection!.setVideoOutput(this.videoPlayer);      
    }
  }

  /*togglePicInPicMode() {
    togglePipMode();
  }*/

}
