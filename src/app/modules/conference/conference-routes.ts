import { Routes } from '@angular/router';
import {ListConferenceComponent} from './list-conference/list-conference.component';
import {CreateConferenceComponent} from './create-conference/create-conference.component';
import {JoinConferenceComponent} from "./join-conference/join-conference.component";
import {AllConfrencesComponent} from "./all-confrences/all-confrences.component";
import { ItemResolver } from '../../resolvers/id.resolver';
export const ConferenceRoutes: Routes = [
  {
    path: 'conferences',
    data: { breadcrumbs: 'Conference' },
    children: [
      { path: '',  component: ListConferenceComponent, },
      {
        path: 'create',
        data: { breadcrumbs: 'Create' },
        component: CreateConferenceComponent
      },
      {
        path: 'edit/:cid',
        data: { breadcrumbs: 'Edit' },        
        component: CreateConferenceComponent
      },
      {
        path: 'join/:cid',
        data: { breadcrumbs: '{{ item.id }}' },
        resolve: { item: ItemResolver },
        
        children: [
          { path: '', component: JoinConferenceComponent }
        ]
      },
      {
        path: 'all',
        data: { breadcrumbs: 'Conferences' },
        component: AllConfrencesComponent 
      }
    ]
  },
  /*{
    path: 'conferences/create',
    component: CreateConferenceComponent
  },
  {
    path: 'conferences/join/:cid',
    component: JoinConferenceComponent
  },
  {
    path: 'conferences/all',
    component: AllConfrencesComponent 
  },
  {
    path: 'disconnected',
    component: DisconnectComponent
  },*/
];

