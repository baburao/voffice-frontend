import {Component, OnInit, Input, ViewChild, ElementRef, Renderer2, OnDestroy} from '@angular/core';
import {ConfService} from "../../../services/conferences/conf.service";
import {Conference} from "../../../interfaces/conference";
import {User} from "../../../interfaces/user";
import { Conversation } from "../../../interfaces/conversation";
import api_urls from 'src/app/constants/api_urls';

import { Call2, CallPeer } from '../../../call/call2';
import { UserService } from "../../../services/global/user.service";
declare var $:any;
@Component({
  selector: 'app-conference-grid',
  templateUrl: './conference-grid.component.html',
  styleUrls: ['./conference-grid.component.css']
})
export class ConferenceGridComponent implements OnInit {
  
  fullScreenElement: HTMLVideoElement | null = null;
  imageAddress: string = api_urls.ProfileImageBig;
  
  @ViewChild('fullScreenContainer')
  set fullScreenContainer(el: ElementRef) {
    if(el != null) {
      this.fullScreenElement = el.nativeElement;
    }  
  }
  
  activeCall: Call2 | null = null;
  
  constructor(public confService: ConfService, public userService: UserService) { }

  ngOnInit(): void {
    
    this.confService.activeCallObservable.subscribe(( newCall: any) => {
    this.activeCall = newCall;    
    //if(this.activeCall != null && this.previewPlayerElement != null){
    //  this.previewPlayerElement!.srcObject = this.activeCall.previewTrackStream;
    //}
    });
  }

  ngAfterViewInit(): void {
   /* setTimeout(() => {
      $('.share-sidebar-scroll').slimScroll({
        height: 'calc(100%)',
        width: '100%',
        color: '#5d5d5d'
      });
    },0);*/
  }
  isConnected(): boolean {
    return this.activeCall != null && this.activeCall.isConnected() && this.activeCall.isConferenceCall();
  }
  numConnections(): number {
    return this.activeCall != null ? this.activeCall.orderedConnections.length: 0;
  }
  getGridClass(): string {
    
    var num: number = this.numConnections();
    
    if(num <= 1) {
      return 'grid-element-2-row';
    } else if(num == 2) {      
      return 'grid-element-3-row';
    } else if(num ==  3){      
      return 'grid-element-4-row';
    } else { //if(num > 3) 
      return 'grid-element-5-row';
    }    
  }
  hasSharingPeer() {
    return this.activeCall != null ? this.activeCall.sharingPeer != null : false;
  }
  
  getSidebarPeers(): CallPeer[] {
    if(this.hasSharingPeer() || this.activeCall!.isScreenshare()) {
      return this.activeCall!.orderedConnections;
    }
    return this.activeCall!.orderedConnections.slice(1);
  }
  getPresentingPeer(): CallPeer | null{
    
    if(!this.isConnected() || this.activeCall!.orderedConnections.length == 0) {
      return null;
    }
        
    if(this.hasSharingPeer()) {
      return this.activeCall!.sharingPeer!;
    } else if(this.activeCall!.isScreenshare()) {
      return this.activeCall!.ownStream;
    }
    
    return this.activeCall!.orderedConnections[0];
  }
  
  showGrid(): boolean {
    return this.confService.showGrid && !(this.hasSharingPeer() || this.activeCall!.isScreenshare());
  }
  openFullscreen() {
    
    if(this.fullScreenElement == null) {
      return;
    }
    var elem: any = this.fullScreenElement;
    
    if(document.fullscreenElement == null){
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
      }      
    } else {
      var d: any = document;
      
      if (d.exitFullscreen) {
        d.exitFullscreen();
      } else if (d.mozCancelFullScreen) { /* Firefox */
        d.mozCancelFullScreen();
      } else if (d.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        d.webkitExitFullscreen();
      } else if (d.msExitFullscreen) { /* IE/Edge */
        d.msExitFullscreen();
      }
    }
  }
}
