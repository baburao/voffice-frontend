import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConferenceGridComponent } from './conference-grid.component';

describe('ConferenceGridComponent', () => {
  let component: ConferenceGridComponent;
  let fixture: ComponentFixture<ConferenceGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConferenceGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferenceGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
