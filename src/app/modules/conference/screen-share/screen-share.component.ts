import { Component, OnInit, Input, ViewChild, ElementRef, Inject } from '@angular/core';

import {ConfService} from "../../../services/conferences/conf.service";
import { UserService } from '../../../services/global/user.service';
//import { CallConnection } from '../../../call/call-connection';
import { CallPeer } from '../../../call/call2';
import {User} from "../../../interfaces/user";

import api_urls from 'src/app/constants/api_urls';
@Component({
  selector: 'app-screen-share',
  templateUrl: './screen-share.component.html',
  styleUrls: ['./screen-share.component.css']
})
export class ScreenShareComponent implements OnInit {

  @Input() connection : CallPeer | null = null;

  videoPlayer: HTMLVideoElement | null = null;
  shareVideoPlayer: HTMLVideoElement | null = null;
  imageAddress: string = api_urls.ProfileImageBig;
  
  @ViewChild('streamPlayer')
  set streamPlayer(el: ElementRef) {
    this.videoPlayer = el.nativeElement;  
    this.connection!.setVideoOutput(this.videoPlayer);
  }
  @ViewChild('sharePlayer')
  set sharePlayer(el: ElementRef) {
    this.shareVideoPlayer = el.nativeElement;  
    this.connection!.setSecondaryOutput(this.shareVideoPlayer);
  }
  
  /*@ViewChild('audioMeter')
  set setAudioMeter(el: ElementRef) {
    this.audioMeter = el.nativeElement;
  }*/
  constructor(private confservice : ConfService, private userService: UserService ) {  
   
  }
  ngOnInit(): void {
  }
  ngOnChanges() {  
    
    if(this.videoPlayer != null) {
      this.connection!.setVideoOutput(this.videoPlayer);
    }
    if(this.shareVideoPlayer != null) {
      this.connection!.setSecondaryOutput(this.shareVideoPlayer);
    }
  }
  hasVideo(): boolean {
    return this.connection!.hasVideo();
  }
  hasShare(): boolean {
    return this.connection!.hasShare();
  }

  
}
