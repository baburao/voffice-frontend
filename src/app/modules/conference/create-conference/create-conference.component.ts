import { Component, OnInit,ViewChild, ElementRef, Directive } from '@angular/core';
import {ConfService} from "../../../services/conferences/conf.service";
import {UserService} from "../../../services/global/user.service";
import {FormBuilder, FormArray, FormControl, Validators} from '@angular/forms';
import { DatePipe } from '@angular/common';
import {User} from "../../../interfaces/user";
import {Room} from "../../../interfaces/room";
import {Conference} from "../../../interfaces/conference";
import {Router, ActivatedRoute} from "@angular/router";
import {UtilsService} from "../../../services/global/utils.service";

import api_urls from 'src/app/constants/api_urls';
declare var $:any;

@Component({
  selector: 'app-create-conference',
  templateUrl: './create-conference.component.html',
  styleUrls: ['./create-conference.component.css']
})
export class CreateConferenceComponent implements OnInit {

  conference: Conference = {id: -1, title:'' ,assignedUsers: [],
    creator: -1,duration: 0, startDate: new Date(Date.now()), time: 0,meeting_room:-1, conversation: null, call: 0};
  users: {[key: number]: User} = {};
  meetingRooms : Room[] = [];
  
  usedMeetingRoom:any = [];
 // meetingRooms : {[key: number]: Room} = {};

  smallProfileImage: string = api_urls.ProfileImageSmall;

  conferenceCreateForm: any;
  errors:any;
  filters: any;
  //designations: any;
  minDate = new Date();
  conferences: {[key: number]: Conference} = {};

  //usedMeetingRoom:any = [];
	constructor(private activatedRoute: ActivatedRoute,
	              public userService:UserService, private confservice : ConfService,
                public datepipe: DatePipe,private router: Router,
                private formBuilder: FormBuilder,private utils: UtilsService
              )
              {
                  this.conferenceCreateForm = this.formBuilder.group(
      { title: '',duration:2, date: new Date(Date.now()), room: -1}
              );

     
      
      
    	/*confservice.onEditConference.subscribe((conference:any) => {

		  // this.conference = conference;
		  this.conferenceCreateForm = this.formBuilder.group({

			//default values for form
			title:  ['', [Validators.required]],
			duration: ['', [Validators.required]],
			room: ['', [Validators.required]],
			date: [this.datepipe.transform('',
        'yyyy-MM-ddThh:mm'),[Validators.required]],
		});

	});*/
  }

  ngOnInit(): void {
    
    //this.getDesignations();
    /*this.confservice.databack.subscribe(res=>{
      console.log("sheredata",res);
      this.conferenceCreateForm.patchValue({
         title: res.value.title,
         duration: res.value.duration,        
         date : res.value.startDate,   
      }  )
    });*/
    this.getUsers();    
   
    this.confservice.getConferences().subscribe(conferences => {
      this.conferences = conferences;
      console.log("meetingdata",this.conference);
      this.activatedRoute.url.subscribe(params =>  {
        
        if(params.length > 0 && params[0].path == "edit") {
          this.activatedRoute.paramMap.subscribe(params =>  {
            var conferenceId: number = parseInt(params.get('cid') as string);
            console.log(params);
            if(conferenceId in this.conferences) {
              this.conference = this.conferences[conferenceId]; 
              
              this.conferenceCreateForm.controls['title'].setValue(this.conference.title);
              this.conferenceCreateForm.controls['duration'].setValue(this.conference.duration / 60);
              this.conferenceCreateForm.controls['room'].setValue(this.conference.meeting_room);
              this.conferenceCreateForm.controls['date'].setValue(this.conference.startDate);
              console.log("get value", this.conference.meeting_room);
              
             this.updateMeetingRooms();
            } else {
              this.router.navigate(['conferences']);
            }
          });
        } else {
          
         this.updateMeetingRooms();
        }
      });
      
    //   if(Object.keys(this.conferences).length) {
    //     for (var key of Object.keys(this.conferences)) {
    //       this.usedMeetingRoom.push(this.conferences[key].meeting_room)
    //     }
    //   }
    //   console.log(this.usedMeetingRoom)
    //   this.userService.meetingRooms.subscribe((meetingRooms: {[key: number]: Room}) => {
    //     if(this.usedMeetingRoom.length && Object.keys(meetingRooms).length) {
    //       for (var key of Object.keys(meetingRooms)) {
    //         if(this.usedMeetingRoom.includes(parseInt(key))) {
    //           delete meetingRooms[key];
    //         }
    //       }
    //     }
    //     this.meetingRooms = meetingRooms;
    //   });
    });
    this.filters = []
  }

  ngAfterViewInit(): void {
    
    $('.create-conference-contactlist > ul').slimScroll({
      height: 'calc(100%)',
      width: '100%',
      color: '#fff'
    });
    
  }

  /*getDesignations() {
    this.userService.getDesignations().subscribe(res => {
      this.designations = res;
    })
  }*/

  getUsers(): void {

	  this.userService.getUsers()
      .subscribe((users) => {
        this.users = users;
        }
      );
  }

  toggleUser(userId: number) : void {
	  this.modifyUserList(userId, !this.conferenceHasUser(userId));
  }

  modifyUserList(userId: number, addUser: boolean) : void {
	if(addUser) {
		this.conference.assignedUsers.push(userId);
		this.isValid();
	} else {
		const index = this.conference.assignedUsers.indexOf(userId, 0);
		if (index > -1) {
		   this.conference.assignedUsers.splice(index, 1);
		   this.isValid();
		}

	}

  }
   saveConference() : void {

	  const date = new Date(this.conferenceCreateForm.value.date);

	  this.conference.title = this.conferenceCreateForm.value.title;
	  this.conference.duration = this.conferenceCreateForm.value.duration * 60;
	  this.conference.startDate = date;
	  this.conference.meeting_room = this.conferenceCreateForm.value.room;

	  if(this.isValid()){
	    this.confservice.saveConference(this.conference, 
			() => this.router.navigate(['conferences']),
			(e:any) => {
        this.errors['assigned_users'] = 'Meeting Room time slot used';
      }
		);
	    
    }

  }

  conferenceHasUser(id: number){
      return this.conference.assignedUsers.indexOf(id)  > -1;
  }

  fullname(usr: any, type=true){
	 return this.utils.fullname(usr, type)
  }

  abbreviate(text: string){
	  return this.utils.abbreviate(text)
  }

  updateMeetingRooms() {
    const date = new Date(this.conferenceCreateForm.value.date);
    const duration = this.conferenceCreateForm.value.duration * 60;
    this.confservice.getAvailableMeetingRooms(date, duration, this.conference.id).subscribe((data: any) => {
      
      this.meetingRooms = data;
      console.log("hello_metting",this.meetingRooms);
      if(this.conference.meeting_room != -1) {
       var room = this.meetingRooms.find((e) => e.id == this.conference.meeting_room);
        
        console.log(room);
        if(room != undefined) {
          this.conferenceCreateForm.controls['room'].setValue(room.id);
        }
      } else {
        if(this.conferenceCreateForm.value.room == -1 && this.meetingRooms.length > 0) {
                  
          this.conferenceCreateForm.controls['room'].setValue(this.meetingRooms[0].id);
        }
      }
    });
  }
  dateChanged() {
    
    this.updateMeetingRooms();
    this.isValid();
  }
	isValid(): boolean {
	  this.errors = {};
	  const date = new Date(this.conferenceCreateForm.value.date);
	  if(this.conferenceCreateForm.value.title.length === 0){
	    this.errors['title'] = 'Title cannot be empty.'
    }
	    // @ts-ignore

    if(isNaN(date.getDate())){
	      this.errors['date'] = 'Invalid Date.'
    }
    if (this.conference.assignedUsers.length < 2){
      this.errors['assigned_users'] = 'Please select two or more contacts.'
    }
    return Object.keys(this.errors).length === 0;
  }

  filtered(id:number){
	  // @ts-ignore
	  return this.filters.includes(id)
  }

  search(event:any){
	  const value = event.target.value.toLowerCase();
	  const len = event.target.value.length;
	  if(len>0){

	    // @ts-ignore
      let users = this.users;
      for (let k of Object.keys(users)){
        // @ts-ignore
        const fullname = this.fullname(this.users[k],false)
        const K = parseInt(k);

        if(fullname.toLowerCase().slice(0, len) === value && this.filtered(K)){
           // @ts-ignore
          this.filters = this.filters.filter(i => i !== K);
        }

        if(fullname.toLowerCase().slice(0, len) !== value){
          this.filters.push(K)
        }


      }
    }else {
	    this.filters = [];
    }

  }

  //designation(designations, id) {    
  //  return this.utils.designationName(designations, id);    
  //}
}
