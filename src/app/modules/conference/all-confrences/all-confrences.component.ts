import { Component, OnInit } from '@angular/core';

import {ConfService} from "../../../services/conferences/conf.service";
import {Conference} from "../../../interfaces/conference";

@Component({
  selector: 'app-all-confrences',
  templateUrl: './all-confrences.component.html',
  styleUrls: ['./all-confrences.component.css']
})
export class AllConfrencesComponent implements OnInit {

  conferences: {[key: number]: Conference} = {};
  constructor(private confService: ConfService) { }

  ngOnInit(): void {
    
    this.confService.getAllConferences().subscribe((data :any) => {

      for(var c in data) {

        var conference = data[c];
        this.conferences[conference.id] = this.confService.parseConference(conference);
      }
    })
  }

}
