import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllConfrencesComponent } from './all-confrences.component';

describe('AllConfrencesComponent', () => {
  let component: AllConfrencesComponent;
  let fixture: ComponentFixture<AllConfrencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllConfrencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllConfrencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
