import { Component, OnInit, Input } from '@angular/core';

import { CallPeer } from '../../../call/call2';

@Component({
  selector: 'app-conference-grid-element',
  templateUrl: './conference-grid-element.component.html',
  styleUrls: ['./conference-grid-element.component.css']
})
export class ConferenceGridElementComponent implements OnInit {
  
  @Input() connection : CallPeer | null = null;
  @Input() name : string = "";
  @Input() ownUser : boolean = false;
  
  constructor() { }

  ngOnInit(): void {    
  }
  hasAudio(): boolean{
    return this.connection!.hasAudio();
  }
}
