import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConferenceGridElementComponent } from './conference-grid-element.component';

describe('ConferenceGridElementComponent', () => {
  let component: ConferenceGridElementComponent;
  let fixture: ComponentFixture<ConferenceGridElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConferenceGridElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferenceGridElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
