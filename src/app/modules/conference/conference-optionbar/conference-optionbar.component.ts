import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Call2 } from 'src/app/call/call2';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Conference } from 'src/app/interfaces/conference';
import { UserService } from 'src/app/services/global/user.service';
import { User } from 'src/app/interfaces/user';
import api_urls from 'src/app/constants/api_urls';

declare var $: any;

@Component({
  selector: 'app-conference-optionbar',
  templateUrl: './conference-optionbar.component.html',
  styleUrls: ['./conference-optionbar.component.css']
})
export class ConferenceOptionbarComponent implements OnInit {

  @Output() openPeopleTab = new EventEmitter;
  activeCall: Call2 | null = null;

  audioUrl:any;

  conferenceId: number = -1;
  conferences : {[key: number]: Conference} = {};
  currentConf : Conference | null =  null;
  peopleTab = false;
  filters: any[] = [];
  users: {[key: number]: User} = {};
  smallProfileImage: string = api_urls.ProfileImageSmall;

  constructor(private confService: ConfService, private sanitizer:DomSanitizer,
    private activatedRoute: ActivatedRoute, public userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
    this.confService.activeCallObservable.subscribe((newCall: any) => {
      this.activeCall = newCall;
    });

    this.activatedRoute.paramMap.subscribe(params =>  {

	    var conferenceId: any = params.get('cid');
	    this.conferenceId = parseInt(conferenceId);
	    this.confService.getConferences().subscribe((cfs: {[key: number]: Conference}) => {
        this.conferences = cfs;
        this.currentConf = this.conferences[this.conferenceId];
		  });
  	});
  }

  getUsers(): void {
	  this.userService.getUsers()
      .subscribe(users => this.users = users);
  }
 
  addScrollBar(): void {
    $('.create-conference-contactlist > ul').slimScroll({
      height: 'calc(100%)',
      width: '100%',
      color: '#5d5d5d'
    });
  }



  //Disconnect
  disconnect(): void {
    if (this.activeCall == null) {
      return;
    }
    if(this.isRecording()) {
      this.toggleRecord();
      return;
    }
    this.confService.leaveCall();
  }

  isMuted(): boolean {
    if (this.activeCall == null) {
      return true;
    }
    return this.activeCall!.isMuted();
  }

  isCam(): boolean {
    if (this.activeCall == null) {
      return false;
    }
    return this.activeCall!.isCam();
  }
  isScreenshare(): boolean {
    if (this.activeCall == null) {
      return false;
    }
    return this.activeCall!.isScreenshare();
  }

  isRecording(): boolean {
    if(this.activeCall == null) {
       return false;
    }
    return this.activeCall.isRecording();
  }
  toggleRecord(): void {
    if(this.activeCall == null) {
       return;
    }
    if(this.activeCall.recordings.length > 0) return;
    this.activeCall.toggleRecord();
  }

  toggleAudio(): void {

		if(this.activeCall == null) {
			return;
		}
		this.activeCall!.toogleMuteAudio();
	}

	//ScreenShare
	toggleScreenShare(): void {

		if(this.activeCall == null) {
			return;
		}
		this.activeCall.toogleScreenshare();
  }
  
  // Video
	toggleVideo(): void {

		if(this.activeCall == null) {
			return;
		}
		this.activeCall.toogleCam();
  }
  
  enableGridView(): void {
    this.confService.showGrid = true;
  }
  enablePresentationView(): void {
    this.confService.showGrid = false;
  }
  isGridView(): boolean {
    if(this.activeCall == null) {
      return true;
    }
    return this.confService.showGrid && this.activeCall.sharingPeer == null;
  }
  onAddUserBtnClick() {
    this.peopleTab = true;
    this.addScrollBar();
  }

  getRecordingUrl(recording: any) {
    if(!this.audioUrl) {
      this.audioUrl = this.sanitizer.bypassSecurityTrustUrl(recording.url);
      return this.audioUrl;
    }
    return this.audioUrl;
  }

  removeRecording(id: number) {
    if(this.activeCall == null) {
       return;
    }
    this.activeCall.removeRecording(id);
    this.audioUrl = null;
  }
  closePeopleTab() {
    this.peopleTab = false;
  }

  get conference(): any {
    return this.conferences[this.conferenceId]
  }

  isAssigned(id: string){
    return this.conference.assignedUsers.includes(parseInt(id))
  }

  filtered(id:number){
	  // @ts-ignore
	  return this.filters.includes(id)
  }

  fullname(user:any){
    return `${user.firstname} ${user.lastname}`;
  }

  search(event:any){
	  const value = event.target.value.toLowerCase();
	  const len = event.target.value.length;
	  if(len>0){

	    // @ts-ignore
      let users = this.users;
      for (let k of Object.keys(users)){
        // @ts-ignore
        const fullname = this.fullname(this.users[k],false)
        const K = parseInt(k);

        if(fullname.toLowerCase().slice(0, len) === value && this.filtered(K)){
           // @ts-ignore
          this.filters = this.filters.filter(i => i !== K);
        }

        if(fullname.toLowerCase().slice(0, len) !== value){
          this.filters.push(K)
        }


      }
    }else {
	    this.filters = [];
    }

  }

  assign(id: string){
    const userId =  parseInt(id);
    this.modifyUserList(userId, true);
  }
  unassign(id:string){
    const userId =  parseInt(id);
    this.modifyUserList(userId, false);

  }

  modifyUserList(userId: number, addUser: boolean) : void {

    if (addUser) {
      this.conference.assignedUsers.push(userId);
    } else {
      const index = this.conference.assignedUsers.indexOf(userId, 0);
      if (index > -1) {
        this.conference.assignedUsers.splice(index, 1);
      }
    }
    this.confService.saveConference(this.conference, () => {}, () => {});
  }
}
