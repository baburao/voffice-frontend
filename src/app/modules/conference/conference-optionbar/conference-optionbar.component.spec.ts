import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConferenceOptionbarComponent } from './conference-optionbar.component';

describe('ConferenceOptionbarComponent', () => {
  let component: ConferenceOptionbarComponent;
  let fixture: ComponentFixture<ConferenceOptionbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConferenceOptionbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferenceOptionbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
