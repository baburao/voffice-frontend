import { Component, OnInit, Input, Renderer2, ElementRef } from '@angular/core';
import { UserService } from '../../../services/global/user.service';
import { ConversationService } from '../../../services/global/conversation.service'

import api_urls from 'src/app/constants/api_urls';
import { Conversation } from 'src/app/interfaces/conversation';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { ChatbarComponent } from 'src/app/layouts/chatbar/chatbar.component';
import { OfficeService } from 'src/app/services/global/office.service';

declare var $: any;

@Component({
  selector: 'app-room-slot',
  templateUrl: './room-slot.component.html',
  styleUrls: ['./room-slot.component.css']
})
export class RoomSlotComponent implements OnInit {

  @Input() room: any = null;
  @Input() userId: number = -1;
  @Input() isSeated: boolean = false;
  @Input() isWorkplace: boolean = false;
  @Input() selectedTeam: any;

  smallImageAddress: string = api_urls.ProfileImageSmall;
  conversations: Conversation[] = [];
  currentConversations: Conversation | null = null;

  chatDrawerClass = 'chatbox-right-toggle-open';
  contactClass = 'contactsbox-toggle-open';

  constructor(public userService: UserService, private renderer: Renderer2,
    public conversationService: ConversationService, private conferenceService: ConfService, private officeService: OfficeService,private el: ElementRef) { }

  ngOnInit(): void {
    this.conversationService.conversations.subscribe(
      (conversations: Conversation[]) => {
        this.conversations = conversations;
      });
    
      this.conversationService.currentConversation.subscribe((newConversation: Conversation | null) => {  
        this.currentConversations = newConversation;
      });
      
      if(this.room != null) {
        
        
        $(this.el.nativeElement).css('flex-basis' , (100.0 / (this.room.width / 2.0)) + '%')
        $(this.el.nativeElement).css('max-width' , (100.0 / (this.room.width / 2.0)) + '%')
        $(this.el.nativeElement).css('height' , (100.0 / ((this.room.height - 1) / 2.0) ) + '%')
        
      }
  }

  makeCallClick(userId: number) {
    this.startChatClick(userId, true);
   
  }
  startChatClick(targetUserId: number, isCall=false) {
    if (this.isNewChat(targetUserId)) {
      this.conversationService.createConversation(targetUserId).subscribe((conversation: Conversation) => {
        this.currentConversations = conversation;
        this.selectConversation(conversation.id);
        this.toggleChatDrawer(conversation.id, false, false, false, conversation)
        if(isCall) {
          this.toggleChatDrawer(this.currentConversations!.id, true, false, false, this.currentConversations)
          this.conferenceService.callUser(this.currentConversations!.id, false);
        }
      });
    } else {
      if(isCall) {
        this.toggleChatDrawer(this.currentConversations!.id, true, false, false, this.currentConversations)
        this.conferenceService.callUser(this.currentConversations!.id, false);
      }
    }
  }

  isNewChat(targetUserId: number) {
    for (let i = 0; i < this.conversations.length; i++) {
      if (this.conversations[i].users.length == 2 && this.conversations[i].users.includes(targetUserId) && this.conversations[i].users.includes(this.userService.currentUser!.id)) {
        this.currentConversations = this.conversations[i];
        this.toggleChatDrawer(this.conversations[i].id, false, false, false, this.conversations[i])
        return false;
      }
    }
    return true;
  }

  selectConversation(conversationId: number) {
    this.conversationService.selectConversation(conversationId);
  }

  toggleChatDrawer(id = -1, contactDrawer = false, force = false, action = false, conversation: any = null) { // toggles Chat Bar and selects Conversation
    let isDirect = this.conversationService.isOneToOne(conversation)
    this.toggleContacts();
    if (id > 0) {
      this.selectConversation(id);
    } else {
      this.selectConversation(-1);
    }
    let hasClass;
    if (isDirect) {
      hasClass = this.hasClass(this.chatDrawerClass);

      if (force) { hasClass = action; }

      if (hasClass) {
        this.removeClass(this.chatDrawerClass);
        this.addClass(this.chatDrawerClass);
      } else {
        this.addClass(this.chatDrawerClass);
      }
    }

  }

  toggleContacts() { // toggles Contact list.
    const hasClass = this.hasClass(this.contactClass);
    if (hasClass) {
      this.removeClass(this.contactClass);
    }
  }

  hasClass(cls: string) { // checks if class exists
    return document.body.classList.contains(cls);
  }

  addClass(cls: string) { // adds class to body
    this.renderer.addClass(document.body, cls);
  }

  removeClass(cls: string) { // removes class from Body
    this.renderer.removeClass(document.body, cls);
  }
}
