import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { OfficeViewComponent } from './office-view/office-view.component';
import { OfficeRoutes } from './office-routes';
import { OfficeLayoutComponent, OfficeElementDirective } from './office-layout/office-layout.component';
import { FormsModule } from '@angular/forms';
import { RoomSlotComponent } from './room-slot/room-slot.component';
import { CalenderComponent } from './calender/calender.component';

import { CalendarModule ,DateAdapter} from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


@NgModule({
  declarations: [OfficeViewComponent, OfficeLayoutComponent, OfficeElementDirective, RoomSlotComponent,CalenderComponent],
  imports: [
  RouterModule.forChild(OfficeRoutes),
  CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
  CommonModule, FormsModule
  ],

  exports: [RoomSlotComponent]
})
export class OfficeModule { }
