import { Component, OnInit } from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import { Conference } from 'src/app/interfaces/conference';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { UserService } from 'src/app/services/global/user.service';
import { User } from 'src/app/interfaces/user';
import { Router } from '@angular/router';
import { id } from 'date-fns/locale';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit {
  ImagePath!: string;

  events: CalendarEvent[] = [];
  notes: any = [
    {
      date: '2020-10-03',
      note: 'This is custom notes'
    }
  ];

  view: CalendarView = CalendarView.Month;
  user: User;
  modal: any;
  modalData: { event: CalendarEvent<any>; action: string } | undefined;
  constructor(
    private confservice: ConfService,
    private userService: UserService,
    private router: Router
  ) {
    this.user = userService.currentUser;
  }

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  ngOnInit(): void {
    this.getConferences();
  }

  refresh: Subject<any> = new Subject();

  activeDayIsOpen: boolean = false;
  addNote: boolean = false;

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.addNote = true;
      this.viewDate = date;
    }
  }

  getConferences(): void {
    this.confservice.getConferences().subscribe(conferences => {
      this.events = Object.values(conferences).map(conference => {
        var isCreater = this.user.id == conference.creator;

        return {
          start: new Date(conference.startDate),
          title: conference.title,
          draggable: isCreater,
          actions: this.getActions(isCreater, conference),
          allDay: false,
          meta: {
            conference
          }
        };
      });
    });
  }
  getActions(isCreater: boolean, conference: Conference): any {
    var actions: CalendarEventAction[] = [];
    if (conference.time < 0)
      actions.push({
        label: '  <div class="btn btn-primary" >Join</div>',
        a11yLabel: 'Join',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.join(conference.id);
        }
      });

    if (isCreater)
      actions.push({
        label:
          '<i class="fa fa-pencil  fa-2x white-bg" aria-hidden="true"></i>',
        a11yLabel: 'Delete',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.handleEvent(conference.id, event);
        }
      });

    if (isCreater)
      actions.push({
        label: '<i class="fa fa-trash fa-2x white-bg" ></i>',
        a11yLabel: 'Delete',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.deleteEvent(event);
        }
      });
    return actions;
  }
  handleEvent(id: any, event: CalendarEvent): void {
    this.router.navigate(['conferences/edit/' + id]);
  }
  modalContent(modalContent: any, arg1: { size: string }) {
    throw new Error('Method not implemented.');
  }
  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    var conference: Conference = event.meta.conference;
    conference.startDate = newStart;
    this.confservice.saveConference(
      conference,
      () => console.log('Success'),
      (e: any) => {
        console.log('Fail');
      }
    );
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.confservice.deleteConference(eventToDelete.meta.conference.id);
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  join(id: number) {
    this.router.navigate(['conferences/join/' + id]);
  }

  addNotes() {
    var date=(document.getElementById("date") as HTMLInputElement).value; 
    var note=(document.getElementById("date") as HTMLInputElement).value; 

    this.notes.push({
      date: date,
      note: note
    });
    this.refresh.next();
  }
  getNote(date:any): string{
    var note: string = '';
    
    var noteItem=(this.notes.find(element => element.date == date));
    if(noteItem!=undefined && noteItem!=null){
      note=noteItem.note;
    }

    return note;
  }
}
