import { Routes } from '@angular/router';
import { CalenderComponent } from './calender/calender.component';
import {OfficeViewComponent} from './office-view/office-view.component';

export const OfficeRoutes: Routes = [
  {
    path: '',
    data: { breadcrumbs: 'office' },
    children: [
      { path: '',  component: OfficeViewComponent, },
      {
        path: 'calender',
        component: CalenderComponent
      },
    ]
  },
  {
    path: 'edit',
    component: OfficeViewComponent
  },
 
];

