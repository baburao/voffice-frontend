import {Message } from './message'

export interface Conversation {

	//conversation id
	  id: number;
    topic: string;
	//Ids of all users in this conversation
    users: number[];
	//the latest message send in this conversation, null if no message was send
    latest: Message | null;
	//List of all received messages, will be filled if this conversation was set as the current conversation
    messages: Message[] | null;
	unread: number;
	  is_conference: boolean;
}
