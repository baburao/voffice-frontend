export interface Department {
  id: number;
  name: string;
  users: number[];
}
export interface Branch {
  id: number;
  name: string;
  departments: Department[];
}
export interface Company {
  //id: number;
  name: string;
  branches: Branch[];
}