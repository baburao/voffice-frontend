export interface TeamDepartment {
  id: number;
  name: string;
  users: number[];
}
export interface Team {
 	id: number;
  name: string;
  creator: number;  
  members: number[];
  departments: TeamDepartment[];
}