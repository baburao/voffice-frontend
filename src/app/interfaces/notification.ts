export interface Notification {
  id: number;
  title: string;
  content: string;
  has_read: boolean;
  type: string; // Y , G , R , available types
  icon: string;
  iconType: string; // T , I ( T : Text , I : Icon)
  buttonText: string;
  buttonLink: string;
  category: string; // conference , greeting etc.
  data: any;
}

