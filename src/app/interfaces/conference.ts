import { User } from './user';
import { Conversation } from './conversation';

type Nullable<T> = T | null;

export interface Conference {
  id: number;
  title: string;
  // Start date, data and time
  startDate: Date;
  creator: number;//any|User;
  // Planned duration of the conference
  duration: number;
  // List of user IDs assigned to this conference
  assignedUsers: number[];
  // Time left until the conference starts, will be updated refequently by service
  time: number;

  meeting_room: number;

  conversation: Conversation | null;

  call: number;
}

