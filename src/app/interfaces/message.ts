
export interface Message {
	//Message id
	id: number; 
	//id of the user who send this message
	sender: number; 
	//type of message(text/file/audio/call)
	type: string;
	//When the message was sent
    date : Date, 
	//text content
	text: string; 
	//id of the file or audio recording, not used by text messages
	data_id: number;
	//if the message was never read by the user
	new_message: boolean;
  //for call messages, stores if call was accepted and the end data
  call: any;
}