export interface Room {
 	id: number;
	//Type of this room
	//of = office
	//de = department room
	//me = meeting room
	//re = reception	
    type: string;
	//Name of the room
    name: string;

	//number of slots in this room
    size: number;

	//id of the room template
    template: number;
	
	//User id of the owner of this room
	//Only applies to offices
	owner: number;
	
	//List of user IDs
	//No user in slot = -1
	slots: number[];
	
	//Mapping from slot index to user id
	//Only applies for department rooms
	workspaces: number[];
  //set with user ids of the department heads
	heads: {};
	
	x: number;
	y: number;
	width: number;
	height: number;
}