//export class UserAuth {
//  token!: string;
//}

export interface User {
  id: number;
  // email: string;
  firstname: string;
  lastname: string;
  //profileImage: number;
  // id of this users designation
  designation: number;
  is_online: boolean;
  //token: string;
}

export interface CurrentUser extends User{
	
  email: string;
  token: string;
}


export interface Designation {
  id: number;
  name: string;
}