declare var MediaRecorder: any;

export class CallRecorder {
	
	mediaRecorder: any = null;
	recordedBlobs: any[] = [];
	buffer: any = null;
	//this.audioElement = inAudioElement;
	availableEvent: any = null;
	recordDestination: any = null;
	
	constructor(private audioContext = new AudioContext()) {
		
		this.recordDestination = audioContext.createMediaStreamDestination();
	}
	addStream(sourceStream: any) {
		
		const source = this.audioContext.createMediaStreamSource(sourceStream);
    source.connect(this.recordDestination);
	}
	record(): void {
		
		var self = this;		

		this.mediaRecorder = new MediaRecorder(this.recordDestination.stream);
		
		this.mediaRecorder.onstop = (event: any) => {
		    
			//console.log(self.recordedBlobs);
			if(self.recordedBlobs.length <= 0) {
				return;
			}			
			
			self.buffer = new Blob(self.recordedBlobs, { 'type' : self.recordedBlobs[0].type });

			//window.open(URL.createObjectURL(self.buffer), 'Download');
			var url = URL.createObjectURL(self.buffer);
			
			/*var a = document.createElement("a");
			document.body.appendChild(a);
			a.href = url;
	        a.download = "rec.webm";
	        a.click();
			*/
			self.availableEvent(url, self.buffer);			
		};
		
		self.mediaRecorder.ondataavailable = (event: any) => {
						
			if (event.data && event.data.size > 0) {				
			
				self.recordedBlobs.push(event.data);
			}
		};
		self.mediaRecorder.start();	
	}
  
	//handleDataAvailable
	stop(): void {
		this.mediaRecorder.stop();
	}
}