import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Call2, CallPeer } from './call2';
import { Conversation } from './../interfaces/conversation';
import api_urls from 'src/app/constants/api_urls';

export class CallState {
  
  
  static INIT: string = 'init';
  static CALLING: string = 'calling';
  static CALLED: string = 'called';
  static ACTIVE: string = 'active';
  static DECLINED: string = 'declined';
  static ENDED: string = 'ended';
}

export class DirectCalledUser {
  
  accepted = false;
  constructor(public userId: number) {}
}

  
  
export class DirectCall {
  
  
  call: Call2 | null = null;
  _state: string = CallState.INIT;
  get state(){return this._state;};
  
  //calledUsers: {[key: number]: DirectCalledUser} = {};
  //calledUser: DirectCalledUser | null = null;
  targetUser: number = -1
  
  conversation: Conversation | null = null;
  callId: number = -1
  
  updateInterval: any = null;
  startTime: number = 0;
  duration: number = 0;
  
  defaultPickTime: number = 10;
  
  constructor(private currentUserId: number, private http: HttpClient) {    
  }
  incoming(conversation: Conversation, userId: number, callId: number): void {
    this.conversation = conversation;
    this.targetUser = userId;
    this.callId = callId;
    this.startTimer();
    
    this.setState(CallState.CALLED);
  }
  setState(newState: string): void {
    this._state = newState;
  }
  callUser(conversation: Conversation, userId: number , startWithCam: boolean, callAccpeted: any): void {    
    
    this.conversation = conversation;
    this.targetUser = userId;    
    this.setState(CallState.CALLING);
    
    this.startTimer();
    
    this.http.post(api_urls.CallUser, {user: userId, conversation: this.conversation.id}).subscribe((data :any) => {
      
      if(data.called) {        
        
        this.callId = data.call;  
        this.call = new Call2(this.currentUserId, data.call, null,() => {}, 
          (sharingPeer: any ) => {});
        
        this.call.startWithCam = startWithCam;
        this.call.onNewPeer = () => {
          
          //Only call event once, after the first peer accepted
          if(this.state == CallState.CALLING) {
            this.setState(CallState.ACTIVE);
            callAccpeted();
          }
        };
        
        this.call.onPeerLeave = () => {this.onPeerLeave()};
        
        //this.activeCallSource.next(this.activeCall);
        this.call!.join().then(() => {
          
          
        });
        
      } else {
        this.callDeclined(userId);
      }
    });     
  }
  startTimer(): void {
    
    this.startTime = new Date().getTime();
    
    this.updateInterval = setInterval(() => {
      
      this.duration = new Date().getTime() - this.startTime;
      
      if(this.state == CallState.CALLING && this.duration > this.defaultPickTime * 1000) {
        this.callDeclined(this.targetUser);
      }
    } , 1000);
    
  }
  callDeclined(sourceUser: number): void {
    
    this.setState(CallState.DECLINED);
    this.http.post(api_urls.LeaveCall, {call: this.callId}).subscribe((data :any) => {}); 
    this.cleanCall();
  }
  acceptCall(): void {
    
    this.http.post(api_urls.JoinCall, {user: this.targetUser, call: this.callId}).subscribe((data :any) => {
      this.call = new Call2(this.currentUserId, this.callId, null,() => {}, 
          (sharingPeer: any ) => {});  
          
      this.call.onNewPeer = () => {
        //Call when joined
        if(this.state == CallState.CALLED) {
          this.setState(CallState.ACTIVE);
        }
      };
      
      this.call.onPeerLeave = () => {this.onPeerLeave()};
      //this.activeCallSource.next(this.activeCall);
      this.call!.join().then(() => {});
    });
  }
  declineCall(): void {
    
    this.setState(CallState.ENDED);
    this.http.post(api_urls.DeclineCall, {call: this.callId}).subscribe((data :any) => {});    
  }
  private cleanCall(): void {
    
    clearInterval(this.updateInterval);
    if(this.call != null) {
      this.call.leaveCall();
    }  
    
  }
  leaveCall(): void {
    
    //No need to send leave message to the server if declining a call
    if(this.state != CallState.CALLED) {
      this.http.post(api_urls.LeaveCall, {call: this.callId}).subscribe((data :any) => {}); 
    }
    
    this.cleanCall();
    this.setState(CallState.ENDED);
  } 
  onPeerLeave(): void {
    
    console.log("Peer left", this.call )
    if(this.call == null) {
      return;
    } 
    
    this.call.leaveCall();
    this.setState(CallState.ENDED);
  }
  getParticipant(): CallPeer| null {
    if(this.call && this.call.orderedConnections.length <= 0) {
      return null;
    }
    
    return this.call!.orderedConnections[0];
  }
}