import { ServerConnection } from '../utils/server-connection';
import { CallConnection } from './call-connection';
import { CallRecorder } from './call-recorder';

export class Call {
	private smallRes = true;

	//public connections: CallConnection[] = [];
	public connections: {[key: number]: CallConnection} = {};
	outputStream: any = null;

	onConnected: any = null;
	onLeave: any = null;
	
	private joined: boolean = false;
		
	private sendingVideo: boolean = false;
	private streamingCam: boolean = false;
	
	private callRecorder: CallRecorder | null = null;
	
	constructor(public socket: ServerConnection, public conferenceId: number) {
	}

	processSignal(data: any) {

		if(data.signal == 'join') {

			//participants.push(data.user);
			//updateParticipants();
		} else if(data.signal == 'enter') {
			
			//participants = data.users;
			//updateParticipants();
			if(this.conferenceId != data.id) {
				console.log("Conference Error" , data);
			}

			for(var i = 0;i < data.users.length;i++) {
	
				console.log('add init ' + data.users[i]);
				this.addConnection(data.users[i]);
			}

		} else if(data.signal == 'leave') {
			
			console.log('leave ' + data.user);
			this.removeConnection(data.user);
		} else if(data.signal == 'offer') {

			var connection = this.getConnection(data.id);

			if(connection == null) {
				
				console.log('add ' + data.id);
				this.addConnectionByOffer(data.id, data.data);
			} else {
				connection.setRemoteDescription(data.data);
			}
		} else if(data.signal == 'answer') {

			this.setAnswer(data.id, data.data);
		} else if(data.signal == 'ice') {

			this.setCandidate(data.id, data.data);
		}

	}
	leaveCall(): void {
		if(this.joined) {
			this.socket.send({type: 'signal', signal: 'leave'});
			this.joined = false;
		}
	}
	mediaAllowed(): void {

		if(this.outputStream == null) {
			return;
		}		
		if(!this.joined) {
			this.socket.send({type: 'signal', signal: 'join', call: this.conferenceId});
			this.joined = true;
		} else {

			for(let c in this.connections) {
				this.connections[c].resetVideo();
			}
			/*for(let i = 0; i < this.connections.length; i++) {

				this.connections[i].resetVideo();
			}*/
		}
	}
	connected(connection: any, stream: any) {

		if(this.onConnected != null) {
			this.onConnected(connection, stream);
		}
	}

	async addMedia(withVideo: boolean) {

		this.outputStream = await this.initUserMedia(withVideo);

		if(this.outputStream == null) {
			return;
		}
		this.streamingCam = withVideo;
		this.sendingVideo = withVideo;
		
		this.mediaAllowed();
	}
	async addScreenMedia() {

		this.outputStream = await this.initScreenShare();
		
		if(this.outputStream == null) {
				return;
		}
		this.streamingCam = false;
		this.sendingVideo = true;
		
		//Try to access the mic but proceed if it fails
		try {

			var audioStream = await this.initUserMedia(false);	
			
			if(audioStream.getAudioTracks().length > 0) {

				this.outputStream.addTrack(audioStream.getAudioTracks()[0]);
			}
		} catch(e) {
		}

		this.mediaAllowed();
	}

	removeVideo() {
		this.sendingVideo = false;
	
		for(let c in this.connections) {
			console.log(c);
			this.connections[c].removeVideo();
		}
		/*
		for(let i = 0; i < this.connections.length; i++) {
			this.connections[i].removeVideo();
		}*/
	}

	toogleMuteAudio(): void {
		if(this.outputStream == null || this.outputStream.getAudioTracks().length <= 0) {
			return;
		}

		var audioTrack = this.outputStream.getAudioTracks()[0];

		audioTrack.enabled = !audioTrack.enabled;
	}
	isMuted(): boolean {

		if(this.outputStream == null || this.outputStream.getAudioTracks().length <= 0) {
			return true;
		}
		var audioTrack = this.outputStream.getAudioTracks()[0];

		return !audioTrack.enabled;
	}
	
	isCam(): boolean {		
		
		return this.sendingVideo && this.streamingCam;		
	}
	isScreenshare(): boolean {		
		
		return this.sendingVideo && !this.streamingCam;		
	}
	hasAudio(): boolean {
		if(this.outputStream == null) {
			return false;
		}
		
		return  this.outputStream.getAudioTracks().length > 0;
		
	}
	addConnection(id: number): any {

		var newConnection = new CallConnection(id, this);
		this.connections[id] = newConnection;
		newConnection.offerConnection();
		return newConnection;
	}
	addConnectionByOffer(id: number , offer: any): any {

		//self.removeConnection(message.id);

		var newConnection = new CallConnection(id, this);
		newConnection.setRemoteDescription(offer);		
		this.connections[id] = newConnection;
		
		//this.connections.push(newConnection);

		return newConnection;
	}
	setAnswer(id: number, answer: any) {

		var connection = this.getConnection(id);
		if(connection != null) {
			connection.setAnswer(answer);
		}
	}
	setCandidate(id: number, candidate: any) {

		var connection = this.getConnection(id);
		if(connection != null) {

			connection.peerConnection.addIceCandidate(candidate).catch((e: any) => {
				console.error("ICE error: ", e);
		    });
		}
	}
	getConnection(id: number) {

		return this.connections[id];
		/*var index = this.connections.findIndex((element) => element.targetId == id);

		if(index > -1) {
			return this.connections[index];
		}
		return null;*/
	}
	removeConnection(id: number) {

		var oldConnection = this.connections[id];
		
		delete this.connections[id];
		
		/*
		var index = this.connections.findIndex((element) => element.targetId = id);

		var oldConnection = this.connections[index];
		if(index > -1) {
			this.connections.splice(index,1);
		}*/

		if(this.onLeave != null) {
			this.onLeave(oldConnection.targetId);
		}
		//.......
	}
	async initUserMedia(withVideo: boolean) {
		var constraints = { audio: true, video: withVideo };

		return await navigator.mediaDevices.getUserMedia(constraints);
	}
	async initScreenShare() {

		let width = 540;
		let height = 320;

		var videoAttribute: any;

		if(this.smallRes) {
			videoAttribute = {width: width, height: height};
		} else {
			videoAttribute = true;
		}

		var n: any = navigator;
		//, width: width, height: height
	    if (n.getDisplayMedia) {
	      return n.getDisplayMedia({video: videoAttribute});
	    } else if (n.mediaDevices.getDisplayMedia) {
	      return n.mediaDevices.getDisplayMedia({video: videoAttribute});
	    } else {
			if(this.smallRes) {
				return n.mediaDevices.getUserMedia({video: {mediaSource: 'screen'}});
			} else {
	     		return n.mediaDevices.getUserMedia({video: {mediaSource: 'screen',
					 width: videoAttribute.width, height: videoAttribute.height}});
			}
	    }
	}
	
	toggleRecord(): void {
		
		if(this.isRecording()) {
			this.stopRecord();
		} else {
			this.record();
		}
	}
	isRecording(): boolean {
		
		return this.callRecorder != null;
	}
	record(): void {
		
		console.log("record...");
		
		this.callRecorder = new CallRecorder();	
		this.callRecorder.availableEvent = function() {
			console.log("rec available");
		}
		this.callRecorder.addStream(this.outputStream);
		for(let c in this.connections) {
			
			this.callRecorder.addStream(this.connections[c].stream);
		}
		
		//this.callRecorder.record();
	}
	stopRecord(): void {
		
		if(this.callRecorder == null) {
			return;
		}
		
		this.callRecorder.stop();
		this.callRecorder = null;
	}
	
}

export class DebugCallConnection extends CallConnection {
	
	video: boolean = false;
	videoId: number = 0;
	
	constructor(public targetId: number, public  call: Call) {
		super(targetId, call);
		
		if(targetId % 2 == 0) {
			this.video = true;
			this.videoId = Math.floor(Math.random() * 4.0);
		}
	}
	
	async init() {}

	setVideoOutput(videoPlayer: any): void {
		if(this.video) {
			videoPlayer!.src = "http://localhost:4200/assets/test/vid" + this.videoId + ".mp4";
			videoPlayer.loop = true;
			videoPlayer.load();
	   		videoPlayer.play();
		}
	}
	addMedia(): void {}
	resetVideo(): void {}
	removeVideo(): void {}
	async offerConnection() {}

	async setRemoteDescription(offer: any) {}
	async setAnswer(answer: any) {}		
	
	send (data: any) {}
	
	hasVideo() : boolean {return this.video;}
	hasAudio() : boolean {return true;}
}

export class DebugCall extends Call {
	
	muted: boolean =  false;
	cam: boolean =  false;
	screen: boolean =  false;
	
	constructor(socket: ServerConnection, conference: any) {
		super(socket, conference.id);	
		
		for(var u in conference.assignedUsers) {
			this.connections[conference.assignedUsers[u]] = new DebugCallConnection(conference.assignedUsers[u], this);
			
			console.log(conference.assignedUsers[u] + " debug")
		}
	}
	
	processSignal(data: any) {}
	leaveCall(): void {}
	mediaAllowed(): void {}
	connected(connection: any, stream: any) {}
	

	async addMedia(withVideo: boolean) { 
		this.muted = false;
		this.screen = false;
		this.cam = withVideo;
	}
	async addScreenMedia() {
		this.screen = true;
		this.cam = false;
	}

	removeVideo() {
		this.screen = false;
		this.cam = false;		
	}

	toogleMuteAudio(): void {}
	isMuted(): boolean {return this.muted;}
	
	isCam(): boolean {return this.cam;}
	isScreenshare(): boolean {return this.screen;}
	hasAudio(): boolean {return this.muted;}
	
	addConnection(id: number): any {return null;}
	addConnectionByOffer(id: number , offer: any): any {return null;}
	setAnswer(id: number, answer: any) {}
	setCandidate(id: number, candidate: any) {}
	//getConnection(id: number) {return null;}
	removeConnection(id: number) {}
	//async initUserMedia(withVideo: boolean) {return null;}
	//async initScreenShare() {return null;}
	
	toggleRecord(): void {}
	isRecording(): boolean {return false;}
	record(): void {}
	stopRecord(): void {}
}
