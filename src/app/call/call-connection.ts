import { Call } from './call';

export class CallConnection {
		
	peerConnection: any = null;
	
	public stream : any = null;
	public onStream: any = null;
		
	public onAudioMeterValue: any = null;	
	public audioMeter: number = -1;
	
	
	constructor(public targetId: number, public  call: Call) {
		
	}
	async init() {
		
		if(this.peerConnection != null) {
			return;
		}
		/*
		self.videoElement = document.createElement("video");
		self.videoElement.style = "width: 50%;height: auto;border-radius: 1px;"
		self.videoElement.setAttribute("playsinline", true);				
		self.videoElement.setAttribute("controls", true);				
		self.videoElement.setAttribute("autoplay", true);				
		
		$("#videoElements").append(self.videoElement);
		*/
		const config = {
			iceServers: [{
				urls:"stun:stun.l.google.com:19302"
			}]		    
		};
		var options: any = {};
				
		this.peerConnection = new RTCPeerConnection(config);//, options);	
		
		var self = this;
		
		this.peerConnection.ontrack = function(e: any){

			if(e.streams.length > 0 ){
				
				console.log('onTrack ', self.targetId, e.streams);
				//if (self.videoElement.srcObject !== e.streams[0]) 
					
				self.stream = e.streams[0];	
		
				if(self.onStream != null) {	
					self.onStream(self.stream);
				}				
				
				if(self.stream.getAudioTracks().length == 0 || self.onAudioMeterValue == null) {
					return;
				}
				var w: any = window;
				var audioContext: any = null;
				try {
				  var AudioContext = w.AudioContext || w.webkitAudioContext;
				  audioContext = new AudioContext();
				} catch (e) {
				  console.log('Web Audio API not supported.');
				}

				const soundMeter = new SoundMeter(audioContext);
				soundMeter.connectToSource(self.stream, function(e:any) {
				    if (e) {
				      console.log(e);
				      return;
				    }
				    setInterval(() => {
					
						self.onAudioMeterValue(soundMeter.instant);
						//self.audioMeter = soundMeter.instant;
						self.audioMeter = soundMeter.instant;
						
	    	 			 //console.log(soundMeter.instant);	    
						
				    }, 100);
				  });
			}
		};

		this.peerConnection.onnegotiationneeded  = function(event: any) {
			console.log('onnegotiationneeded ===========>');
		}
		
		this.peerConnection.oniceconnectionstatechange = function(event: any) {

			console.log('iceconnectionstatechange ' + self.targetId, self.peerConnection.iceConnectionState);
		};	
		
		this.peerConnection.onicecandidate = function(event: any) {
			
			if(event.candidate != null) {
				var message = {
	    			type:"signal",
	    			signal:"ice",
	    			id: self.targetId,
	    			data: event.candidate
		    	};
		    	self.send(message);
			}
		};	
		
		this.peerConnection.onerror = function(event: any) {	

			console.log('WebRTC Error', event);	
		};
		this.peerConnection.onclose = function(event: any) {
			console.log('Close', event);		
		};
		this.peerConnection.onremovestream = function(event: any) {
			console.log('onremovestream', event);	
		};	
		
		this.addMedia();
	}

	setVideoOutput(videoPlayer: any): void {
		
		if(this.stream != null) {
			videoPlayer!.srcObject = this.stream;
		}
	}
	addMedia(): void {
		
		var self = this;
		
		if(self.call.outputStream != null) {
			this.call.outputStream.getTracks().forEach(
				(track: any) => self.peerConnection.addTrack(track, self.call.outputStream)
			);
		}		
	}
	resetVideo(): void {
		
		var self = this;
		
		this.peerConnection.getSenders().forEach(function(s: any) {
						
			self.peerConnection.removeTrack(s);
      	});

		this.call.outputStream.getTracks().forEach(
			(track: any) => this.peerConnection.addTrack(track, this.call.outputStream)
		);
			
		this.offerConnection();
	}
	removeVideo(): void {
		
		var sender = this.peerConnection.getSenders().find(function(s: any) {
			
			if(s.track == null) {
				return false;
			}
        	return s.track.kind == 'video';
      	});
		if(sender != null) {
			
			this.peerConnection.removeTrack(sender);
		}
		
		this.offerConnection();
	}
	async offerConnection() {
		
		await this.init();
		
		var offerOptions = {
			offerToReceiveAudio: 1,
			offerToReceiveVideo: 1,
			voiceActivityDetection: false
		};
		
		var self = this;
		
		this.peerConnection.createOffer(offerOptions).then(function(desc: any){
	    	  
	    	self.peerConnection.setLocalDescription(desc);

			console.log("Offer Created",self.targetId, desc);
	    	var message = {
				type: "signal",
				signal: "offer",
				id: self.targetId,
				data: desc
	    	};
	    	self.send(message);
	    	  //console.log(message);
	      })
	      .catch(function(e: any){
	    	  console.log(e);
	      });
	}

	async setRemoteDescription(offer: any) {

		await this.init();
		
		var self = this;
		
		this.peerConnection.setRemoteDescription(offer).then(function () {

			console.log("Remote Offer received ", self.targetId);
	    	  
	    	self.peerConnection.createAnswer().then(function(answer: any) {
	    		self.peerConnection.setLocalDescription(answer);

	  			console.log("Answer Created ", self.targetId);
		
		    	  var message = {
		    			  type:"signal",
		    			  signal: "answer",
		    			  id: self.targetId,
		    			  data: answer
		    	  };
		    	  self.send(message);
	    	}).catch(function(e: any){
		    	  console.log(e);	    	  
	    	});
	    	  
	    	  
		}).catch(function(e: any){
	    	  console.log(e);	    	  
	    });
	}
	async setAnswer(answer: any) {
		
		var self = this;
		
		this.peerConnection.setRemoteDescription(answer)
		.then(function () {

	    	  console.log("set answer description ", self.targetId);
		}).catch(function(e: any){
	    	  console.log(e);	    	  
	    });
	}	
	
	/*async receiveConnection(offerSDP: any) {
		
		await this.init();		
	}*/
	
	send (data: any) {
		if(this.call.socket != null) {
			this.call.socket.send(data);
		}
	}
	
	hasVideo() : boolean {
		if(this.stream == null) {
			return false;
		}
		
		return this.stream.getVideoTracks().length > 0;
	}
	hasAudio() : boolean {
		if(this.stream == null) {
			return false;
		}
		
		return this.stream.getAudioTracks().length > 0;
	}
}


export class SoundMeter {
	
  //context = context;
  instant = 0.0;
  slow = 0.0;
  clip = 0.0;
  script: any = null;
  mic: any = null;

  constructor(private context: any) {
	
	  this.script = context.createScriptProcessor(2048, 1, 1);
	
	  const that = this;
	  this.script.onaudioprocess = function(event: any) {
	    const input = event.inputBuffer.getChannelData(0);
	    let i;
	    let sum = 0.0;
	    let clipcount = 0;
	    for (i = 0; i < input.length; ++i) {
	      sum += input[i] * input[i];
	      if (Math.abs(input[i]) > 0.99) {
	        clipcount += 1;
	      }
	    }
	    that.instant = Math.sqrt(sum / input.length);
	    that.slow = 0.95 * that.slow + 0.05 * that.instant;
	    that.clip = clipcount / input.length;
	  };
  }

	connectToSource(stream: any, callback: any): void {
	  //console.log('SoundMeter connecting');
	  try {
	    this.mic = this.context.createMediaStreamSource(stream);
	    this.mic.connect(this.script);
	    // necessary to make sample run, but should not be.
	    this.script.connect(this.context.destination);
	    if (typeof callback !== 'undefined') {
	      callback(null);
	    }
	  } catch (e) {
	    console.error(e);
	    if (typeof callback !== 'undefined') {
	      callback(e);
	    }
	  }
	}

	stop(): void {
	  this.mic.disconnect();
	  this.script.disconnect();
	}
}
