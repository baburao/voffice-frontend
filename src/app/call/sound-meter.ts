
export class SoundMeter {
  
  context: any = null;
  instant = 0.0;
  //slow = 0.0;
  //clip = 0.0;
  script: any = null;
  mic: any = null;

  constructor(stream: any) {
  
    var w: any = window;
    try {
      var AudioContext = w.AudioContext || w.webkitAudioContext;
      this.context = new AudioContext();
    } catch (e) {
      console.log('Web Audio API not supported.');
      return;
    }
    this.script = this.context.createScriptProcessor(2048, 1, 1);
  
    const that = this;
    this.script.onaudioprocess = function(event: any) {
      const input = event.inputBuffer.getChannelData(0);
      let i;
      let sum = 0.0;
      let clipcount = 0;
      for (i = 0; i < input.length; ++i) {
        sum += input[i] * input[i];
        if (Math.abs(input[i]) > 0.99) {
          clipcount += 1;
        }
      }
      that.instant = Math.sqrt(sum / input.length);
      //that.slow = 0.95 * that.slow + 0.05 * that.instant;
      //that.clip = clipcount / input.length;
    };
    
    this.connectToSource(stream);
  }

  connectToSource(stream: any): void {
    //console.log('SoundMeter connecting');
    try {
      this.mic = this.context.createMediaStreamSource(stream);
      this.mic.connect(this.script);
      // necessary to make sample run, but should not be.
      this.script.connect(this.context.destination);
      
      
      console.log('SoundMeter Connected');
      
      //setInterval(() => {      
          //self.onAudioMeterValue(soundMeter.instant);
          //self.audioMeter = soundMeter.instant;
          //this.audioLevel = soundMeter.instant;        
           //console.log(soundMeter.instant); 
        //}, 100);
    } catch (e) {
      console.log('SoundMeter Error', e);
    }
  }

  stop(): void {
    this.mic.disconnect();
    this.script.disconnect();
  }
}