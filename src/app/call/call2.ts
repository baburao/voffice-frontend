import { map, catchError } from 'rxjs/operators';

import * as mediasoupClient from "mediasoup-client";
import * as protooClient from 'protoo-client';
import {environment} from '../../environments/environment';

import { CallRecorder } from './call-recorder';
import { SoundMeter } from './sound-meter';
import { Conference } from '../interfaces/conference';

export class CallPeer {

	//public onStream: any = null;
  
	primaryStream: any = null;
	private secondaryStream: any = null;
	//public shareStream: any = null;
	//private consumers: any[] = [];

  private soundMeter: SoundMeter | null = null;
  
  audioLevel: number = 0;
	//public sharing: boolean = false;
  	
	constructor(public targetId: number) {

		this.primaryStream = new MediaStream();
		this.secondaryStream = new MediaStream();
	}
  setTrack(newTrack: any, isShare: boolean = false) {
    
    if(newTrack.kind == 'video') {
      this.removeVideo(isShare);    
    }
    
    if(isShare) {
      this.secondaryStream.addTrack(newTrack);
    } else {
      this.primaryStream.addTrack(newTrack);
      
      if(newTrack.kind == 'audio') {
        this.attachSoundMeter(this.primaryStream);
      } 
    }
  }
  removeVideo(isShare: boolean = false) {
    
    if(isShare) {
      if(this.secondaryStream.getVideoTracks().length > 0) {
        this.secondaryStream.removeTrack(this.secondaryStream.getVideoTracks()[0]);
      }    
    } else {
      if(this.primaryStream.getVideoTracks().length > 0) {
        this.primaryStream.removeTrack(this.primaryStream.getVideoTracks()[0]);
      }    
    }
  }
  attachSoundMeter(stream: any) {   
    
    this.soundMeter = new SoundMeter(stream);
  }
  removeTrack(track: any, isShare: boolean = false) {
    
    if(isShare) {
      this.secondaryStream.removeTrack(track);          
    } else {
      this.primaryStream.removeTrack(track);          
    }
  }
	setVideoOutput(videoPlayer: any): void {

    if(videoPlayer?.srcObject != this.primaryStream) {      
      videoPlayer!.srcObject = this.primaryStream;
    } 
	}
  setSecondaryOutput(videoPlayer: any): void {
     
    if(videoPlayer?.srcObject != this.secondaryStream) {    
      videoPlayer!.srcObject = this.secondaryStream;
    }    
  }
	hasVideo(): boolean {
		return this.primaryStream.getVideoTracks().length > 0;
	}
  hasShare(): boolean {
    return this.secondaryStream.getVideoTracks().length > 0;
  }
  hasAudio(): boolean {
    return this.primaryStream.getAudioTracks().length > 0;
  }
  getAudioLevel() {
    if(this.soundMeter == null) {
      return 0;
    }
    var clampMin = 0.001;
    var audioLevel = this.soundMeter.instant * 5;
    return audioLevel > 1.0 ? 1.0 : (audioLevel < clampMin ? 0 : audioLevel);
  }
}

const VIDEO_CONSTRAINS =
{
	qvga: { width: { ideal: 320 }, height: { ideal: 240 } },
	vga: { width: { ideal: 640 }, height: { ideal: 480 } },
	hd: { width: { ideal: 1280 }, height: { ideal: 720 } }
};

const PC_PROPRIETARY_CONSTRAINTS =
{
	optional: [{ googDscp: true }]
};
const WEBCAM_SIMULCAST_ENCODINGS =
	[
		/*{ scaleResolutionDownBy: 4, maxBitrate: 500000 },
		{ scaleResolutionDownBy: 2, maxBitrate: 1000000 },
		{ scaleResolutionDownBy: 1, maxBitrate: 5000000 }*/
    
    { scaleResolutionDownBy: 4, maxBitrate: 50000 },
    { scaleResolutionDownBy: 2, maxBitrate: 100000 },
    { scaleResolutionDownBy: 1, maxBitrate: 1000000 }
	];
const WEBCAM_KSVC_ENCODINGS =
	[
		{ scalabilityMode: 'S3T3_KEY' }
	];
const SCREEN_SHARING_SIMULCAST_ENCODINGS =
	[
		{ scaleResolutionDownBy: 1, dtx: true, maxBitrate: 1500000 },
		{ scaleResolutionDownBy: 1, dtx: true, maxBitrate: 6000000 }
	];

// Used for VP9 screen sharing.
const SCREEN_SHARING_SVC_ENCODINGS =
	[
		{ scalabilityMode: 'S3T3', dtx: true }
	];

export class Call2 {

	public connections: { [key: number]: CallPeer } = {};
	public orderedConnections: CallPeer[] = [];

	_protoo: any = null;
	//_protooUrl: string = 'ws://' + window.location.hostname + ':3001';
	//_protooUrl: string = 'ws://192.168.2.103:3001';
	//_protooUrl: string = 'ws://' + '192.168.2.103' + ':3001/';
	_sendTransport: any = null;
	_recvTransport: any = null;
	_mediasoupDevice: any = null;
	_micProducer: any = null;
	_externalVideo: any = null;
	_externalVideoStream: any = null;
	_webcamProducer: any = null;
	_webcam: any =
		{
			device: null,
			//resolution: 'hd'
			resolution: 'qvga'
		};
	_webcams: any = new Map();
	_shareProducer: any = null;

	_closed: boolean = false;
	_consume: boolean = true;
	_produce: boolean = true;
	_forceH264: boolean = false;
	_forceVP9: boolean = false;
	_useSimulcast: boolean = false;
	_useSharingSimulcast: boolean = false;

	connected = false;

	smallTestShare = false;
	
	//previewTrackStream: MediaStream = new MediaStream();
	// mediasoup Consumers.
	// @type {Map<String, mediasoupClient.Consumer>}
	_consumers: any = new Map();

  public startWithCam: boolean = false;
  
  ownStream: CallPeer;
  sharingPeer: CallPeer | null = null;
  
	onNewPeer: any = null;
	onPeerLeave: any = null;
  
  private callRecorder: CallRecorder | null = null;
  recordings: any[] = [];//Urls
  
	constructor(
		private userId: number, 
		private callId: number, 
		public conference: Conference | null, 
		private onClosed: any, 
		private onShare: any) {
		
    this.ownStream = new CallPeer(userId);
	}
	isConference(): boolean {
		return this.conference != null;
	}

	close() {
		if (this._closed)
			return;

		this.connected = false;
		this._closed = true;

		console.log('close()');

		// Close protoo Peer
		this._protoo.close();

		// Close mediasoup Transports.
		if (this._sendTransport)
			this._sendTransport.close();

		if (this._recvTransport)
			this._recvTransport.close();

		this.onClosed();
		////store.dispatch(
		//	//stateActions.setRoomState('closed'));
	}
	async join() {
		const protooTransport = new protooClient.WebSocketTransport(
			environment.SFU_ADDRESS + '?roomId=' + this.callId + '&peerId=' + this.userId);

		this._protoo = new protooClient.Peer(protooTransport);

		//////store.dispatch(
		//////stateActions.setRoomState('connecting'));

		this._protoo.on('open', () => {

			this.connected = true;
			this._joinRoom()
		});

		this._protoo.on('failed', () => {
			this.connected = false;
			this.close();
			
			this.onClosed();
			console.error("protoo failed");
		});

		this._protoo.on('disconnected', () => {
			this.connected = false;
			// Close mediasoup Transports.
			if (this._sendTransport) {
				this._sendTransport.close();
				this._sendTransport = null;
			}

			if (this._recvTransport) {
				this._recvTransport.close();
				this._recvTransport = null;
			}
		});

		this._protoo.on('close', () => {
			if (this._closed)
				return;

			this.close();
		});

		// eslint-disable-next-line no-unused-vars
		this._protoo.on('request', async (request: any, accept: any, reject: any) => {
			console.log(
				'proto "request" event [method:%s, data:%o]',
				request.method, request.data);

			switch (request.method) {
				case 'newConsumer':
					{
						if (!this._consume) {
							reject(403, 'I do not want to consume');

							break;
						}

						const {
							peerId,
							producerId,
							id,
							kind,
							rtpParameters,
							type,
							appData,
							producerPaused,
							displayName
						} = request.data;
						
						try {
							const consumer = await this._recvTransport.consume(
								{
									id,
									producerId,
									kind,
									rtpParameters,
									appData: { ...appData, peerId } // Trick.
								});

							// Store in the map.
							this._consumers.set(consumer.id, consumer);

							consumer.on('transportclose', () => {
								this._consumers.delete(consumer.id);
							});

							const { spatialLayers, temporalLayers } =
								mediasoupClient.parseScalabilityMode(
									consumer.rtpParameters.encodings[0].scalabilityMode);

							var newUserId: number = parseInt(displayName);

              console.log("Joined ", newUserId, newUserId in this.connections, this.connections);
							
              var peer: CallPeer = this.createPeer(newUserId);							
              
							if(appData.share) {
                peer.setTrack(consumer.track, true);
                
                this.sharingPeer = peer;
                this.onShare(peer);                
                
              } else {
                peer.setTrack(consumer.track);
		          }
              if(this.onNewPeer != null) {
                this.onNewPeer(peer);
              }

							//////store.dispatch(//////stateActions.addConsumer(
							/*	{
									id                     : consumer.id,
									type                   : type,
									locallyPaused          : false,
									remotelyPaused         : producerPaused,
									rtpParameters          : consumer.rtpParameters,
									spatialLayers          : spatialLayers,
									temporalLayers         : temporalLayers,
									preferredSpatialLayer  : spatialLayers - 1,
									preferredTemporalLayer : temporalLayers - 1,
									priority               : 1,
									codec                  : consumer.rtpParameters.codecs[0].mimeType.split('/')[1],
									track                  : consumer.track
								},
								peerId));*/

							// We are ready. Answer the protoo request so the server will
							// resume this Consumer (which was paused for now if video).
							accept();

						}
						catch (error) {
							console.log('"newConsumer" request failed:%o', error);
							throw error;
						}
						break;
					}
			}
		});

		this._protoo.on('notification', (notification: any) => {
			//console.log(
			//	'proto "notification" event [method:%s, data:%o]',
			//	notification.method, notification.data);

			switch (notification.method) {
				case 'producerScore':
					{
						break;
					}

				case 'newPeer':
					{
						const peer = notification.data;
						console.log('newPeer', peer);
            var newUserId: number = parseInt(peer.displayName);
            
            if(newUserId != this.userId) {
              this.createPeer(newUserId);
            }
						break;
					}

				case 'peerClosed':
					{
						const { peerId, displayName } = notification.data;

						var targetUserId = parseInt(displayName);

						console.log("peerClosed", displayName);

						if (targetUserId in this.connections) {

              if(this.connections[targetUserId].hasShare()) {
                this.sharingPeer = null;
                this.onShare(null);
              }
              delete this.connections[targetUserId];
              this.orderedConnections = this.orderedConnections.filter((peer: CallPeer) => peer.targetId != targetUserId);
              
              if(this.onPeerLeave) {
                this.onPeerLeave();
              }
						}
          
						break;
					}

				case 'downlinkBwe':
					{
						//console.log('\'downlinkBwe\' event:%o', notification.data);

						break;
					}

				case 'consumerClosed':
					{
						const { consumerId, displayName } = notification.data;
						const consumer = this._consumers.get(consumerId);

						if (!consumer)
							break;

						consumer.close();
						this._consumers.delete(consumerId);


						const { peerId } = consumer.appData;
						var targetUserId = parseInt(displayName);
						
						
						console.log("consumerClosed", displayName, consumer.appData);

						if (targetUserId in this.connections) {

							var peer: CallPeer = this.connections[targetUserId];
							
							
							if(consumer.appData.share) {
                
								peer.removeTrack(consumer.track, true);  
								this.sharingPeer = null;
								this.onShare(null);
							} else {
                
                peer.removeTrack(consumer.track);  
              }
											
						}

						break;
					}

				case 'consumerPaused':
					{
						const { consumerId } = notification.data;
						const consumer = this._consumers.get(consumerId);

						if (!consumer)
							break;

						consumer.pause();

						break;
					}

				case 'consumerResumed':
					{
						const { consumerId } = notification.data;
						const consumer = this._consumers.get(consumerId);

						if (!consumer)
							break;

						consumer.resume();

						break;
					}

				case 'consumerLayersChanged':
					{
						const { consumerId, spatialLayer, temporalLayer } = notification.data;
						const consumer = this._consumers.get(consumerId);

						if (!consumer)
							break;

						break;
					}

				case 'speakerOrder':
				{
					console.log(notification.data.order);
					
          if(notification.data.order.length == 0) {
            return;
          }
          var speakerId: number = notification.data.order[0];
          
          if(speakerId == this.userId) {
            return;
          }
          
          if(this.orderedConnections.length > 0 && this.orderedConnections[0].targetId == speakerId) {
            return;
          }
          
          if(!(speakerId in this.connections)) {
                console.log("order ID error", speakerId);
                return;
          }
					var newOrder: CallPeer[] = this.orderedConnections.filter((peer: CallPeer) => peer.targetId != speakerId);
					
          //Maybe?
          /*newOrder.sort((peerA: CallPeer, peerB: CallPeer) => {
            
            if(peerA.hasVideo() && peerB.hasVideo()){ return 0; }
            else if(peerA.hasVideo() && !peerB.hasVideo()){ return -1;}
            return 1;
          });*/
          newOrder.splice(0, 0, this.connections[speakerId]);
          
          //this.orderedConnections = newOrder;
          
          /*
					for(var i in notification.data.order) {
            if(!(notification.data.order[i] in this.connections)) {
                console.log("order ID error", notification.data.order[i]);
                continue;
            }
            newOrder.push(this.connections[notification.data.order[i]]);
          }
					for(var i in this.orderedConnections) {
            
            if(!notification.data.order.find()) {
              
            }
          }
					for(var i in this.orderedConnections) {
						
						if(this.userId != notification.data.order[i]) {
              
              if(!(notification.data.order[i] in this.connections)) {
                console.log("order ID error", notification.data.order[i]);
                continue;
              }
							newOrder.push(this.connections[notification.data.order[i]]);
							//console.log(i, notification.data.order[i] , this.connections)
						}
					}*/
					//this.aaorderedConnections = newOrder;
					//console.log(this.orderedConnections);
					break;
				}
				case 'consumerScore':
					{
						break;
					}


				default:
					{
						console.log(
							'unknown protoo notification.method "%s"', notification.method);
					}
			}
		});
	}
  createPeer(newUserId: number) : CallPeer {
        
    if (!(newUserId in this.connections)) {
      var peer: CallPeer = new CallPeer(newUserId);
      this.connections[newUserId] = peer;
      this.orderedConnections.push(peer);
      
      return peer;
    } else {
      return this.connections[newUserId];
    }
  }
	async _joinRoom() {
		console.log('_joinRoom()');

		try {
			this._mediasoupDevice = new mediasoupClient.Device();
			/*	{
					handlerName : this._handlerName
				});*/

			const routerRtpCapabilities =
				await this._protoo.request('getRouterRtpCapabilities');

			await this._mediasoupDevice.load({ routerRtpCapabilities });

			// NOTE: Stuff to play remote audios due to browsers' new autoplay policy.
			//
			// Just get access to the mic and DO NOT close the mic track for a while.
			// Super hack!
			{
        
        var autoGainControl: boolean = localStorage.getItem("autoGainControl") === "false";  
        var echoCancellation: boolean = localStorage.getItem("echoCancellation") === "false";   
        var noiseSuppression: boolean = localStorage.getItem("noiseSuppression") === "false";
        
        console.log(navigator.mediaDevices.getSupportedConstraints());
        
        /**
        googTypingNoiseDetection
        VoiceActivityDetection
         */
				const stream = await navigator.mediaDevices.getUserMedia({ audio: {
          autoGainControl: false,
          echoCancellation: true,
          noiseSuppression: true,
        } });
        
        this.ownStream.attachSoundMeter(stream);
				const audioTrack = stream.getAudioTracks()[0];

				audioTrack.enabled = false;

				setTimeout(() => audioTrack.stop(), 120000);
			}

			// Create mediasoup Transport for sending (unless we don't want to produce).
			if (this._produce) {
				const transportInfo = await this._protoo.request(
					'createWebRtcTransport',
					{
						forceTcp: false,//this._forceTcp,
						producing: true,
						consuming: false,
						sctpCapabilities: undefined
					});

				const {
					id,
					iceParameters,
					iceCandidates,
					dtlsParameters,
					sctpParameters
				} = transportInfo;

				this._sendTransport = this._mediasoupDevice.createSendTransport(
					{
						id,
						iceParameters,
						iceCandidates,
						dtlsParameters,
						sctpParameters,
						iceServers: [],
						proprietaryConstraints: PC_PROPRIETARY_CONSTRAINTS
					});

				this._sendTransport.on(
					'connect', ({ dtlsParameters }, callback, errback) => // eslint-disable-line no-shadow
				{
					this._protoo.request(
						'connectWebRtcTransport',
						{
							transportId: this._sendTransport.id,
							dtlsParameters
						})
						.then(callback)
						.catch(errback);
				});

				this._sendTransport.on(
					'produce', async ({ kind, rtpParameters, appData }, callback, errback) => {
					try {
						// eslint-disable-next-line no-shadow
						const { id } = await this._protoo.request(
							'produce',
							{
								transportId: this._sendTransport.id,
								kind,
								rtpParameters,
								appData
							});

						callback({ id });
					}
					catch (error) {
						errback(error);
					}
				});

				this._sendTransport.on('producedata', async (
					{
						sctpStreamParameters,
						label,
						protocol,
						appData
					},
					callback,
					errback
				) => {
					console.log(
						'"producedata" event: [sctpStreamParameters:%o, appData:%o]',
						sctpStreamParameters, appData);

					try {
						// eslint-disable-next-line no-shadow
						const { id } = await this._protoo.request(
							'produceData',
							{
								transportId: this._sendTransport.id,
								sctpStreamParameters,
								label,
								protocol,
								appData
							});

						callback({ id });
					}
					catch (error) {
						errback(error);
					}
				});
			}

			// Create mediasoup Transport for sending (unless we don't want to consume).
			if (this._consume) {
				const transportInfo = await this._protoo.request(
					'createWebRtcTransport',
					{
						forceTcp: false,//this._forceTcp,
						producing: false,
						consuming: true,
						sctpCapabilities: undefined
						//this._useDataChannel
						//? this._mediasoupDevice.sctpCapabilities
						//: undefined
					});

				const {
					id,
					iceParameters,
					iceCandidates,
					dtlsParameters,
					sctpParameters
				} = transportInfo;

				this._recvTransport = this._mediasoupDevice.createRecvTransport(
					{
						id,
						iceParameters,
						iceCandidates,
						dtlsParameters,
						sctpParameters,
						iceServers: []
					});

				this._recvTransport.on(
					'connect', ({ dtlsParameters }, callback, errback) => // eslint-disable-line no-shadow
				{
					this._protoo.request(
						'connectWebRtcTransport',
						{
							transportId: this._recvTransport.id,
							dtlsParameters
						})
						.then(callback)
						.catch(errback);
				});
			}

			// Join now into the room.
			// NOTE: Don't send our RTP capabilities if we don't want to consume.
			const { peers } = await this._protoo.request(
				'join',
				{
					displayName: this.userId + "",
					//device          : this._device,
					rtpCapabilities: this._mediasoupDevice.rtpCapabilities,
					sctpCapabilities: undefined
				});


			// Enable mic/webcam.
			if (this._produce) {
				this.enableMic();
				//this.enableShare();	
        if(this.startWithCam)	{		
				  this.enableWebcam();
        }
				this._sendTransport.on('connectionstatechange', (connectionState) => {
					if (connectionState === 'connected') { }
				});
			}

		}
		catch (error) {
			console.log('_joinRoom() failed:%o', error);

			////store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Could not join the room: ${error}`
			//	}));

			this.close();			
		}
	}

	async _pauseConsumer(consumer) {
		if (consumer.paused)
			return;

		try {
			await this._protoo.request('pauseConsumer', { consumerId: consumer.id });

			consumer.pause();

			////store.dispatch(
			//	//stateActions.setConsumerPaused(consumer.id, 'local'));
		}
		catch (error) {
			console.log('_pauseConsumer() | failed:%o', error);

			////store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error pausing Consumer: ${error}`
			//	}));
		}
	}
	async enableMic() {
		console.log('enableMic()');

		if (this._micProducer)
			return;

		if (!this._mediasoupDevice.canProduce('audio')) {
			console.log('enableMic() | cannot produce audio');

			return;
		}

		let track;

		try {
			if (!this._externalVideo) {
				console.log('enableMic() | calling getUserMedia()');
        
        //var autoGainControl: boolean = localStorage.getItem("autoGainControl") === "false";  
        //var echoCancellation: boolean = localStorage.getItem("echoCancellation") === "false";   
        //var noiseSuppression: boolean = localStorage.getItem("noiseSuppression") === "false";
        
				const stream = await navigator.mediaDevices.getUserMedia({ audio:  {
            autoGainControl: false,
            echoCancellation: true,
            noiseSuppression: true,
              
           }
         });
        this.ownStream.attachSoundMeter(stream);
				track = stream.getAudioTracks()[0];
        
        console.log("Audio Settings: " , track.getSettings());
			} else {
				const stream = await this._getExternalVideoStream();

				track = stream.getAudioTracks()[0].clone();
			}

			this._micProducer = await this._sendTransport.produce(
				{
					track,
					codecOptions:
					{
						opusStereo: false,//true,
						opusDtx: true,
            opusFec: true, //?
            opusPtime: '3',//?
            opusMaxPlaybackRate: 48000
					}
					// NOTE: for testing codec selection.
					// codec : this._mediasoupDevice.rtpCapabilities.codecs
					// 	.find((codec) => codec.mimeType.toLowerCase() === 'audio/pcma')
				});

			//store.dispatch(//stateActions.addProducer(
			//	{
			//		id            : this._micProducer.id,
			//		paused        : this._micProducer.paused,
			//		track         : this._micProducer.track,
			//		rtpParameters : this._micProducer.rtpParameters,
			//		codec         : this._micProducer.rtpParameters.codecs[0].mimeType.split('/')[1]
			//	}));

			this._micProducer.on('transportclose', () => {
				this._micProducer = null;
			});

			this._micProducer.on('trackended', () => {
				//store.dispatch(requestActions.notify(
				//	{
				//		type : 'error',
				//		text : 'Microphone disconnected!'
				//	}));

				this.disableMic()
					.catch(() => { });
			});
		}
		catch (error) {
			console.log('enableMic() | failed:%o', error);

			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error enabling microphone: ${error}`
			//	}));

			if (track)
				track.stop();
		}
	}

	async disableMic() {
		console.log('disableMic()');

		if (!this._micProducer)
			return;

		this._micProducer.close();

		//store.dispatch(
		//stateActions.removeProducer(this._micProducer.id));

		try {
			await this._protoo.request(
				'closeProducer', { producerId: this._micProducer.id });
		}
		catch (error) {
			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error closing server-side mic Producer: ${error}`
			//	}));
		}

		this._micProducer = null;
	}

	async muteMic() {
		console.log('muteMic()');

		this._micProducer.pause();

		try {
			await this._protoo.request(
				'pauseProducer', { producerId: this._micProducer.id });

			//store.dispatch(
			//stateActions.setProducerPaused(this._micProducer.id));
		}
		catch (error) {
			console.log('muteMic() | failed: %o', error);

			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error pausing server-side mic Producer: ${error}`
			//	}));
		}
	}

	async unmuteMic() {
		console.log('unmuteMic()');

		this._micProducer.resume();

		try {
			await this._protoo.request(
				'resumeProducer', { producerId: this._micProducer.id });

			//store.dispatch(
			//stateActions.setProducerResumed(this._micProducer.id));
		}
		catch (error) {
			console.log('unmuteMic() | failed: %o', error);

			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error resuming server-side mic Producer: ${error}`
			//	}));
		}
	}

	async enableWebcam() {
		console.log('enableWebcam()');

		if (this._webcamProducer)
			return;
		//else if (this._shareProducer)
		//	await this.disableShare();

		if (!this._mediasoupDevice.canProduce('video')) {
			console.log('enableWebcam() | cannot produce video');

			return;
		}

		let track;
		let device;

		//store.dispatch(
		//stateActions.setWebcamInProgress(true));

		try {
			if (!this._externalVideo) {
				await this._updateWebcams();
				device = this._webcam.device;

				const { resolution } = this._webcam;

				if (!device)
					throw new Error('no webcam devices');

				console.log('enableWebcam() | calling getUserMedia()');

				const stream = await navigator.mediaDevices.getUserMedia(
					{
						video:
						{
							deviceId: { ideal: device.deviceId },
							...VIDEO_CONSTRAINS[resolution]
						}
					});

				track = stream.getVideoTracks()[0];
				
			} else {
				device = { label: 'external video' };

				const stream = await this._getExternalVideoStream();

				track = stream.getVideoTracks()[0].clone();
			}

			let encodings;
			let codec;
			const codecOptions =
			{
				videoGoogleStartBitrate: 1000
			};

			if (this._forceH264) {
				codec = this._mediasoupDevice.rtpCapabilities.codecs
					.find((c) => c.mimeType.toLowerCase() === 'video/h264');

				if (!codec) {
					throw new Error('desired H264 codec+configuration is not supported');
				}
			}
			else if (this._forceVP9) {
				codec = this._mediasoupDevice.rtpCapabilities.codecs
					.find((c) => c.mimeType.toLowerCase() === 'video/vp9');

				if (!codec) {
					throw new Error('desired VP9 codec+configuration is not supported');
				}
			}

			if (this._useSimulcast) {
				// If VP9 is the only available video codec then use SVC.
				const firstVideoCodec = this._mediasoupDevice
					.rtpCapabilities
					.codecs
					.find((c) => c.kind === 'video');

				if (
					(this._forceVP9 && codec) ||
					firstVideoCodec.mimeType.toLowerCase() === 'video/vp9'
				) {
					encodings = WEBCAM_KSVC_ENCODINGS;
				}
				else {
					encodings = WEBCAM_SIMULCAST_ENCODINGS;
				}
			}
			
			this.setPreviewTrack(track);
			
			this._webcamProducer = await this._sendTransport.produce(
				{
					track,
					encodings,
					codecOptions,
					codec
				});

			//store.dispatch(//stateActions.addProducer(
			//	{
			//		id            : this._webcamProducer.id,
			//		deviceLabel   : device.label,
			//		type          : this._getWebcamType(device),
			//		paused        : this._webcamProducer.paused,
			//		track         : this._webcamProducer.track,
			//		rtpParameters : this._webcamProducer.rtpParameters,
			//		codec         : this._webcamProducer.rtpParameters.codecs[0].mimeType.split('/')[1]
			//	}));

			this._webcamProducer.on('transportclose', () => {
				this._webcamProducer = null;
			});

			this._webcamProducer.on('trackended', () => {
				//store.dispatch(requestActions.notify(
				//	{
				//		type : 'error',
				//		text : 'Webcam disconnected!'
				//	}));

				this.disableWebcam()
					.catch(() => { });
			});
		}
		catch (error) {
			console.log('enableWebcam() | failed:%o', error);

			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error enabling webcam: ${error}`
			//	}));

			if (track)
				track.stop();
		}

		//store.dispatch(
		//stateActions.setWebcamInProgress(false));
	}

	async disableWebcam() {
		console.log('disableWebcam()');

		if (!this._webcamProducer)
			return;

		this._webcamProducer.close();

		//store.dispatch(
		//stateActions.removeProducer(this._webcamProducer.id));

		try {
			await this._protoo.request(
				'closeProducer', { producerId: this._webcamProducer.id });
		}
		catch (error) {
			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error closing server-side webcam Producer: ${error}`
			//	}));
		}

		this._webcamProducer = null;
    this.ownStream.removeVideo();
	}

	async changeWebcam() {
		console.log('changeWebcam()');

		//store.dispatch(
		//stateActions.setWebcamInProgress(true));

		try {
			await this._updateWebcams();

			const array = Array.from(this._webcams.keys());
			const len = array.length;
			const deviceId =
				this._webcam.device ? this._webcam.device.deviceId : undefined;
			let idx = array.indexOf(deviceId);

			if (idx < len - 1)
				idx++;
			else
				idx = 0;

			this._webcam.device = this._webcams.get(array[idx]);

			console.log(
				'changeWebcam() | new selected webcam [device:%o]',
				this._webcam.device);

			// Reset video resolution to HD.
			//this._webcam.resolution = 'hd';
			this._webcam.resolution = 'vga';

			if (!this._webcam.device)
				throw new Error('no webcam devices');

			// Closing the current video track before asking for a new one (mobiles do not like
			// having both front/back cameras open at the same time).
			this._webcamProducer.track.stop();

			console.log('changeWebcam() | calling getUserMedia()');

			const stream = await navigator.mediaDevices.getUserMedia(
				{
					video:
					{
						deviceId: { exact: this._webcam.device.deviceId },
						...VIDEO_CONSTRAINS[this._webcam.resolution]
					}
				});

			const track = stream.getVideoTracks()[0];

			await this._webcamProducer.replaceTrack({ track });

			//store.dispatch(
			//stateActions.setProducerTrack(this._webcamProducer.id, track));
		}
		catch (error) {
			console.log('changeWebcam() | failed: %o', error);

			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Could not change webcam: ${error}`
			//	}));
		}

		//store.dispatch(
		//stateActions.setWebcamInProgress(false));
	}

	async changeWebcamResolution() {
		console.log('changeWebcamResolution()');

		//store.dispatch(
		//stateActions.setWebcamInProgress(true));

		try {
			switch (this._webcam.resolution) {
				case 'qvga':
					this._webcam.resolution = 'vga';
					break;
				case 'vga':
					this._webcam.resolution = 'hd';
					break;
				case 'hd':
					this._webcam.resolution = 'qvga';
					break;
				default:
					this._webcam.resolution = 'hd';
			}

			console.log('changeWebcamResolution() | calling getUserMedia()');

			const stream = await navigator.mediaDevices.getUserMedia(
				{
					video:
					{
						deviceId: { exact: this._webcam.device.deviceId },
						...VIDEO_CONSTRAINS[this._webcam.resolution]
					}
				});

			const track = stream.getVideoTracks()[0];

			await this._webcamProducer.replaceTrack({ track });

			//store.dispatch(
			//stateActions.setProducerTrack(this._webcamProducer.id, track));
		}
		catch (error) {
			console.log('changeWebcamResolution() | failed: %o', error);

			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Could not change webcam resolution: ${error}`
			//	}));
		}

		//store.dispatch(
		//stateActions.setWebcamInProgress(false));
	}

	async enableShare() {
		console.log('enableShare()');

		if (this._shareProducer)
			return;
		//else if (this._webcamProducer)
		//	await this.disableWebcam();
    if(this.sharingPeer != null) {
      return;
    }
		if (!this._mediasoupDevice.canProduce('video')) {
			console.log('enableShare() | cannot produce video');

			return;
		}

		let track;

		//store.dispatch(
		//stateActions.setShareInProgress(true));
    
    const maxShareWidth = this.smallTestShare ?  480 : 1920;
    const maxShareHeight = this.smallTestShare ?  320 : 1080;
		
		
		try {
			console.log('enableShare() | calling getUserMedia()');

			var navigatorStub: any = navigator;
			const stream = await navigatorStub.mediaDevices.getDisplayMedia(
				{
					audio: false,
					video:
					{
						displaySurface: 'monitor',
						logicalSurface: true,
						cursor: true,
						width: { max: maxShareWidth },
						height: { max: maxShareHeight },
						frameRate: { max: 30 }
					}
				});

			// May mean cancelled (in some implementations).
			if (!stream) {
				//store.dispatch(
				//stateActions.setShareInProgress(true));

				return;
			}

			track = stream.getVideoTracks()[0];

			let encodings;
			let codec;
			const codecOptions =
			{
				videoGoogleStartBitrate: 1000
			};

			if (this._forceH264) {
				codec = this._mediasoupDevice.rtpCapabilities.codecs
					.find((c) => c.mimeType.toLowerCase() === 'video/h264');

				if (!codec) {
					throw new Error('desired H264 codec+configuration is not supported');
				}
			}
			else if (this._forceVP9) {
				codec = this._mediasoupDevice.rtpCapabilities.codecs
					.find((c) => c.mimeType.toLowerCase() === 'video/vp9');

				if (!codec) {
					throw new Error('desired VP9 codec+configuration is not supported');
				}
			}

			if (this._useSharingSimulcast) {
				// If VP9 is the only available video codec then use SVC.
				const firstVideoCodec = this._mediasoupDevice
					.rtpCapabilities
					.codecs
					.find((c) => c.kind === 'video');

				if (
					(this._forceVP9 && codec) ||
					firstVideoCodec.mimeType.toLowerCase() === 'video/vp9'
				) {
					encodings = SCREEN_SHARING_SVC_ENCODINGS;
				}
				else {
					encodings = SCREEN_SHARING_SIMULCAST_ENCODINGS
						.map((encoding) => ({ ...encoding, dtx: true }));
				}
			}

			this.setSharePreviewTrack(track);
			
			this._shareProducer = await this._sendTransport.produce(
				{
					track,
					encodings,
					codecOptions,
					codec,
					appData:
					{
						share: true
					}
				});

			//store.dispatch(//stateActions.addProducer(
			//	{
			//		id            : this._shareProducer.id,
			//		type          : 'share',
			//		paused        : this._shareProducer.paused,
			//		track         : this._shareProducer.track,
			//		rtpParameters : this._shareProducer.rtpParameters,
			//		codec         : this._shareProducer.rtpParameters.codecs[0].mimeType.split('/')[1]
			//	}));

			this._shareProducer.on('transportclose', () => {
				this._shareProducer = null;
			});

			this._shareProducer.on('trackended', () => {
				//store.dispatch(requestActions.notify(
				//	{
				//		type : 'error',
				//		text : 'Share disconnected!'
				//	}));

				this.disableShare()
					.catch(() => { });
			});
		}
		catch (error) {
			console.log('enableShare() | failed:%o', error);

			if (error.name !== 'NotAllowedError') {
				//store.dispatch(requestActions.notify(
				//	{
				//		type : 'error',
				//		text : `Error sharing: ${error}`
				//	}));
			}

			if (track)
				track.stop();
		}

		//store.dispatch(
		//stateActions.setShareInProgress(false));
	}
	setPreviewTrack(track: any): void {
    this.ownStream.setTrack(track);
	}
  setSharePreviewTrack(track: any): void {
    this.ownStream.setTrack(track, true);
  }
	async _resumeConsumer(consumer) {
		if (!consumer.paused)
			return;

		try {
			await this._protoo.request('resumeConsumer', { consumerId: consumer.id });

			consumer.resume();

			//store.dispatch(
			//	stateActions.setConsumerResumed(consumer.id, 'local'));
		}
		catch (error) {
			console.log('_resumeConsumer() | failed:%o', error);

			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error resuming Consumer: ${error}`
			//	}));
		}
	}
	async disableShare() {
		console.log('disableShare()');

		if (!this._shareProducer)
			return;

		this._shareProducer.close();

		//store.dispatch(
		//stateActions.removeProducer(this._shareProducer.id));

		try {
			await this._protoo.request(
				'closeProducer', { producerId: this._shareProducer.id });
		}
		catch (error) {
			//store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error closing server-side share Producer: ${error}`
			//	}));
		}

		this._shareProducer = null;
    this.ownStream.removeTrack(true);
	}

	/*async enableAudioOnly()
	{
		console.log('enableAudioOnly()');

		//store.dispatch(
			//stateActions.setAudioOnlyInProgress(true));

		this.disableWebcam();

		for (const consumer of this._consumers.values())
		{
			if (consumer.kind !== 'video')
				continue;

			this._pauseConsumer(consumer);
		}

		//store.dispatch(
			//stateActions.setAudioOnlyState(true));

		//store.dispatch(
			//stateActions.setAudioOnlyInProgress(false));
	}

	async disableAudioOnly()
	{
		console.log('disableAudioOnly()');

		//store.dispatch(
			//stateActions.setAudioOnlyInProgress(true));

		if (
			!this._webcamProducer &&
			this._produce //&&
			//(cookiesManager.getDevices() || {}).webcamEnabled
		)
		{
			this.enableWebcam();
		}

		for (const consumer of this._consumers.values())
		{
			if (consumer.kind !== 'video')
				continue;

			this._resumeConsumer(consumer);
		}

		//store.dispatch(
			//stateActions.setAudioOnlyState(false));

		//store.dispatch(
			//stateActions.setAudioOnlyInProgress(false));
	}*/

	async restartIce() {
		console.log('restartIce()');

		//store.dispatch(
		//stateActions.setRestartIceInProgress(true));

		try {
			if (this._sendTransport) {
				const iceParameters = await this._protoo.request(
					'restartIce',
					{ transportId: this._sendTransport.id });

				await this._sendTransport.restartIce({ iceParameters });
			}

			if (this._recvTransport) {
				const iceParameters = await this._protoo.request(
					'restartIce',
					{ transportId: this._recvTransport.id });

				await this._recvTransport.restartIce({ iceParameters });
			}

			////store.dispatch(requestActions.notify(
			//	{
			//		text : 'ICE restarted'
			//	}));
		}
		catch (error) {
			console.log('restartIce() | failed:%o', error);

			////store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `ICE restart failed: ${error}`
			//	}));
		}

		////store.dispatch(
		//	//stateActions.setRestartIceInProgress(false));
	}

	async setMaxSendingSpatialLayer(spatialLayer) {
		console.log('setMaxSendingSpatialLayer() [spatialLayer:%s]', spatialLayer);

		try {
			if (this._webcamProducer)
				await this._webcamProducer.setMaxSpatialLayer(spatialLayer);
			else if (this._shareProducer)
				await this._shareProducer.setMaxSpatialLayer(spatialLayer);
		}
		catch (error) {
			console.log('setMaxSendingSpatialLayer() | failed:%o', error);

			////store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error setting max sending video spatial layer: ${error}`
			//	}));
		}
	}

	async setConsumerPreferredLayers(consumerId, spatialLayer, temporalLayer) {
		console.log(
			'setConsumerPreferredLayers() [consumerId:%s, spatialLayer:%s, temporalLayer:%s]',
			consumerId, spatialLayer, temporalLayer);

		try {
			await this._protoo.request(
				'setConsumerPreferredLayers', { consumerId, spatialLayer, temporalLayer });

			////store.dispatch(//stateActions.setConsumerPreferredLayers(
			//	consumerId, spatialLayer, temporalLayer));
		}
		catch (error) {
			console.log('setConsumerPreferredLayers() | failed:%o', error);

			////store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error setting Consumer preferred layers: ${error}`
			//	}));
		}
	}

	async setConsumerPriority(consumerId, priority) {
		console.log(
			'setConsumerPriority() [consumerId:%s, priority:%d]',
			consumerId, priority);

		try {
			await this._protoo.request('setConsumerPriority', { consumerId, priority });

			////store.dispatch(//stateActions.setConsumerPriority(consumerId, priority));
		}
		catch (error) {
			console.log('setConsumerPriority() | failed:%o', error);

			////store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error setting Consumer priority: ${error}`
			//	}));
		}
	}

	async requestConsumerKeyFrame(consumerId) {
		console.log('requestConsumerKeyFrame() [consumerId:%s]', consumerId);

		try {
			await this._protoo.request('requestConsumerKeyFrame', { consumerId });

			////store.dispatch(requestActions.notify(
			//	{
			//		text : 'Keyframe requested for video consumer'
			//	}));
		}
		catch (error) {
			console.log('requestConsumerKeyFrame() | failed:%o', error);

			////store.dispatch(requestActions.notify(
			//	{
			//		type : 'error',
			//		text : `Error requesting key frame for Consumer: ${error}`
			//	}));
		}
	}
	async _getExternalVideoStream() {
		if (this._externalVideoStream)
			return this._externalVideoStream;

		if (this._externalVideo.readyState < 3) {
			await new Promise((resolve) => (
				this._externalVideo.addEventListener('canplay', resolve)
			));
		}

		if (this._externalVideo.captureStream)
			this._externalVideoStream = this._externalVideo.captureStream();
		else if (this._externalVideo.mozCaptureStream)
			this._externalVideoStream = this._externalVideo.mozCaptureStream();
		else
			throw new Error('video.captureStream() not supported');

		return this._externalVideoStream;
	}
	async _updateWebcams() {
		console.log('_updateWebcams()');

		// Reset the list.
		this._webcams = new Map();

		console.log('_updateWebcams() | calling enumerateDevices()');

		const devices = await navigator.mediaDevices.enumerateDevices();

		console.log(">", devices)
		for (const device of devices) {
			console.log(">>", device)
			if (device.kind !== 'videoinput')
				continue;

			this._webcams.set(device.deviceId, device);
		}

		const array = Array.from(this._webcams.values());
		const len = array.length;
		const currentWebcamId =
			this._webcam.device ? this._webcam.device.deviceId : undefined;

		console.log('_updateWebcams() [webcams:%o]', array);

		if (len === 0)
			this._webcam.device = null;
		else if (!this._webcams.has(currentWebcamId))
			this._webcam.device = array[0];

		//store.dispatch(
		//	stateActions.setCanChangeWebcam(this._webcams.size > 1));
	}

	_getWebcamType(device) {
		if (/(back|rear)/i.test(device.label)) {
			console.log('_getWebcamType() | it seems to be a back camera');

			return 'back';
		}
		else {
			console.log('_getWebcamType() | it seems to be a front camera');

			return 'front';
		}
	}

	isConnected(): boolean { 
		return true;//this.connected && !this._closed;
	}

	leaveCall(): void { 
    if(this.isRecording()) {
      this.stopRecord();
    }
		this.close();
	}
	toggleRecord(): void {
    
    if(this.isRecording()) {
      this.stopRecord();
    } else {
      this.startRecordFile();
    }
  }
  isRecording(): boolean {
    
    return this.callRecorder != null;
  }
  removeRecording(id: number): void {
    
    var index = this.recordings.find((e: any) => {return e.id == id});
    if(index == - 1) {
      return;
    }
    this.recordings.splice(index, 1);
  }
  recordingIdCounter: number = 0;
  private addRecording(audioUrl: any): void {
    
    this.recordingIdCounter++;
    
    this.recordings.push({
      id: this.recordingIdCounter,
      url: audioUrl
    });
  }
  
  startRecordFile(): void {
    
    this.startRecord((audioUrl: any, buffer: any) => {
      console.log("rec available", audioUrl);
      this.addRecording(audioUrl);
      /*var a = document.createElement("a");
      document.body.appendChild(a);
      a.href = audioUrl;
      a.download = "rec.webm";
      a.click();*/
      
    });
  }
  startRecord(availableEvent: any): void {
    
    this.callRecorder = new CallRecorder(); 
    this.callRecorder.availableEvent = availableEvent;
    
    if (!this.isMuted()) {
      var recordStream = new MediaStream();
      recordStream.addTrack(this._micProducer.track);
      this.callRecorder.addStream(recordStream);
    }
    
    for(let c in this.connections) {
      
      if(this.connections[c].primaryStream.getAudioTracks().length > 0) {
        this.callRecorder.addStream(this.connections[c].primaryStream);
      }
    }
    
    this.callRecorder.record();
  }
  stopRecord(): void {
    
    if(this.callRecorder == null) {
      return;
    }
    
    this.callRecorder.stop();
    this.callRecorder = null;
  }
  
	isCam(): boolean { 
		return this._webcamProducer != null; 
	}
	
	isScreenshare(): boolean { 
		return this._shareProducer != null; 
	}
	
	toogleCam(): void {
		if(this.isCam()) {
			this.disableWebcam();
		} else {
			this.enableWebcam();
		}
	}
	toogleScreenshare(): void {
		
		if(this.isScreenshare()) {
			this.disableShare();
		} else {
			this.enableShare();
		}	
	}
	toogleMuteAudio(): void {

		if (this.isMuted()) {
			this.enableMic();
		} else {
			this.disableMic();
		}
	}
	
	isMuted(): boolean { 
		return this._micProducer == null || this._micProducer.paused; 
	}
	isConferenceCall(): boolean {
		return this.conference != null;
	}
  
  hasUserConnection(userId: number) {
    return userId in this.connections;
  }
  
  getUserConnection(userId: number) {
    return this.connections[userId];
  }
}


export class FakeCallPeer extends CallPeer {
	
	video: boolean = false;
	videoId: number = 0;
	
	constructor(public targetId: number) {
		super(targetId);
		
		if(targetId % 2 == 0) {
			this.video = true;
			this.videoId = Math.floor(Math.random() * 4.0);
		}
	}
	

	setVideoOutput(videoPlayer: any): void {
		if(this.video) {
			videoPlayer!.src = "http://localhost:4200/assets/test/vid" + this.videoId + ".mp4";
			videoPlayer.loop = true;
			videoPlayer.load();
	   		videoPlayer.play();
		}
	}	
	setVideoOrShareOutput(videoPlayer: any): void {
    this.setVideoOutput(videoPlayer);
  }
	hasVideo() : boolean {return this.video;}
}

export class FakeCall extends Call2 {
	
	constructor(private inUserId: number, private count: number) {
		super(inUserId, 1 ,null, null, null);
		
		for(var i = 0; i < count; i++) {

			var u: number = (i ) + 1;

			this.connections[u] = new FakeCallPeer(u);
			this.orderedConnections.push(this.connections[u]);
		}
		
		setInterval(() => {
			
			//this.shuffle(this.orderedConnections);			
		} , 4000);
	}
	shuffle(array) {
	  var currentIndex = array.length, temporaryValue, randomIndex;
	
	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {
	
	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;
	
	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }
    if(this.sharingPeer != null) {
      this.sharingPeer = this.orderedConnections[1];   
    }
	  return array;
	}
	isConferenceCall(): boolean {
		return true;
	}
	async join() {
		
	}
	isConnected(): boolean { return true; }

	leaveCall(): void {}

	isCam(): boolean { return true; }
	
	isScreenshare(): boolean { return false; }
	
	toogleCam(): void {}
	toogleScreenshare(): void {
    
    if(this.sharingPeer == null) {
      this.sharingPeer = this.orderedConnections[1];
    } else {
      this.sharingPeer = null;
    }
  }
	toogleMuteAudio(): void {}
	
	isMuted(): boolean { return false; }
}