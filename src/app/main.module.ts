
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layouts/header/header.component';
import { HiddenCallUserComponent } from './layouts/hidden-call-user/hidden-call-user.component';
import { FooterComponent } from './layouts/footer/footer.component';
import {ChatbarComponent} from './layouts/chatbar/chatbar.component';
import {WrapperWithSidebarComponent} from './layouts/wrapper-with-sidebar/wrapper-with-sidebar.component';

import {WrapperWithSidebarAdminComponent} from './layouts/wrapper-with-sidebar-admin/wrapper-with-sidebar-admin.component';


import {LeftSidebarComponent} from './layouts/left-sidebar/left-sidebar.component';
import {HeaderTwoComponent} from './layouts/header-two/header-two.component';
import { JwtInterceptor } from './services/global/jwt.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BarNotificationComponent } from './modules/notifications/bar-notification/bar-notification.component';
import { AllNotificationsComponent } from './modules/notifications/all-notifications/all-notifications.component';
import { AudioPlayerComponent } from './layouts/audio-player/audio-player.component';

import { CallingDialScreenComponent } from './layouts/shared/calling-dial-screen/calling-dial-screen.component';
import { GroupChatWindowComponent } from './layouts/shared/group-chat-window/group-chat-window.component';
import { SingleChatWindowComponent } from './layouts/shared/single-chat-window/single-chat-window.component';

import { OfficeService } from './services/global/office.service';

import { CommonModule } from '@angular/common';
import { MainRoutes } from './main-routes';
import { UserSearchListComponent } from './layouts/shared/user-search-list/user-search-list.component';
import { DirectCallUserComponent } from './layouts/shared/direct-call-user/direct-call-user.component';
import { CreateTeamComponent } from './modules/team/create-team/create-team.component';
import { UsersContactListComponent } from './modules/shared/users-contact-list/users-contact-list.component';
import { ConferenceModule } from  './modules/conference/conference.module';
import { ChatComponent } from './modules/chat/chat/chat.component';
import { QuickChatComponent } from './modules/chat/quick-chat/quick-chat.component';
import { AllContactsComponent } from './modules/chat/all-contacts/all-contacts.component';
import { CreateGroupComponent } from './modules/chat/create-group/create-group.component';
import { ChatWindowComponent } from './modules/chat/chat-window/chat-window.component';
import { CallingScreenComponent } from './modules/chat/calling-screen/calling-screen.component';
import { CallNotPickedWindowComponent } from './modules/chat/call-not-picked-window/call-not-picked-window.component';
import { PpBreadcrumbsModule } from 'pp-breadcrumbs';
import { ItemResolver } from './resolvers/id.resolver';

import {DisconnectComponent} from "./modules/shared/disconnect/disconnect.component";
@NgModule({
  exports: [WrapperWithSidebarAdminComponent],
  declarations: [
    ChatbarComponent,
    WrapperWithSidebarComponent,
    WrapperWithSidebarAdminComponent,
    LeftSidebarComponent,
    HeaderTwoComponent,
    HiddenCallUserComponent,
    BarNotificationComponent,
    AllNotificationsComponent,
    AudioPlayerComponent,
    CallingDialScreenComponent,
    GroupChatWindowComponent,
    SingleChatWindowComponent,
    UserSearchListComponent,
    
    DirectCallUserComponent,
    CreateTeamComponent,
    UsersContactListComponent,
    ChatComponent,
    QuickChatComponent,
    AllContactsComponent,
    CreateGroupComponent,
    ChatWindowComponent,
    CallingScreenComponent,
    CallNotPickedWindowComponent,
    DisconnectComponent,
  ],
  imports: [
  CommonModule,
	FormsModule,
  ReactiveFormsModule,
  RouterModule.forChild(MainRoutes),
  ConferenceModule,
  PpBreadcrumbsModule
  ],
  providers: [ ChatbarComponent, ItemResolver ]
  //providers: [DatePipe, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },  ],
  //bootstrap: [AppComponent]

})
export class MainModule { }
