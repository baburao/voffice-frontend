import { NgModule, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Routes, RouterModule } from '@angular/router';
import {MainWrapperComponent} from './layouts/main-wrapper/main-wrapper.component';

import {WrapperWithSidebarComponent} from './layouts/wrapper-with-sidebar/wrapper-with-sidebar.component';
import {AuthGuard} from './_guards/auth.guard';
import {UnauthenticatedGuard} from './_guards/unauthenticated.guard';

import { MainModule } from './main.module';

import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from './services/global/user.service';
import { ConfService } from './services/conferences/conf.service';
//import {WrapperWithSidebar2Component} from './layouts/wrapper-with-sidebar-2/wrapper-with-sidebar-2.component';
import {WrapperWithSidebarAdminComponent} from './layouts/wrapper-with-sidebar-admin/wrapper-with-sidebar-admin.component'



@Injectable({ providedIn: 'root' })
export class UserResolve implements Resolve<any> {

  constructor(private userService: UserService, private confService: ConfService) {}

  resolve(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
    //return this.userService.loadData();
    return new Promise<any>((resolve: any, reject: any) => {
    
      this.userService.loadData().then(() => {
        this.confService.init().then( () => {
          resolve();
        });
      });
        
    });
    
  }
}

const routes: Routes = [
  {
    path: '',
    component: MainWrapperComponent,
    loadChildren: () => import('./modules/accounts/accounts.module').then(mod => mod.AccountsModule),
    canActivate: [UnauthenticatedGuard],
  },
  {
    path: '',
    component: WrapperWithSidebarComponent,
    loadChildren: () => import('./main.module').then(mod => mod.MainModule),
  canActivate: [AuthGuard],
	resolve: { data : UserResolve }
  },
  {
    path: '',
    component: WrapperWithSidebarAdminComponent,
    loadChildren: () =>  import('./modules/admin/admin.module').then(mod => mod.AdminModule),
  canActivate: [AuthGuard],
	resolve: { data : UserResolve }
  },


  /*{
    path: '',
    component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/conference/conference.module').then(mod => mod.ConferenceModule),
  canActivate: [AuthGuard],
	resolve: { data : UserResolve }
  },
{
    path: 'office',
    component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/conversation/conversation.module').then(mod => mod.ConversationModule),
  canActivate: [AuthGuard],
	resolve: { data : UserResolve }
  },
/*
  {
    path: '',
    component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/team/team.module').then(mod => mod.TeamModule),
  canActivate: [AuthGuard],
	resolve: { data : UserResolve }
  },

  {
    path: '',
    component: WrapperWithSidebarComponent,
    loadChildren: () => import('./modules/department/department.module').then(mod => mod.DepartmentModule),
    canActivate: [AuthGuard],
	resolve: { data : UserResolve }
  },*/

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  
})
// @ts-ignore
export class AppRoutingModule { }
