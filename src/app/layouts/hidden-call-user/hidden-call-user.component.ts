import { Component, OnInit, Renderer2, ViewChild, ElementRef, Input } from '@angular/core';
import { CallPeer } from '../../call/call2';

@Component({
  selector: 'app-hidden-call-user',
  templateUrl: './hidden-call-user.component.html',
  styleUrls: ['./hidden-call-user.component.css']
})
export class HiddenCallUserComponent implements OnInit {

  @Input() connection : CallPeer | null = null;
 
  videoPlayer: HTMLVideoElement | null = null;

  @ViewChild('streamPlayer')
  set streamPlayer(el: ElementRef) {
    this.videoPlayer = el.nativeElement;
  
    this.connection!.setVideoOutput(this.videoPlayer);
  }
  constructor() { }
  ngOnInit(): void { }
  
  ngOnChanges() {
  
    if(this.videoPlayer != null) {
      
      this.connection!.setVideoOutput(this.videoPlayer);
    }
  }

}
