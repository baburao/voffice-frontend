import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiddenCallUserComponent } from './hidden-call-user.component';

describe('HiddenCallUserComponent', () => {
  let component: HiddenCallUserComponent;
  let fixture: ComponentFixture<HiddenCallUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiddenCallUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiddenCallUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
