import { Component, OnInit, Renderer2, ViewChild, ElementRef, Input } from '@angular/core';
import {AuthService} from "../../services/accounts/auth.service";
import {Router, ActivatedRoute, Event} from "@angular/router";
import { UserService } from 'src/app/services/global/user.service';
import { CurrentUser } from 'src/app/interfaces/user';
import api_urls from 'src/app/constants/api_urls';
import { HttpClient } from '@angular/common/http';

import { OfficeService } from '../../services/global/office.service';
import { ConfService } from '../../services/conferences/conf.service';

import { Location } from "@angular/common";
import { NotificationService } from 'src/app/services/global/notification.service';  

import { Conference } from 'src/app/interfaces/conference';  
import { Call2 } from 'src/app/call/call2';  

import { PpBreadcrumbsService, Breadcrumb } from 'pp-breadcrumbs';
import { ignoreElements } from 'rxjs/operators';
declare var $:any;

@Component({
  selector: 'app-header-two',
  templateUrl: './header-two.component.html',
  styleUrls: ['./header-two.component.css']
})
export class HeaderTwoComponent implements OnInit {
  currentUser: CurrentUser | null = null;
  departments: any[] = []; //TODO
  updateData: any = {department: 0}
  
  imageUrl: string = '';
  errors: string[]= [];
  currentRouteUrl: string = '';
  notifications: Notification[] = [];
  
  notificationsOpen: boolean = false;
  notificationCount: number = 0;

  contactClass = 'contactsbox-toggle-open';
  selectedTabName = 'profile';

  newPassword:string='';
  confirmNewPassword:string='';
  isShowProfile = false;

  currentConference: Conference | null = null;
  currentCall: Call2 | null = null;

  breadcrumbs:any;

  conferenceId:any;

  disableDepartment = true;
  constructor(private auth: AuthService, private router: Router, private userService: UserService, private confService: ConfService,

    private http: HttpClient, private officeService: OfficeService, private activatedRoute: ActivatedRoute, location: Location,
    private ns: NotificationService, private renderer: Renderer2,
    private breadcrumbsService: PpBreadcrumbsService,
    ) {

    this.imageUrl = api_urls.ProfileImageSmall + this.userService.currentUser!.id;
    router.events.subscribe(val => {
      if (location.path() != "") {
        
        this.currentRouteUrl = location.path();
        this.currentRouteUrl  = this.currentRouteUrl.replace(/\?.*$/g,"");
        if(this.currentRouteUrl.split("/").length - 1 > 1)
          this.currentRouteUrl = this.currentRouteUrl.substring(0, this.currentRouteUrl.lastIndexOf("/"))
          
      } else {
        this.currentRouteUrl = "/office";
      }
    });

    this.breadcrumbsService.postProcess = (breadcrumbs: Breadcrumb[]) => {
      breadcrumbs.unshift({ text: `Creativeland Asia`, path: '/' });
      this.breadcrumbs = breadcrumbs;
        this.confService.getConferences().subscribe((cfs: {[key: number]: Conference}) => {          
          if(this.breadcrumbs && cfs) {
            this.breadcrumbs.forEach(e => {
              if(e.path.includes('/conferences/join/')) {
                e.text = cfs[parseInt(e.text)]['title'];
              }
            });
          }          
        })
      return breadcrumbs;
    };


          
    }

  

  ngOnInit(): void {
    this.currentUser = this.userService.currentUser;
    
    this.officeService.rooms.subscribe((rooms: {[key: number]: any}) => {
      
      for(var k in rooms) {
        
        if(rooms[k].type =='de'){
          this.departments.push(rooms[k]);
          
          if(this.isOwnRoom(rooms[k])) {
            this.updateData.department = rooms[k].id;
          }
        }
      }
    });

    this.ns.getNotifications();
    this.ns.notifications.subscribe(ntfs => {
      this.notifications = ntfs;
    });
    this.ns.notificationCountObservable.subscribe((count: number) => {
      this.notificationCount = count;
    });
    
    this.confService.activeCallObservable.subscribe(( newCall: Call2| null) => {
      
      this.currentCall = newCall;
      this.currentConference = newCall != null ? newCall.conference : null;
    });
  }

  ngAfterViewInit(): void {
    $('.user-profile-form').slimScroll({
      height: '35vw',
      width: '100%',
      color: '#5d5d5d'
    });

    $('.notification-drop-list').slimScroll({
      height: '398px',
      width: '100%',
      color: '#5d5d5d'
    });
  }

  addUserProfileScrollBar() {
    setTimeout(() => {
      $('.user-profile-form').slimScroll({
        height: '35vw',
        width: '100%',
        color: '#5d5d5d'
      });
    }, 0);
    
  }
  logout(){
    this.auth.logout();
    this.router.navigate([''])
  }

  onUploadProfile(event) {
    const file = event.target.files.item(0);

    const formData: FormData = new FormData();
      formData.append('file', file, file.name);
  
      this.http.post(api_urls.UploadAvatar, formData).subscribe(() => { 
        this.imageUrl = api_urls.ProfileImageBig + this.userService.currentUser!.id + "?" + new Date().getTime();
      });	
  }

  updateProfile(selectedTabName) {    
    if(selectedTabName == 'password') {
      if(this.validateChangePassword()){
        this.userService.changePassword(this.newPassword).subscribe(res => {
          this.newPassword = '';
          this.confirmNewPassword = '';
          this.errors = [];
        });
      }
    } else {
      let userUpdateObj = {
        firstname: this.currentUser!.firstname, 
        lastname: this.currentUser!.lastname,
        email: this.currentUser!.email,
        department: this.updateData.department
      }
      this.userService.updateUserProfile(userUpdateObj);      
    }

    this.isShowProfile = false;
  }

  validateChangePassword() {
    if(!this.newPassword) {
      this.errors['newPasswordEmpty'] = 'Please enter new password.';
      return false;
    } else if(!this.confirmNewPassword){
      this.errors['confirmNewPasswordEmpty'] = 'Please enter confirm password.';
      return false;
    } else if(this.newPassword !== this.confirmNewPassword) {
      this.errors['passwordNotSame'] = 'New password and confirm password not same.';
      return false;
    }else {
      return true;
    }
  }
  
  isOwnRoom(room: any): boolean {
    
    if(room.type == 'de') {
      
      return room.workspaces.includes(this.userService.currentUser!.id);
    } else {
      return false;
    }
  }
  
  toggleNotifications(): void {
    this.notificationsOpen = !this.notificationsOpen;
    
    this.ns.setNotificationWindowOpen(this.notificationsOpen);
    
  }

  openChatBar() {
    const hasClass = document.body.classList.contains(this.contactClass);
    if (hasClass) {
      this.renderer.removeClass(document.body, this.contactClass);
    } else {
      this.renderer.addClass(document.body, this.contactClass);
    }
  }

  selectedTab(selectedTab:string) {
    this.selectedTabName = selectedTab;
    if(this.selectedTabName == 'profile') {
      this.addUserProfileScrollBar();
    }
  }

  toggleEditProfile() {
    this.isShowProfile = this.isShowProfile ? false : true;
  }

  isBlink() {
    return !this.currentRouteUrl.startsWith("/conferences") && this.currentConference !== null
  }
}

