import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wrapper-with-sidebar-admin',
  templateUrl: './wrapper-with-sidebar-admin.component.html',
  styleUrls: ['./wrapper-with-sidebar-admin.component.css']
})
export class WrapperWithSidebarAdminComponent implements OnInit {

  constructor() { }

  public isChild:boolean;

  ngOnInit(): void {
  }

  toggleClass():boolean{
    var x = this.isChild = !this.isChild;
    return x;
  }

}
