import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrapperWithSidebarComponent } from './wrapper-with-sidebar.component';

describe('WrapperWithSidebarComponent', () => {
  let component: WrapperWithSidebarComponent;
  let fixture: ComponentFixture<WrapperWithSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrapperWithSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperWithSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
