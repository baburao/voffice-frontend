import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wrapper-with-sidebar',
  templateUrl: './wrapper-with-sidebar.component.html',
  styleUrls: ['./wrapper-with-sidebar.component.css']
})
export class WrapperWithSidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
