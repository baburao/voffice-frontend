import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrapperWithSidebar2Component } from './wrapper-with-sidebar-2.component';

describe('WrapperWithSidebar2Component', () => {
  let component: WrapperWithSidebar2Component;
  let fixture: ComponentFixture<WrapperWithSidebar2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrapperWithSidebar2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperWithSidebar2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
