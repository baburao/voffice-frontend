import { Component, OnInit, Input, Output, EventEmitter, Renderer2 } from '@angular/core';
import { Conversation } from 'src/app/interfaces/conversation';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { environment } from 'src/environments/environment';
import api_urls from 'src/app/constants/api_urls';
import {User} from '../../../interfaces/user';
import { UserService } from 'src/app/services/global/user.service';
import { ConversationService } from 'src/app/services/global/conversation.service';
import { AudioRecorder } from 'src/app/utils/audio-recorder';

@Component({
  selector: 'app-group-chat-window',
  templateUrl: './group-chat-window.component.html',
  styleUrls: ['./group-chat-window.component.css']
})
export class GroupChatWindowComponent implements OnInit {
  @Input() currentConversations: Conversation | null = null;
  @Input() users: { [key: number]: User } = {};
  @Output() onCallUserClick = new EventEmitter;
  
  @Output() closeChat = new EventEmitter;
  @Output() onSendMessageClick = new EventEmitter;
  @Output() uploadFile = new EventEmitter;
  @Output() toggleRecording = new EventEmitter;
  public audioRecorder: AudioRecorder | null = null;
  fileRoot = environment.FileRoot;
  audioUrl: string = api_urls.GetAudioMessage;

  constructor(public chatUtil: ChatUtilsService, public userService: UserService,
    public conversationService: ConversationService, private renderer: Renderer2) { }

  ngOnInit(): void {
  
  }

  closechatWindow(forced = true) {    
    this.closeChat.emit(forced)
  }
  selectConversation(conversationId: number) {
	  this.conversationService.selectConversation(conversationId);
  }

  dateSection(index:number){
    const messages = this.currentConversations?.messages;
    if (messages!=null){
      try {
        const current = this.chatUtil.formatDate(new Date(messages[index]['date']));
        const next = this.chatUtil.formatDate(new Date(messages[index+1]['date']));
        return current !== next;
      }catch (e) {
        return true
      }
    }
    return false
  }
  
  sendMessage(text: string): void {
    this.onSendMessageClick.emit(text)
  }

  onUploadFile(event) {    
    this.uploadFile.emit(event)
  }

  onToggleRecording() {
    this.toggleRecording.emit()
  }


  currentConversionFilteredUsers(): number[]{

    if(this.currentConversations == null){
      return [];
    }
      return this.currentConversations!.users.filter((targetId: number) => !this.isSelf(targetId))	;
  }
  isSelf(userId: number) {
    return this.userService.currentUser?.id == userId;
  }

  callUser(userId: number) {
    this.onCallUserClick.emit(userId)
  }

}
