import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallingDialScreenComponent } from './calling-dial-screen.component';

describe('CallingDialScreenComponent', () => {
  let component: CallingDialScreenComponent;
  let fixture: ComponentFixture<CallingDialScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallingDialScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallingDialScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
