import { Component, OnInit, Input, ViewChild, Renderer2, Output, EventEmitter } from '@angular/core';
import api_urls from 'src/app/constants/api_urls';
import { UserService } from 'src/app/services/global/user.service';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { CallPeer } from 'src/app/call/call2';
import { DirectCall, CallState } from 'src/app/call/direct-call';
import { NotificationService } from 'src/app/services/global/notification.service';
import { ConversationService } from 'src/app/services/global/conversation.service';

@Component({
  selector: 'app-calling-dial-screen',
  templateUrl: './calling-dial-screen.component.html',
  styleUrls: ['./calling-dial-screen.component.css']
})
export class CallingDialScreenComponent implements OnInit {
  
  @Input() users;
  directCall: DirectCall| null = null;
  @Output() onCallNotPicked = new EventEmitter;

  @Output() callNotPicked = new EventEmitter;
  
  smallProfileImage: string = api_urls.ProfileImageSmall;

  addUserToCallDrawer: boolean = false;
  
  
  
  //search user to add on call variables
  filteredUsers:any[] = [];
  userFilterStr: string = '';
  isCallAccepted: boolean = false;

  callConnectTime: number = 0;
  interval: any;
  defaultPickTime: number = 30; //seconds
  onCallTime: number = 0; //seconds
  onCallTimeCaller: number = 0;

  callTimeCallerInterval: any;
  callTimeReceiverInterval: any;
  constructor( public userService: UserService,
    private conferenceService: ConfService, private notificationService: NotificationService, 
    private conversationService: ConversationService) {
      this.conferenceService.directCallObservable.subscribe((newCall: DirectCall) => {
        this.directCall = newCall;
      });
    }

  ngOnInit(): void {
    //this.startTimer();    
  }

  onAcceptCall(): void {
    console.log("call accepted")
    this.notificationService.stopNotificationAudio();
    this.conferenceService.acceptCall(); 
  }

  onDeclineCall(): void {
    this.notificationService.stopNotificationAudio();  
    this.conferenceService.declineCall();
  }

  onHangUpCall(): void {
    this.notificationService.stopNotificationAudio();
    this.conferenceService.leaveDirectCall();    
    //this.hangUpCallClick.emit(userId)
  }
  isConnected(): boolean {    
    
    return this.directCall != null ? this.directCall.state == CallState.ACTIVE : false;
  }
  getConnection(): CallPeer | null {
    
    return this.isConnected() ? this.directCall!.call!.orderedConnections[0] : null;
  }
  toggleVideo(): void {
    if(!this.isConnected()) {
      return;
    }
    this.directCall!.call!.toogleCam();
  }

  toggleAudio(): void {
    if(!this.isConnected()) {
      return;
    }
    this.directCall!.call!.toogleMuteAudio();
  }

  isMuted(): boolean {
    
    if(!this.isConnected()) {
      return false;
    }
        
    return this.directCall!.call!.isMuted();
  }
  isCam(): boolean {
    
    if(!this.isConnected()) {
      return false;
    }
        
    return this.directCall!.call!.isCam();
  }

  isRecording(): boolean {
    if(this.directCall == null) {
       return false;
    }
    return this.directCall!.call!.isRecording();
  }

  callStatus() {
    if (this.directCall != null && this.directCall!.state == 'declined' || this.directCall!.state == "ended") {
      this.directCall = null;
      return false;
    } else {
      return true;
    }
  }
  addUserToCallToggle() {
    this.addUserToCallDrawer = this.addUserToCallDrawer ? false : true;;
  }

  filterUserToaddToCall(fullname: string) {    
    /*this.userFilterStr = fullname;
    let userArr:object[] = [];
    if(fullname.length == 0) {
      this.filteredUsers = [];
    }
      
    if (fullname.length > 0) {
      let usersObj = this.users;
      if (Object.keys(usersObj).length) {
        Object.keys(usersObj).forEach(function (key) {
          usersObj[key].fullname = usersObj[key].firstname + " " + usersObj[key].lastname;  
          userArr.push(usersObj[key]);
        });
        this.filteredUsers = userArr.filter(o =>
          Object.keys(o).some(k => o['fullname'].toLowerCase().includes(fullname.toLowerCase())));      
      }
    }*/
  }
  toggleRecord(): void {
    if(!this.isConnected()) {
      return;
    }
    
    if(this.directCall!.call!.isRecording()) {
      this.directCall!.call!.stopRecord();
    } else {
      this.directCall!.call!.startRecord((audioUrl: any, buffer: any) => { 
        
        this.conversationService.sendAudioMessage(buffer);
      });
    }
  }
}
