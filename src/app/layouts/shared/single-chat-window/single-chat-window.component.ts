import { Component, OnInit, EventEmitter, Input, Output, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { Conversation } from 'src/app/interfaces/conversation';
import { Message } from 'src/app/interfaces/message';
import { User } from '../../../interfaces/user';
import { AudioRecorder } from 'src/app/utils/audio-recorder';
import { environment } from 'src/environments/environment';
import api_urls from 'src/app/constants/api_urls';
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { UserService } from 'src/app/services/global/user.service';
import { ConversationService } from 'src/app/services/global/conversation.service';

import { ConfService } from 'src/app/services/conferences/conf.service';
import { DirectCall, CallState } from 'src/app/call/direct-call';
import { NotificationService } from 'src/app/services/global/notification.service';
declare var $: any;
@Component({
  selector: 'app-single-chat-window',
  templateUrl: './single-chat-window.component.html',
  styleUrls: ['./single-chat-window.component.css']
})
export class SingleChatWindowComponent implements OnInit {
  @ViewChild('messageText') messageInput:ElementRef | undefined;

  @Input() currentConversations: Conversation | null = null;
  @Input() users: { [key: number]: User } = {};
  @Input() hasConvoConversation;
  //@Input() callNotPicked:boolean = false;
  @Output() closeChat = new EventEmitter;
  @Output() onCallUserClick = new EventEmitter;

  @Output() onSendMessageClick = new EventEmitter;
  @Output() uploadFile = new EventEmitter;
  @Output() toggleRecording = new EventEmitter;
  @Output() callWindowClosed = new EventEmitter;

  public audioRecorder: AudioRecorder | null = null;
  fileRoot = environment.FileRoot;
  audioUrl: string = api_urls.GetAudioMessage;
  smallProfileImage: string = api_urls.ProfileImageSmall;

  directCall: DirectCall| null = null;
  
  isRecording: boolean = false;
  personalCallMessageBox: boolean = true;
  stopRecording: boolean = true;
  constructor(public chatUtil: ChatUtilsService, public userService: UserService, private conferenceService: ConfService,
    public conversationService: ConversationService, private renderer: Renderer2,
    private notificationService:NotificationService) {
    
    this.conferenceService.directCallObservable.subscribe((newCall: DirectCall) => {
        this.directCall = newCall;
     });
  }

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    // setTimeout(() => {
    //   this.messageInput?.nativeElement.foccus();
    // });

    $('.chat-msgbox-popup #chat-msgbox-surface-bg').slimScroll({
      height: '28vw',
      width: '100%',
      color: '#fff'
    });
      
  }

  closechatWindow(forced = true) {
    this.closeChat.emit(forced)
  }
  selectConversation(conversationId: number) {
    this.conversationService.selectConversation(conversationId);
  }

  removeClass(cls: string) { // removes class from Body
    this.renderer.removeClass(document.body, cls);
  }

  dateSection(index: number) {
    const messages = this.currentConversations?.messages;
    if (messages != null) {
      try {
        const current = this.chatUtil.formatDate(new Date(messages[index]['date']));
        const next = this.chatUtil.formatDate(new Date(messages[index + 1]['date']));
        return current !== next;
      } catch (e) {
        return true
      }
    }
    return false
  }

  sendMessage(text: string): void {
    console.log(text);
    
    this.onSendMessageClick.emit(text)
  }

  onUploadFile(event) {    
    this.uploadFile.emit(event)
  }

  onToggleRecording() {
    this.stopRecording = !this.stopRecording
    this.toggleRecording.emit()
  }
  currentConversionFilteredUsers(): number[] {
    if (this.currentConversations == null) {
      return [];
    }
    return this.currentConversations!.users.filter((targetId: number) => !this.isSelf(targetId));
  }

  isSelf(userId: number) {
    return this.userService.currentUser?.id == userId;
  }

  callUser(userId: number) {
    this.onCallUserClick.emit(userId);
    this.notificationService.stopNotificationAudio();
  }

  closeWindow(isRecordMessageWindow = false) {
    if (isRecordMessageWindow) {
      this.isRecording = false;
    }
    this.conferenceService.closeDirectCall();
   // this.callNotPicked = false;
    this.callWindowClosed.emit(false);
  }

  recordMessage() {
    this.isRecording = true;
    this.personalCallMessageBox = false;
  }
  callDuration(message: Message): Date {
    
    return new Date(new Date(message.call.end_date).getTime() - new Date(message.date).getTime());
  }
  callAgain(): void {
    
    console.log("Again", this.directCall);
    
    if(this.directCall == null) {
      return;
    }
    var userId = this.directCall.targetUser;
    this.conferenceService.closeDirectCall();
    this.onCallUserClick.emit(userId);
  }
  showDeclinedWindow(): boolean {
    return this.directCall != null ? this.directCall.state == 'declined' : false;
  }
}
