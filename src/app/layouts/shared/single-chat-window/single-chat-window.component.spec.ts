import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleChatWindowComponent } from './single-chat-window.component';

describe('SingleChatWindowComponent', () => {
  let component: SingleChatWindowComponent;
  let fixture: ComponentFixture<SingleChatWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleChatWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleChatWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
