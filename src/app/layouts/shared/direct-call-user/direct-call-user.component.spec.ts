import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectCallUserComponent } from './direct-call-user.component';

describe('DirectCallUserComponent', () => {
  let component: DirectCallUserComponent;
  let fixture: ComponentFixture<DirectCallUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectCallUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectCallUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
