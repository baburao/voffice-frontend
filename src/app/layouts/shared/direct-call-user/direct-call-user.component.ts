import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

import { CallPeer } from '../../../call/call2';

@Component({
  selector: 'old-app-direct-call-user',
  templateUrl: './direct-call-user.component.html',
  styleUrls: ['./direct-call-user.component.css']
})
export class DirectCallUserComponent implements OnInit {
  
  @Input() connection : CallPeer | null = null;

  videoPlayer: HTMLVideoElement | null = null;

  @ViewChild('streamPlayer')
  set streamPlayer(el: ElementRef) {
    this.videoPlayer = el.nativeElement;
  
    this.connection!.setVideoOutput(this.videoPlayer);
  }
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
  
    /*if(this.videoPlayer != null && this.videoPlayer?.srcObject != this.connection!.stream) {
      
      this.connection!.setVideoOutput(this.videoPlayer);
    }
    console.log("changes vid", this.connection);*/
  }
}
