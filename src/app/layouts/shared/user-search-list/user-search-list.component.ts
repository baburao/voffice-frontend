import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import api_urls from 'src/app/constants/api_urls';

@Component({
  selector: 'app-user-search-list',
  templateUrl: './user-search-list.component.html',
  styleUrls: ['./user-search-list.component.css']
})
export class UserSearchListComponent implements OnInit {
  
  @Input() users: { [key: number]: User } = {};
  @Input() addUserToCallDrawer;

  filteredUsers:any[] = [];
  userFilterStr: string = '';

  smallProfileImage: string = api_urls.ProfileImageSmall;
  constructor() { }

  ngOnInit(): void {
  }

  filterUserToaddToCall(fullname: string) {    
    this.userFilterStr = fullname;
    let userArr:object[] = [];
    if(fullname.length == 0) {
      this.filteredUsers = [];
    }
      
    if (fullname.length > 0) {
      let usersObj = this.users;
      if (Object.keys(usersObj).length) {
        Object.keys(usersObj).forEach(function (key) {
          usersObj[key].fullname = usersObj[key].firstname + " " + usersObj[key].lastname;  
          userArr.push(usersObj[key]);
        });
        this.filteredUsers = userArr.filter(o =>
          Object.keys(o).some(k => o['fullname'].toLowerCase().includes(fullname.toLowerCase())));      
      }
    }
  }

  addUserToCallToggle() {
    this.addUserToCallDrawer = this.addUserToCallDrawer ? false : true;
  }

}
