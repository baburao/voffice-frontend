import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import {Router} from "@angular/router";
import { HttpClient } from '@angular/common/http';

import { UserService } from 'src/app/services/global/user.service';
import api_urls from 'src/app/constants/api_urls';
import { User } from 'src/app/interfaces/user';
import { Room } from 'src/app/interfaces/room';
import { ConfService } from 'src/app/services/conferences/conf.service';
import { Conference } from 'src/app/interfaces/conference';

import {AuthService} from "../../services/accounts/auth.service";
declare var $:any;
@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.css']
})
export class LeftSidebarComponent implements OnInit {

  bodyClass = 'left-slide-toggle-open';
  user: User;
  smallProfileImage: string = api_urls.ProfileImageSmall;
  dateTime: any;
  department: Room | null = null;
  noOfConfInvite: any = 0;
  constructor(
    private renderer: Renderer2, private auth: AuthService, private router: Router,
    private userService:UserService,private http: HttpClient,public confservice: ConfService,
  ) {
    this.user = this.userService.currentUser;
    console.log(this.user)
   }

  ngOnInit(): void {
    this.getCurrentDate();
    this.getDepartMent();
    this.getConferences();
  }

  ngAfterViewInit(): void {
    $('.left-sidebar-inner-2').slimScroll({
      height: '100%',
      width: '100%',
      color: '#fff'
  });
    
  }
  toggleSidebar(){
    const hasClass = document.body.classList.contains(this.bodyClass);
    if (hasClass){
      this.renderer.removeClass(document.body, this.bodyClass);
    } else {
      this.renderer.addClass(document.body, this.bodyClass);
    }
  }

  removesidebar(){
    this.renderer.removeClass(document.body, this.bodyClass);
  }

   logout(){
    this.auth.logout();
    this.router.navigate([''])
  }

  
  getCurrentDate() {
    setInterval(() => {
      this.dateTime = new Date();
    }, 1000);
  }

  getDepartMent() {
    this.http.get(api_urls.GetDepartment).subscribe((department: any) => {		
      if(department.id != -1) {
        this.department = department;
        if(department == null){
          return;
        }    
      }	
    });
  }

  getConferences(): void {
    this.confservice.getConferences().subscribe(conferences => {this.noOfConfInvite = Object.keys(conferences).length;});
  }

  navigate(url:string) {
    if(this.noOfConfInvite)
      this.router.navigate([url]);
  }
  
}
