import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConversationService } from '../../services/global/conversation.service';
import { UserService } from '../../services/global/user.service';
import { ConfService } from '../../services/conferences/conf.service';
import { User } from '../../interfaces/user';
import { Conversation } from '../../interfaces/conversation';
import { DirectCall } from '../../call/direct-call';
import api_urls from 'src/app/constants/api_urls';
import { UtilsService } from '../../services/global/utils.service';
import { AudioRecorder } from '../../utils/audio-recorder';
import { environment } from "../../../environments/environment";
import { FormBuilder } from "@angular/forms";
import { ChatUtilsService } from 'src/app/services/global/chat-utils.service';
import { NotificationService } from 'src/app/services/global/notification.service';

declare var $: any;

@Component({
  selector: 'app-chatbar',
  templateUrl: './chatbar.component.html',
  styleUrls: ['./chatbar.component.css']
})
export class ChatbarComponent implements OnInit {
  fileRoot = environment.FileRoot;
  chatBar = false;
  chatDrawerClass = 'chatbox-right-toggle-open';
  groupDrawerClass = 'group-messagebox-toggle-open';
  contactClass = 'contactsbox-toggle-open';
  groupClass = 'creategroup-toggle-open';

  smallProfileImage: string = api_urls.ProfileImageSmall;

  users: { [key: number]: User } = {};
  conversations: Conversation[] = [];
  currentConversations: Conversation | null = null;
  conferenceConversation: Conversation | null = null;
  numUnreadMessages = 0;
  duration = 0;

  directCall: DirectCall | null = null;
  
  //incomingCall: any = null;

  public audioRecorder: AudioRecorder | null = null;
  audioUrl: string = api_urls.GetAudioMessage;

  shouldScroollDown = false;

  groupForm: any;
  errors: any;
  filters: any;
  groupUsers = [];

  userSearchStr: string = '';
  usersSearchResult: any[] = [];
  searchedGroups: any[] = [];

  //outGoingCallUserId: any;
  //outGoingCall = false;
  addUserToCallDrawer = false;

  selectedUser: User | null = null;

  //callNotPicked: boolean = false;
  showCallNotPicked: boolean = false;
  removedUsers: number[] = [];

  constructor(private renderer: Renderer2, private http: HttpClient,
    public userService: UserService, private formBuilder: FormBuilder,
    public conversationService: ConversationService, private utils: UtilsService,
    private conferenceService: ConfService, public chatUtil: ChatUtilsService,
    private ns:NotificationService) {

    this.groupForm = this.formBuilder.group({ title: '' });
    this.conversationService.removedUsersObserver.subscribe((removedUsers: number[]) => {
      
      this.removedUsers = removedUsers;
    });
  }

  ngOnInit(): void {
    this.utils.confConvo.subscribe(convo => this.conferenceConversation = convo);


     this.getUsers();
	   this.conversationService.conversations.subscribe(
	    (conversations: Conversation[]) => {
         this.conversations = conversations
      });

    this.conversationService.numUnreadMessages.subscribe(
      (numUnreadMessages: number) => this.numUnreadMessages = numUnreadMessages);

    this.conversationService.currentConversation.subscribe((newConversation: Conversation | null) => {

      if (newConversation != null && newConversation.is_conference) {
        //close open chat windows and chat bar first
        //this.closechatDrawer(true);
        this.chatBar = false;
        
      }

      this.currentConversations = newConversation;
      if(this.currentConversations) {
        if(this.removedUsers.length && this.removedUsers.includes(this.currentConversations.id)) {
          this.removedUsers.splice(this.removedUsers.indexOf(this.currentConversations.id), 1);
        }
      }
        
      console.log("currentConversations", this.currentConversations);
    });

    this.conferenceService.directCallObservable.subscribe((newCall: DirectCall) => {
      this.directCall = newCall;
      if (this.directCall !== null && !this.hasClass(this.contactClass)) {
        this.ns.playNotificationAudio();
        this.toggleContacts()
      }
    });
     
   /* this.conferenceService.incomingCallObservable.subscribe((newCall: any) => {

      this.incomingCall = newCall;
      if (this.incomingCall !== null && !this.hasClass(this.contactClass)) {
        this.toggleContacts()
      }
      console.log("incomingCall", this.incomingCall);
    });
    this.conferenceService.callDeclineSource.subscribe((callDecline: any) => {
      this.outGoingCall = callDecline;
      this.incomingCall = callDecline;
    });*/
    
    this.conversationService.onNewMessage.subscribe(() => { this.shouldScroollDown = true; });
    this.filters = []
  }

  ngAfterViewInit(): void {
    $('#all-contactsbox-main #all-contactsbox-surface-bg').slimScroll({
      height: '28vw',
      width: '100%',
      color: '#fff'
    });    
  }

  ngAfterViewChecked(): void {

    if (this.shouldScroollDown) {
      this.scrollDown();
      this.shouldScroollDown = false;
    }
    
    //Dirty hack to show the call not picked window
    if(this.directCall != null && this.directCall.state == 'declined' && !this.showCallNotPicked) {
     
      this.showCallNotPicked = true;
      this.toggleChatDrawer(this.currentConversations!.id, true,false,false,this.currentConversations);
    }
  }

  // Desgin Functions //
  toggleChat(action = false) { // toggles chat bar

    // @ts-ignore

    // this.chatBar = action;
    if (!this.conversations.length || this.getLatestMessageCount() == 0
    || this.removedUsers.length == this.conversations.length) {
      this.toggleContacts();
      return;
    }
    
    if(this.currentConversations && this.currentConversations.is_conference){
      this.toggleChatDrawer(this.currentConversations.id,false,true,!action);
      
       this.conversationService.setCurrentConversation(this.currentConversations);
    } else {
      this.chatBar = action;
	  }
    // if(this.conferenceConversation && this.conferenceConversation.is_conference)
    //  this.toggleChatDrawer(this.conferenceConversation.id,false,true,!action)
    // }

  }



  // Get latest message count from conversation
  getLatestMessageCount() {
    let count = 0;
    if (this.conversations.length) {
      this.conversations.forEach(e => {
        if (e.latest)
          count++;
      })
    }
    return count;
  }

  hasClass(cls: string) { // checks if class exists
    return document.body.classList.contains(cls);
  }

  addClass(cls: string) { // adds class to body
    this.renderer.addClass(document.body, cls);
  }

  removeClass(cls: string) { // removes class from Body
    this.renderer.removeClass(document.body, cls);
  }

  toggleChatDrawer(id = -1, contactDrawer = false, force = false, action = false, conversation: any = null) { // toggles Chat Bar and selects Conversation
    let isDirect = this.isDirect(conversation);

    if (contactDrawer) {
      this.toggleContacts();
    }
    if (id > 0) {
      this.selectConversation(id);
    } else {
      this.selectConversation(-1);
    }
    let hasClass;
    if (isDirect) {
      hasClass = this.hasClass(this.chatDrawerClass);

      if (force) { hasClass = action; }

      if (hasClass) {
        this.removeClass(this.chatDrawerClass);
      } else {
        this.addClass(this.chatDrawerClass);
      }
    } else {
      hasClass = this.hasClass(this.groupDrawerClass);

      if (force) { hasClass = action; }

      if (hasClass) {
        this.removeClass(this.groupDrawerClass);
      } else {
        this.addClass(this.groupDrawerClass)
      }
    }
    this.searchContact('');

  }

  closechatDrawer(forced = false) {
    if (forced) {
      this.removeClass(this.chatDrawerClass);
      this.removeClass(this.groupDrawerClass);
    } else {
    }
    if (this.currentConversations == null || !this.currentConversations.is_conference) {
      this.selectConversation(-1);
    }
  }

  toggleContacts() { // toggles Contact list.
    const hasClass = this.hasClass(this.contactClass);
    if (hasClass) {
      this.removeClass(this.contactClass);
    } else {
      this.addClass(this.contactClass);
    }
  }

  toggleGroup() {
    const hasClass = this.hasClass(this.groupClass);
    if (hasClass) {
      this.removeClass(this.groupClass);
    } else {
      this.addClass(this.groupClass);
    }

  }
  // core functions //

  sendMessage(text: string): void {
    this.conversationService.sendMessage(text);
    // this.scrollDown();
  }
  scrollDown() {

    var containers = $(".scrollDownContainer");

    for (var i = 0; i < containers.length; i++) {

      $(containers[i]).scrollTop(containers[i].scrollHeight);
    }
  }
  createConversation(targetUserId: number) {

    this.conversationService.createConversation(targetUserId).subscribe((conversation: Conversation) => {

      this.selectConversation(conversation.id);
    });
  }
  selectConversation(conversationId: number) {

    //this.currentConversations =
    this.conversationService.selectConversation(conversationId);
    // this.scrollDown();
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => {
      this.users = users
    });
  }



  ciricleBGC() {
    const colours = ['pink-l', 'blue-l', 'green-l'];
    // return  colours[Math.floor(Math.random() * colours.length)]
    return 'pink-l';
  }

  chatClass(userId: number) {
    return this.chatUtil.isCurrentUser(userId) ? 'receiver-msg-contain' : 'sender-msg-contain';
  }

  unreadMessages(conversation: any) {
    if (conversation.latest == null) {
      return false;
    }
    return conversation.latest.new_message;
  }
  latest(convo: any) {
    if (convo.latest) {
      return convo.latest.text;
    } else {
      return 'No messages.';
    }
  }

  toggleRecording(): void {
    if (this.audioRecorder == null) {
      this.startRecording();
    } else {
      this.stopRecording();
    }
  }

  startRecording(): void {

    let self = this;

    this.audioRecorder = new AudioRecorder();
    this.audioRecorder.record(function() { self.audioRecorder = null; });
  }

  stopRecording() {
    if (this.audioRecorder == null) {
      return;
    }
    let self = this;

    this.audioRecorder.stop((buffer: any) => {

      self.conversationService.sendAudioMessage(buffer);
    });

    this.audioRecorder = null;
  }

  isRecording(): boolean {
    return this.audioRecorder != null;
  }

  uploadFile(event: any) {
    const file = event.target.files.item(0);
    this.conversationService.sendFileMessage(file);
  }

  setDuration(load_event: any): void {
    this.duration = Math.round(load_event.currentTarget.duration);
  }

  // Group Form

  isValid(): boolean {
    this.errors = {};
    if (this.groupForm.value.title.length === 0) {
      this.errors['title'] = 'Title cannot be empty.'
    }
    // @ts-ignore
    if (this.groupUsers.length < 2) {
      this.errors['assigned_users'] = 'Please select two or more contacts.'
    }
    return Object.keys(this.errors).length === 0;
  }

  filtered(id: number) {
    // @ts-ignore
    return this.filters.includes(id)
  }

  search(event: any) {
    const value = event.target.value.toLowerCase();
    const len = event.target.value.length;
    if (len > 0) {

      // @ts-ignore
      let users = this.users;
      for (let k of Object.keys(users)) {
        // @ts-ignore
        const fullname = this.fullname(this.users[k], false)
        const K = parseInt(k);

        if (fullname.toLowerCase().slice(0, len) === value && this.filtered(K)) {
          // @ts-ignore
          this.filters = this.filters.filter(i => i !== K);
        }

        if (fullname.toLowerCase().slice(0, len) !== value) {
          this.filters.push(K)
        }
      }
    } else {
      this.filters = [];
    }

  }
  fullname(usr: any, type=true){
    return this.utils.fullname(usr, type)
   }

  groupHasUser(id: number) {
    // @ts-ignore
    return this.groupUsers.indexOf(id) > -1;
  }

  toggleUser(userId: number): void {
    this.modifyUserList(userId, !this.groupHasUser(userId));
  }

  modifyUserList(userId: number, addUser: boolean): void {
    if (addUser) {
      // @ts-ignore
      this.groupUsers.push(userId);
      this.isValid();
    } else {
      // @ts-ignore
      const index = this.groupUsers.indexOf(userId, 0);
      if (index > -1) {
        this.groupUsers.splice(index, 1);
        this.isValid();
      }

    }

  }

  saveGroup() {
    if (this.isValid()) {
      // create group API
      this.conversationService.createGroupConversation(this.groupForm.value.title, this.groupUsers).subscribe((conversation: Conversation) => {

        this.selectConversation(conversation.id);
        this.toggleGroup();
      });
    }
  }

  isDirect(conversation) {
    return this.conversationService.isOneToOne(conversation)
  }

  currentConversionFilteredUsers(): number[] {

    if (this.currentConversations == null) {
      return [];
    }
    return this.currentConversations!.users.filter((targetId: number) => !this.isSelf(targetId));
  }
  isSelf(userId: number) {
    return this.userService.currentUser?.id == userId;
  }

  get hasConvoConversation() {
    return !!this.conferenceConversation;
  }

  callUser(userId: number): void {
    if(this.currentConversations == null) {
      return;
    }
    //this.outGoingCallUserId = userId;
    //this.outGoingCall = true;
    this.toggleChatDrawer(this.currentConversations.id, true, false, false, this.currentConversations)
    this.conferenceService.callUser(this.currentConversations.id, false);
  } 
  acceptCall(): void {

    this.conferenceService.acceptCall();    
    /*this.conferenceService.acceptCall(this.incomingCall, (newPeer: any) => {
      console.log("Callaccept", newPeer, this.callPlayer)
      newPeer.setVideoOutput(this.callPlayer.nativeElement);
    });*/
  }
  declineCall(): void {
    
    this.conferenceService.declineCall();    
    //this.conferenceService.declineCall(this.incomingCall);
    console.log("call declined");
    
  }

  hangUpCall(userId: number): void {
    //let call = { 'user': userId }
    //this.conferenceService.declineCall(call);
    //this.outGoingCall = false;
    this.conferenceService.leaveCall();
  }

  toggleChatSideBar(conversation) {
    let id = conversation.id;
    let isDirect = this.isDirect(conversation);
    if (id > 0) {
      this.selectConversation(id);
    } else {
      this.selectConversation(-1);
    }

    let hasClass;
    if (!isDirect) {
      hasClass = this.hasClass(this.groupDrawerClass);
      if (hasClass) {
        this.removeClass(this.groupDrawerClass);
      } else {
        this.addClass(this.groupDrawerClass);
      }
    } else {
      hasClass = this.hasClass(this.chatDrawerClass);
      if (hasClass) {
        this.removeClass(this.chatDrawerClass);
      } else {
        this.addClass(this.chatDrawerClass);
      }
    }
  }

  searchContact(searchStr) {
    this.userSearchStr = searchStr;
    let userArr: object[] = [];
    let conversations = this.conversations;
    if (searchStr.length == 0) {
      this.usersSearchResult = [];
      this.searchedGroups = [];
    }

    if (searchStr.length > 0) {
      let usersObj = this.users;
      if (Object.keys(usersObj).length) {
        Object.keys(usersObj).forEach(function(key) {
          usersObj[key].fullname = usersObj[key].firstname + " " + usersObj[key].lastname;
          userArr.push(usersObj[key]);
        });
        this.usersSearchResult = userArr.filter(o =>
          Object.keys(o).some(k => o['fullname'].toLowerCase().includes(searchStr.toLowerCase())));
      }

      if (conversations.length) {
        this.searchedGroups = conversations.filter(o => {
          if (o.users.length > 2) {
            return o['topic'].toLowerCase().includes(searchStr.toLowerCase())
          }
          return;
        });
      }
    }
  }

  startNewConversation(targetUserId: number) {
    this.searchContact('');
    if (this.isNewChat(targetUserId)) {
      this.conversationService.createConversation(targetUserId).subscribe((conversation: Conversation) => {
        this.selectConversation(conversation.id);
        this.toggleChatDrawer(conversation.id, true, false, false, conversation)
      });
    }
  }

  isNewChat(targetUserId: number) {
    for (let i = 0; i < this.conversations.length; i++) {
      if (this.conversations[i].users.length == 2 && this.conversations[i].users.includes(targetUserId) && this.conversations[i].users.includes(this.userService.currentUser!.id)) {
        this.toggleChatDrawer(this.conversations[i].id, true, false, false, this.conversations[i])
        return false;
      }
    }
    return true;
  }


  /*callPickedStatus(callNotPicked) {
    //get call status from call-dial-screen component 
    this.callNotPicked = callNotPicked;
    if(callNotPicked) {      
      //this.hangUpCall(this.outGoingCallUserId)
      this.toggleChatDrawer(this.currentConversations!.id, true,false,false,this.currentConversations)
    }    
  }*/

  callWindowClosed(callClosed) {
    //set callNotPicked to false if call window closed
   // this.callNotPicked = callClosed;
   this.showCallNotPicked = false;
  }


  // removeQuickChatUser(conversationId) {   
  //   this.removedUsers.push(conversationId); 
  //   localStorage.setItem('quickchatRemovedusers', JSON.stringify(this.removedUsers));
  //   if (this.removedUsers.length == this.getLatestMessageCount()) {
  //     this.chatBar = false;
  //   }   

  removeQuickChatUser(conversationId) {
    this.conversationService.removeQuickChatUser(conversationId);
    if (this.removedUsers.length == this.getLatestMessageCount()) {
      this.chatBar = false;
    }   
  }

  isUserRemovedFromquickChat(id) {
    if (this.removedUsers.length) {
      if (this.removedUsers.includes(id)) {
        return false;

      } else {
        return true;

      }
    }
    return true;
  }
}
