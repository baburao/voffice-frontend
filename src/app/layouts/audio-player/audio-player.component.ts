import {Component, Input, OnInit,ChangeDetectorRef} from '@angular/core';;
import {StreamState} from "../../interfaces/sream-state"
import {AudioService} from "../../services/global/audio.service";
import {BehaviorSubject, Subject} from "rxjs";
import * as moment from "moment";

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.css']
})
export class AudioPlayerComponent implements OnInit {

  @Input() audioSrc = '';
  stop$ = new Subject();
  audio = new Audio();
  currentTime = 0 ;
  duration = 0;
  readableDuration = undefined;
  playing  = false;
  played = false;
  PLAY = 'play';
  PAUSE = 'pause';
  btn = this.PLAY;
  constructor(private audioService: AudioService,private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
      this.audio.src = this.audioSrc;
      this.audio.load();
      this.audio.ontimeupdate = (event) => {
        this.currentTime =  Math.round(this.audio.currentTime);
        this.duration = Math.round(this.audio.duration);
        if (this.played)
          if (this.audio.duration === this.audio.currentTime){
            this.played = false;
            this.playing = false;
            this.btn = this.PLAY;
          }else{
            this.playing = true
          }
        // @ts-ignore
        this.readableDuration = this.formatTime(this.audio.currentTime);
        this.cd.detectChanges();
     };
  }


  play() {
    this.audio.play().then()
  }

  pause(){
    this.playing = false;
    this.audio.pause()
  }

  stop() {
    this.stop$.next();
  }

  formatTime(time: number, format: string = "mm:ss") {
    const momentTime = time * 1000;
    return moment.utc(momentTime).format(format);
  }

  togglePlay(){
    if(this.playing && this.played){
      this.pause();
      this.btn = this.PLAY;
      this.played = false;
    }
    else if(!this.playing && !this.played){
      this.play();
      this.btn = this.PAUSE;
      this.played = true;
    }
  }


}
